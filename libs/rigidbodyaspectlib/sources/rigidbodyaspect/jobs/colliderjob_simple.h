#ifndef RIGIDBODYASPECT_COLLIDERJOB_SIMPLE_H
#define RIGIDBODYASPECT_COLLIDERJOB_SIMPLE_H

#include "../types.h"

// qt
#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>


namespace rigidbodyaspect
{

  class RigidBodyAspect;

  class ColliderJobSimple : public Qt3DCore::QAspectJob {
  public:
    ColliderJobSimple(RigidBodyAspect* aspect);

    void setFrameTimeDt(seconds_type dt);

  private:
    RigidBodyAspect* m_aspect;
    seconds_type     m_dt;
    seconds_type     m_elapsed_time{0};


    void printStatistics() const;
    void printSpheres() const;


    // QAspectJob interface
  public:
    void run() override;
  };

  using ColliderSimpleJobPtr = QSharedPointer<ColliderJobSimple>;

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_COLLIDERJOB_SIMPLE_H
