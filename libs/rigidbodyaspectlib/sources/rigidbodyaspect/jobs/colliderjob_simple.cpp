#include "colliderjob_simple.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/movingbody.h"
#include "../algorithms/movingsphere.h"
#include "../algorithms/sphere_fixedplane.h"
#include "../algorithms/sphere_sphere.h"
#include "../algorithms/ContactPoint.h"

#include "../algorithms/helpers.h"
#include "../algorithms/SSTree.h"

// qt
#include <QDebug>

// stl
#include <tuple>
#include <chrono>
using namespace std::chrono_literals;

namespace rigidbodyaspect
{

  using namespace algorithms;


  ColliderJobSimple::ColliderJobSimple(RigidBodyAspect* aspect)
    : m_aspect{aspect}
  {
  }

  void ColliderJobSimple::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void ColliderJobSimple::run()
  {
    const bool adjust = true;
    const bool revert = true;

    // sort criteria
    using UniqueContainer        = QSet<Qt3DCore::QNodeId>;
    auto collision_sort_criteria = [](const auto& a, const auto& b) {
      return std::get<0>(a) > std::get<0>(b);
    };
    //  Containers

    singularities_simple::collisionType collisions;
    ContactNTtype                       contact_normals;
    auto&                               rigid_bodies = m_aspect->rigidBodies();
    auto&                               spheres      = rigid_bodies.spheres();
    const auto& fixed_planes = rigid_bodies.fixedPlanes();
    const auto& surfs        = rigid_bodies.surfs();

    UniqueContainer  unique_container;
    RigidBodyAspect& asp = *m_aspect;

    auto unique_rem_criteria = [&unique_container, &asp](const auto& a) {
      // check that active sphere was not in collision before
      const auto a_sid = std::get<1>(a);
      if (unique_container.contains(a_sid)) return true;
      unique_container.insert(a_sid);
      // check that passive is sphere and it was not in collision before
      const auto a_sid_second = std::get<2>(a);
      if (asp.rigidBodies().spheres().contains(a_sid_second)) {
        if (unique_container.contains(a_sid_second)) return true;
        unique_container.insert(a_sid_second);
      }
      return false;
    };
    // Helper: sort and make unique driver
    auto sort_and_make_unique_driver
      = [&collisions, collision_sort_criteria, &unique_container,
         unique_rem_criteria]() {
          std::sort(std::begin(collisions), std::end(collisions),
                    collision_sort_criteria);
          unique_container.clear();
          collisions.erase(std::rend(collisions).base(),
                           std::remove_if(std::rbegin(collisions),
                                          std::rend(collisions),
                                          unique_rem_criteria)
                             .base());
        };

    //=======================START

    //  Compute initial cache variables
    for (auto i = spheres.begin(); i != spheres.end(); ++i) {
      auto& sphere   = i.value();
      sphere.m_dt_tp = 0ms;
      // manual check slide to roll. To be changed.
      if (sphere.m_state == MovingBodyState::Sliding) {
        GM2Vector n = algorithms::helpers::nToPlane(
          fixed_planes[*sphere.m_attached_objects.begin()]);
        if (blaze::length(
              sphere.m_velocity
              + blaze::cross(sphere.m_angular_velocity, -sphere.m_radius * n))
            < constants::Zero) {
          sphere.m_state = MovingBodyState::Rolling;
          sphere.m_aa *= 0.0;
        }
      }

      helpers::computeCacheProps(i.key(), fixed_planes, spheres, m_dt,
                                 *m_aspect, adjust, not revert);
    }
    // this is brute-force collision detection with spheres precheck
    algorithms::singularities_simple::detectCollisionsBruteSphereS(
      collisions, rigid_bodies, contact_normals, m_dt);
    // this is brute-force collision detection with exact bodies
    //   algorithms::singularities_simple::detectCollisionsBruteForce(collisions,rigid_bodies,contact_normals,m_dt);

    // sort and make unique
    sort_and_make_unique_driver();

    //  While collisions
    while (collisions.size()) {
      //  Take last collision, which is the closest to the current point in the
      //  time
      const auto col = collisions.back();
      collisions.pop_back();
      const auto& col_dt_tp = std::get<0>(col);
      auto&       sphere_id = std::get<1>(col);
      auto&       object_id = std::get<2>(col);
      // spheres of the collison
      QSet<Qt3DCore::QNodeId> col_spheres_ids;
      col_spheres_ids.insert(sphere_id);
      if (spheres.contains(object_id)) {
        col_spheres_ids.insert(object_id);
      }
      //  Simulate spheres of the collision to the time of the collision
      for (auto& col_sp_id : col_spheres_ids) {
        auto&      col_sp_temp = spheres[col_sp_id];
        const auto sub_dt_temp = col_dt_tp - col_sp_temp.m_dt_tp;
        algorithms::simulation::simulateMovingBody(col_sp_temp, sub_dt_temp,
                                                   m_dt, fixed_planes);
      }
      // here is the collision resolution
      algorithms::singularities_simple::resolveImpactResponse(
        rigid_bodies, sphere_id, object_id, contact_normals);
      for (auto& col_sp_id : col_spheres_ids) {
        helpers::computeCacheProps(col_sp_id, fixed_planes, spheres, m_dt,
                                   *m_aspect, not adjust, revert);
        algorithms::statechange::stateChangeForces(
          col_sp_id, object_id, spheres, fixed_planes, surfs, contact_normals);
      }
      for (auto& col_sp_id : col_spheres_ids) {
        helpers::computeCacheProps(col_sp_id, fixed_planes, spheres, m_dt,
                                   *m_aspect, adjust, not revert);
      }

      // this is inner brute-force collision detection with spheres precheck
      algorithms::singularities_simple::detectCollisionsBruteSphereSInner(
        collisions, col_spheres_ids, rigid_bodies, contact_normals, m_dt);
      // this is inner brute-force collision detection with exact bodies
      //      algorithms::singularities_simple::detectCollisionsBruteForceInner(collisions,col_spheres_ids,rigid_bodies,contact_normals,m_dt);

      // Sort and make unique
      sort_and_make_unique_driver();
    }


    // Sim the rest of the objects and check their state
    for (auto& sphere : rigid_bodies.spheres()) {
      const auto sub_dt      = m_dt - sphere.m_dt_tp;
      const auto sub_dt_frac = sub_dt / m_dt;
      if (sub_dt_frac > 0 and sub_dt_frac <= 1) {
        if (sub_dt
            == m_dt)   // when simulate for the whole timestep check state
        {
          algorithms::simulation::simulateMovingBody(sphere, sub_dt, m_dt,
                                                     fixed_planes);
        }
        else   // otherwise do not check the state
          algorithms::simulation::simulateMovingBody(sphere, sub_dt, m_dt,
                                                     fixed_planes);
      }
    }

    m_elapsed_time += m_dt;
  }




  void ColliderJobSimple::printSpheres() const
  {
    auto& spheres = m_aspect->rigidBodies().spheres();
    for (auto sp = spheres.begin(); sp != spheres.end(); sp++) {
      auto        id  = sp.key();
      const auto& ssp = sp.value();
      qDebug() << "id: " << id << "dt: " << ssp.m_dt_tp.count()
               << "remdt: " << ssp.m_rem_dt.count()
               << "velocity: " << ssp.m_velocity[0] << ssp.m_velocity[1]
               << ssp.m_velocity[2] << "ds: " << ssp.m_ds[0] << ssp.m_ds[1]
               << ssp.m_ds[2];
    }
  };


  void ColliderJobSimple::printStatistics() const
  {
    auto& rigid_bodies = m_aspect->rigidBodies();
    auto& spheres      = rigid_bodies.spheres();

    std::cout << "Elapsed time: " << m_elapsed_time.count() << '\n';
    seconds_type et_min, et_max;
    for (auto sit = spheres.begin(); sit != spheres.end(); ++sit) {

      const auto sid    = sit.key();
      const auto sphere = sit.value();

      if (sit == spheres.begin())
        et_min = et_max = sphere.m_elapsed_time;
      else {
        et_min = std::min(et_min, sphere.m_elapsed_time);
        et_max = std::max(et_max, sphere.m_elapsed_time);
      }
      std::cout << "  sphere(" << sid.id()
                << "): " << sphere.m_elapsed_time.count() << '\n';
    }
    std::cout << "  --\n";
    std::cout << "  min diff: " << (m_elapsed_time - et_min).count() << '\n';
    std::cout << "  max diff: " << (m_elapsed_time - et_max).count() << '\n';
    std::cout << std::endl;
  }


}   // namespace rigidbodyaspect
