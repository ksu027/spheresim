#include "genericcolliderjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/collision.h"

// qt
#include <QDebug>

namespace rigidbodyaspect
{

  GenericColliderJob::GenericColliderJob(RigidBodyAspect* aspect)
    : m_aspect{aspect}
  {
  }

  void GenericColliderJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void GenericColliderJob::run()
  {
    auto& rigid_bodies = m_aspect->rigidBodies();

    for (auto& sphere : rigid_bodies.spheres()) {
      for (auto& fixed_plane : rigid_bodies.fixedPlanes()) {
        algorithms::collision::detectCollisionExample(sphere, fixed_plane,
                                                      m_dt);
      }
    }
  }

}   // namespace rigidbodyaspect
