#ifndef RIGIDBODYASPECT_COLLIDERJOB_H
#define RIGIDBODYASPECT_COLLIDERJOB_H

#include "../types.h"

// qt
#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>


namespace rigidbodyaspect
{

  class RigidBodyAspect;

  class ColliderJob : public Qt3DCore::QAspectJob {
  public:
    ColliderJob(RigidBodyAspect* aspect);

    void setFrameTimeDt(seconds_type dt);

  private:
    RigidBodyAspect* m_aspect;
    seconds_type     m_dt;
    seconds_type     m_elapsed_time{0};


    void printStatistics() const;
    void printSpheres() const;


    // QAspectJob interface
  public:
    void run() override;
  };

  using ColliderJobPtr = QSharedPointer<ColliderJob>;

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_COLLIDERJOB_H
