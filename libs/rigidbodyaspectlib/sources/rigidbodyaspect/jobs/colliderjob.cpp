#include "colliderjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/movingbody.h"
#include "../algorithms/movingsphere.h"
#include "../algorithms/sphere_fixedplane.h"
#include "../algorithms/sphere_sphere.h"
#include "../algorithms/ContactPoint.h"

#include "../algorithms/helpers.h"
#include "../algorithms/SSTree.h"

// qt
#include <QDebug>

// stl
#include <tuple>
#include <chrono>
using namespace std::chrono_literals;

namespace rigidbodyaspect
{

  using namespace algorithms;


  ColliderJob::ColliderJob(RigidBodyAspect* aspect) : m_aspect{aspect} {}

  void ColliderJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void ColliderJob::run()
  {
    const bool adjust = true;
    const bool revert = true;

    //  Containers
    std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>
      collision_sets;
    std::vector<std::tuple<seconds_type, Qt3DCore::QNodeId, Qt3DCore::QNodeId>>
                  collisions;
    ContactNTtype contact_normals;
    auto&         rigid_bodies = m_aspect->rigidBodies();
    auto&         spheres      = rigid_bodies.spheres();
    const auto&   fixed_planes = rigid_bodies.fixedPlanes();
    const auto&   surfs        = rigid_bodies.surfs();

    //=======================START

    //  Compute initial cache variables
    for (auto i = spheres.begin(); i != spheres.end(); ++i) {
      auto& sphere   = i.value();
      sphere.m_dt_tp = 0ms;
      // manual check slide to roll. To be changed.
      if (sphere.m_state == MovingBodyState::Sliding) {
        GM2Vector n = algorithms::helpers::nToPlane(
          fixed_planes[*sphere.m_attached_objects.begin()]);
        if (blaze::length(
              sphere.m_velocity
              + blaze::cross(sphere.m_angular_velocity, -sphere.m_radius * n))
            < constants::Zero) {
          sphere.m_state = MovingBodyState::Rolling;
          sphere.m_aa *= 0.0;
        }
      }

      helpers::computeCacheProps(i.key(), fixed_planes, spheres, m_dt,
                                 *m_aspect, adjust, not revert);
    }

    // this is collision detection with the prediction based on trees
    contact::SpheresTree* tree;
    tree = new contact::SpheresTree(rigid_bodies);
    contact::detectCollisionsTree(*tree, collision_sets, rigid_bodies,
                                  contact_normals, m_dt);

    // this is brute-force collision detection with spheres precheck
    //      contact::detectCollisionsBruteSphere(collision_sets,rigid_bodies,contact_normals,m_dt);
    // this is brute-force collision detection with exact bodies
    //      algorithms::singularities::detectCollisionsBruteForce(collision_sets,rigid_bodies,contact_normals,m_dt);


    // sort and make unique
    algorithms::singularities::cleanUpCollisions(collision_sets, spheres);

    //  While collisions
    while (collision_sets.size()) {
      //  Take last collision, which is the closest to the current point in the
      //  time
      const auto col = collision_sets.back();
      collision_sets.pop_back();
      const auto& col_dt_tp   = std::get<0>(col);
      auto&       col_set_obj = std::get<1>(col);
      // spheres of the collison
      auto col_spheres_ids = helpers::getSpheres(col_set_obj, *m_aspect);
      auto col_planes_ids  = helpers::getPlanes(col_set_obj, *m_aspect);
      auto col_surfs_ids   = helpers::getSurfs(col_set_obj, *m_aspect);

      //  Simulate spheres of the collision to the time of the collision
      for (auto& col_sp_id : col_spheres_ids) {
        auto&      col_sp_temp = spheres[col_sp_id];
        const auto sub_dt_temp = col_dt_tp - col_sp_temp.m_dt_tp;
        algorithms::simulation::simulateMovingBody(col_sp_temp, sub_dt_temp,
                                                   m_dt, fixed_planes);
      }
      // create vector of contact points
      auto collision_points = contact::getVectorOfCPs(
        spheres, fixed_planes, surfs, col_spheres_ids, col_planes_ids,
        col_surfs_ids, contact_normals);
      auto cps_of_node_id = contact::getCPsOfNodeID(collision_points);

      // here is the collision resolution
      algorithms::simulation::resolveImpactResponse(spheres, collision_points,
                                                    cps_of_node_id);

      for (auto& cp : collision_points) {
        helpers::computeCacheProps(cp.contact_objects.first, fixed_planes,
                                   spheres, m_dt, *m_aspect, not adjust,
                                   revert);
        algorithms::statechange::stateChangeForces(
          cp.contact_objects.first, cp.contact_objects.second, spheres,
          fixed_planes, surfs, contact_normals);
      }
      for (auto i = cps_of_node_id.constBegin(); i != cps_of_node_id.constEnd();
           ++i) {
        helpers::computeCacheProps(i.key(), fixed_planes, spheres, m_dt,
                                   *m_aspect, adjust, not revert);
      }

      // this is inner collision detection with the prediction based on trees
      contact::detectCollisionsTreeInner(collision_sets, col_spheres_ids, *tree,
                                         rigid_bodies, contact_normals, m_dt);
      // this is inner brute-force collision detection with spheres precheck
      //        contact::detectCollisionsBruteSphereInner(collision_sets,col_spheres_ids,rigid_bodies,contact_normals,m_dt);
      // this is inner brute-force collision detection with exact bodies
      //        algorithms::singularities::detectCollisionsBruteForceInner(collision_sets,col_spheres_ids,col_planes_ids,rigid_bodies,contact_normals,m_dt);



      // Sort and make unique
      algorithms::singularities::cleanUpCollisions(collision_sets, spheres);
    }


    // Sim the rest of the objects and check their state
    for (auto& sphere : rigid_bodies.spheres()) {
      const auto sub_dt      = m_dt - sphere.m_dt_tp;
      const auto sub_dt_frac = sub_dt / m_dt;
      if (sub_dt_frac > 0 and sub_dt_frac <= 1) {
        if (sub_dt
            == m_dt)   // when simulate for the whole timestep check state
        {
          algorithms::simulation::simulateMovingBody(sphere, sub_dt, m_dt,
                                                     fixed_planes);
        }
        else   // otherwise do not check the state
          algorithms::simulation::simulateMovingBody(sphere, sub_dt, m_dt,
                                                     fixed_planes);
      }
    }

    m_elapsed_time += m_dt;
  }




  void ColliderJob::printSpheres() const
  {
    auto& spheres = m_aspect->rigidBodies().spheres();
    for (auto sp = spheres.begin(); sp != spheres.end(); sp++) {
      auto        id  = sp.key();
      const auto& ssp = sp.value();
      qDebug() << "id: " << id << "dt: " << ssp.m_dt_tp.count()
               << "remdt: " << ssp.m_rem_dt.count()
               << "velocity: " << ssp.m_velocity[0] << ssp.m_velocity[1]
               << ssp.m_velocity[2] << "ds: " << ssp.m_ds[0] << ssp.m_ds[1]
               << ssp.m_ds[2];
    }
  };


  void ColliderJob::printStatistics() const
  {
    auto& rigid_bodies = m_aspect->rigidBodies();
    auto& spheres      = rigid_bodies.spheres();

    std::cout << "Elapsed time: " << m_elapsed_time.count() << '\n';
    seconds_type et_min, et_max;
    for (auto sit = spheres.begin(); sit != spheres.end(); ++sit) {

      const auto sid    = sit.key();
      const auto sphere = sit.value();

      if (sit == spheres.begin())
        et_min = et_max = sphere.m_elapsed_time;
      else {
        et_min = std::min(et_min, sphere.m_elapsed_time);
        et_max = std::max(et_max, sphere.m_elapsed_time);
      }
      std::cout << "  sphere(" << sid.id()
                << "): " << sphere.m_elapsed_time.count() << '\n';
    }
    std::cout << "  --\n";
    std::cout << "  min diff: " << (m_elapsed_time - et_min).count() << '\n';
    std::cout << "  max diff: " << (m_elapsed_time - et_max).count() << '\n';
    std::cout << std::endl;
  }


}   // namespace rigidbodyaspect
