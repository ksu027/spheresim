#ifndef RIGIDBODYASPECT_FPSJOB_H
#define RIGIDBODYASPECT_FPSJOB_H

#include "../types.h"

// qt
#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>


namespace rigidbodyaspect
{

  class RigidBodyAspect;

  class FpsJob : public Qt3DCore::QAspectJob {
  public:
    FpsJob(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;
    qint64           m_dt;
    qint64           all_time = 0;
    bool             m_stop   = true;

    // QAspectJob interface
  public:
    void setDT(qint64 dt);
    void run() override;
  };

  using FpsJobPtr = QSharedPointer<FpsJob>;

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_FPSJOB_H
