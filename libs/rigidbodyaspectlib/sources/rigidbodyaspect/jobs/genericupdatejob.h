#ifndef RIGIDBODYASPECT_GENERICUPDATEJOB_H
#define RIGIDBODYASPECT_GENERICUPDATEJOB_H

#include "../types.h"

// qt
#include <Qt3DCore/QAspectJob>
#include <QSharedPointer>


namespace rigidbodyaspect
{

  class RigidBodyAspect;

  class GenericUpdateJob : public Qt3DCore::QAspectJob {
  public:
    GenericUpdateJob(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;

    // QAspectJob interface
  public:
    void run() override;
  };

  using GenericUpdateJobPtr = QSharedPointer<GenericUpdateJob>;

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_GENERICUPDATEJOB_H
