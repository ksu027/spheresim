// for fps count addopted from https://www.kdab.com/writing-custom-qt-3d-aspect/
#include "fpsjob.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/fpsmonitorbackend.h"

// qt
#include <QDebug>

namespace rigidbodyaspect
{

  FpsJob::FpsJob(RigidBodyAspect* aspect) : m_aspect{aspect} {}

  void FpsJob::setDT(qint64 dt)
  {
    m_dt = dt;
    all_time += dt;
  }

  void FpsJob::run()
  {
    const seconds_type t_in_sec    = seconds_type(all_time / 1000000000.0);
    const float        fps         = 1.0f / (float(m_dt) * 1.0e-9f);
    const auto&        fpsMonitors = m_aspect->fpsMonitorBackends();
    for (const auto fpsMonitor : fpsMonitors) {
      // Don't process disabled monitors
      if (!fpsMonitor->isEnabled()) continue;

      fpsMonitor->addFpsSample(fps);
    }
  }

}   // namespace rigidbodyaspect
