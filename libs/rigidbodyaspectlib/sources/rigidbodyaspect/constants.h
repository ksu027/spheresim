#ifndef RIGIDBODYASPECT_CONSTANTS_H
#define RIGIDBODYASPECT_CONSTANTS_H

#include "types.h"

namespace rigidbodyaspect::constants
{

  // Gravity [cm/s^2]
  constexpr auto G    = GM2UnitType{980.665};
  const auto     Gn   = GM2Vector{0.0, -G / 100.0, 0.0};
  const auto     Zero = 1e-6;
  const auto     Pi   = 3.14159265358979323846;

  namespace qt
  {
    const auto Gn = QVector3D{0.0f, -float(G / 100.0), 0.0f};
  }

}   // namespace rigidbodyaspect::constants



#endif   // RIGIDBODYASPECT_CONSTANTS_H
