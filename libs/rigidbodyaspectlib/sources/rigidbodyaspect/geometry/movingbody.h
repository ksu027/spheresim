#ifndef RIGIDBODYASPECT_MOVINGBODY_H
#define RIGIDBODYASPECT_MOVINGBODY_H

#include "../types.h"
#include "../SurroundSphere.h"

namespace rigidbodyaspect
{

  enum class MovingBodyState { Free, Rolling, Sliding, AtRest };

  class MovingBody : public GM2SpaceObjectType {

  public:
    // members
    Unit              m_friction;
    Unit              m_mass;
    GM2Vector         m_velocity;
    Qt3DCore::QNodeId m_env_id;

    MovingBodyState         m_state{MovingBodyState::Free};
    QSet<Qt3DCore::QNodeId> m_attached_objects;

    double m_inertia;

    double m_mu          = 0.01;
    double m_restitution = 0.9;

    // cache
    mutable GM2Vector    m_angular_velocity{0.0, 0.0, 0.0};
    mutable GM2Vector    m_ds;
    mutable GM2Vector    m_a;
    mutable GM2Vector    m_aa{0.0, 0.0, 0.0};   // angular acceleration
    mutable GM2Vector    m_pos;
    mutable seconds_type m_dt_tp;
    mutable seconds_type m_rem_dt;

    mutable seconds_type m_elapsed_time{0};

    integration::SurroundSphere& getSurroundSphere()
    {
      return m_surround_sphere;
    }
    void setSurroundSphere(const integration::SurroundSphere& sp)
    {
      m_surround_sphere = sp;
    }
    std::vector<integration::SurroundSphere>& getSurroundSphereS()
    {
      return m_surround_spheres;
    }
    void setSurroundSphereS(const std::vector<integration::SurroundSphere>& sps)
    {
      m_surround_spheres = sps;
    }

  protected:
    integration::SurroundSphere              m_surround_sphere;
    std::vector<integration::SurroundSphere> m_surround_spheres;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_MOVINGBODY_H
