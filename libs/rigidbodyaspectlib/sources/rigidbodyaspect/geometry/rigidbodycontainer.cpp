#include "rigidbodycontainer.h"

#include "../utils.h"

namespace rigidbodyaspect
{

  RigidBodyContainer::RigidBodyContainer() {}

  void RigidBodyContainer::constructSphere(Qt3DCore::QNodeId id)
  {
    m_spheres.insert(id, {});
  }

  void RigidBodyContainer::destroySphere(Qt3DCore::QNodeId id)
  {
    m_spheres.remove(id);
  }

  rbtypes::Sphere& RigidBodyContainer::sphere(Qt3DCore::QNodeId id)
  {
    return m_spheres[id];
  }

  const rbtypes::Sphere RigidBodyContainer::sphere(Qt3DCore::QNodeId id) const
  {
    return m_spheres[id];
  }

  RigidBodyContainer::SphereQHash& RigidBodyContainer::spheres()
  {
    return m_spheres;
  }

  const RigidBodyContainer::SphereQHash& RigidBodyContainer::spheres() const
  {
    return m_spheres;
  }

  void RigidBodyContainer::constructFixedPlane(Qt3DCore::QNodeId id)
  {
    m_fixed_planes.insert(id, {});
  }

  void RigidBodyContainer::destroyFixedPlane(Qt3DCore::QNodeId id)
  {
    m_fixed_planes.remove(id);
  }

  rbtypes::FixedPlane& RigidBodyContainer::fixedPlane(Qt3DCore::QNodeId id)
  {
    return m_fixed_planes[id];
  }

  RigidBodyContainer::FixedPlaneQHash& RigidBodyContainer::fixedPlanes()
  {
    return m_fixed_planes;
  }

  const RigidBodyContainer::FixedPlaneQHash&
  RigidBodyContainer::fixedPlanes() const
  {
    return m_fixed_planes;
  }

  void RigidBodyContainer::constructSurf(Qt3DCore::QNodeId id)
  {
    m_surfs.insert(id, {});
  }

  void RigidBodyContainer::destroySurf(Qt3DCore::QNodeId id)
  {
    m_surfs.remove(id);
  }

  rbtypes::Surf& RigidBodyContainer::surf(Qt3DCore::QNodeId id)
  {
    return m_surfs[id];
  }

  const rbtypes::Surf RigidBodyContainer::surf(Qt3DCore::QNodeId id) const
  {
    return m_surfs[id];
  }

  RigidBodyContainer::SurfQHash& RigidBodyContainer::surfs() { return m_surfs; }

  const RigidBodyContainer::SurfQHash& RigidBodyContainer::surfs() const
  {
    return m_surfs;
  }

}   // namespace rigidbodyaspect
