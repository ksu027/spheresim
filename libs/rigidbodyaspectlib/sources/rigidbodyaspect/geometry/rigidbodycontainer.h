#ifndef RIGIDBODYASPECT_RIGIDBODYCONTAINER_H
#define RIGIDBODYASPECT_RIGIDBODYCONTAINER_H

#include "rbtypes.h"

namespace rigidbodyaspect
{

  class RigidBodyContainer {
  public:
    using SphereQHash     = QHash<Qt3DCore::QNodeId, rbtypes::Sphere>;
    using FixedPlaneQHash = QHash<Qt3DCore::QNodeId, rbtypes::FixedPlane>;
    using SurfQHash       = QHash<Qt3DCore::QNodeId, rbtypes::Surf>;


    RigidBodyContainer();

    // Sphere
    void                  constructSphere(Qt3DCore::QNodeId id);
    void                  destroySphere(Qt3DCore::QNodeId id);
    rbtypes::Sphere&      sphere(Qt3DCore::QNodeId id);
    const rbtypes::Sphere sphere(Qt3DCore::QNodeId id) const;
    SphereQHash&          spheres();
    const SphereQHash&    spheres() const;

    // Plane
    void                   constructFixedPlane(Qt3DCore::QNodeId id);
    void                   destroyFixedPlane(Qt3DCore::QNodeId id);
    rbtypes::FixedPlane&   fixedPlane(Qt3DCore::QNodeId id);
    FixedPlaneQHash&       fixedPlanes();
    const FixedPlaneQHash& fixedPlanes() const;

    // Surf
    void                constructSurf(Qt3DCore::QNodeId id);
    void                destroySurf(Qt3DCore::QNodeId id);
    rbtypes::Surf&      surf(Qt3DCore::QNodeId id);
    const rbtypes::Surf surf(Qt3DCore::QNodeId id) const;
    SurfQHash&          surfs();
    const SurfQHash&    surfs() const;

  private:
    SphereQHash     m_spheres;
    FixedPlaneQHash m_fixed_planes;
    SurfQHash       m_surfs;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_RIGIDBODYCONTAINER_H
