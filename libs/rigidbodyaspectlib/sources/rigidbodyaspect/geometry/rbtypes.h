#ifndef RIGIDBODYASPECT_RBTYPES_H
#define RIGIDBODYASPECT_RBTYPES_H

#include "movingbody.h"
#include "fixedbody.h"

namespace rigidbodyaspect::rbtypes
{

  using Sphere     = gmlib2::parametric::Sphere<MovingBody>;
  using FixedPlane = gmlib2::parametric::Plane<FixedBody>;
  using Surf       = gmlib2::parametric::BezierSurface<FixedBody>;


}   // namespace rigidbodyaspect::rbtypes



#endif   // RIGIDBODYASPECT_RBTYPES_H
