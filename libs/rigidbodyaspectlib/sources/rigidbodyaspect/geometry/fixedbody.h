#ifndef RIGIDBODYASPECT_FIXEDBODY_H
#define RIGIDBODYASPECT_FIXEDBODY_H

#include "../types.h"
#include "../SurroundSphere.h"

namespace rigidbodyaspect
{

  class FixedBody : public GM2SpaceObjectType {
  public:
    // members
    Unit                        m_friction;
    Qt3DCore::QNodeId           m_env_id;
    integration::SurroundSphere getSurroundSphere() const
    {
      return m_surround_sphere;
    }
    void setSurroundSphere(const integration::SurroundSphere& sp)
    {
      m_surround_sphere = sp;
    }
    std::vector<integration::SurroundSphere> getSurroundSphereS() const
    {
      return m_surround_spheres;
    }
    void setSurroundSphereS(const std::vector<integration::SurroundSphere>& sps)
    {
      m_surround_spheres = sps;
    }

  protected:
    integration::SurroundSphere              m_surround_sphere;
    std::vector<integration::SurroundSphere> m_surround_spheres;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_FIXEDBODY_H
