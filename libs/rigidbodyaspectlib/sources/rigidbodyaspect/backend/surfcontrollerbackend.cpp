#include "surfcontrollerbackend.h"

#include "rigidbodyaspect.h"
#include "../frontend/surfcontroller.h"
#include "../utils.h"

namespace rigidbodyaspect
{

  SurfControllerBackend::SurfControllerBackend(RigidBodyContainer& rigid_bodies)
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite), m_rigid_bodies{
                                                                   rigid_bodies}
  {
  }

  SurfControllerBackend::~SurfControllerBackend()
  {
    m_rigid_bodies.destroySurf(peerId());
  }

  void SurfControllerBackend::queueFrontendUpdate()
  {

    const auto& pframe = m_rigid_bodies.surf(peerId()).pSpaceFrameParent();

    // Convert data to sendable frame
    QMatrix3x3 q_pframe;
    for (auto i = 0UL; i < 3UL; ++i) {
      q_pframe(int(i), 0)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 0UL));   // dir
      q_pframe(int(i), 1)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 2UL));   // up
      q_pframe(int(i), 2)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 3UL));   // pos
    }

    // Send data
    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("dupframe_BE"));
    e->setValue(QVariant::fromValue(q_pframe));
    notifyObservers(e);
  }

  void SurfControllerBackend::setDupFrame(const QMatrix3x3 dup_frame)
  {
    if (qFuzzyCompare(dup_frame, m_dup_frame)) return;

    m_dup_frame = dup_frame;

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    m_rigid_bodies.surf(peerId()).setFrameParent(dir, up, pos);
  }

  void SurfControllerBackend::setInitialFriction(const float& friction)
  {
    if (qFuzzyCompare(m_initial_friction, friction)) return;

    m_initial_friction = friction;

    m_rigid_bodies.surf(peerId()).m_friction = double(friction);
  }

  void
  SurfControllerBackend::setConstraints(const RigidBodyConstraints& constraints)
  {
    if (constraints == m_constraints) return;
  }

  void SurfControllerBackend::setEnvironmentId(const Qt3DCore::QNodeId& id)
  {
    if (m_environment_id == id) return;

    m_environment_id = id;

    m_rigid_bodies.surf(peerId()).m_env_id = m_environment_id;
  }

  void SurfControllerBackend::setScale(const QVector2D& scale)
  {

    auto& surf = m_rigid_bodies.surf(peerId());
  }


  void SurfControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange
      = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<SurfInitialData>>(
        change);
    const auto& data   = typedChange->data;
    m_constraints      = data.m_constraints;
    m_dup_frame        = data.m_dup_frame;
    m_initial_friction = data.m_initial_friction;
    m_environment_id   = data.m_environment_id;

    m_rigid_bodies.constructSurf(peerId());
    auto& surf = m_rigid_bodies.surf(peerId());

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    surf.setFrameParent(dir, up, pos);


    setScale(data.m_scale);
    surf.m_env_id   = m_environment_id;
    surf.m_friction = double(m_initial_friction);

    // surround sphere
    generateSurroundSphere(surf);
    generateSurroundSpheres(surf);
  }

  void SurfControllerBackend::generateSurroundSphere(rbtypes::Surf& surf)
  {
    integration::SurroundSphere ss{GM2Vector{
      surf.m_C.at(0, 0)[0], surf.m_C.at(0, 0)[1], surf.m_C.at(0, 0)[2]}};
    auto                        c_net = surf.m_C;
    for (size_t r = 0; r < surf.m_C.rows(); r++)
      for (size_t c = 0; c < surf.m_C.columns(); c++) ss += surf.m_C(r, c);
    surf.setSurroundSphere(ss);
    qDebug() << "MAIN center" << ss.getCenter()[0] << ss.getCenter()[1]
             << ss.getCenter()[2] << "radius" << ss.getRadius();
  }

  void SurfControllerBackend::generateSurroundSpheres(rbtypes::Surf& surf)
  {
    std::vector<integration::SurroundSphere> ssv;
    auto                                     c_net = surf.m_C;
    for (size_t r = 0; r < surf.m_C.rows() - 1; r++)
      for (size_t c = 0; c < surf.m_C.columns() - 1; c++) {
        integration::SurroundSphere ss{GM2Vector{
          surf.m_C.at(r, c)[0], surf.m_C.at(r, c)[1], surf.m_C.at(r, c)[2]}};
        ss += surf.m_C(r + 1, c + 1);
        ss += surf.m_C(r, c + 1);
        ss += surf.m_C(r + 1, c);
        ssv.push_back(ss);
      }
    surf.setSurroundSphereS(ssv);
  }

  void
  SurfControllerBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {

    if (e->type() == Qt3DCore::PropertyUpdated) {


      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      if (change->propertyName() == QByteArrayLiteral("constraints"))
        setConstraints(change->value().value<RigidBodyConstraints>());
      else if (change->propertyName() == QByteArrayLiteral("initialFriction"))
        setInitialFriction(change->value().toFloat());
      else if (change->propertyName() == QByteArrayLiteral("environment"))
        setEnvironmentId(change->value().value<Qt3DCore::QNodeId>());
      else if (change->propertyName() == QByteArrayLiteral("scale"))
        setScale(change->value().value<QVector2D>());
    }

    QBackendNode::sceneChangeEvent(e);
  }

  SurfControllerBackendMapper::SurfControllerBackendMapper(
    RigidBodyAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* SurfControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new SurfControllerBackend(m_aspect->rigidBodies());
    m_aspect->addSurfControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  SurfControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->surfControllerBackend(id);
  }

  void SurfControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takeSurfControllerBackend(id);
  }

}   // namespace rigidbodyaspect
