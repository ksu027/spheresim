#ifndef RIGIDBODYASPECT_RIGIDBODYASPECT_H
#define RIGIDBODYASPECT_RIGIDBODYASPECT_H

#include "../jobs/colliderjob.h"
#include "../jobs/colliderjob_simple.h"
#include "../jobs/genericupdatejob.h"
#include "../jobs/fpsjob.h"

#include "../geometry/rigidbodycontainer.h"

// qt
#include <QPair>
#include <Qt3DCore>

namespace rigidbodyaspect
{
  class EnvironmentBackend;
  class SimulatorSettingsBackend;
  class SphereControllerBackend;
  class PlaneControllerBackend;
  class SurfControllerBackend;

  class FpsMonitorBackend;

  class RigidBodyAspect : public Qt3DCore::QAbstractAspect {
    Q_OBJECT
  public:
    // Types
    using SimulatorSettingsBackendSP = QSharedPointer<SimulatorSettingsBackend>;
    using EnvironmentBackendQHash
      = QHash<Qt3DCore::QNodeId, EnvironmentBackend*>;
    using SphereControllerBackendQHash
      = QHash<Qt3DCore::QNodeId, SphereControllerBackend*>;
    using PlaneControllerBackendQHash
      = QHash<Qt3DCore::QNodeId, PlaneControllerBackend*>;
    using SurfControllerBackendQHash
      = QHash<Qt3DCore::QNodeId, SurfControllerBackend*>;
    using FpsMonitorBackendQHash = QHash<Qt3DCore::QNodeId, FpsMonitorBackend*>;

    // Constructor
    explicit RigidBodyAspect(QObject* parent = nullptr);


    ////////////////////////////
    // SimulatorSettings Backend
    SimulatorSettingsBackend*
                              constructSimulatorSettingsBackend(Qt3DCore::QNodeId id);
    SimulatorSettingsBackend* simulatorSettingsBackend(Qt3DCore::QNodeId id);
    void releaseSimulatorSettingsBackend(Qt3DCore::QNodeId id);


    //////////////////////
    // Environment Backend
    void                addEnvironmentBackend(Qt3DCore::QNodeId   id,
                                              EnvironmentBackend* backend);
    EnvironmentBackend* environmentBackend(Qt3DCore::QNodeId id);
    EnvironmentBackend* takeEnvironmentBackend(Qt3DCore::QNodeId id);
    const QHash<Qt3DCore::QNodeId, EnvironmentBackend*>&
    environmentBackends() const;

    ////////////////////////////////////////
    // Rigidbody Controller Backend : Sphere
    void                     addSphereControllerBackend(Qt3DCore::QNodeId        id,
                                                        SphereControllerBackend* backend);
    SphereControllerBackend* sphereControllerBackend(Qt3DCore::QNodeId id);
    SphereControllerBackend* takeSphereControllerBackend(Qt3DCore::QNodeId id);
    const QHash<Qt3DCore::QNodeId, SphereControllerBackend*>&
    sphereControllerBackends() const;

    ///////////////////////////////////////
    // Rigidbody Controller Backend : Plane
    void                    addPlaneControllerBackend(Qt3DCore::QNodeId       id,
                                                      PlaneControllerBackend* backend);
    PlaneControllerBackend* planeControllerBackend(Qt3DCore::QNodeId id);
    PlaneControllerBackend* takePlaneControllerBackend(Qt3DCore::QNodeId id);
    const QHash<Qt3DCore::QNodeId, PlaneControllerBackend*>&
    planeControllerBackends() const;

    ///////////////////////////////////////
    // Rigidbody Controller Backend : Surf
    void                   addSurfControllerBackend(Qt3DCore::QNodeId      id,
                                                    SurfControllerBackend* backend);
    SurfControllerBackend* surfControllerBackend(Qt3DCore::QNodeId id);
    SurfControllerBackend* takeSurfControllerBackend(Qt3DCore::QNodeId id);
    const QHash<Qt3DCore::QNodeId, SurfControllerBackend*>&
    surfControllerBackends() const;

    //////////////////////
    // FpsMonitor Backend
    void addFpsMonitorBackend(Qt3DCore::QNodeId id, FpsMonitorBackend* backend);
    FpsMonitorBackend* fpsMonitorBackend(Qt3DCore::QNodeId id);
    FpsMonitorBackend* takeFpsMonitorBackend(Qt3DCore::QNodeId id);
    const QHash<Qt3DCore::QNodeId, FpsMonitorBackend*>&
    fpsMonitorBackends() const;

    /////////////////
    // Object Manager
    RigidBodyContainer& rigidBodies();

    static void registerFunctionWindowsOnly();

    bool m_debug_stop{false};

  private:
    SimulatorSettingsBackendSP m_simulator_settings_backend{nullptr};
    Qt3DCore::QNodeId          m_simulator_settings_backend_peerid;

    EnvironmentBackendQHash m_environment_backends;

    SphereControllerBackendQHash m_spherecontroller_backends;
    PlaneControllerBackendQHash  m_planecontroller_backends;
    SurfControllerBackendQHash   m_surfcontroller_backends;

    FpsMonitorBackendQHash m_fpsmonitors_backends;

    RigidBodyContainer   m_rigid_bodies;
    seconds_type         m_elapsed_time{0};
    qint64               m_last_time{0};
    ColliderJobPtr       m_colliderworker;
    ColliderSimpleJobPtr m_collider_simpleworker;
    GenericUpdateJobPtr  m_updateworker;
    FpsJobPtr            m_fpsworker;

    // QAbstractAspect interface
  private:
    QVector<Qt3DCore::QAspectJobPtr> jobsToExecute(qint64 time) override;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_RIGIDBODYASPECT_H
