#include "environmentbackend.h"

#include "../utils.h"
#include "../frontend/environment.h"
#include "rigidbodyaspect.h"

namespace rigidbodyaspect
{

  EnvironmentBackend::EnvironmentBackend()
    : QBackendNode(QBackendNode::ReadWrite)
  {
  }

  void EnvironmentBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {

    if (e->type() == Qt3DCore::PropertyUpdated) {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      //      qDebug() << change->propertyName();
      if (change->propertyName() == QByteArrayLiteral("gravity")) {
        const auto new_value = change->value().value<QVector3D>();
        m_gravity            = utils::qVecToGM2Vec3(new_value);
      }
    }

    QBackendNode::sceneChangeEvent(e);
  }

  void EnvironmentBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange = qSharedPointerCast<
      Qt3DCore::QNodeCreatedChange<EnvironmentInitialData>>(change);
    const auto& data = typedChange->data;
    m_gravity        = utils::qVecToGM2Vec3(data.m_gravity);
  }


  EnvironmentBackendMapper::EnvironmentBackendMapper(RigidBodyAspect* aspect)
    : m_aspect{aspect}
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* EnvironmentBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto* env_backend = new EnvironmentBackend;
    m_aspect->addEnvironmentBackend(change->subjectId(), env_backend);
    return env_backend;
  }

  Qt3DCore::QBackendNode*
  EnvironmentBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->environmentBackend(id);
  }

  void EnvironmentBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takeEnvironmentBackend(id);
  }

}   // namespace rigidbodyaspect
