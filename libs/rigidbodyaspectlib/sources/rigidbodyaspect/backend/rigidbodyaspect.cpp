constexpr auto rba_name = "rigidbodyaspect";

#include "rigidbodyaspect.h"

#include "../frontend/simulatorsettings.h"
#include "simulatorsettingsbackend.h"

#include "../frontend/environment.h"
#include "environmentbackend.h"

#include "../frontend/spherecontroller.h"
#include "spherecontrollerbackend.h"

#include "../frontend/planecontroller.h"
#include "planecontrollerbackend.h"

#include "../frontend/surfcontroller.h"
#include "surfcontrollerbackend.h"

#include "../frontend/fpsmonitor.h"
#include "fpsmonitorbackend.h"

#include <QAbstractAspect>
using namespace Qt3DCore;

#include <iostream>
#include <chrono>
using namespace std::chrono_literals;

namespace rigidbodyaspect
{

  RigidBodyAspect::RigidBodyAspect(QObject* parent)
    : Qt3DCore::QAbstractAspect(parent),
      m_colliderworker{ColliderJobPtr::create(this)},
      m_collider_simpleworker{ColliderSimpleJobPtr::create(this)},
      m_updateworker{GenericUpdateJobPtr::create(this)},
      m_fpsworker{FpsJobPtr::create(this)}
  {

    qDebug() << "Constructing RigidBodyAspect";

    auto sim_settings_mapper
      = QSharedPointer<SimulatorSettingsBackendMapper>::create(this);
    registerBackendType<SimulatorSettings>(sim_settings_mapper);

    auto environment_mapper
      = QSharedPointer<EnvironmentBackendMapper>::create(this);
    registerBackendType<Environment>(environment_mapper);

    auto sphere_mapper
      = QSharedPointer<SphereControllerBackendMapper>::create(this);
    registerBackendType<SphereController>(sphere_mapper);

    auto plane_mapper
      = QSharedPointer<PlaneControllerBackendMapper>::create(this);
    registerBackendType<PlaneController>(plane_mapper);

    auto surf_mapper
      = QSharedPointer<SurfControllerBackendMapper>::create(this);
    registerBackendType<SurfController>(surf_mapper);

    auto fps_mapper = QSharedPointer<FpsMonitorBackendMapper>::create(this);
    registerBackendType<FpsMonitor>(fps_mapper);

    m_updateworker->addDependency(m_colliderworker.toWeakRef());
    m_updateworker->addDependency(m_collider_simpleworker.toWeakRef());
  }

  SimulatorSettingsBackend*
  RigidBodyAspect::constructSimulatorSettingsBackend(QNodeId id)
  {

    if (m_simulator_settings_backend not_eq nullptr) {
      qWarning() << "Simulator settings already specified";
      return nullptr;
    }

    m_simulator_settings_backend.reset(new SimulatorSettingsBackend);
    m_simulator_settings_backend_peerid = id;

    return m_simulator_settings_backend.get();
  }

  SimulatorSettingsBackend*
  RigidBodyAspect::simulatorSettingsBackend(QNodeId id)
  {
    if (m_simulator_settings_backend not_eq nullptr
        and m_simulator_settings_backend_peerid == id)
      return m_simulator_settings_backend.get();

    return nullptr;
  }

  void RigidBodyAspect::releaseSimulatorSettingsBackend(QNodeId id)
  {
    if (m_simulator_settings_backend not_eq nullptr
        and m_simulator_settings_backend_peerid == id)
      m_simulator_settings_backend.reset(nullptr);
  }

  void RigidBodyAspect::addEnvironmentBackend(QNodeId             id,
                                              EnvironmentBackend* backend)
  {
    m_environment_backends.insert(id, backend);
  }

  EnvironmentBackend* RigidBodyAspect::environmentBackend(QNodeId id)
  {
    return m_environment_backends.value(id, nullptr);
  }

  EnvironmentBackend* RigidBodyAspect::takeEnvironmentBackend(QNodeId id)
  {
    return m_environment_backends.take(id);
  }

  const QHash<QNodeId, EnvironmentBackend*>&
  RigidBodyAspect::environmentBackends() const
  {
    return m_environment_backends;
  }


  void
  RigidBodyAspect::addSphereControllerBackend(Qt3DCore::QNodeId        id,
                                              SphereControllerBackend* backend)
  {
    m_spherecontroller_backends.insert(id, backend);
  }

  SphereControllerBackend*
  RigidBodyAspect::sphereControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_spherecontroller_backends.value(id, nullptr);
  }

  SphereControllerBackend*
  RigidBodyAspect::takeSphereControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_spherecontroller_backends.take(id);
  }

  const QHash<Qt3DCore::QNodeId, SphereControllerBackend*>&
  RigidBodyAspect::sphereControllerBackends() const
  {
    return m_spherecontroller_backends;
  }

  void
  RigidBodyAspect::addPlaneControllerBackend(Qt3DCore::QNodeId       id,
                                             PlaneControllerBackend* backend)
  {
    m_planecontroller_backends.insert(id, backend);
  }

  PlaneControllerBackend*
  RigidBodyAspect::planeControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_planecontroller_backends.value(id, nullptr);
  }

  PlaneControllerBackend*
  RigidBodyAspect::takePlaneControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_planecontroller_backends.take(id);
  }

  const QHash<Qt3DCore::QNodeId, PlaneControllerBackend*>&
  RigidBodyAspect::planeControllerBackends() const
  {
    return m_planecontroller_backends;
  }

  void RigidBodyAspect::addSurfControllerBackend(Qt3DCore::QNodeId      id,
                                                 SurfControllerBackend* backend)
  {
    m_surfcontroller_backends.insert(id, backend);
  }

  SurfControllerBackend*
  RigidBodyAspect::surfControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_surfcontroller_backends.value(id, nullptr);
  }

  SurfControllerBackend*
  RigidBodyAspect::takeSurfControllerBackend(Qt3DCore::QNodeId id)
  {
    return m_surfcontroller_backends.take(id);
  }

  const QHash<Qt3DCore::QNodeId, SurfControllerBackend*>&
  RigidBodyAspect::surfControllerBackends() const
  {
    return m_surfcontroller_backends;
  }

  void RigidBodyAspect::addFpsMonitorBackend(QNodeId            id,
                                             FpsMonitorBackend* backend)
  {
    m_fpsmonitors_backends.insert(id, backend);
  }

  FpsMonitorBackend* RigidBodyAspect::fpsMonitorBackend(QNodeId id)
  {
    return m_fpsmonitors_backends.value(id, nullptr);
  }

  FpsMonitorBackend* RigidBodyAspect::takeFpsMonitorBackend(QNodeId id)
  {
    return m_fpsmonitors_backends.take(id);
  }

  const QHash<QNodeId, FpsMonitorBackend*>&
  RigidBodyAspect::fpsMonitorBackends() const
  {
    return m_fpsmonitors_backends;
  }

  RigidBodyContainer& RigidBodyAspect::rigidBodies() { return m_rigid_bodies; }



  QVector<Qt3DCore::QAspectJobPtr> RigidBodyAspect::jobsToExecute(qint64 time)
  {

    [[maybe_unused]] const auto time_dt = time - m_last_time;
    m_last_time                         = time;

    if (m_debug_stop) return {};

    if (not m_simulator_settings_backend
        or not m_simulator_settings_backend->runStatus())
      return {};


    [[maybe_unused]] static constexpr auto time_resolution = 1000000000.0;
    const seconds_type                     dt              = 16ms;


    // Collider worker
    m_colliderworker->setFrameTimeDt(dt);
    m_collider_simpleworker->setFrameTimeDt(dt);
    m_fpsworker->setDT(time_dt);

    return {m_colliderworker, m_updateworker};
  }


}   // namespace rigidbodyaspect



QT3D_REGISTER_NAMESPACED_ASPECT(rba_name, rigidbodyaspect, RigidBodyAspect)

namespace rigidbodyaspect
{
  void RigidBodyAspect::registerFunctionWindowsOnly()
  {
#ifdef _MSVC_LANG
    // Do this manually for now, because it doesn't work on the Windows
    // platform...
    qt3d_RigidBodyAspect_registerFunction();
#endif
  }
}   // namespace rigidbodyaspect
