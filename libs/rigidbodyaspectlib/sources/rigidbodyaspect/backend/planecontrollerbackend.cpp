#include "planecontrollerbackend.h"

#include "rigidbodyaspect.h"
#include "../frontend/planecontroller.h"
#include "../utils.h"

namespace rigidbodyaspect
{

  PlaneControllerBackend::PlaneControllerBackend(
    RigidBodyContainer& rigid_bodies)
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite), m_rigid_bodies{
                                                                   rigid_bodies}
  {
  }

  PlaneControllerBackend::~PlaneControllerBackend()
  {
    m_rigid_bodies.destroyFixedPlane(peerId());
  }

  void PlaneControllerBackend::queueFrontendUpdate()
  {

    const auto& pframe
      = m_rigid_bodies.fixedPlane(peerId()).pSpaceFrameParent();

    // Convert data to sendable frame
    QMatrix3x3 q_pframe;
    for (auto i = 0UL; i < 3UL; ++i) {
      q_pframe(int(i), 0)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 0UL));   // dir
      q_pframe(int(i), 1)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 2UL));   // up
      q_pframe(int(i), 2)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 3UL));   // pos
    }

    // Send data
    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("dupframe_BE"));
    e->setValue(QVariant::fromValue(q_pframe));
    notifyObservers(e);
  }

  void PlaneControllerBackend::setDupFrame(const QMatrix3x3 dup_frame)
  {
    if (qFuzzyCompare(dup_frame, m_dup_frame)) return;

    m_dup_frame = dup_frame;

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    m_rigid_bodies.fixedPlane(peerId()).setFrameParent(dir, up, pos);
  }

  void PlaneControllerBackend::setInitialFriction(const float& friction)
  {
    if (qFuzzyCompare(m_initial_friction, friction)) return;

    m_initial_friction = friction;

    m_rigid_bodies.fixedPlane(peerId()).m_friction = double(friction);
  }

  void PlaneControllerBackend::setConstraints(
    const RigidBodyConstraints& constraints)
  {
    if (constraints == m_constraints) return;
  }

  void PlaneControllerBackend::setEnvironmentId(const Qt3DCore::QNodeId& id)
  {
    if (m_environment_id == id) return;

    m_environment_id = id;

    m_rigid_bodies.fixedPlane(peerId()).m_env_id = m_environment_id;
  }

  void PlaneControllerBackend::setOrigin(const QVector3D& p)
  {

    auto& plane = m_rigid_bodies.fixedPlane(peerId());
    plane.m_pt
      = gmlib2::utils::extendStaticContainer(utils::qVecToGM2Vec3(p), 1.0);
  }

  void PlaneControllerBackend::setUAxis(const QVector3D& u)
  {

    auto& plane = m_rigid_bodies.fixedPlane(peerId());
    plane.m_u
      = gmlib2::utils::extendStaticContainer(utils::qVecToGM2Vec3(u), 1.0);
  }

  void PlaneControllerBackend::setVAxis(const QVector3D& v)
  {

    auto& plane = m_rigid_bodies.fixedPlane(peerId());
    plane.m_v
      = gmlib2::utils::extendStaticContainer(utils::qVecToGM2Vec3(v), 1.0);
  }

  void PlaneControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange
      = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<PlaneInitialData>>(
        change);
    const auto& data   = typedChange->data;
    m_constraints      = data.m_constraints;
    m_dup_frame        = data.m_dup_frame;
    m_initial_friction = data.m_initial_friction;
    m_environment_id   = data.m_environment_id;

    m_rigid_bodies.constructFixedPlane(peerId());
    auto& plane = m_rigid_bodies.fixedPlane(peerId());

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    plane.setFrameParent(dir, up, pos);

    setOrigin(data.m_p);
    setUAxis(data.m_u);
    setVAxis(data.m_v);

    plane.m_env_id = m_environment_id;


    plane.m_friction = double(m_initial_friction);
    // surround sphere
    generateSurroundSphere(plane);
    generateSurroundSpheres(plane);
  }

  void
  PlaneControllerBackend::generateSurroundSphere(rbtypes::FixedPlane& plane)
  {
    auto                        start = plane.startParameters();
    auto                        end   = plane.endParameters();
    auto                        es    = plane.evaluateParent(start).at(0, 0);
    auto                        esb   = es + plane.m_u;
    auto                        esb2  = es + plane.m_v;
    auto                        ee    = plane.evaluateParent(end).at(0, 0);
    integration::SurroundSphere ss{GM2Vector{es[0], es[1], es[2]}};
    ss += GM2Vector{ee[0], ee[1], ee[2]};
    ss += GM2Vector{esb2[0], esb2[1], esb2[2]};
    ss += GM2Vector{esb[0], esb[1], esb[2]};
    plane.setSurroundSphere(ss);
  }

  void
  PlaneControllerBackend::generateSurroundSpheres(rbtypes::FixedPlane& plane)
  {
    std::vector<integration::SurroundSphere> ssv;
    auto                                     scale = 4;
    auto                                     start = plane.startParameters();
    auto es = plane.evaluateParent(start).at(0, 0);

    for (auto i = 0; i < scale; i++)
      for (auto j = 0; j < scale; j++) {
        auto ests = es + plane.m_u * (double(i) / double(scale))
                    + plane.m_v * (double(j) / double(scale));
        integration::SurroundSphere ss{GM2Vector{ests[0], ests[1], ests[2]}};
        auto estf = es + plane.m_u * (double(i + 1) / double(scale))
                    + plane.m_v * (double(j + 1) / double(scale));
        ss += GM2Vector{estf[0], estf[1], estf[2]};
        auto estf2 = es + plane.m_u * (double(i) / double(scale))
                     + plane.m_v * (double(j + 1) / double(scale));
        ss += GM2Vector{estf2[0], estf2[1], estf2[2]};
        auto estf3 = es + plane.m_u * (double(i + 1) / double(scale))
                     + plane.m_v * (double(j) / double(scale));
        ss += GM2Vector{estf3[0], estf3[1], estf3[2]};
        ssv.push_back(ss);
      }
    plane.setSurroundSphereS(ssv);
  }

  void
  PlaneControllerBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {

    if (e->type() == Qt3DCore::PropertyUpdated) {


      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);

      if (change->propertyName() == QByteArrayLiteral("constraints"))
        setConstraints(change->value().value<RigidBodyConstraints>());
      else if (change->propertyName() == QByteArrayLiteral("initialFriction"))
        setInitialFriction(change->value().toFloat());
      else if (change->propertyName() == QByteArrayLiteral("environment"))
        setEnvironmentId(change->value().value<Qt3DCore::QNodeId>());
      else if (change->propertyName() == QByteArrayLiteral("origin"))
        setOrigin(change->value().value<QVector3D>());
      else if (change->propertyName() == QByteArrayLiteral("uAxis"))
        setUAxis(change->value().value<QVector3D>());
      else if (change->propertyName() == QByteArrayLiteral("vAxis"))
        setVAxis(change->value().value<QVector3D>());
    }

    QBackendNode::sceneChangeEvent(e);
  }

  PlaneControllerBackendMapper::PlaneControllerBackendMapper(
    RigidBodyAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* PlaneControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new PlaneControllerBackend(m_aspect->rigidBodies());
    m_aspect->addPlaneControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  PlaneControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->planeControllerBackend(id);
  }

  void PlaneControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takePlaneControllerBackend(id);
  }

}   // namespace rigidbodyaspect
