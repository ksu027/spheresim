#ifndef RIGIDBODYASPECT_SURFCONTROLLERBACKEND_H
#define RIGIDBODYASPECT_SURFCONTROLLERBACKEND_H

#include "../types.h"

#include "../geometry/rigidbodycontainer.h"

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QBackendNode>

namespace rigidbodyaspect
{

  class SurfControllerBackend : public Qt3DCore::QBackendNode {
  public:
    SurfControllerBackend(RigidBodyContainer& rigid_bodies);
    ~SurfControllerBackend() override;

    void queueFrontendUpdate();

  private:
    RigidBodyConstraints m_constraints;
    QMatrix3x3           m_dup_frame;
    float                m_initial_friction;
    RigidBodyContainer&  m_rigid_bodies;
    Qt3DCore::QNodeId    m_environment_id;


    void setDupFrame(const QMatrix3x3 dup_frame);

    void setInitialFriction(const float& friction);

    void setConstraints(const RigidBodyConstraints& constraints);
    void setEnvironmentId(const Qt3DCore::QNodeId& id);

    void setScale(const QVector2D& scale);

    rbtypes::Surf& surf();

    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
    void generateSurroundSphere(rbtypes::Surf& surf);
    void generateSurroundSpheres(rbtypes::Surf& surf);
  };


  class RigidBodyAspect;

  class SurfControllerBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit SurfControllerBackendMapper(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_SURFCONTROLLERBACKEND_H
