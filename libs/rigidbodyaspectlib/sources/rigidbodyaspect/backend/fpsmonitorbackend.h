// for fps count addopted from https://www.kdab.com/writing-custom-qt-3d-aspect/

#ifndef RIGIDBODYASPECT_FPSMONITORBACKEND_H
#define RIGIDBODYASPECT_FPSMONITORBACKEND_H

#include <Qt3DCore/QBackendNode>
#include "../algorithms/movingaverage.h"

namespace rigidbodyaspect
{
  class FpsMonitorBackend : public Qt3DCore::QBackendNode {
  public:
    FpsMonitorBackend();

    void addFpsSample(float fpsSample);

  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;

    int           m_rollingMeanFrameCount;
    MovingAverage m_average;
  };


  class RigidBodyAspect;

  class FpsMonitorBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit FpsMonitorBackendMapper(RigidBodyAspect* aspect);

    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;

  private:
    RigidBodyAspect* m_aspect;
  };
}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_FPSMONITORBACKEND_H
