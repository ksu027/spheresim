#ifndef RIGIDBODYASPECT_SPHERECONTROLLERBACKEND_H
#define RIGIDBODYASPECT_SPHERECONTROLLERBACKEND_H

#include "../types.h"

#include "../geometry/rigidbodycontainer.h"

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QBackendNode>

namespace rigidbodyaspect
{

  class SphereControllerBackend : public Qt3DCore::QBackendNode {
  public:
    SphereControllerBackend(RigidBodyContainer& rigid_bodies);
    ~SphereControllerBackend() override;

    void queueFrontendUpdate();

  private:
    RigidBodyConstraints m_constraints;
    QMatrix3x3           m_dup_frame;
    QVector3D            m_initial_velocity;
    float                m_initial_mass;
    float                m_initial_friction;
    float                m_radius;
    float                m_initial_inertia;
    RigidBodyContainer&  m_rigid_bodies;
    Qt3DCore::QNodeId    m_environment_id;


    void setRadius(float radius);
    void setDupFrame(const QMatrix3x3 dup_frame);
    void setInitialVelocity(const QVector3D& init_velocity);
    void setInitialMass(const float& init_velocity);
    void setInitialFriction(const float& init_friction);

    void setConstraints(const RigidBodyConstraints& constraints);
    void setEnvironmentId(const Qt3DCore::QNodeId& id);
    void updateInertia();


    rbtypes::Sphere& sphere();

    // QBackendNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e) override;

  private:
    void initializeFromPeer(
      const Qt3DCore::QNodeCreatedChangeBasePtr& change) override;
  };


  class RigidBodyAspect;

  class SphereControllerBackendMapper : public Qt3DCore::QBackendNodeMapper {
  public:
    explicit SphereControllerBackendMapper(RigidBodyAspect* aspect);

  private:
    RigidBodyAspect* m_aspect;

    // QBackendNodeMapper interface
  public:
    Qt3DCore::QBackendNode*
                            create(const Qt3DCore::QNodeCreatedChangeBasePtr& change) const override;
    Qt3DCore::QBackendNode* get(Qt3DCore::QNodeId id) const override;
    void                    destroy(Qt3DCore::QNodeId id) const override;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_SPHERECONTROLLERBACKEND_H
