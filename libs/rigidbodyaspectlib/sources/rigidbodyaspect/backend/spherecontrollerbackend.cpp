#include "spherecontrollerbackend.h"

#include "rigidbodyaspect.h"
#include "../frontend/spherecontroller.h"
#include "../utils.h"

namespace rigidbodyaspect
{

  SphereControllerBackend::SphereControllerBackend(
    RigidBodyContainer& rigid_bodies)
    : Qt3DCore::QBackendNode(Qt3DCore::QBackendNode::ReadWrite), m_rigid_bodies{
                                                                   rigid_bodies}
  {
  }

  SphereControllerBackend::~SphereControllerBackend()
  {
    m_rigid_bodies.destroySphere(peerId());
  }

  void SphereControllerBackend::queueFrontendUpdate()
  {

    const auto& pframe = m_rigid_bodies.sphere(peerId()).pSpaceFrameParent();

    // Convert data to sendable frame
    QMatrix3x3 q_pframe;
    for (auto i = 0UL; i < 3UL; ++i) {
      q_pframe(int(i), 0)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 0UL));   // dir
      q_pframe(int(i), 1)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 2UL));   // up
      q_pframe(int(i), 2)
        = float(blaze::submatrix<0UL, 0UL, 3UL, 4UL>(pframe)(i, 3UL));   // pos
    }

    // Send data
    auto e = Qt3DCore::QPropertyUpdatedChangePtr::create(peerId());
    e->setDeliveryFlags(Qt3DCore::QSceneChange::Nodes);
    e->setPropertyName(QByteArrayLiteral("dupframe_BE"));
    e->setValue(QVariant::fromValue(q_pframe));
    notifyObservers(e);
  }

  void SphereControllerBackend::setRadius(float radius)
  {
    if (qFuzzyCompare(m_radius, radius)) return;

    m_radius = radius;
    this->updateInertia();

    if (m_constraints not_eq RigidBodyConstraints::NoConstraints) return;

    m_rigid_bodies.sphere(peerId()).m_radius = double(radius);
  }

  void SphereControllerBackend::setDupFrame(const QMatrix3x3 dup_frame)
  {
    if (qFuzzyCompare(dup_frame, m_dup_frame)) return;

    m_dup_frame = dup_frame;

    if (m_constraints not_eq RigidBodyConstraints::NoConstraints) return;

    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    m_rigid_bodies.sphere(peerId()).setFrameParent(dir, up, pos);
  }

  void
  SphereControllerBackend::setInitialVelocity(const QVector3D& init_velocity)
  {
    if (qFuzzyCompare(m_initial_velocity, init_velocity)) return;

    m_initial_velocity = init_velocity;

    if (m_constraints not_eq RigidBodyConstraints::NoConstraints) return;

    m_rigid_bodies.sphere(peerId()).m_velocity
      = utils::qVecToGM2Vec3(m_initial_velocity);
  }

  void SphereControllerBackend::updateInertia()
  {
    m_initial_inertia = 2.0f / 5.0f * m_initial_mass * std::pow(m_radius, 2);
    if (m_constraints not_eq RigidBodyConstraints::NoConstraints) return;
    m_rigid_bodies.sphere(peerId()).m_inertia
      = double(2.0 / 5.0 * m_rigid_bodies.sphere(peerId()).m_mass
               * std::pow(m_rigid_bodies.sphere(peerId()).m_radius, 2));
  }

  void SphereControllerBackend::setInitialMass(const float& mass)
  {
    if (qFuzzyCompare(m_initial_mass, mass)) return;

    m_initial_mass = mass;
    this->updateInertia();

    if (m_constraints not_eq RigidBodyConstraints::NoConstraints) return;

    m_rigid_bodies.sphere(peerId()).m_mass = double(mass);
  }

  void SphereControllerBackend::setInitialFriction(const float& friction)
  {
    if (qFuzzyCompare(m_initial_friction, friction)) return;

    m_initial_friction = friction;

    if (m_constraints not_eq RigidBodyConstraints::NoConstraints) return;

    m_rigid_bodies.sphere(peerId()).m_friction = double(friction);
  }

  void SphereControllerBackend::setConstraints(
    const RigidBodyConstraints& constraints)
  {
    if (constraints == m_constraints) return;
  }

  void SphereControllerBackend::setEnvironmentId(const Qt3DCore::QNodeId& id)
  {
    if (m_environment_id == id) return;

    m_environment_id = id;

    if (m_constraints not_eq RigidBodyConstraints::NoConstraints) return;

    m_rigid_bodies.sphere(peerId()).m_env_id = m_environment_id;
  }

  void SphereControllerBackend::initializeFromPeer(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change)
  {
    const auto typedChange
      = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<SphereInitialData>>(
        change);
    const auto& data   = typedChange->data;
    m_constraints      = data.m_constraints;
    m_dup_frame        = data.m_dup_frame;
    m_initial_velocity = data.m_initial_velocity;
    m_initial_mass     = data.m_initial_mass;
    m_initial_friction = data.m_initial_friction;
    m_radius           = data.m_radius;
    m_environment_id   = data.m_environment_id;

    m_rigid_bodies.constructSphere(peerId());
    auto& sphere = m_rigid_bodies.sphere(peerId());

    // geometric properties
    const auto& [dir, up, pos] = utils::dupFrameToDUP(m_dup_frame);
    sphere.setFrameParent(dir, up, pos);
    sphere.m_radius = double(m_radius);
    sphere.m_env_id = m_environment_id;

    // init physical properties
    sphere.m_mass     = double(m_initial_mass);
    sphere.m_friction = double(m_initial_friction);
    sphere.m_velocity = utils::qVecToGM2Vec3(m_initial_velocity);
    updateInertia();
    sphere.m_inertia = double(m_initial_inertia);

    // set surrounding sphere
    integration::SurroundSphere ss(sphere.frameOriginParent(), sphere.m_radius);
    sphere.setSurroundSphere(ss);
    sphere.m_pos = sphere.frameOriginParent();
  }

  void
  SphereControllerBackend::sceneChangeEvent(const Qt3DCore::QSceneChangePtr& e)
  {
    if (e->type() == Qt3DCore::PropertyUpdated) {
      const auto change
        = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(e);
      if (change->propertyName() == QByteArrayLiteral("dupFrame"))
        setDupFrame(change->value().value<QMatrix3x3>());
      else if (change->propertyName() == QByteArrayLiteral("constraints"))
        setConstraints(change->value().value<RigidBodyConstraints>());
      else if (change->propertyName() == QByteArrayLiteral("initialVelocity"))
        setInitialVelocity(change->value().value<QVector3D>());
      else if (change->propertyName() == QByteArrayLiteral("initialMass"))
        setInitialMass(change->value().toFloat());
      else if (change->propertyName() == QByteArrayLiteral("initialFriction"))
        setInitialFriction(change->value().toFloat());
      else if (change->propertyName() == QByteArrayLiteral("environment"))
        setEnvironmentId(change->value().value<Qt3DCore::QNodeId>());
      else if (change->propertyName() == QByteArrayLiteral("radius"))
        setRadius(change->value().toFloat());
    }

    QBackendNode::sceneChangeEvent(e);
  }

  SphereControllerBackendMapper::SphereControllerBackendMapper(
    RigidBodyAspect* aspect)
    : m_aspect(aspect)
  {
    Q_ASSERT(m_aspect);
  }

  Qt3DCore::QBackendNode* SphereControllerBackendMapper::create(
    const Qt3DCore::QNodeCreatedChangeBasePtr& change) const
  {
    auto backend = new SphereControllerBackend(m_aspect->rigidBodies());
    m_aspect->addSphereControllerBackend(change->subjectId(), backend);
    return backend;
  }

  Qt3DCore::QBackendNode*
  SphereControllerBackendMapper::get(Qt3DCore::QNodeId id) const
  {
    return m_aspect->sphereControllerBackend(id);
  }

  void SphereControllerBackendMapper::destroy(Qt3DCore::QNodeId id) const
  {
    delete m_aspect->takeSphereControllerBackend(id);
  }

}   // namespace rigidbodyaspect
