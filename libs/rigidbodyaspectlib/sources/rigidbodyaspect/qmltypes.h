#ifndef RIGIDBODYASPECT_QMLTYPES_H
#define RIGIDBODYASPECT_QMLTYPES_H


#include "types.h"

namespace rigidbodyaspect
{

  class QMLTypes : public QObject {
    Q_OBJECT

  public:
    using DynamicsType = RigidBodyDynamicsType;
    Q_ENUM(DynamicsType)

    QMLTypes() = default;
  };

}   // namespace rigidbodyaspect


#endif   // RIGIDBODYASPECT_QMLTYPES_H
