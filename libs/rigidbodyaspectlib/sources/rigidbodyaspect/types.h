#ifndef RIGIDBODYASPECT_TYPES_H
#define RIGIDBODYASPECT_TYPES_H


// gmlib2
#include <integration/sceneobject.h>

// qt
#include <QGenericMatrix>

// stl
#include <chrono>

namespace rigidbodyaspect
{

  // std::ratio<1,1> is implicit
  // default and means seconds
  //
  // Literals are enabled through:
  // using namespace std::chrono_literals;
  using seconds_type = std::chrono::duration<double>;

  using GM2SpaceObjectType = gmlib2::ProjectiveSpaceObject<>;
  using GM2UnitType        = GM2SpaceObjectType::Unit;
  using GM2Vector          = GM2SpaceObjectType::Vector;
  using GM2VectorH         = GM2SpaceObjectType::VectorH;
  using GM2ASFrameH        = GM2SpaceObjectType::ASFrameH;

  using ContactNTtype
    = QHash<std::pair<Qt3DCore::QNodeId, Qt3DCore::QNodeId>, GM2Vector>;



  namespace algorithms
  {
    namespace collision
    {
      namespace detail
      {

        enum class SphereFixedPlaneCollisionStatus {
          NoCollision,
          Collision,
          SingularityException
        };

        using SphereFixedPlaneColliderReturnType
          = std::pair<seconds_type, SphereFixedPlaneCollisionStatus>;

        using SphereSurfColliderReturnType
          = std::tuple<seconds_type, SphereFixedPlaneCollisionStatus,
                       GM2Vector>;

        // not sure yet if needed
        enum class CollisionStatus { NoCollision, Collision, Singularity };

        using ColliderReturnType = std::pair<seconds_type, CollisionStatus>;
      }   // namespace detail

      namespace types
      {
        // type of the second object in the collision
        enum class ColType { Sp, Pl, Su };

        using SpFPlStatus
          = algorithms::collision::detail::SphereFixedPlaneCollisionStatus;

        // collision object:
        //    time            - of collision,
        //    NodeId          - id of the acting object (sphere)
        //    Set of NodeIds  - the objects colliding at the same time (either
        //    sphere or plane or whatever) (need helpers to find out what is the
        //    object)
        using ColObj       = std::tuple<seconds_type, Qt3DCore::QNodeId,
                                  QSet<Qt3DCore::QNodeId>>;
        using ColContainer = std::vector<ColObj>;

      }   // namespace types
    }     // namespace collision

    namespace statechange
    {

      namespace detail
      {

        enum class StateChangeStatus {
          NoChange,
          ToFree,
          ToSliding,
          ToRolling,
          ToAtRest
        };

      }   // namespace detail

    }   // namespace statechange





    namespace contact
    {
      struct ContactPoint {
        ContactPoint(std::pair<Qt3DCore::QNodeId, Qt3DCore::QNodeId> c_o,
                     algorithms::collision::types::ColType c_t, double v_d,
                     GM2Vector n)
          : contact_objects(c_o), contact_type(c_t), v_delta(v_d), n(n)
        {
        }

        std::pair<Qt3DCore::QNodeId, Qt3DCore::QNodeId> contact_objects;
        algorithms::collision::types::ColType           contact_type;
        double                                          v_delta;
        double                                          penetrate;
        GM2Vector                                       n;
        size_t                                          visits   = 0;
        bool                                            resolved = false;

        bool operator<(ContactPoint other_cp)
        {
          return v_delta < other_cp.v_delta;
        }

        bool operator==(ContactPoint other_cp)
        {
          return contact_objects == other_cp.contact_objects;
        }
      };
    }   // namespace contact
  }     // namespace algorithms


}   // namespace rigidbodyaspect



// QML asseccible types
namespace rigidbodyaspect
{
  Q_NAMESPACE
  enum class RigidBodyConstraints { NoConstraints, Fixed };
  Q_ENUM_NS(RigidBodyConstraints)

}   // namespace rigidbodyaspect

// Q_DECLARE_METATYPE(rigidbodyaspect::RigidBodyConstraints)

#endif   // RIGIDBODYASPECT_TYPES_H
