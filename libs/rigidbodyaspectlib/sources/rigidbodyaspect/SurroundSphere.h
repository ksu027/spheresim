#ifndef INTEGRATION_UTILS_SURROUNDSPHERE_H
#define INTEGRATION_UTILS_SURROUNDSPHERE_H

// gmlib2
#include <gmlib2.h>
#include "constants.h"

namespace integration
{
  using GM2SpaceObjectType = gmlib2::ProjectiveSpaceObject<>;
  using GM2Vector          = GM2SpaceObjectType::Vector;

  class SurroundSphere {
  public:
    SurroundSphere(const GM2Vector& in_cp = GM2Vector{0.0, 0.0, 0.0},
                   const double&    in_r  = 0.0)
      : cp(in_cp), r(in_r)
    {
    }
    SurroundSphere(const SurroundSphere& other_sphere)
      : cp(other_sphere.getCenter()), r(other_sphere.getRadius())
    {
    }

    double    getRadius() const { return r; }
    GM2Vector getCenter() const { return cp; }
    bool      intersectWith(const SurroundSphere& other_sphere) const
    {
      // distance between centers is <= sum of radiuses
      return blaze::length(cp - other_sphere.getCenter())
               - (r + other_sphere.getRadius())
             <= 1e-6;
    }
    void   reSetRadius(const double& in_r) { r = in_r; }
    void   reSetCenter(const GM2Vector& in_cp) { cp = in_cp; }
    double getSize() const
    {
      return 1.333333 * rigidbodyaspect::constants::Pi * std::pow(r, 3);
    }
    double getGrowth(const SurroundSphere& other_sphere) const
    {
      SurroundSphere new_sphere = *this;
      new_sphere += other_sphere;
      return std::pow(new_sphere.getRadius(), 2) - std::pow(r, 2);
    }

    SurroundSphere& operator=(const SurroundSphere& other_sphere) = default;
    const SurroundSphere& operator+=(const SurroundSphere& other_sphere)
    {
      GM2Vector v = other_sphere.cp - this->cp;
      double    new_r, d = blaze::length(v);
      if (this->r > other_sphere.r) {
        if (d > this->r - other_sphere.r) {
          new_r = (d + this->r + other_sphere.r) / 2.0;
          this->cp += ((new_r - this->r) / d) * v;
          this->r = new_r;
        }
      }
      else {
        if (d > other_sphere.r - this->r) {
          new_r = (d + this->r + other_sphere.r) / 2.0;
          this->cp += ((new_r - this->r) / d) * v;
          this->r = new_r;
        }
        else   // with double it is very rare to happen
          *this = other_sphere;
      }
      return *this;
    }

    const SurroundSphere& operator+=(const GM2Vector& point)
    {
      GM2Vector v = point - this->cp;
      double    new_r, d = blaze::length(v);

      if (d > this->r) {
        new_r = (d + this->r) / 2.0;
        this->cp += ((new_r - this->r) / d) * v;
        this->r = new_r;
      }
      return *this;
    }

    SurroundSphere operator+(const SurroundSphere& other_sphere) const
    {
      SurroundSphere our_sphere = *this;
      our_sphere += other_sphere;
      return our_sphere;
    }
    SurroundSphere operator+(const GM2Vector& point) const
    {
      SurroundSphere our_sphere = *this;
      our_sphere += point;
      return our_sphere;
    }

  private:
    GM2Vector cp;   // center point
    double    r;    // radius
  };

}   // namespace integration


#endif   // INTEGRATION_UTILS_SURROUNDSPHERE_H
