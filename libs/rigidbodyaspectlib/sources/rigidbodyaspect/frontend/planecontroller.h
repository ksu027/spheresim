#ifndef RIGIDBODYASPECT_PLANECONTROLLER_H
#define RIGIDBODYASPECT_PLANECONTROLLER_H


#include "abstractrigidbodycontroller.h"

namespace rigidbodyaspect
{

  class PlaneController : public AbstractRigidBodyController {
    Q_OBJECT

    Q_PROPERTY(QVector3D origin MEMBER m_p NOTIFY originChanged)
    Q_PROPERTY(QVector3D uAxis MEMBER m_u NOTIFY uAxisChanged)
    Q_PROPERTY(QVector3D vAxis MEMBER m_v NOTIFY vAxisChanged)

  public:
    PlaneController(Qt3DCore::QNode* parent = nullptr);


    QVector3D m_p{0, 0, 0};
    QVector3D m_u{0.0f, 0.0f, 10.0f};
    QVector3D m_v{10.0f, 0.0f, 0.0f};

  signals:
    void originChanged(const QVector3D& origin);
    void uAxisChanged(const QVector3D& uaxis);
    void vAxisChanged(const QVector3D& vaxis);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr
    createNodeCreationChange() const override;
  };


  struct PlaneInitialData : AbstractRigidBodyInitialData {
    QVector3D m_p;
    QVector3D m_u;
    QVector3D m_v;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_PLANECONTROLLER_H
