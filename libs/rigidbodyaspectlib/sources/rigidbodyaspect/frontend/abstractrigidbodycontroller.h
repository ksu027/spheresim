#ifndef RIGIDBODYASPECT_ABSTRACTRIGIDBODYCONTROLLER_H
#define RIGIDBODYASPECT_ABSTRACTRIGIDBODYCONTROLLER_H


#include "environment.h"
#include "../types.h"

// blaze
#include <blaze/Math.h>

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QComponent>
#include <Qt3DCore/QTransform>

namespace rigidbodyaspect
{

  struct AbstractRigidBodyInitialData;

  class AbstractRigidBodyController : public Qt3DCore::QComponent {
    Q_OBJECT

    Q_PROPERTY(QVariant constraints READ constraints WRITE setConstraints NOTIFY
                 constraintsChanged)
    Q_PROPERTY(QMatrix3x3 dupFrame READ dupFrame NOTIFY dupFrameChanged)
    Q_PROPERTY(QVector3D initialVelocity READ initialVelocity WRITE
                 setInitialVelocity NOTIFY initialVelocityChanged)
    Q_PROPERTY(float initialMass READ initialMass WRITE setInitialMass NOTIFY
                 initialMassChanged)
    Q_PROPERTY(float initialFriction READ initialFriction WRITE
                 setInitialFriction NOTIFY initialFrictionChanged)
    Q_PROPERTY(Environment* environment READ environment WRITE setEnvironment
                 NOTIFY environmentChanged)

  public:
    ~AbstractRigidBodyController() override = default;

    QVariant constraints() const;
    void     setConstraints(const QVariant& dynamics_type);

    QMatrix3x3       dupFrame() const;
    Q_INVOKABLE void resetFrameByDup(const QVector3D& dir, const QVector3D& up,
                                     const QVector3D& pos);

    QVector3D initialVelocity() const;
    void      setInitialVelocity(const QVector3D& velocity);

    float initialMass() const;
    void  setInitialMass(const float& mass);

    float initialFriction() const;
    void  setInitialFriction(const float& friction);

    Environment* environment() const;
    void         setEnvironment(Environment* environment);

  protected:
    void fillAbstRBInitialData(AbstractRigidBodyInitialData& init_data) const;

    RigidBodyConstraints m_constraints{RigidBodyConstraints::NoConstraints};
    QMatrix3x3           m_dup_frame;
    Environment*         m_environment{nullptr};
    QVector3D            m_initial_velocity{0, 0, 0};
    float                m_initial_mass{1};
    float                m_initial_friction{0};

  signals:
    void frameComputed(const QVector3D& dir, const QVector3D& up,
                       const QVector3D& pos);

    void constraintsChanged(RigidBodyConstraints constraints);
    void dupFrameChanged(const QMatrix3x3& dupFrame);
    void initialVelocityChanged(const QVector3D& velocity);
    void initialMassChanged(float mass);
    void initialFrictionChanged(float friction);
    void environmentChanged(Environment* environment);

    // QNode interface
  protected:
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr& change) override;

  protected:
    AbstractRigidBodyController(Qt3DCore::QNode* parent = nullptr);
  };

  struct AbstractRigidBodyInitialData {
    RigidBodyConstraints m_constraints;
    QMatrix3x3           m_dup_frame;
    QVector3D            m_initial_velocity;
    float                m_initial_mass;
    float                m_initial_friction;
    Qt3DCore::QNodeId    m_environment_id;
  };

}   // namespace rigidbodyaspect


#endif   // RIGIDBODYASPECT_ABSTRACTRIGIDBODYCONTROLLER_H
