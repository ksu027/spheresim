#include "planecontroller.h"

namespace rigidbodyaspect
{




  PlaneController::PlaneController(Qt3DCore::QNode* parent)
    : AbstractRigidBodyController(parent)
  {
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  PlaneController::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<PlaneInitialData>::create(this);

    auto& data = creationChange->data;
    fillAbstRBInitialData(data);

    data.m_p = m_p;
    data.m_u = m_u;
    data.m_v = m_v;

    return std::move(creationChange);
  }

}   // namespace rigidbodyaspect
