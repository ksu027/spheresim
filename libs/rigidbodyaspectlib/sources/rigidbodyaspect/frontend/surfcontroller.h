#ifndef RIGIDBODYASPECT_SURFCONTROLLER_H
#define RIGIDBODYASPECT_SURFCONTROLLER_H


#include "abstractrigidbodycontroller.h"

namespace rigidbodyaspect
{

  class SurfController : public AbstractRigidBodyController {
    Q_OBJECT

    Q_PROPERTY(QVector2D scale MEMBER m_scale NOTIFY scaleChanged)

  public:
    SurfController(Qt3DCore::QNode* parent = nullptr);


    QVector2D m_scale{2, 2};

  signals:
    void scaleChanged(const QVector2D& scale);


    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr
    createNodeCreationChange() const override;
  };


  struct SurfInitialData : AbstractRigidBodyInitialData {
    QVector2D m_scale;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_SURFCONTROLLER_H
