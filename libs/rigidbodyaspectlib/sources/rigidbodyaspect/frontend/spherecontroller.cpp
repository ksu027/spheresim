#include "spherecontroller.h"

namespace rigidbodyaspect
{




  SphereController::SphereController(Qt3DCore::QNode* parent)
    : AbstractRigidBodyController(parent)
  {
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  SphereController::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<SphereInitialData>::create(this);

    auto& data = creationChange->data;
    fillAbstRBInitialData(data);
    data.m_radius = m_radius;

    return std::move(creationChange);
  }

}   // namespace rigidbodyaspect
