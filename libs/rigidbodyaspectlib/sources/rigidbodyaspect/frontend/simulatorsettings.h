#ifndef RIGIDBODYASPECT_SIMULATORSETTINGS_H
#define RIGIDBODYASPECT_SIMULATORSETTINGS_H

#include "../types.h"

// blaze
#include <blaze/Math.h>

// qt
#include <QMatrix4x4>
#include <Qt3DCore/QComponent>
#include <Qt3DCore/QTransform>

namespace rigidbodyaspect
{

  class SimulatorSettings : public Qt3DCore::QComponent {
    Q_OBJECT

    Q_PROPERTY(
      bool runStatus READ runStatus WRITE setRunStatus NOTIFY runStatusChanged)

  public:
    SimulatorSettings(Qt3DCore::QNode* parent = nullptr);
    ~SimulatorSettings() override = default;

    bool runStatus() const;
    void setRunStatus(bool run_status);

  private:
    bool m_run_status;

  signals:
    void runStatusChanged(bool status);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr
    createNodeCreationChange() const override;
  };

  struct SimulatorSettingsInitialData {
    bool m_run_status;
  };

}   // namespace rigidbodyaspect




#endif   // RIGIDBODYASPECT_SIMULATORSETTINGS_H
