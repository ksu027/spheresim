#include "surfcontroller.h"

namespace rigidbodyaspect
{




  SurfController::SurfController(Qt3DCore::QNode* parent)
    : AbstractRigidBodyController(parent)
  {
  }

  Qt3DCore::QNodeCreatedChangeBasePtr
  SurfController::createNodeCreationChange() const
  {
    auto creationChange
      = Qt3DCore::QNodeCreatedChangePtr<SurfInitialData>::create(this);

    auto& data = creationChange->data;
    fillAbstRBInitialData(data);

    data.m_scale = m_scale;

    return std::move(creationChange);
  }

}   // namespace rigidbodyaspect
