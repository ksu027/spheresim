#ifndef RIGIDBODYASPECT_SPHERECONTROLLER_H
#define RIGIDBODYASPECT_SPHERECONTROLLER_H


#include "abstractrigidbodycontroller.h"

namespace rigidbodyaspect
{

  class SphereController : public AbstractRigidBodyController {
    Q_OBJECT

    Q_PROPERTY(float radius MEMBER m_radius NOTIFY radiusChanged)

  public:
    SphereController(Qt3DCore::QNode* parent = nullptr);


    float m_radius{1.0f};

  signals:
    void radiusChanged(float radius);

    // QNode interface
  private:
    Qt3DCore::QNodeCreatedChangeBasePtr
    createNodeCreationChange() const override;
  };


  struct SphereInitialData : AbstractRigidBodyInitialData {
    float m_radius;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_SPHERECONTROLLER_H
