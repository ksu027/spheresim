#include "abstractrigidbodycontroller.h"


// gmlib2
#include <integration/scenegraph.h>

namespace rigidbodyaspect
{

  AbstractRigidBodyController::AbstractRigidBodyController(
    Qt3DCore::QNode* parent)
    : Qt3DCore::QComponent(parent)
  {
  }

  void AbstractRigidBodyController::sceneChangeEvent(
    const Qt3DCore::QSceneChangePtr& change)
  {
    if (change->type() == Qt3DCore::PropertyUpdated) {
      if (const auto e
          = qSharedPointerCast<Qt3DCore::QPropertyUpdatedChange>(change);
          e->propertyName() == QByteArrayLiteral("dupframe_BE")) {

        const auto dup = e->value().value<QMatrix3x3>();

        const auto was_blocked = blockNotifications(true);
        emit       frameComputed({dup(0, 0), dup(1, 0), dup(2, 0)},
                           {dup(0, 1), dup(1, 1), dup(2, 1)},
                           {dup(0, 2), dup(1, 2), dup(2, 2)});
        blockNotifications(was_blocked);
        return;
      }
    }

    QComponent::sceneChangeEvent(change);
  }

  QVariant AbstractRigidBodyController::constraints() const
  {
    QVariant s;
    s.setValue(m_constraints);
    return s;
  }

  void AbstractRigidBodyController::setConstraints(const QVariant& constraints)
  {
    m_constraints = constraints.value<RigidBodyConstraints>();
    constraintsChanged(m_constraints);
  }

  QMatrix3x3 AbstractRigidBodyController::dupFrame() const
  {
    return m_dup_frame;
  }

  void AbstractRigidBodyController::resetFrameByDup(const QVector3D& dir,
                                                    const QVector3D& up,
                                                    const QVector3D& pos)
  {
    m_dup_frame(0, 0) = dir.x();
    m_dup_frame(1, 0) = dir.y();
    m_dup_frame(2, 0) = dir.z();

    m_dup_frame(0, 1) = up.x();
    m_dup_frame(1, 1) = up.y();
    m_dup_frame(2, 1) = up.z();

    m_dup_frame(0, 2) = pos.x();
    m_dup_frame(1, 2) = pos.y();
    m_dup_frame(2, 2) = pos.z();

    dupFrameChanged(m_dup_frame);
  }

  QVector3D AbstractRigidBodyController::initialVelocity() const
  {
    return m_initial_velocity;
  }

  void
  AbstractRigidBodyController::setInitialVelocity(const QVector3D& velocity)
  {
    m_initial_velocity = velocity;
    emit initialVelocityChanged(m_initial_velocity);
  }

  float AbstractRigidBodyController::initialMass() const
  {
    return m_initial_mass;
  }

  void AbstractRigidBodyController::setInitialMass(const float& mass)
  {
    m_initial_mass = mass;
    emit initialMassChanged(m_initial_mass);
  }

  float AbstractRigidBodyController::initialFriction() const
  {
    return m_initial_friction;
  }

  void AbstractRigidBodyController::setInitialFriction(const float& friction)
  {
    m_initial_friction = friction;
    emit initialFrictionChanged(m_initial_friction);
  }

  Environment* AbstractRigidBodyController::environment() const
  {
    return m_environment;
  }

  void AbstractRigidBodyController::setEnvironment(Environment* environment)
  {
    m_environment = environment;
    emit environmentChanged(m_environment);
  }

  void AbstractRigidBodyController::fillAbstRBInitialData(
    AbstractRigidBodyInitialData& data) const
  {
    data.m_constraints      = m_constraints;
    data.m_dup_frame        = m_dup_frame;
    data.m_initial_velocity = m_initial_velocity;
    data.m_initial_mass     = m_initial_mass;
    data.m_initial_friction = m_initial_friction;
    data.m_environment_id   = Qt3DCore::qIdForNode(m_environment);
  }

}   // namespace rigidbodyaspect
