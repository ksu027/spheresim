#include "movingbody.h"
#include "sphere_fixedplane.h"
#include "Integrators.h"


namespace rigidbodyaspect::algorithms::simulation
{

  void computeMovingBodyTrajectoryCache(MovingBody& body, const GM2Vector& Fc)
  {
    // compute dynamics
    const auto dt = body.m_rem_dt.count();
    const auto a  = Fc;
    const auto ds = (body.m_velocity + 0.5 * a * dt) * dt;

    body.m_a  = a;
    body.m_ds = ds;
  }

  void
  simulateMovingBody(rbtypes::Sphere& body, seconds_type sub_dt,
                     seconds_type                               dt,
                     const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
                     const bool                                 state_check)
  {
    body.m_dt_tp += sub_dt;
    body.m_elapsed_time += sub_dt;

    if (body.m_state != MovingBodyState::AtRest) {
      const auto sub_dt_frac = sub_dt / body.m_rem_dt;
      body.translateParent(body.m_ds * sub_dt_frac);
      const auto dt = body.m_rem_dt.count() * sub_dt_frac;

      // regular logic
      const auto [dv, ds, a]
        = integrator::Exact(body.m_a, body.m_velocity, body.m_mass, dt);

      // in case of Euler:
      //      const auto [dv,ds,a] =
      //      integrator::Euler(body.m_a,body.m_velocity,body.m_mass,dt);

      // in case of Improved Euler:
      //      const auto [dv,ds,a] =
      //      integrator::ImprovedEuler(body.m_a,body.m_velocity,body.m_mass,dt);

      // in case of Runge-Kutta:
      //      const auto [dv,ds,a] =
      //      integrator::RungeKutta(body.m_a,body.m_velocity,body.m_mass,dt);

      body.m_velocity = body.m_velocity + dv;
      body.m_angular_velocity += (body.m_aa * dt);

      body.m_pos += body.m_ds * sub_dt_frac;

      // visualize rotation
      const auto& angular_speed
        = blaze::length(body.m_angular_velocity) * body.m_radius;
      if (angular_speed > constants::Zero)
        body.rotateParent(angular_speed * sub_dt.count(),
                          body.m_angular_velocity);
    }
  }


}   // namespace rigidbodyaspect::algorithms::simulation
