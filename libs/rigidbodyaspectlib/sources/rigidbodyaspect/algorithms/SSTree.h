#ifndef RIGIDBODYASPECT_ALGORITHMS_SSTREE_H
#define RIGIDBODYASPECT_ALGORITHMS_SSTREE_H

#include "../types.h"
#include "../geometry/rbtypes.h"
#include "../geometry/rigidbodycontainer.h"
#include "../utils.h"
#include "helpers.h"
#include "../SurroundSphere.h"
#include "sphere_fixedplane.h"

// stl
#include <variant>

namespace rigidbodyaspect::algorithms
{
  namespace contact
  {
    using PossibleContact
      = std::pair<std::pair<Qt3DCore::QNodeId, collision::types::ColType>,
                  std::pair<Qt3DCore::QNodeId, collision::types::ColType>>;

    // BoundryVolumeHierarchy
    // Based on the example from Millington, but with the improved errors and
    // optimization to our logic
    class TreeNode {
    public:
      TreeNode()
        : children({nullptr, nullptr}), sphere(integration::SurroundSphere()),
          node_id(Qt3DCore::QNodeId()), parent(nullptr), moving_body(false)
      {
      }
      TreeNode(TreeNode* in_parent, const integration::SurroundSphere in_sphere,
               Qt3DCore::QNodeId         in_node_id     = Qt3DCore::QNodeId(),
               const bool                in_moving_body = false,
               collision::types::ColType object_type
               = collision::types::ColType::Pl)
        : children({nullptr, nullptr}), sphere(in_sphere), node_id(in_node_id),
          parent(in_parent), moving_body(in_moving_body), node_type(object_type)
      {
      }
      bool isLeaf() const { return node_id != 0; }
      void getPossibleContacts(std::vector<PossibleContact>& contacts) const;
      TreeNode*                       insert(Qt3DCore::QNodeId                  new_node_id,
                                             const integration::SurroundSphere& new_sphere,
                                             const bool                         moving,
                                             collision::types::ColType          object_type);
      std::pair<TreeNode*, TreeNode*> children;
      integration::SurroundSphere     sphere;
      Qt3DCore::QNodeId               node_id;
      TreeNode*                       parent;
      bool                            moving_body;
      collision::types::ColType       node_type;
      ~TreeNode();
      void
           getPossibleContactsWith(const TreeNode*               other,
                                   std::vector<PossibleContact>& contacts) const;
      void printTree(int level = 0);
      void recalculateSurroundSphere(bool recurse = true);

    protected:
      bool overlaps(const TreeNode* other) const;
    };

    struct SpheresTree {
      SpheresTree() {}
      SpheresTree(RigidBodyContainer& all_bodies);
      QHash<Qt3DCore::QNodeId, TreeNode*> leafs;
      TreeNode*                           root = nullptr;
      void                                insert(Qt3DCore::QNodeId                  new_node_id,
                                                 const integration::SurroundSphere& new_sphere,
                                                 const bool moving, collision::types::ColType object_type);
      bool                                multy_spheres = false;
      std::vector<PossibleContact>        getContactsRoot() const;
      std::vector<PossibleContact>        getContactsOneByOne() const;
      void removeLeaf(Qt3DCore::QNodeId node_id_remove);
      ~SpheresTree();
    };


    void predictCollisions(SpheresTree&                  tree,
                           std::vector<PossibleContact>& possible_collisions,
                           bool                          from_root = true);
    void detectCollisionsBase(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                    collision_sets,
      std::vector<PossibleContact>& possible_collisions,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);
    void detectCollisionsTree(
      SpheresTree& tree,
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                collision_sets,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);
    void detectCollisionsTreeInner(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                     collision_sets,
      const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids, SpheresTree& current_tree,
      RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);
    void detectCollisionsBruteSphere(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                collision_sets,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);
    void detectCollisionsBruteSphereInner(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                     collision_sets,
      const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);

  }   // namespace contact

}   // namespace rigidbodyaspect::algorithms


#endif   // RIGIDBODYASPECT_ALGORITHMS_SSTREE_H
