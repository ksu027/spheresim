#include "sphere_sphere.h"
#include "../backend/rigidbodyaspect.h"

// stl
#include <iostream>
#include <chrono>
using namespace std::chrono_literals;



namespace rigidbodyaspect::algorithms
{

  namespace collision
  {
    detail::SphereFixedPlaneColliderReturnType
    detectCollisionSp(const rbtypes::Sphere&        sphere_active,
                      const rbtypes::Sphere&        sphere_passive,
                      [[maybe_unused]] seconds_type dt)
    {
      const auto dt_rem_act = dt - sphere_active.m_dt_tp;
      const auto dt_rem_pas = dt - sphere_passive.m_dt_tp;
      const auto dt_rem_min = std::min(dt_rem_act, dt_rem_pas);

      const auto go_after_ratio_act = dt_rem_min / dt_rem_act;
      const auto go_after_ratio_pas = dt_rem_min / dt_rem_pas;

      const auto go_before_ratio_act = (dt_rem_act - dt_rem_min) / dt_rem_act;
      const auto go_before_ratio_pas = (dt_rem_pas - dt_rem_min) / dt_rem_pas;

      // const auto dt_rem = dt - sphere_active.m_dt_tp;
      if (dt_rem_min.count() <= 0)
        return {0ms, detail::SphereFixedPlaneCollisionStatus::NoCollision};

      const GM2Vector p_act
        = sphere_active.m_pos + sphere_active.m_ds * go_before_ratio_act;
      const auto      r_act = sphere_active.m_radius;
      const GM2Vector p_pas
        = sphere_passive.m_pos + sphere_passive.m_ds * go_before_ratio_pas;
      const auto r_pas = sphere_passive.m_radius;
      const auto r     = r_act + r_pas;

      const GM2Vector ds_act = sphere_active.m_ds * go_after_ratio_act;
      const GM2Vector ds_pas = sphere_passive.m_ds * go_after_ratio_pas;

      const auto Q = p_act - p_pas;
      const auto R = ds_act - ds_pas;

      const auto QR = blaze::inner(Q, R);
      const auto RR = blaze::inner(R, R);
      const auto QQ = blaze::inner(Q, Q);

      const auto root = pow(QR, 2) - RR * (QQ - pow(r, 2));

      if (root < 0.0)
        return {0ms, detail::SphereFixedPlaneCollisionStatus::NoCollision};

      const auto x = (-QR - sqrt(root)) / RR;

      if (x > 0.0 and x <= 1.0) {
        return {sphere_active.m_dt_tp + (x * dt_rem_act),
                detail::SphereFixedPlaneCollisionStatus::Collision};
      }

      return {0ms, detail::SphereFixedPlaneCollisionStatus::NoCollision};
    }
  }   // namespace collision

  namespace statechange
  {
    detail::StateChangeStatus
    detectSpSpPenetration(const rbtypes::Sphere& sphere1,
                          const rbtypes::Sphere& sphere2)
    {
      const auto& end_point_1 = sphere1.m_pos + sphere1.m_ds;
      const auto& end_point_2 = sphere2.m_pos + sphere2.m_ds;

      const auto distance = blaze::length(end_point_1 - end_point_2);

      if (distance < sphere1.m_radius + sphere2.m_radius)
        return detail::StateChangeStatus::ToSliding;
      return detail::StateChangeStatus::ToFree;
    };


  }   // namespace statechange


  namespace simulation
  {

    std::pair<GM2Vector, GM2Vector> resolveVelocitiesSpSp(
      const std::pair<GM2Vector, GM2Vector>& nd,
      const GM2Vector& velocity_active, const GM2Vector& velocity_passive,
      const rbtypes::Sphere& act_sphere, const rbtypes::Sphere& pas_sphere)
    {
      auto [n, d] = nd;

      const auto v0 = act_sphere.m_velocity;
      const auto v1 = pas_sphere.m_velocity;

      const auto v0d  = blaze::inner(v0, d);
      const auto v1d  = blaze::inner(v1, d);
      const auto v0nn = v0 - v0d * d;
      const auto v1nn = v1 - v1d * d;

      const auto m0 = act_sphere.m_mass;
      const auto m1 = pas_sphere.m_mass;

      const auto v0d_new
        = (m0 - m1) / (m0 + m1) * v0d + 2 * m1 * v1d / (m0 + m1);
      const auto v1d_new
        = (m1 - m0) / (m0 + m1) * v1d + 2 * m0 * v0d / (m0 + m1);


      const auto new_act_vel = v0nn + v0d_new * d;
      const auto new_pas_vel = v1nn + v1d_new * d;

      return {new_act_vel, new_pas_vel};
    };
  }   // namespace simulation

}   // namespace rigidbodyaspect::algorithms
