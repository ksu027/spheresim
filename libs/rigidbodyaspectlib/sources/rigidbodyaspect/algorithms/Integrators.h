#ifndef RIGIDBODYASPECT_ALGORITHMS_INTEGRATORS_H
#define RIGIDBODYASPECT_ALGORITHMS_INTEGRATORS_H

#include "../types.h"

// stl
#include <variant>

namespace rigidbodyaspect::algorithms::integrator
{
  std::tuple<GM2Vector, GM2Vector, GM2Vector> Exact(const GM2Vector& Fc,
                                                    const GM2Vector& v,
                                                    const double&    m,
                                                    const double&    dt);
  std::tuple<GM2Vector, GM2Vector, GM2Vector> Euler(const GM2Vector& Fc,
                                                    const GM2Vector& v,
                                                    const double&    m,
                                                    const double&    dt);
  std::tuple<GM2Vector, GM2Vector, GM2Vector> ImprovedEuler(const GM2Vector& Fc,
                                                            const GM2Vector& v,
                                                            const double&    m,
                                                            const double& dt);
  std::tuple<GM2Vector, GM2Vector, GM2Vector> RungeKutta(const GM2Vector& Fc,
                                                         const GM2Vector& v,
                                                         const double&    m,
                                                         const double&    dt);
  GM2Vector variativeForce(const GM2Vector& v);


}   // namespace rigidbodyaspect::algorithms::integrator


#endif   // RIGIDBODYASPECT_ALGORITHMS_INTEGRATORS_H
