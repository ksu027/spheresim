#ifndef RIGIDBODYASPECT_ALGORITHMS_SPHERE_FIXEDPLANE_H
#define RIGIDBODYASPECT_ALGORITHMS_SPHERE_FIXEDPLANE_H

#include "../types.h"
#include "../constants.h"
#include "../geometry/rbtypes.h"
#include "../geometry/rigidbodycontainer.h"
#include "../utils.h"
#include "helpers.h"

// stl
#include <variant>

namespace rigidbodyaspect::algorithms
{
  namespace collision
  {
    detail::SphereFixedPlaneColliderReturnType
    detectCollision(const rbtypes::Sphere&     sphere,
                    const rbtypes::FixedPlane& plane, seconds_type dt);
  }   // namespace collision


  namespace statechange
  {
    detail::StateChangeStatus detectStateChange(const rbtypes::Sphere& sphere,
                                                const GM2Vector&       n);

    void doStateCgange(rbtypes::Sphere&           sphere,
                       const rbtypes::FixedPlane& plane,
                       const Qt3DCore::QNodeId&   plane_id);


    void
    stateChangeForces(const Qt3DCore::QNodeId&                   sphere_id,
                      const Qt3DCore::QNodeId&                   plane_id,
                      RigidBodyContainer::SphereQHash&           all_spheres,
                      const RigidBodyContainer::FixedPlaneQHash& all_planes,
                      const RigidBodyContainer::SurfQHash&       all_surfs,
                      ContactNTtype& contact_normals);

  }   // namespace statechange


  namespace simulation
  {
    void resolveImpactResponse(
      RigidBodyContainer::SphereQHash&                        all_spheres,
      std::vector<contact::ContactPoint>&                     collision_points,
      QHash<Qt3DCore::QNodeId, QSet<contact::ContactPoint*>>& cps_of_node_id);

    std::pair<double, GM2Vector>
                                    getImpulseScalar(RigidBodyContainer::SphereQHash& all_spheres,
                                                     contact::ContactPoint& cp, const GM2Vector& n);
    std::pair<GM2Vector, GM2Vector> getDvDw(rbtypes::Sphere& sphere,
                                            const GM2Vector  n,
                                            const double     impulse_scalar,
                                            const GM2Vector  u);

    std::pair<GM2Vector, GM2Vector> getDvDwBourg(rbtypes::Sphere& sphere,
                                                 const GM2Vector  n,
                                                 const double    impulse_scalar,
                                                 const GM2Vector u);

    std::pair<GM2Vector, GM2Vector>
    getDvDwMillington(rbtypes::Sphere& sphere, const GM2Vector n,
                      const double impulse_scalar, const GM2Vector u);
  }   // namespace simulation


  namespace singularities
  {

    namespace types
    {

      using SpFPlSphereIdPair
        = std::pair<const rbtypes::Sphere&, Qt3DCore::QNodeId>;
      using SpFPlFixedPlanesIdPair
        = std::pair<const RigidBodyContainer::FixedPlaneQHash&,
                    Qt3DCore::QNodeId>;
      using PlanesAllPlanesAttPair
        = std::pair<const RigidBodyContainer::FixedPlaneQHash&,
                    QSet<Qt3DCore::QNodeId>>;
      using SpheresAllSpheresAttPair
        = std::pair<const RigidBodyContainer::SphereQHash&,
                    QSet<Qt3DCore::QNodeId>>;
    }   // namespace types

    void detectInitialSingularities(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                                 collision_sets,
      types::SpFPlSphereIdPair                   sphere_pair,
      const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
      const RigidBodyContainer::SphereQHash&     spheres,
      const RigidBodyContainer::SurfQHash&       surfs,
      ContactNTtype& contact_normals, seconds_type dt);

    void detectCollisionsBruteForce(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                          collision_sets,
      RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);

    void cleanUpCollisions(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                             collision_sets,
      const RigidBodyContainer::SphereQHash& all_spheres);

    void detectCollisionsBruteForceInner(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                     collision_sets,
      const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids,
      const QSet<Qt3DCore::QNodeId>& curr_col_pl_ids,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);

    void objToColTouchObjs(const seconds_type       collision_time,
                           seconds_type&            min_time,
                           const Qt3DCore::QNodeId  obj_id,
                           QSet<Qt3DCore::QNodeId>& touch_objects);

  }   // namespace singularities

  namespace singularities_simple
  {
    using collisionType = std::vector<
      std::tuple<seconds_type, Qt3DCore::QNodeId, Qt3DCore::QNodeId>>;

    void detectInitialSingularities(
      collisionType&                             collisions,
      singularities::types::SpFPlSphereIdPair    sphere_pair,
      const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
      const RigidBodyContainer::SphereQHash&     spheres,
      const RigidBodyContainer::SurfQHash&       surfs,
      ContactNTtype& contact_normals, seconds_type dt);

    void detectCollisionsBruteForce(collisionType&      collisions,
                                    RigidBodyContainer& all_bodies,
                                    ContactNTtype&      contact_normals,
                                    seconds_type        dt);

    void detectCollisionsBruteForceInner(
      collisionType& collisions, const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);

    void detectCollisionsBruteSphereS(collisionType&      collisions,
                                      RigidBodyContainer& all_bodies,
                                      ContactNTtype&      contact_normals,
                                      seconds_type        dt);

    void detectCollisionsBruteSphereSInner(
      collisionType& collisions, const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids,
      RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt);

    void resolveImpactResponse(RigidBodyContainer&  all_bodies,
                               Qt3DCore::QNodeId    sphere_id,
                               Qt3DCore::QNodeId    object_id,
                               const ContactNTtype& contact_normals);
  }   // namespace singularities_simple

}   // namespace rigidbodyaspect::algorithms


#endif   // RIGIDBODYASPECT_ALGORITHMS_SPHERE_FIXEDPLANE_H
