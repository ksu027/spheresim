#ifndef RIGIDBODYASPECT_ALGORITHMS_SPHERE_SPHERE_H
#define RIGIDBODYASPECT_ALGORITHMS_SPHERE_SPHERE_H

#include "../types.h"
#include "../geometry/rbtypes.h"
#include "../geometry/rigidbodycontainer.h"
#include "../utils.h"
#include "helpers.h"

// stl
#include <variant>

namespace rigidbodyaspect::algorithms
{

  namespace collision
  {
    detail::SphereFixedPlaneColliderReturnType
    detectCollisionSp(const rbtypes::Sphere& sphere_active,
                      const rbtypes::Sphere& sphere_passive, seconds_type dt);
  }   // namespace collision


  namespace statechange
  {
    detail::StateChangeStatus
    detectSpSpPenetration(const rbtypes::Sphere& sphere1,
                          const rbtypes::Sphere& sphere2);
  }   // namespace statechange


  namespace simulation
  {

    std::pair<GM2Vector, GM2Vector> resolveVelocitiesSpSp(
      const std::pair<GM2Vector, GM2Vector>& nd,
      const GM2Vector& velocity_active, const GM2Vector& velocity_passive,
      const rbtypes::Sphere& act_sphere, const rbtypes::Sphere& pas_sphere);

  }   // namespace simulation


  namespace singularities
  {



  }   // namespace singularities

}   // namespace rigidbodyaspect::algorithms


#endif   // RIGIDBODYASPECT_ALGORITHMS_SPHERE_FIXEDPLANE_H
