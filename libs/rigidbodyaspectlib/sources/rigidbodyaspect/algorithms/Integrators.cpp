#include "Integrators.h"

// stl
#include <iostream>

using namespace std::chrono_literals;



namespace rigidbodyaspect::algorithms::integrator
{
  std::tuple<GM2Vector, GM2Vector, GM2Vector> Exact(const GM2Vector& Fc,
                                                    const GM2Vector& v,
                                                    const double&    m,
                                                    const double&    dt)
  // these are the exact kinematic formulas. Used with the constant acceleration
  {
    const GM2Vector a  = Fc / m;
    GM2Vector       dv = a * dt;
    GM2Vector       ds = (v + dv / 2.0) * dt;
    return {dv, ds, a};
  };

  std::tuple<GM2Vector, GM2Vector, GM2Vector> Euler(const GM2Vector& Fc,
                                                    const GM2Vector& v,
                                                    const double&    m,
                                                    const double&    dt)
  {
    GM2Vector  F  = Fc - variativeForce(v);
    const auto k1 = dt * F / m;
    GM2Vector  dv = k1;
    GM2Vector  ds = (v)*dt;
    return {dv, ds, Fc};
  };
  std::tuple<GM2Vector, GM2Vector, GM2Vector> ImprovedEuler(const GM2Vector& Fc,
                                                            const GM2Vector& v,
                                                            const double&    m,
                                                            const double&    dt)
  {
    GM2Vector  F  = Fc - variativeForce(v);
    const auto k1 = dt * F / m;
    const auto l1 = dt * v;
    F             = Fc - variativeForce(v + k1);
    const auto k2 = dt * F / m;
    const auto l2 = dt * (v + k1);

    GM2Vector dv = (k1 + k2) / 2.0;
    GM2Vector ds = (l1 + l2) / 2.0;

    return {dv, ds, Fc};
  };
  std::tuple<GM2Vector, GM2Vector, GM2Vector> RungeKutta(const GM2Vector& Fc,
                                                         const GM2Vector& v,
                                                         const double&    m,
                                                         const double&    dt)
  {
    GM2Vector  F  = Fc - variativeForce(v);
    const auto k1 = dt * F / m;
    const auto l1 = dt * v;
    F             = Fc - variativeForce(v + k1 / 2.0);
    const auto k2 = dt * F / m;
    const auto l2 = dt * (v + k1 / 2.0);
    F             = Fc - variativeForce(v + k2 / 2.0);
    const auto k3 = dt * F / m;
    const auto l3 = dt * (v + k2 / 2.0);
    F             = Fc - variativeForce(v + k3);
    const auto k4 = dt * F / m;
    const auto l4 = dt * (v + k3);
    GM2Vector  dv = (k1 + 2.0 * k2 + 2.0 * k3 + k4) / 6.0;
    GM2Vector  ds = (l1 + 2.0 * l2 + 2.0 * l3 + l4) / 6.0;

    return {dv, ds, Fc};
  };
  GM2Vector variativeForce(const GM2Vector& v)
  {
    // this is a force dependent on velocity
    // we will use drag simulation for the integration testing
    const auto k_drag = 0.1;
    return k_drag * v;
  };

}   // namespace rigidbodyaspect::algorithms::integrator
