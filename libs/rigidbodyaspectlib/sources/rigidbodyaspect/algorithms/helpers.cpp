#include "helpers.h"

#include "../algorithms/movingsphere.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"


namespace rigidbodyaspect::algorithms::helpers
{
  // property cache compute driver
  void computeCacheProps(
    const Qt3DCore::QNodeId&                   sphere_id,
    const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
    RigidBodyContainer::SphereQHash& spheres, const seconds_type& dt,
    rigidbodyaspect::RigidBodyAspect& in_aspect, bool adjust, bool revert)
  {
    auto       sphere  = spheres[sphere_id];
    const auto env_id  = sphere.m_env_id;
    const auto env_BE  = in_aspect.environmentBackend(env_id);
    const auto Gravity = (env_BE ? env_BE->m_gravity : GM2Vector{0, 20.0, 0});
    GM2Vector  env_Forces = Gravity * sphere.m_mass;

    algorithms::movingsphere::computeCacheProperties(
      sphere_id, fixed_planes, spheres, env_Forces, dt, adjust, revert);
  };


  GM2Vector findClosePointOnPlane(const rbtypes::FixedPlane& plane,
                                  const GM2Vector&           point)
  {
    auto      u = 0.0, v = 0.0, du = 1.0, dv = 1.0;
    GM2Vector q_betta;

    while (du > 1e-7 or dv > 1e-7) {

      const auto pl_eval
        = plane.evaluateLocal(rbtypes::FixedPlane::PSpacePoint{u, v},
                              rbtypes::FixedPlane::PSpaceSizeArray{1UL, 1UL});
      const auto Su = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
      const auto Sv = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));

      const auto q_alpha = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));

      auto SuSu = blaze::inner(Su, Su);
      auto SuSv = blaze::inner(Su, Sv);
      auto SvSv = blaze::inner(Sv, Sv);

      blaze::DynamicMatrix<double> A(2UL, 2UL);
      A = {{SuSu, SuSv}, {SuSv, SvSv}};

      const auto p = point;
      const auto d = q_alpha - p;

      auto dSu = blaze::inner(d, Su);
      auto dSv = blaze::inner(d, Sv);

      blaze::DynamicVector<double, blaze::columnVector> b{-dSu, -dSv};


      auto                                              A_inv = blaze::inv(A);
      blaze::DynamicVector<double, blaze::columnVector> x     = A_inv * b;

      du = x[0];
      dv = x[1];

      u += du;
      v += dv;

      q_betta = q_alpha;
    };

    return q_betta;
  };

  GM2Vector nToPlane(const rbtypes::FixedPlane& plane)
  {
    const auto pl_eval
      = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                             rbtypes::FixedPlane::PSpaceSizeArray{1UL, 1UL});
    const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
    const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
    return blaze::normalize(blaze::cross(pl_u, pl_v));
  }

  std::pair<GM2Vector, GM2Vector>
  findNDSpheres(const rbtypes::Sphere& act_sphere,
                const rbtypes::Sphere& pas_sphere)
  {
    // here n - norm to the distance between the centers of the spheres
    // d - normalized vector from centers of sphere 1 to center of sphere 2
    const auto p0 = act_sphere.m_pos;
    const auto p1 = pas_sphere.m_pos;
    const auto d  = blaze::normalize(p1 - p0);
    const auto liv_d
      = gmlib2::algorithms::linearIndependentVector(GM2Vector(d));
    const auto n
      = blaze::cross(liv_d, d) / blaze::length((blaze::cross(liv_d, d)));
    return {n, d};
  }

  blaze::StaticMatrix<double, 3, 3> contactSpace(const GM2Vector& n)
  {
    // first - columns; second - rows
    // linearly independent vector
    const auto vector2
      = gmlib2::algorithms::linearIndependentVector(GM2Vector(n));
    const auto vector3
      = blaze::cross(vector2, n) / blaze::length((blaze::cross(vector2, n)));
    blaze::StaticMatrix<double, 3, 3> return_matrix;
    return_matrix.at(0, 0) = n[0];
    return_matrix.at(0, 1) = n[1];
    return_matrix.at(0, 2) = n[2];
    return_matrix.at(1, 0) = vector2[0];
    return_matrix.at(1, 1) = vector2[1];
    return_matrix.at(1, 2) = vector2[2];
    return_matrix.at(2, 0) = vector3[0];
    return_matrix.at(2, 1) = vector3[1];
    return_matrix.at(2, 2) = vector3[2];

    return return_matrix;
  };

  bool isPlane(Qt3DCore::QNodeId                 in_id,
               rigidbodyaspect::RigidBodyAspect& in_aspect)
  {

    return in_aspect.rigidBodies().fixedPlanes().contains(in_id);
  }

  bool isSphere(Qt3DCore::QNodeId                 in_id,
                rigidbodyaspect::RigidBodyAspect& in_aspect)
  {
    return in_aspect.rigidBodies().spheres().contains(in_id);
  }

  bool isSurf(Qt3DCore::QNodeId                 in_id,
              rigidbodyaspect::RigidBodyAspect& in_aspect)
  {
    return in_aspect.rigidBodies().surfs().contains(in_id);
  }

  QSet<Qt3DCore::QNodeId> getPlanes(const QSet<Qt3DCore::QNodeId>&    ids,
                                    rigidbodyaspect::RigidBodyAspect& in_aspect)
  {
    return ids & in_aspect.rigidBodies().fixedPlanes().keys().toSet();
  };
  QSet<Qt3DCore::QNodeId>
  getSpheres(const QSet<Qt3DCore::QNodeId>&    ids,
             rigidbodyaspect::RigidBodyAspect& in_aspect)
  {
    return ids & in_aspect.rigidBodies().spheres().keys().toSet();
  };
  QSet<Qt3DCore::QNodeId> getSurfs(const QSet<Qt3DCore::QNodeId>&    ids,
                                   rigidbodyaspect::RigidBodyAspect& in_aspect)
  {
    return ids & in_aspect.rigidBodies().surfs().keys().toSet();
  };


  std::pair<GM2Vector, GM2Vector> closestPointSurf(const GM2Vector&     point,
                                                   const rbtypes::Surf& surf)
  {
    auto      u = 0.0, v = 0.0, du = 1.0, dv = 1.0;
    GM2Vector q_betta, n;

    while (std::abs(du) > constants::Zero or std::abs(dv) > constants::Zero) {

      const auto surf_eval_d1
        = surf.evaluateParent(rbtypes::FixedPlane::PSpacePoint{u, v},
                              rbtypes::FixedPlane::PSpaceSizeArray{1UL, 1UL});
      const auto surf_eval_d2
        = surf.evaluateParent(rbtypes::FixedPlane::PSpacePoint{u, v},
                              rbtypes::FixedPlane::PSpaceSizeArray{2UL, 2UL});

      const auto q_alpha = blaze::subvector<0UL, 3UL>(surf_eval_d1(0UL, 0UL));
      const auto Su      = blaze::subvector<0UL, 3UL>(surf_eval_d1(1UL, 0UL));
      const auto Sv      = blaze::subvector<0UL, 3UL>(surf_eval_d1(0UL, 1UL));
      const auto Suv     = blaze::subvector<0UL, 3UL>(surf_eval_d1(1UL, 1UL));

      const auto Suu = blaze::subvector<0UL, 3UL>(surf_eval_d2(1UL, 0UL));
      const auto Svv = blaze::subvector<0UL, 3UL>(surf_eval_d2(0UL, 1UL));

      n = blaze::normalize(blaze::cross(Su, Sv));

      auto SuSu = blaze::inner(Su, Su);
      auto SuSv = blaze::inner(Su, Sv);
      auto SvSv = blaze::inner(Sv, Sv);

      const auto p = point;
      const auto d = q_alpha - p;

      auto dSu = blaze::inner(d, Su);
      auto dSv = blaze::inner(d, Sv);

      auto dSuu = blaze::inner(d, Suu);
      auto dSuv = blaze::inner(d, Suv);
      auto dSvv = blaze::inner(d, Svv);

      blaze::DynamicMatrix<double> A(2UL, 2UL);
      A = {{dSuu + SuSu, dSuv + SuSv}, {dSuv + SuSv, dSvv + SvSv}};
      blaze::DynamicVector<double, blaze::columnVector> b{-dSu, -dSv};

      auto                                              A_inv = blaze::inv(A);
      blaze::DynamicVector<double, blaze::columnVector> x     = A_inv * b;

      du = x[0];
      dv = x[1];

      u += du;
      v += dv;

      q_betta = q_alpha;
    };

    return {q_betta, n};
  }

  std::tuple<GM2Vector, GM2Vector, double>
  intersectDsSurf(const rbtypes::Sphere& sphere, const GM2Vector& ds,
                  const rbtypes::Surf& surf)
  {
    const GM2Vector p = sphere.m_pos;
    auto            r = sphere.m_radius;
    auto            u = 0.0, v = 0.0, t = 0.0, du = 1.0, dv = 1.0, dt = 1.0;
    GM2Vector       q_betta;
    GM2Vector       Sn;

    auto iter = 0;
    while (((std::abs(du) > constants::Zero) or (std::abs(dv) > constants::Zero)
            or (std::abs(dt) > constants::Zero))
           and iter < 50) {
      iter++;
      const auto surf_eval_d1
        = surf.evaluateParent(rbtypes::FixedPlane::PSpacePoint{u, v},
                              rbtypes::FixedPlane::PSpaceSizeArray{1UL, 1UL});

      const auto q         = blaze::subvector<0UL, 3UL>(surf_eval_d1(0UL, 0UL));
      const auto Su        = blaze::subvector<0UL, 3UL>(surf_eval_d1(1UL, 0UL));
      const auto Sv        = blaze::subvector<0UL, 3UL>(surf_eval_d1(0UL, 1UL));
      const auto SuCrossSv = blaze::cross(Su, Sv);
      Sn                   = blaze::normalize(SuCrossSv);

      auto p_alpha  = p + ds * t;
      auto b_vector = p_alpha - q - Sn * r;

      blaze::DynamicMatrix<double> A(3UL, 3UL);
      A = {
        {Su[0], Sv[0], -ds[0]}, {Su[1], Sv[1], -ds[1]}, {Su[2], Sv[2], -ds[2]}};
      blaze::DynamicVector<double, blaze::columnVector> b{b_vector};

      auto                                              A_inv = blaze::inv(A);
      blaze::DynamicVector<double, blaze::columnVector> x     = A_inv * b;

      du = x[0];
      dv = x[1];
      dt = x[2];

      u += du;
      v += dv;
      t += dt;

      q_betta = q;
    };

    if (iter == 50) t = 50;
    return {q_betta, Sn, t};
  }
}   // namespace rigidbodyaspect::algorithms::helpers
