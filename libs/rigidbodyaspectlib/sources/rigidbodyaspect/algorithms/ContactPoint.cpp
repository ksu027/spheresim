#include "ContactPoint.h"
#include "../backend/rigidbodyaspect.h"

// stl
#include <iostream>
#include <chrono>
using namespace std::chrono_literals;



namespace rigidbodyaspect::algorithms
{

  namespace contact
  {
    QHash<Qt3DCore::QNodeId, QSet<contact::ContactPoint*>>
    getCPsOfNodeID(std::vector<ContactPoint>& collision_points)
    {
      // create a QHash, sphere_id to set of references to c_ps
      QHash<Qt3DCore::QNodeId, QSet<contact::ContactPoint*>> cps_of_node_id;
      for (auto& cp : collision_points) {
        if (!cps_of_node_id.contains(cp.contact_objects.first))
          cps_of_node_id.insert(cp.contact_objects.first, {});
        cps_of_node_id[cp.contact_objects.first].insert(&cp);
        if (cp.contact_type == algorithms::collision::types::ColType::Sp) {
          if (!cps_of_node_id.contains(cp.contact_objects.second))
            cps_of_node_id.insert(cp.contact_objects.second, {});
          cps_of_node_id[cp.contact_objects.second].insert(&cp);
        }
      }
      return cps_of_node_id;
    };

    std::vector<ContactPoint>
    getVectorOfCPs(const RigidBodyContainer::SphereQHash&     all_spheres,
                   const RigidBodyContainer::FixedPlaneQHash& all_planes,
                   const RigidBodyContainer::SurfQHash&       all_surfs,
                   const QSet<Qt3DCore::QNodeId>&             curr_col_sp_ids,
                   const QSet<Qt3DCore::QNodeId>&             curr_col_pl_ids,
                   const QSet<Qt3DCore::QNodeId>&             curr_col_SU_ids,
                   const ContactNTtype&                       contact_normals)
    {
      std::vector<ContactPoint> contact_points;
      QSet<std::pair<Qt3DCore::QNodeId, Qt3DCore::QNodeId>>
                              set_of_contact_pairs;
      QSet<Qt3DCore::QNodeId> spheres_to_check = curr_col_sp_ids;
      QSet<Qt3DCore::QNodeId> colliding_objects
        = curr_col_sp_ids + curr_col_pl_ids + curr_col_SU_ids;
      QSet<Qt3DCore::QNodeId> spheres_checked;

      // first add all contact points of current sphere and colliding objects
      //    in_sphere_id                     - id of the current sphere
      //    all_spheres                      - set of all spheres
      //    all_planes                       - set of all planes
      //    curr_col_sp_ids+curr_col_pl_ids  - set of objects to check. here
      //    spheres and planes of collision object set_of_contact_pairs -
      //    changing set of contact pairs spheres_to_check                 -
      //    changing set of spheres to check spheres_checked                  -
      //    set of checked spheres contact_points)                  - changing
      //    resulting vector
      // now for all spheres add contact points
      while (spheres_to_check.size() > 0) {
        const auto& id_sp_check = *spheres_to_check.begin();
        addCP(id_sp_check, all_spheres, all_planes, all_surfs,
              all_spheres[id_sp_check].m_attached_objects + colliding_objects,
              set_of_contact_pairs, spheres_to_check, spheres_checked,
              contact_points, contact_normals);
        spheres_to_check.remove(id_sp_check);
        spheres_checked.insert(id_sp_check);
      }
      std::sort(contact_points.begin(), contact_points.end());
      return contact_points;
    };

    void addCP(const Qt3DCore::QNodeId&                   in_sphere_id,
               const RigidBodyContainer::SphereQHash&     all_spheres,
               const RigidBodyContainer::FixedPlaneQHash& all_planes,
               const RigidBodyContainer::SurfQHash&       all_surfs,
               const QSet<Qt3DCore::QNodeId>&             objects_to_check,
               QSet<std::pair<Qt3DCore::QNodeId, Qt3DCore::QNodeId>>&
                                              set_of_contact_pairs,
               QSet<Qt3DCore::QNodeId>&       spheres_to_check,
               const QSet<Qt3DCore::QNodeId>& spheres_checked,
               std::vector<ContactPoint>&     contact_points,
               const ContactNTtype&           contact_normals)
    {
      // TO DO: change the logic here.
      //        add normals for sp-sp and sp-pl in collision detection,
      //        similarly to sp-surf here check if the pair of object has normal
      //        - then these objects in touch - hence add their contact point
      auto& a_sph = all_spheres[in_sphere_id];
      for (auto& ob_id : objects_to_check) {
        // check that this is not the same object
        if (in_sphere_id != ob_id) {
          const std::pair<Qt3DCore::QNodeId, Qt3DCore::QNodeId> c_pair
            = std::make_pair(in_sphere_id, ob_id);
          if (all_planes.contains(ob_id)) {
            const auto c_type    = algorithms::collision::types::ColType::Pl;
            const auto c_n       = helpers::nToPlane(all_planes[ob_id]);
            const auto c_v_delta = blaze::inner(a_sph.m_velocity, c_n);
            contact_points.push_back(
              ContactPoint(c_pair, c_type, c_v_delta, c_n));
          }
          else if (all_surfs.contains(ob_id)) {
            const auto      c_type = algorithms::collision::types::ColType::Su;
            const GM2Vector c_n = *contact_normals.find({in_sphere_id, ob_id});
            const auto      c_v_delta = blaze::inner(a_sph.m_velocity, c_n);
            contact_points.push_back(
              ContactPoint(c_pair, c_type, c_v_delta, c_n));
          }
          else if (all_spheres.contains(ob_id)) {
            // check if this pair does not exist in the set of contacts
            if (not set_of_contact_pairs.contains({in_sphere_id, ob_id})) {
              // add to the set of spheres to check, if it was not checked
              // before
              if (not spheres_checked.contains(ob_id))
                spheres_to_check.insert(ob_id);
              auto&      s_sph  = all_spheres[ob_id];
              const auto c_type = algorithms::collision::types::ColType::Sp;
              const auto c_n    = helpers::findNDSpheres(a_sph, s_sph).second;
              const auto c_v_delta
                = blaze::inner(s_sph.m_velocity - a_sph.m_velocity, c_n);
              set_of_contact_pairs.insert(c_pair);
              set_of_contact_pairs.insert({c_pair.second, c_pair.first});
              contact_points.push_back(
                ContactPoint(c_pair, c_type, c_v_delta, c_n));
            }
          }
        }
      }
    }
  }   // namespace contact

}   // namespace rigidbodyaspect::algorithms
