#include "sphere_fixedplane.h"
#include "sphere_sphere.h"
#include "sphere_surface.h"
#include "../backend/rigidbodyaspect.h"
#include "ContactPoint.h"

// stl
#include <iostream>
#include <chrono>
using namespace std::chrono_literals;



namespace rigidbodyaspect::algorithms
{

  namespace collision
  {
    detail::SphereFixedPlaneColliderReturnType
    detectCollision(const rbtypes::Sphere&        sphere,
                    const rbtypes::FixedPlane&    plane,
                    [[maybe_unused]] seconds_type dt)
    {
      const auto dt_rem = dt - sphere.m_dt_tp;
      // was singularity exception - changed to NoCollision
      if (dt_rem.count() <= 0)
        return {0ms, detail::SphereFixedPlaneCollisionStatus::NoCollision};

      const auto pl_eval
        = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                               rbtypes::FixedPlane::PSpaceSizeArray{1UL, 1UL});

      const auto q = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));
      const auto n = helpers::nToPlane(plane);

      const auto p = sphere.m_pos;
      const auto r = sphere.m_radius;

      // \vec{d} = ( \vec{q} + r \vec{n} ) - \pnt{p}
      const auto d  = (q + r * n) - p;
      const auto ds = sphere.m_ds;

      const auto dn  = blaze::inner(d, n);
      const auto dsn = blaze::inner(ds, n);

      // if ds is in the same direction as the normal of the plane
      // or is moving in parallel to the plane
      if (dsn > 0.0 or (std::abs(dsn) < constants::Zero))
        return {0ms, detail::SphereFixedPlaneCollisionStatus::NoCollision};

      // collision time
      const auto x = dn / dsn;

      if (x <= 1.0) {
        return {sphere.m_dt_tp + (x * dt_rem),
                detail::SphereFixedPlaneCollisionStatus::Collision};
      }

      return {0ms, detail::SphereFixedPlaneCollisionStatus::NoCollision};
    }


  }   // namespace collision




  namespace statechange
  {



    detail::StateChangeStatus detectStateChange(const rbtypes::Sphere& sphere,
                                                const GM2Vector&       n)
    {
      const auto ds = sphere.m_ds;

      const auto cond1_eq = blaze::inner(ds, n);   // dsn
      const auto cond2_eq
        = blaze::inner(-n * sphere.m_radius, ds) - blaze::inner(ds, ds);


      [[maybe_unused]] const auto cond1 = cond1_eq <= constants::Zero;
      [[maybe_unused]] const auto cond2 = std::abs(cond2_eq) < constants::Zero;


      if (cond1 and cond2)
        return detail::StateChangeStatus::ToAtRest;
      else if (cond1) {
        // check if clear dirt in n direction from velocity, may be it is
        // rolling
        const auto& v = sphere.m_velocity;
        const auto  clean_v
          = sphere.m_velocity - blaze::dot(sphere.m_velocity, n) * n;
        const auto& va
          = sphere.m_radius * blaze::cross(-n, sphere.m_angular_velocity);
        const auto& v_tilda = clean_v - va;


        const auto      k_friction  = 0.2;
        const auto      v_direction = blaze::normalize(clean_v);
        const GM2Vector K_n = -blaze::dot(constants::Gn, n) * n * sphere.m_mass;
        const auto      coef = k_friction * std::pow(sphere.m_radius, 2)
                          * blaze::length(K_n) / sphere.m_mass;
        const GM2Vector g_t = constants::Gn - blaze::dot(constants::Gn, n) * n;

        const GM2Vector a_t = g_t - coef * v_direction;

        const auto R_max = k_friction * blaze::length(K_n);
        const auto R_roll
          = sphere.m_mass * sphere.m_inertia * blaze::length(a_t)
            / (sphere.m_mass * std::pow(sphere.m_radius, 2) + sphere.m_inertia);

        if (blaze::length(v_tilda) < constants::Zero)
          return detail::StateChangeStatus::ToRolling;
        else
          return detail::StateChangeStatus::ToSliding;
      }
      // Default
      return detail::StateChangeStatus::ToFree;
    }


    void doStateCgange(rbtypes::Sphere&           sphere,
                       const rbtypes::FixedPlane& plane,
                       const Qt3DCore::QNodeId&   plane_id)
    {
      const auto n = helpers::nToPlane(plane);
      const auto sc_status
        = algorithms::statechange::detectStateChange(sphere, n);

      bool print;

      if (sc_status
          == algorithms::statechange::detail::StateChangeStatus::ToAtRest) {

        sphere.m_state = MovingBodyState::Sliding;
        sphere.m_attached_objects.insert(plane_id);
        sphere.m_aa *= 0.0;
        return;
      }
      else if (sc_status
               == algorithms::statechange::detail::StateChangeStatus::
                 ToSliding) {

        print          = sphere.m_attached_objects.size() == 0;
        sphere.m_state = MovingBodyState::Sliding;
        sphere.m_attached_objects.insert(plane_id);
      }
      else if (sc_status
               == algorithms::statechange::detail::StateChangeStatus::ToFree) {

        bool print = sphere.m_attached_objects.size() != 0;
        sphere.m_attached_objects.remove(plane_id);
        if (sphere.m_attached_objects.size() == 0) {
          sphere.m_state = MovingBodyState::Free;
          sphere.m_aa *= 0.0;
        }
      }
      return;
    }


    void
    stateChangeForces(const Qt3DCore::QNodeId&                   sphere_id,
                      const Qt3DCore::QNodeId&                   plane_id,
                      RigidBodyContainer::SphereQHash&           all_spheres,
                      const RigidBodyContainer::FixedPlaneQHash& all_planes,
                      const RigidBodyContainer::SurfQHash&       all_surfs,
                      ContactNTtype& contact_normals)
    {
      auto&                     sphere = all_spheres[sphere_id];
      detail::StateChangeStatus new_state;

      // if the second object is a plane
      if (all_planes.contains(plane_id)) {
        GM2Vector n;
        // if plane
        auto& plane = all_planes[plane_id];
        n           = helpers::nToPlane(plane);

        new_state
          = algorithms::statechange::detectStateChange(sphere, n /*plane*/);
        if (new_state
            == algorithms::statechange::detail::StateChangeStatus::ToAtRest) {
          sphere.m_state = MovingBodyState::AtRest;
          sphere.m_attached_objects.insert(plane_id);
          // correct velocity which was left in normal direction
          sphere.m_velocity -= blaze::dot(sphere.m_velocity, n) * n;
        }
        else if (new_state
                 == algorithms::statechange::detail::StateChangeStatus::
                   ToSliding) {
          sphere.m_state = MovingBodyState::Sliding;
          sphere.m_attached_objects.insert(plane_id);
          // correct velocity which was left in normal direction
          sphere.m_velocity -= blaze::dot(sphere.m_velocity, n) * n;
        }
        else if (new_state
                 == algorithms::statechange::detail::StateChangeStatus::
                   ToRolling) {
          sphere.m_state = MovingBodyState::Rolling;
          sphere.m_attached_objects.insert(plane_id);
          // correct velocity which was left in normal direction
          sphere.m_velocity -= blaze::dot(sphere.m_velocity, n) * n;
        }
        else if (new_state
                 == algorithms::statechange::detail::StateChangeStatus::
                   ToFree) {
          sphere.m_attached_objects.remove(plane_id);
          if (sphere.m_attached_objects.size() == 0) {
            sphere.m_state = MovingBodyState::Free;
          }
        }
      }
      return;
    };

  }   // namespace statechange



  namespace simulation
  {
    void resolveImpactResponse(
      RigidBodyContainer::SphereQHash&                        all_spheres,
      std::vector<contact::ContactPoint>&                     collision_points,
      QHash<Qt3DCore::QNodeId, QSet<contact::ContactPoint*>>& cps_of_node_id)
    {
      const auto stop_fraction = (1 / 1000) * collision_points.begin()->v_delta;
      const auto max_visits    = 50;

      // while the penetration condition for the smallest impulse
      while (collision_points.begin()->v_delta < -constants::Zero
             and not collision_points.begin()->resolved) {
        auto&       c_point   = *collision_points.begin();
        const auto& id_sphere = c_point.contact_objects.first;
        const auto& id_second = c_point.contact_objects.second;
        c_point.visits++;
        if (c_point.v_delta > stop_fraction or c_point.visits > max_visits) {
          // compute special response and "turn off" contact point
          c_point.resolved = true;
          // now make velocity = 0, may be has to be changed to smth more clever
          all_spheres[id_sphere].m_velocity
            = all_spheres[id_sphere].m_velocity * 0.0;
          if (c_point.contact_type == collision::types::ColType::Sp)
            all_spheres[id_second].m_velocity
              = all_spheres[id_second].m_velocity * 0.0;
        }
        else {
          // compute regular
          if (c_point.contact_type != collision::types::ColType::Sp) {
            auto&       sphere1 = all_spheres[id_sphere];
            GM2Vector   n       = c_point.n;
            const auto& m       = all_spheres[id_sphere].m_mass;
            const auto  I       = all_spheres[id_sphere].m_inertia;

            auto E_b
              = 0.5 * m
                  * std::pow(blaze::length(all_spheres[id_sphere].m_velocity),
                             2)
                + 0.5 * I
                    * std::pow(
                      blaze::length(all_spheres[id_sphere].m_angular_velocity),
                      2);

            GM2Vector v_change;
            GM2Vector w_change;
            double    impulse_scalar;
            GM2Vector u;
            std::tie(impulse_scalar, u)
              = getImpulseScalar(all_spheres, c_point, n);
            std::tie(v_change, w_change)
              = getDvDw(all_spheres[id_sphere], n, impulse_scalar, u);
            sphere1.m_velocity += v_change;
            sphere1.m_angular_velocity += w_change;
            auto E_a
              = 0.5 * m
                  * std::pow(blaze::length(all_spheres[id_sphere].m_velocity),
                             2)
                + 0.5 * I
                    * std::pow(
                      blaze::length(all_spheres[id_sphere].m_angular_velocity),
                      2);
            auto Loc = all_spheres[id_sphere].m_pos;
          }
          else if (c_point.contact_type == collision::types::ColType::Sp) {
            auto&     sphere1 = all_spheres[id_sphere];
            auto&     sphere2 = all_spheres[id_second];
            GM2Vector n       = -c_point.n;

            const auto& m  = sphere1.m_mass;
            const auto  I  = sphere1.m_inertia;
            const auto& m2 = sphere2.m_mass;
            const auto  I2 = sphere2.m_inertia;

            auto E_b
              = 0.5 * m * std::pow(blaze::length(sphere1.m_velocity), 2)
                + 0.5 * I
                    * std::pow(blaze::length(sphere1.m_angular_velocity), 2)
                + 0.5 * m2 * std::pow(blaze::length(sphere2.m_velocity), 2)
                + 0.5 * I2
                    * std::pow(blaze::length(sphere2.m_angular_velocity), 2);
            GM2Vector v_change;
            GM2Vector w_change;
            double    impulse_scalar;
            GM2Vector u;
            std::tie(impulse_scalar, u)
              = getImpulseScalar(all_spheres, c_point, n);

            std::tie(v_change, w_change)
              = getDvDw(sphere1, n, impulse_scalar, u);

            sphere1.m_velocity += v_change;
            sphere1.m_angular_velocity += w_change;

            std::tie(v_change, w_change)
              = getDvDw(sphere2, -n, impulse_scalar, -u);

            sphere2.m_velocity += v_change;
            sphere2.m_angular_velocity += w_change;

            auto E_a
              = 0.5 * m * std::pow(blaze::length(sphere1.m_velocity), 2)
                + 0.5 * I
                    * std::pow(blaze::length(sphere1.m_angular_velocity), 2)
                + 0.5 * m2 * std::pow(blaze::length(sphere2.m_velocity), 2)
                + 0.5 * I2
                    * std::pow(blaze::length(sphere2.m_angular_velocity), 2);
          }
        }
        // here change all the delta velocity in all contact points sharing the
        // same MOVING objects as the current contact point TO DO: here second
        // should be added only if second is sphere
        for (auto& cp : cps_of_node_id.value(id_sphere)
                          + cps_of_node_id.value(id_second)) {
          auto& sp_1 = all_spheres[cp->contact_objects.first];
          if (cp->resolved) {
            cp->v_delta = 0.0;
            sp_1.m_velocity *= 0.0;
            if (cp->contact_type == collision::types::ColType::Sp)
              all_spheres[cp->contact_objects.second].m_velocity *= 0.0;
          }
          else {
            if (cp->contact_type == collision::types::ColType::Sp) {
              cp->v_delta = blaze::inner(
                all_spheres[cp->contact_objects.second].m_velocity
                  - sp_1.m_velocity,
                cp->n);
            }
            else {
              cp->v_delta = blaze::inner(sp_1.m_velocity, cp->n);
            }
          }
        }
        std::sort(collision_points.begin(), collision_points.end());
      }
    }

    std::pair<double, GM2Vector>
    getImpulseScalar(RigidBodyContainer::SphereQHash& all_spheres,
                     contact::ContactPoint& cp, const GM2Vector& n)
    {
      auto&            sphere = all_spheres[cp.contact_objects.first];
      const auto       e      = 1.0;
      const auto&      r      = sphere.m_radius;
      const GM2Vector  rn     = -r * n;
      const auto&      m      = sphere.m_mass;
      const GM2Vector& v      = sphere.m_velocity;
      const GM2Vector& w      = sphere.m_angular_velocity;
      const GM2Vector& wr     = blaze::cross(w, rn);

      auto      delta_v = 1.0 / m;
      GM2Vector u       = v + wr;

      if (cp.contact_type == collision::types::ColType::Sp) {
        auto&            sphere2 = all_spheres[cp.contact_objects.second];
        const auto&      r2      = sphere2.m_radius;
        const GM2Vector  rn2     = r2 * n;
        const auto&      m2      = sphere2.m_mass;
        const GM2Vector& v2      = sphere2.m_velocity;
        const GM2Vector& w2      = sphere2.m_angular_velocity;
        const GM2Vector& wr2     = blaze::cross(w2, rn2);
        delta_v += 1.0 / m2;
        GM2Vector u2 = v2 + wr2;
        u -= u2;
      }

      auto contact_v_n     = blaze::dot(u, n);
      auto desired_delta_v = -contact_v_n * (1.0 + e);
      auto impulse_scalar  = desired_delta_v / delta_v;

      return {impulse_scalar, u};
    };



    std::pair<GM2Vector, GM2Vector> getDvDw(rbtypes::Sphere& sphere,
                                            const GM2Vector  n,
                                            const double     impulse_scalar,
                                            const GM2Vector  u)
    {
      const auto&      r  = sphere.m_radius;
      const GM2Vector  rn = -r * n;
      const auto&      m  = sphere.m_mass;
      const auto&      I  = sphere.m_inertia;
      const auto       mu = 0.1;   // friction coefficient
      const GM2Vector& v  = sphere.m_velocity;
      const GM2Vector& w  = sphere.m_angular_velocity;
      const GM2Vector& wr = blaze::cross(w, rn);

      GM2Vector dv_n
        = impulse_scalar * n / m;   // change of velocity in n direction

      auto world_to_contact = helpers::contactSpace(n);
      auto contact_to_world = world_to_contact;
      contact_to_world.transpose();

      GM2Vector u_in_contact = world_to_contact * u;
      u_in_contact[0]        = 0.0;
      u_in_contact[1] *= -1.0;
      u_in_contact[2] *= -1.0;
      GM2Vector u_out_contact = contact_to_world * u_in_contact;
      GM2Vector fr_dir        = blaze::normalize(u_out_contact);

      GM2Vector d_v;
      GM2Vector d_w;
      if (blaze::length(fr_dir) > constants::Zero) {
        GM2Vector v_in_contact = world_to_contact * v;
        v_in_contact[0]        = 0.0;
        GM2Vector vt           = contact_to_world * v_in_contact;
        GM2Vector vn           = world_to_contact * v;
        vn[1] *= 0.0;
        vn[2] *= 0.0;
        vn = contact_to_world * vn;

        /* for friction */
        auto E_ang
          = 0.5 * I * std::pow(blaze::length(sphere.m_angular_velocity), 2);
        E_ang = E_ang < constants::Zero ? 0.0 : E_ang;

        d_v          = fr_dir * mu * impulse_scalar;
        auto delta_J = 0.0;
        // here we check which one is increasing. We do not care in how much we
        // just need to know which goes where
        auto d_v_temp = fr_dir
                        * std::min(std::min(blaze::length(u_out_contact),
                                            blaze::length(d_v)),
                                   blaze::length(vt));   // this is original
        auto E_lin_new_temp
          = 0.5 * m
            * std::pow(blaze::length(sphere.m_velocity + dv_n + d_v_temp), 2);
        auto E_lin_new_temp_n
          = 0.5 * m * std::pow(blaze::length(sphere.m_velocity + dv_n), 2);
        auto E_delta_temp = E_lin_new_temp - E_lin_new_temp_n;
        if (E_delta_temp > -constants::Zero) {
          // linear increases, so we adjust it to angular (as angular has a
          // limited amount of energy to take from) we select the minimum
          // between impulse part, linear velocity or angular velocity
          delta_J = std::min(
            std::min(blaze::length(u_out_contact), blaze::length(d_v)),
            blaze::length(wr));
          auto t_d_w = fr_dir * delta_J;
          d_w        = blaze::cross(t_d_w / r, n);

          auto E_ang_new = 0.5 * I * std::pow(blaze::length(w + d_w), 2);
          E_ang_new      = E_ang_new < constants::Zero ? 0.0 : E_ang_new;
          auto E_delta   = E_ang_new - E_ang;
          // if for some reason the angular energy was increased, change the
          // direction of the angular velocity impulse
          if (E_delta > constants::Zero) {
            d_w       = -d_w;
            E_ang_new = 0.5 * I * std::pow(blaze::length(w + d_w), 2);
            E_delta   = E_ang_new - E_ang;
          }
          // linear velocity should be changed so that its energy will be
          // changed on the same amount
          auto E_lin_new = E_lin_new_temp_n - E_delta;
          if (E_lin_new > constants::Zero) {
            auto v_len_new_sq = 2.0 * E_lin_new / m;
            auto v_n1         = v + dv_n;
            auto D = std::pow(v_n1[0] * fr_dir[0] + v_n1[1] * fr_dir[1]
                                + v_n1[2] * fr_dir[2],
                              2)
                     - (std::pow(fr_dir[0], 2) + std::pow(fr_dir[1], 2)
                        + std::pow(fr_dir[2], 2))
                         * (std::pow(v_n1[0], 2) + std::pow(v_n1[1], 2)
                            + std::pow(v_n1[2], 2) - v_len_new_sq);
            if (D < constants::Zero) {
              fr_dir *= -1.0;
              D = std::pow(v_n1[0] * fr_dir[0] + v_n1[1] * fr_dir[1]
                             + v_n1[2] * fr_dir[2],
                           2)
                  - (std::pow(fr_dir[0], 2) + std::pow(fr_dir[1], 2)
                     + std::pow(fr_dir[2], 2))
                      * (std::pow(v_n1[0], 2) + std::pow(v_n1[1], 2)
                         + std::pow(v_n1[2], 2) - v_len_new_sq);
              if (D < constants::Zero) {
                return std::make_pair(dv_n, d_w * 0.0);
              }
            }
            auto factor_1 = (-(v_n1[0] * fr_dir[0] + v_n1[1] * fr_dir[1]
                               + v_n1[2] * fr_dir[2])
                             + std::sqrt(D))
                            / ((std::pow(fr_dir[0], 2) + std::pow(fr_dir[1], 2)
                                + std::pow(fr_dir[2], 2)));
            auto factor_2 = (-(v_n1[0] * fr_dir[0] + v_n1[1] * fr_dir[1]
                               + v_n1[2] * fr_dir[2])
                             - std::sqrt(D))
                            / ((std::pow(fr_dir[0], 2) + std::pow(fr_dir[1], 2)
                                + std::pow(fr_dir[2], 2)));
            auto factor = factor_1 > 0.0 ? factor_1 : factor_2;
            factor      = mu > constants::Zero ? factor : 0.0;
            d_v         = factor * fr_dir;
          }
          else {
            d_v = -v - dv_n;
          }
        }
        // otherwise DW is increasing, so we have to adjust DV to it
        else {
          d_v            = d_v_temp;
          auto E_delta   = E_delta_temp;
          auto E_ang_new = E_ang - E_delta;
          if (E_ang_new > constants::Zero) {
            auto w_len_sq = E_ang_new * 2.0 / I;
            auto dir      = blaze::cross(fr_dir, n);
            auto D = std::pow(w[0] * dir[0] + w[1] * dir[1] + w[2] * dir[2], 2)
                     - (std::pow(dir[0], 2) + std::pow(dir[1], 2)
                        + std::pow(dir[2], 2))
                         * (std::pow(w[0], 2) + std::pow(w[1], 2)
                            + std::pow(w[2], 2) - w_len_sq);
            D = D < constants::Zero ? 0.0 : D;
            auto factor_1
              = ((w[0] * dir[0] + w[1] * dir[1] + w[2] * dir[2]) + std::sqrt(D))
                / (std::pow(dir[0], 2) + std::pow(dir[1], 2)
                   + std::pow(dir[2], 2));
            auto factor_2
              = ((w[0] * dir[0] + w[1] * dir[1] + w[2] * dir[2]) - std::sqrt(D))
                / (std::pow(dir[0], 2) + std::pow(dir[1], 2)
                   + std::pow(dir[2], 2));
            auto factor = factor_1 < 0.0 ? factor_1 : factor_2;
            d_w         = -factor * dir;
          }
          else
            d_w = -w;
        }
      }
      else {
        d_v = 0.0;
        d_w = 0.0;
      }

      return std::make_pair(dv_n + d_v, d_w);
    };

    std::pair<GM2Vector, GM2Vector> getDvDwBourg(rbtypes::Sphere& sphere,
                                                 const GM2Vector  n,
                                                 const double    impulse_scalar,
                                                 const GM2Vector u)
    {
      // an example of collision response with angular velocity and friction
      // from Bourg
      const auto&     r  = sphere.m_radius;
      const GM2Vector rn = -r * n;
      const auto&     m  = sphere.m_mass;
      const auto&     I  = sphere.m_inertia;
      const auto      mu = 0.1;   // friction coefficient

      auto world_to_contact = helpers::contactSpace(n);
      auto contact_to_world = world_to_contact;
      contact_to_world.transpose();

      GM2Vector u_in_contact = world_to_contact * u;
      u_in_contact[0]        = 0.0;
      u_in_contact[1] *= -1.0;
      u_in_contact[2] *= -1.0;
      GM2Vector u_out_contact = contact_to_world * u_in_contact;
      GM2Vector fr_dir        = blaze::normalize(u_out_contact);

      GM2Vector dv_n
        = impulse_scalar * n / m;   // change of velocity in n direction
      GM2Vector d_v = fr_dir * mu * impulse_scalar
                      / m;   // change of velocity in t direction
      GM2Vector d_w
        = blaze::cross(rn, (impulse_scalar * n + fr_dir * mu * impulse_scalar))
          / I;   // change of angular velocity

      return std::make_pair(dv_n + d_v, d_w);
    }

    std::pair<GM2Vector, GM2Vector>
    getDvDwMillington(rbtypes::Sphere& sphere, const GM2Vector n,
                      const double impulse_scalar, const GM2Vector u)
    {
      // an example of collision response with angular velocity and friction
      // inspired by the method from Millington implementation only for sp_pl
      // and sp_su
      const auto      e      = 1.0;
      const auto&     r      = sphere.m_radius;
      const GM2Vector rn     = -r * n;
      const auto&     m      = sphere.m_mass;
      const auto&     I      = sphere.m_inertia;
      const auto      mu     = 0.1;   // friction coefficient
      double          deltaV = 1.0 / m;

      auto world_to_contact = helpers::contactSpace(n);
      auto contact_to_world = world_to_contact;
      contact_to_world.transpose();

      GM2Vector u_in_contact = world_to_contact * u;

      auto      desiredDeltaVelocity = -u_in_contact[0] * (1.0 + e);
      GM2Vector velKill{desiredDeltaVelocity, -u_in_contact[1],
                        -u_in_contact[2]};
      GM2Vector impulseContact{velKill[0] / deltaV, velKill[1] / deltaV,
                               velKill[2] / deltaV};
      auto      planarImpulse = std::sqrt(std::pow(impulseContact[1], 2)
                                     + std::pow(impulseContact[2], 2));

      if (planarImpulse > impulseContact[0] * mu) {
        impulseContact[1] /= planarImpulse;
        impulseContact[2] /= planarImpulse;

        impulseContact[0]
          = deltaV /*+deltaV*impulseContact[1]+deltaV*impulseContact[2]*/;
        impulseContact[0] = desiredDeltaVelocity / impulseContact[0];
        impulseContact[1] *= impulseContact[0] * mu;
        impulseContact[2] *= impulseContact[0] * mu;
      }

      GM2Vector dv_t{0.0, impulseContact[1], impulseContact[2]};
      dv_t = contact_to_world * dv_t;
      GM2Vector dv_n
        = impulseContact[0] * n / m;   // change of velocity in n direction
      GM2Vector d_v = dv_t / m;        // change of velocity in t direction
      GM2Vector d_w = blaze::cross(rn, (impulse_scalar * n + dv_t))
                      / I;   // change of angular velocity

      return std::make_pair(dv_n + d_v, d_w);
    };

  }   // namespace simulation

  namespace singularities
  {

    void cleanUpCollisions(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                             collision_sets,
      const RigidBodyContainer::SphereQHash& all_spheres)
    {
      auto sort_criteria = [](const auto& a, const auto& b) {
        return std::get<0>(a) > std::get<0>(b);
      };
      // first sort with respect to time
      std::sort(std::begin(collision_sets), std::end(collision_sets),
                sort_criteria);

      // temporary resulting vector
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>
        new_collision_sets;

      const auto& all_spheres_ids = all_spheres.keys().toSet();

      // iterate through the given vector of the collisions to group and make
      // unique collision sets
      for (auto i = collision_sets.rbegin(); i != collision_sets.rend(); ++i) {
        auto         current_set = *i;
        QSet<size_t> groups_to_delete;
        bool         add_new_set = true;
        for (size_t j = 0; j < new_collision_sets.size(); j++) {
          auto        group = new_collision_sets[j];
          const auto& intersection
            = group.second & current_set.second & all_spheres_ids;
          if (intersection.size() > 0) {
            // if time is the same - make groups
            if (group.first == current_set.first) {
              current_set.second.unite(group.second);
              groups_to_delete.insert(j);
            }
            else {
              // there was an intersection in the time before so there is no
              // need to add it
              add_new_set = false;
            }
          }
        }
        // delete those groups in the same time which had
        auto compensate = 0;
        for (auto& index_delete_group : groups_to_delete) {
          new_collision_sets.erase(new_collision_sets.begin()
                                   + int(index_delete_group) - compensate);
          compensate++;
        }
        if (add_new_set) {
          new_collision_sets.emplace_back(current_set);
        }
      }

      collision_sets.clear();
      collision_sets = new_collision_sets;

      // may be sort is extra, or may be not (if we say that collision sets is a
      // new collision set in reverse)
      std::sort(std::begin(collision_sets), std::end(collision_sets),
                sort_criteria);
    }

    void detectInitialSingularities(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                                 collision_sets,
      types::SpFPlSphereIdPair                   sphere_pair,
      const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
      const RigidBodyContainer::SphereQHash&     spheres,
      const RigidBodyContainer::SurfQHash&       surfs,
      ContactNTtype& contact_normals, seconds_type dt)
    {
      const auto& sphere = std::get<0>(sphere_pair);
      const auto& sid    = std::get<1>(sphere_pair);

      QSet<Qt3DCore::QNodeId> touching_objects;
      seconds_type            min_col_time = 300ms;

      // check the planes
      for (auto fpit = fixed_planes.begin(); fpit != fixed_planes.end();
           ++fpit) {
        const auto& fpid        = fpit.key();
        const auto& fixed_plane = fpit.value();

        // Skip attached fixed planes
        if (sphere.m_attached_objects.contains(fpid)) continue;

        const auto [col_dt_tp, col_status]
          = algorithms::collision::detectCollision(sphere, fixed_plane, dt);

        // if collision -> add to the set of touch_objects
        if (col_status == collision::types::SpFPlStatus::Collision) {
          // insert only those with the smaller time
          objToColTouchObjs(col_dt_tp, min_col_time, fpid, touching_objects);
        }
      }

      // check the SURFS
      for (auto sfit = surfs.begin(); sfit != surfs.end(); ++sfit) {
        const auto& sfid = sfit.key();
        const auto& surf = sfit.value();

        // Skip attached fixed surfs
        if (sphere.m_attached_objects.contains(sfid)) continue;

        const auto [col_dt_tp, col_status, normal]
          = algorithms::collision::detectCollisionSurf(sphere, surf, dt);

        // if collision -> add to the set of touch_objects
        if (col_status == collision::types::SpFPlStatus::Collision) {
          // insert only those with the smaller time
          objToColTouchObjs(col_dt_tp, min_col_time, sfid, touching_objects);
          contact_normals.insert({sid, sfid}, normal);
        }
      }

      // check the SPHERES
      for (auto spit = spheres.begin(); spit != spheres.end(); ++spit) {
        const auto& spid           = spit.key();
        const auto& sphere_passive = spit.value();

        // skip itself
        if (sid == spid) continue;
        // Skip attached fixed planes
        if (sphere.m_attached_objects.contains(spid)) continue;
        if (sphere_passive.m_attached_objects.contains(sid)) continue;

        const auto [col_dt_tp, col_status]
          = algorithms::collision::detectCollisionSp(sphere, sphere_passive,
                                                     dt);

        // if collision -> add to the set of touch_objects
        if (col_status == collision::types::SpFPlStatus::Collision) {
          // insert only those with the smaller time
          objToColTouchObjs(col_dt_tp, min_col_time, spid, touching_objects);
        }
      }
      // if there were found collision points - add the collision to the vector
      if (touching_objects.size() > 0) {
        // collisions.emplace_back(min_col_time, sid, touch_objects);
        touching_objects.insert(sid);
        collision_sets.emplace_back(min_col_time, touching_objects);
      }
    }

    void detectCollisionsBruteForce(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                          collision_sets,
      RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      auto&       spheres      = all_bodies.spheres();
      const auto& fixed_planes = all_bodies.fixedPlanes();
      const auto& surfs        = all_bodies.surfs();

      for (auto sit = spheres.begin(); sit != spheres.end(); ++sit) {
        algorithms::singularities::detectInitialSingularities(
          collision_sets,
          algorithms::singularities::types::SpFPlSphereIdPair{sit.value(),
                                                              sit.key()},
          fixed_planes, spheres, surfs, contact_normals, dt);
      }
    };


    void detectCollisionsBruteForceInner(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                     collision_sets,
      const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids,
      const QSet<Qt3DCore::QNodeId>& curr_col_pl_ids,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      const auto& all_spheres = all_bodies.spheres();
      const auto& all_planes  = all_bodies.fixedPlanes();
      const auto& all_surfs   = all_bodies.surfs();
      // search for the collisions for all the spheres in the givven collision
      for (auto& sp_id : curr_col_sp_ids) {
        QSet<Qt3DCore::QNodeId> touching_objects;
        seconds_type            min_col_time = 300ms;
        const auto&             sphere       = all_spheres[sp_id];
        // check with all the planes
        for (auto fpit = all_planes.begin(); fpit != all_planes.end(); ++fpit) {
          const auto& fpid        = fpit.key();
          const auto& fixed_plane = fpit.value();
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(fpid)) continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollision(sphere, fixed_plane, dt);

          if (col_status == collision::types::SpFPlStatus::Collision) {
            objToColTouchObjs(col_dt_tp, min_col_time, fpid, touching_objects);
          }
        }
        // check the surfs
        for (auto suit = all_surfs.begin(); suit != all_surfs.end(); ++suit) {
          const auto& sfid = suit.key();
          const auto& surf = suit.value();
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(sfid)) continue;
          const auto [col_dt_tp, col_status, normal]
            = algorithms::collision::detectCollisionSurf(sphere, surf, dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            objToColTouchObjs(col_dt_tp, min_col_time, sfid, touching_objects);
            contact_normals.insert({sp_id, sfid}, normal);
          }
        }
        // check with all the spheres
        for (auto spit = all_spheres.begin(); spit != all_spheres.end();
             ++spit) {

          const auto& spp_id         = spit.key();
          const auto& sphere_passive = spit.value();
          // skip itself
          if (sp_id == spp_id) continue;
          // Skip attached fixed spheres
          if (sphere.m_attached_objects.contains(spp_id)) continue;
          if (sphere_passive.m_attached_objects.contains(sp_id)) continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollisionSp(sphere, sphere_passive,
                                                       dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            objToColTouchObjs(col_dt_tp, min_col_time, spp_id,
                              touching_objects);
          }
        }

        // if there were found collision points - add the collision to the
        // vector
        if (touching_objects.size() > 0) {
          touching_objects.insert(sp_id);
          collision_sets.emplace_back(min_col_time, touching_objects);
        }
      }
    }

    void objToColTouchObjs(const seconds_type       collision_time,
                           seconds_type&            min_time,
                           const Qt3DCore::QNodeId  obj_id,
                           QSet<Qt3DCore::QNodeId>& touch_objects)
    {
      if (collision_time < min_time) {
        min_time = collision_time;
        touch_objects.clear();
        touch_objects.insert(obj_id);
      }
      else if (collision_time == min_time) {
        touch_objects.insert(obj_id);
      }
    };

  }   // namespace singularities

  namespace singularities_simple
  {
    void detectInitialSingularities(
      collisionType&                             collisions,
      singularities::types::SpFPlSphereIdPair    sphere_pair,
      const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
      const RigidBodyContainer::SphereQHash&     spheres,
      const RigidBodyContainer::SurfQHash&       surfs,
      ContactNTtype& contact_normals, seconds_type dt)
    {
      const auto& sphere = std::get<0>(sphere_pair);
      const auto& sid    = std::get<1>(sphere_pair);

      QSet<Qt3DCore::QNodeId> touching_objects;

      // check the planes
      for (auto fpit = fixed_planes.begin(); fpit != fixed_planes.end();
           ++fpit) {
        const auto& fpid        = fpit.key();
        const auto& fixed_plane = fpit.value();
        // Skip attached fixed planes
        if (sphere.m_attached_objects.contains(fpid)) continue;
        const auto [col_dt_tp, col_status]
          = algorithms::collision::detectCollision(sphere, fixed_plane, dt);
        // if collision -> add to vector of collision objects
        if (col_status == collision::types::SpFPlStatus::Collision) {
          collisions.emplace_back(col_dt_tp, sid, fpid);
        }
      }

      // check the SURFS
      for (auto sfit = surfs.begin(); sfit != surfs.end(); ++sfit) {
        const auto& sfid = sfit.key();
        const auto& surf = sfit.value();
        // Skip attached fixed surfs
        if (sphere.m_attached_objects.contains(sfid)) continue;
        const auto [col_dt_tp, col_status, normal]
          = algorithms::collision::detectCollisionSurf(sphere, surf, dt);
        // if collision -> add to vector of collision objects
        if (col_status == collision::types::SpFPlStatus::Collision) {
          collisions.emplace_back(col_dt_tp, sid, sfid);
          contact_normals.insert({sid, sfid}, normal);
        }
      }

      // check the SPHERES
      for (auto spit = spheres.begin(); spit != spheres.end(); ++spit) {
        const auto& spid           = spit.key();
        const auto& sphere_passive = spit.value();
        // skip itself
        if (sid == spid) continue;
        // Skip attached fixed planes
        if (sphere.m_attached_objects.contains(spid)) continue;
        if (sphere_passive.m_attached_objects.contains(sid)) continue;
        const auto [col_dt_tp, col_status]
          = algorithms::collision::detectCollisionSp(sphere, sphere_passive,
                                                     dt);
        // if collision -> add to vector of collision objects
        if (col_status == collision::types::SpFPlStatus::Collision) {
          collisions.emplace_back(col_dt_tp, sid, spid);
        }
      }
    };

    void detectCollisionsBruteForce(collisionType&      collisions,
                                    RigidBodyContainer& all_bodies,
                                    ContactNTtype&      contact_normals,
                                    seconds_type        dt)
    {
      auto&       spheres      = all_bodies.spheres();
      const auto& fixed_planes = all_bodies.fixedPlanes();
      const auto& surfs        = all_bodies.surfs();
      for (auto sit = spheres.begin(); sit != spheres.end(); ++sit) {
        algorithms::singularities_simple::detectInitialSingularities(
          collisions, {sit.value(), sit.key()}, fixed_planes, spheres, surfs,
          contact_normals, dt);
      }
    };

    void detectCollisionsBruteForceInner(
      collisionType& collisions, const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      const auto& all_spheres = all_bodies.spheres();
      const auto& all_planes  = all_bodies.fixedPlanes();
      const auto& all_surfs   = all_bodies.surfs();
      // search for the collisions for all the spheres in the givven collision
      for (auto& sp_id : curr_col_sp_ids) {
        const auto& sphere = all_spheres[sp_id];
        // check with all the planes
        for (auto fpit = all_planes.begin(); fpit != all_planes.end(); ++fpit) {
          const auto& fpid        = fpit.key();
          const auto& fixed_plane = fpit.value();
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(fpid)) continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollision(sphere, fixed_plane, dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            collisions.emplace_back(col_dt_tp, sp_id, fpid);
          }
        }
        // check the surfs
        for (auto suit = all_surfs.begin(); suit != all_surfs.end(); ++suit) {
          const auto& sfid = suit.key();
          const auto& surf = suit.value();
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(sfid)) continue;
          const auto [col_dt_tp, col_status, normal]
            = algorithms::collision::detectCollisionSurf(sphere, surf, dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            collisions.emplace_back(col_dt_tp, sp_id, sfid);
            contact_normals.insert({sp_id, sfid}, normal);
          }
        }
        // check with all the spheres
        for (auto spit = all_spheres.begin(); spit != all_spheres.end();
             ++spit) {
          const auto& spp_id         = spit.key();
          const auto& sphere_passive = spit.value();
          // skip itself
          if (sp_id == spp_id) continue;
          // Skip attached fixed spheres
          if (sphere.m_attached_objects.contains(spp_id)) continue;
          if (sphere_passive.m_attached_objects.contains(sp_id)) continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollisionSp(sphere, sphere_passive,
                                                       dt);
          // if collision -> add to the set of touch_objects
          if (col_status == collision::types::SpFPlStatus::Collision) {
            collisions.emplace_back(col_dt_tp, sp_id, spp_id);
          }
        }
      }
    };

    void resolveImpactResponse(RigidBodyContainer&  all_bodies,
                               Qt3DCore::QNodeId    sphere_id,
                               Qt3DCore::QNodeId    object_id,
                               const ContactNTtype& contact_normals)
    {
      const auto& planes  = all_bodies.fixedPlanes();
      auto&       spheres = all_bodies.spheres();
      const auto& surfs   = all_bodies.surfs();
      // first find normal
      GM2Vector                             n;
      algorithms::collision::types::ColType c_type;
      if (planes.contains(object_id)) {
        c_type = algorithms::collision::types::ColType::Pl;
        n      = helpers::nToPlane(planes[object_id]);
      }
      else if (surfs.contains(object_id)) {
        c_type = algorithms::collision::types::ColType::Su;
        n      = *contact_normals.find({sphere_id, object_id});
      }
      else if (spheres.contains(object_id)) {
        c_type = algorithms::collision::types::ColType::Sp;
        n      = helpers::findNDSpheres(spheres[sphere_id], spheres[object_id])
              .second;
      }
      contact::ContactPoint fake_contact_point({sphere_id, object_id}, c_type,
                                               0.0, n);
      // compute response

      if (c_type != collision::types::ColType::Sp) {
        auto& sphere1 = spheres[sphere_id];

        GM2Vector v_change;
        GM2Vector w_change;
        double    impulse_scalar;
        GM2Vector u;
        std::tie(impulse_scalar, u)
          = simulation::getImpulseScalar(spheres, fake_contact_point, n);
        std::tie(v_change, w_change)
          = simulation::getDvDw(sphere1, n, impulse_scalar, u);
        sphere1.m_velocity += v_change;
        sphere1.m_angular_velocity += w_change;
      }
      else if (c_type == collision::types::ColType::Sp) {
        auto& sphere1 = spheres[sphere_id];
        auto& sphere2 = spheres[object_id];
        n             = -n;

        GM2Vector v_change;
        GM2Vector w_change;
        double    impulse_scalar;
        GM2Vector u;
        std::tie(impulse_scalar, u)
          = simulation::getImpulseScalar(spheres, fake_contact_point, n);

        std::tie(v_change, w_change)
          = simulation::getDvDw(sphere1, n, impulse_scalar, u);

        sphere1.m_velocity += v_change;
        sphere1.m_angular_velocity += w_change;

        std::tie(v_change, w_change)
          = simulation::getDvDw(sphere2, -n, impulse_scalar, -u);

        sphere2.m_velocity += v_change;
        sphere2.m_angular_velocity += w_change;
      }
    }

    void detectCollisionsBruteSphereS(collisionType&      collisions,
                                      RigidBodyContainer& all_bodies,
                                      ContactNTtype&      contact_normals,
                                      seconds_type        dt)
    {
      // collision detection with simple collision object and bounding spheres
      // for all objects
      auto&       spheres = all_bodies.spheres();
      const auto& planes  = all_bodies.fixedPlanes();
      const auto& surfs   = all_bodies.surfs();

      for (auto sit = spheres.begin(); sit != spheres.end(); ++sit) {
        const auto& sid    = sit.key();
        auto&       sphere = sit.value();
        // check the planes
        for (auto fpit = planes.begin(); fpit != planes.end(); ++fpit) {
          const auto& fpid        = fpit.key();
          const auto& fixed_plane = fpit.value();
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(fpid)) continue;
          // Skip planes with the surrounding sphere not intersecting with our
          // sphere
          if (blaze::length(sphere.getSurroundSphere().getCenter()
                            - fixed_plane.getSurroundSphere().getCenter())
                - (sphere.getSurroundSphere().getRadius()
                   + fixed_plane.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          // Make exact collision detection
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollision(sphere, fixed_plane, dt);
          // if collision -> add to vector of collision objects
          if (col_status == collision::types::SpFPlStatus::Collision) {
            collisions.emplace_back(col_dt_tp, sid, fpid);
          }
        }
        // check the SURFS
        for (auto sfit = surfs.begin(); sfit != surfs.end(); ++sfit) {
          const auto& sfid = sfit.key();
          const auto& surf = sfit.value();
          // Skip attached fixed surfs
          if (sphere.m_attached_objects.contains(sfid)) continue;
          // Skip surfs with the surrounding sphere not intersecting with our
          // sphere
          if (blaze::length(sphere.getSurroundSphere().getCenter()
                            - surf.getSurroundSphere().getCenter())
                - (sphere.getSurroundSphere().getRadius()
                   + surf.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          // Make exact collision detection
          const auto [col_dt_tp, col_status, normal]
            = algorithms::collision::detectCollisionSurf(sphere, surf, dt);
          // if collision -> add to the set of touch_objects
          if (col_status == collision::types::SpFPlStatus::Collision) {
            // insert only those with the smaller time
            collisions.emplace_back(col_dt_tp, sid, sfid);
            contact_normals.insert({sid, sfid}, normal);
          }
        }
        // check the SPHERES
        for (auto spit = spheres.begin(); spit != spheres.end(); ++spit) {
          const auto& spid           = spit.key();
          auto&       sphere_passive = spit.value();
          // skip itself
          if (sid == spid) continue;
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(spid)) continue;
          if (sphere_passive.m_attached_objects.contains(sid)) continue;
          // Skip sphers with the surrounding sphere not intersecting with our
          // sphere's surrounding sphere
          if (blaze::length(sphere.getSurroundSphere().getCenter()
                            - sphere_passive.getSurroundSphere().getCenter())
                - (sphere.getSurroundSphere().getRadius()
                   + sphere_passive.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          // Make exact collision detection
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollisionSp(sphere, sphere_passive,
                                                       dt);
          // if collision -> add to the set of touch_objects
          if (col_status == collision::types::SpFPlStatus::Collision) {
            collisions.emplace_back(col_dt_tp, sid, spid);
          }
        }
      }
    };

    void detectCollisionsBruteSphereSInner(
      collisionType& collisions, const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids,
      RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      auto& all_spheres = all_bodies.spheres();
      auto& all_planes  = all_bodies.fixedPlanes();
      auto& all_surfs   = all_bodies.surfs();
      // search for the collisions for all the spheres in the givven collision
      for (auto& sp_id : curr_col_sp_ids) {
        auto& sphere = all_spheres[sp_id];
        // check with all the planes
        for (auto fpit = all_planes.begin(); fpit != all_planes.end(); ++fpit) {
          const auto& fpid        = fpit.key();
          auto&       fixed_plane = fpit.value();
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(fpid)) continue;
          // Skip planes with the surrounding sphere not intersecting with our
          // sphere
          if (blaze::length(sphere.getSurroundSphere().getCenter()
                            - fixed_plane.getSurroundSphere().getCenter())
                - (sphere.getSurroundSphere().getRadius()
                   + fixed_plane.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollision(sphere, fixed_plane, dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            collisions.emplace_back(col_dt_tp, sp_id, fpid);
          }
        }
        // check the surfs
        for (auto suit = all_surfs.begin(); suit != all_surfs.end(); ++suit) {
          const auto& sfid = suit.key();
          auto&       surf = suit.value();
          // Skip attached surfs
          if (sphere.m_attached_objects.contains(sfid)) continue;
          // Skip surfs with the surrounding sphere not intersecting with our
          // sphere
          if (blaze::length(sphere.getSurroundSphere().getCenter()
                            - surf.getSurroundSphere().getCenter())
                - (sphere.getSurroundSphere().getRadius()
                   + surf.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          const auto [col_dt_tp, col_status, normal]
            = algorithms::collision::detectCollisionSurf(sphere, surf, dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            collisions.emplace_back(col_dt_tp, sp_id, sfid);
            contact_normals.insert({sp_id, sfid}, normal);
          }
        }
        // check with all the spheres
        for (auto spit = all_spheres.begin(); spit != all_spheres.end();
             ++spit) {
          const auto& spp_id         = spit.key();
          auto&       sphere_passive = spit.value();
          // skip itself
          if (sp_id == spp_id) continue;
          // Skip attached fixed spheres
          if (sphere.m_attached_objects.contains(spp_id)) continue;
          if (sphere_passive.m_attached_objects.contains(sp_id)) continue;
          // Skip sphers with the surrounding sphere not intersecting with our
          // sphere's surrounding sphere
          if (blaze::length(sphere.getSurroundSphere().getCenter()
                            - sphere_passive.getSurroundSphere().getCenter())
                - (sphere.getSurroundSphere().getRadius()
                   + sphere_passive.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollisionSp(sphere, sphere_passive,
                                                       dt);
          // if collision -> add to the set of touch_objects
          if (col_status == collision::types::SpFPlStatus::Collision) {
            collisions.emplace_back(col_dt_tp, sp_id, spp_id);
          }
        }
      }
    };
  }   // namespace singularities_simple


}   // namespace rigidbodyaspect::algorithms
