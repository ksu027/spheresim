#ifndef RIGIDBODYASPECT_ALGORITHMS_MOVINGSPHERE_H
#define RIGIDBODYASPECT_ALGORITHMS_MOVINGSPHERE_H

#include "../geometry/rigidbodycontainer.h"

namespace rigidbodyaspect::algorithms::movingsphere
{

  std::pair<GM2Vector, GM2Vector>
  computeTrajectory(const rbtypes::Sphere& sphere, GM2Vector& Fc);

  void computeTrajectoryCacheProperties(rbtypes::Sphere& sphere, GM2Vector& Fc);
  void computeAdjustedSlidingTrajectoryCacheProperties(
    const Qt3DCore::QNodeId&                   sphere_id,
    const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
    RigidBodyContainer::SphereQHash& spheres, GM2Vector& Fc);

  void computeCacheProperties(
    const Qt3DCore::QNodeId&                   sphere_id,
    const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
    RigidBodyContainer::SphereQHash& spheres, GM2Vector& env_Forces,
    seconds_type dt_sec, bool adjust, bool revert);

  void computeSlidingTrajectory(
    const Qt3DCore::QNodeId&                   sphere_id,
    const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
    RigidBodyContainer::SphereQHash& spheres, GM2Vector& Fc);

}   // namespace rigidbodyaspect::algorithms::movingsphere





#endif   // RIGIDBODYASPECT_ALGORITHMS_MOVINGSPHERE_H
