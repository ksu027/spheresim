// for fps count addopted from https://www.kdab.com/writing-custom-qt-3d-aspect/
#ifndef RIGIDBODYASPECT_MOVINGAVERAGE_H
#define RIGIDBODYASPECT_MOVINGAVERAGE_H

#include <QtCore/qvector.h>

namespace rigidbodyaspect
{
  class MovingAverage {
  public:
    explicit MovingAverage(unsigned int samples = 3);

    void  addSample(float sample);
    float average() const;

  private:
    unsigned int   m_maxSampleCount;
    unsigned int   m_sampleCount;
    unsigned int   m_currentSample;
    float          m_total;
    QVector<float> m_samples;
  };
}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_MOVINGAVERAGE_H
