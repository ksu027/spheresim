#ifndef RIGIDBODYASPECT_ALGORITHMS_CONTACTPOINT_H
#define RIGIDBODYASPECT_ALGORITHMS_CONTACTPOINT_H

#include "../types.h"
#include "../geometry/rbtypes.h"
#include "../geometry/rigidbodycontainer.h"
#include "../utils.h"
#include "helpers.h"

// stl
#include <variant>

namespace rigidbodyaspect::algorithms
{

  namespace contact
  {
    QHash<Qt3DCore::QNodeId, QSet<contact::ContactPoint*>>
    getCPsOfNodeID(std::vector<ContactPoint>& collision_points);
    std::vector<ContactPoint>
    getVectorOfCPs(const RigidBodyContainer::SphereQHash&     all_spheres,
                   const RigidBodyContainer::FixedPlaneQHash& all_planes,
                   const RigidBodyContainer::SurfQHash&       all_surfs,
                   const QSet<Qt3DCore::QNodeId>&             curr_col_sp_ids,
                   const QSet<Qt3DCore::QNodeId>&             curr_col_pl_ids,
                   const QSet<Qt3DCore::QNodeId>&             curr_col_SU_ids,
                   const ContactNTtype&                       contact_normals);

    void addCP(const Qt3DCore::QNodeId&                   in_sphere_id,
               const RigidBodyContainer::SphereQHash&     all_spheres,
               const RigidBodyContainer::FixedPlaneQHash& all_planes,
               const RigidBodyContainer::SurfQHash&       all_surfs,
               const QSet<Qt3DCore::QNodeId>&             objects_to_check,
               QSet<std::pair<Qt3DCore::QNodeId, Qt3DCore::QNodeId>>&
                                              set_of_contact_pairs,
               QSet<Qt3DCore::QNodeId>&       spheres_to_check,
               const QSet<Qt3DCore::QNodeId>& spheres_checked,
               std::vector<ContactPoint>&     contact_points,
               const ContactNTtype&           contact_normals);
  }   // namespace contact

}   // namespace rigidbodyaspect::algorithms


#endif   // RIGIDBODYASPECT_ALGORITHMS_CONTACTPOINT_H
