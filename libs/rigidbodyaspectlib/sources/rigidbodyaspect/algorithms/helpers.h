#ifndef RIGIDBODYASPECT_ALGORITHMS_HELPERS_H
#define RIGIDBODYASPECT_ALGORITHMS_HELPERS_H

#include "../geometry/rigidbodycontainer.h"
#include "../constants.h"

#include <tuple>
#include <chrono>

namespace rigidbodyaspect
{
  class RigidBodyAspect;
}

namespace rigidbodyaspect::algorithms::helpers
{
  class RigidBodyAspect;
  // property cache compute driver
  void computeCacheProps(
    const Qt3DCore::QNodeId&                   sphere_id,
    const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
    RigidBodyContainer::SphereQHash& spheres, const seconds_type& dt,
    rigidbodyaspect::RigidBodyAspect& in_aspect, bool adjust, bool revert);

  GM2Vector                         nToPlane(const rbtypes::FixedPlane& plane);
  blaze::StaticMatrix<double, 3, 3> contactSpace(const GM2Vector& n);
  std::pair<GM2Vector, GM2Vector>
  findNDSpheres(const rbtypes::Sphere& act_sphere,
                const rbtypes::Sphere& pas_sphere);

  GM2Vector findClosePointOnPlane(const rbtypes::FixedPlane& plane,
                                  const GM2Vector&           point);

  bool isPlane(Qt3DCore::QNodeId                 in_id,
               rigidbodyaspect::RigidBodyAspect& in_aspect);
  bool isSphere(Qt3DCore::QNodeId                 in_id,
                rigidbodyaspect::RigidBodyAspect& in_aspect);
  bool isSurf(Qt3DCore::QNodeId                 in_id,
              rigidbodyaspect::RigidBodyAspect& in_aspect);
  QSet<Qt3DCore::QNodeId>
  getPlanes(const QSet<Qt3DCore::QNodeId>&    ids,
            rigidbodyaspect::RigidBodyAspect& in_aspect);
  QSet<Qt3DCore::QNodeId>
                          getSpheres(const QSet<Qt3DCore::QNodeId>&    ids,
                                     rigidbodyaspect::RigidBodyAspect& in_aspect);
  QSet<Qt3DCore::QNodeId> getSurfs(const QSet<Qt3DCore::QNodeId>&    ids,
                                   rigidbodyaspect::RigidBodyAspect& in_aspect);

  std::pair<GM2Vector, GM2Vector> closestPointSurf(const GM2Vector&     point,
                                                   const rbtypes::Surf& surf);

  std::tuple<GM2Vector, GM2Vector, double>
  intersectDsSurf(const rbtypes::Sphere& sphere, const GM2Vector& ds,
                  const rbtypes::Surf& surf);
}   // namespace rigidbodyaspect::algorithms::helpers


#endif   // RIGIDBODYASPECT_ALGORITHMS_HELPERS_H
