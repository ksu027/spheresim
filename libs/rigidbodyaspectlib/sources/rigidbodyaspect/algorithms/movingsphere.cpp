#include "movingsphere.h"
#include "helpers.h"
#include "sphere_fixedplane.h"
#include "Integrators.h"

namespace rigidbodyaspect::algorithms::movingsphere
{

  // The different integration method could be added here. Runge-kutta in
  // addition to the already existing Euler's.

  std::pair<GM2Vector, GM2Vector>
  computeTrajectory(const rbtypes::Sphere& sphere, GM2Vector& Fc)
  {
    // compute dynamics
    const auto dt = sphere.m_rem_dt.count();

    // in case of Exact formulas:
    const auto [dv, ds, a]
      = integrator::Exact(Fc, sphere.m_velocity, sphere.m_mass, dt);

    // in case of Euler:
    //    const auto [dv,ds,a] =
    //    integrator::Euler(Fc,sphere.m_velocity,sphere.m_mass,dt);

    // in case of Improved Euler:
    //    const auto [dv,ds,a] =
    //    integrator::ImprovedEuler(Fc,sphere.m_velocity,sphere.m_mass,dt);

    // in case of Runge-Kutta:
    //    const auto [dv,ds,a] =
    //    integrator::RungeKutta(Fc,sphere.m_velocity,sphere.m_mass,dt);

    return {ds, a};
  }


  void computeTrajectoryCacheProperties(rbtypes::Sphere& sphere, GM2Vector& Fc)
  {
    // unpacks the tuple
    std::tie(sphere.m_ds, sphere.m_a) = computeTrajectory(sphere, Fc);
  }

  void computeAdjustedSlidingTrajectoryCacheProperties(
    const Qt3DCore::QNodeId&                   sphere_id,
    const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
    RigidBodyContainer::SphereQHash& spheres, GM2Vector& Fc)
  {
    auto& sphere    = spheres[sphere_id];
    auto [ds, a]    = computeTrajectory(sphere, Fc);
    auto initial_ds = ds;
    // correct for each attached plane
    for (auto& afpl_id : sphere.m_attached_objects) {
      if (fixed_planes.contains(afpl_id)) {
        const auto& afpl = fixed_planes[afpl_id];
        const auto  n    = algorithms::helpers::nToPlane(afpl);
        const auto  sp_c = sphere.m_pos;
        const auto  p_mrk
          = ds + sp_c;   // center point of the sphere at the end of ds
        // Find Fixed Plane closest point
        const auto q = algorithms::helpers::findClosePointOnPlane(afpl, p_mrk);

        const auto r = sphere.m_radius;
        const auto d = (q - p_mrk) + (n * r);
        ds += d;
      }
      if (spheres.contains(afpl_id)) {
        const auto& sp2 = spheres[afpl_id];
        // vector between centers after ds
        auto d_s1_s2 = (sphere.m_pos + ds) - (sp2.m_pos + sp2.m_ds);
        auto penetration
          = blaze::length(d_s1_s2) - (sphere.m_radius + sp2.m_radius);
        // still penetrating -
        if (penetration < 0.0) {
          // if both are connected to each other -> adjust only one with the
          // higher mass
          if (sp2.m_attached_objects.contains(sphere_id)) {
            // do not adjust here, will be adjusted in another sphere
            // so go to the next attached object
            if (sphere.m_mass > sp2.m_mass) continue;
          }
          // otherwise adjust
          auto new_p_s1  = sphere.m_pos + ds - d_s1_s2 * penetration;
          auto new_ds_s1 = new_p_s1 - sphere.m_pos;
          ds             = new_ds_s1;
        }
        else {
          sphere.m_attached_objects.remove(afpl_id);
        }
      }
    }
    sphere.m_a  = a;
    sphere.m_ds = ds;
  }


  void computeSlidingTrajectory(
    const Qt3DCore::QNodeId&                   sphere_id,
    const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
    RigidBodyContainer::SphereQHash& spheres, GM2Vector& Fc)
  {
    const GM2Vector G = Fc;
    // based on collecting forces
    auto& sphere = spheres[sphere_id];
    auto  mu     = sphere.m_mu;

    // Here F is Gravity. We have to find the projection of gravity on the
    // plane.
    GM2Vector collect_Force{0.0, 0.0, 0.0};
    GM2Vector collect_G_n{0.0, 0.0, 0.0};
    GM2Vector collect_n{0.0, 0.0, 0.0};
    // correct for each attached plane
    for (auto& afpl_id : sphere.m_attached_objects) {
      GM2Vector G_on_plane{0.0, 0.0, 0.0};
      GM2Vector G_n{0.0, 0.0, 0.0};
      if (fixed_planes.contains(afpl_id)) {
        const auto& afpl = fixed_planes[afpl_id];
        const auto  n    = algorithms::helpers::nToPlane(afpl);
        G_n              = blaze::dot(G, n) / std::pow(blaze::length(n), 2) * n;
        G_on_plane       = G - G_n;
        collect_n += n;
      }
      collect_Force += G_on_plane;
      collect_G_n += G_n;
    }
    auto ds                 = computeTrajectory(sphere, collect_Force).first;
    spheres[sphere_id].m_ds = ds;
    spheres[sphere_id].m_a  = collect_Force / spheres[sphere_id].m_mass;

    collect_n = blaze::normalize(collect_n);
    if (sphere.m_state == MovingBodyState::Sliding) {
      // we can assume that the friction is in the opposite direction of DS
      GM2Vector wr  = blaze::cross(spheres[sphere_id].m_angular_velocity,
                                  -spheres[sphere_id].m_radius * collect_n);
      GM2Vector u_0 = spheres[sphere_id].m_velocity + wr;
      GM2Vector t   = -(u_0) / blaze::length(u_0);
      auto      friction_magnitude
        = mu * blaze::length(collect_G_n / spheres[sphere_id].m_mass);
      GM2Vector friction = friction_magnitude * t / sphere.m_radius;

      spheres[sphere_id].m_aa = blaze::cross(friction, collect_n);
      // here velocity supposed to be parallel to the contact plane, so assume
      // to use it

      GM2Vector u_1 = sphere.m_velocity + sphere.m_a * sphere.m_rem_dt.count()
                      + blaze::cross(sphere.m_angular_velocity,
                                     -spheres[sphere_id].m_radius * collect_n);
      // the direction is changed
      if (blaze::length(friction * sphere.m_rem_dt.count())
          > blaze::length(u_0) / 2.0) {
        friction    = -u_1 / sphere.m_rem_dt.count() / 2.0;
        sphere.m_aa = blaze::cross(friction, collect_n) / sphere.m_radius;
      }
      sphere.m_a += friction;
    }
    // if rolling - make angular acceleration same as liner
    else if (sphere.m_state == MovingBodyState::Rolling) {
      sphere.m_aa = blaze::cross(-sphere.m_a, collect_n) / sphere.m_radius;
    }
  }

  void computeCacheProperties(
    const Qt3DCore::QNodeId&                                    sphere_id,
    [[maybe_unused]] const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
    RigidBodyContainer::SphereQHash& spheres, GM2Vector& env_Forces,
    seconds_type dt, bool adjust, bool revert)
  {
    auto&      sphere     = spheres[sphere_id];
    const auto old_rem_dt = sphere.m_rem_dt;
    sphere.m_rem_dt       = dt - sphere.m_dt_tp;

    if (sphere.m_state == MovingBodyState::Free) {
      computeTrajectoryCacheProperties(sphere, env_Forces);
    }
    else if (sphere.m_state == MovingBodyState::Sliding
             or sphere.m_state == MovingBodyState::Rolling) {
      if (adjust)
        computeSlidingTrajectory(sphere_id, fixed_planes, spheres, env_Forces);
      else
        computeTrajectoryCacheProperties(sphere, env_Forces);
    }
    else if (sphere.m_state == MovingBodyState::AtRest) {
      sphere.m_ds = GM2Vector(0);
      sphere.m_a  = GM2Vector(0);
    }

    if (revert) {
      sphere.m_rem_dt = old_rem_dt;
    }
    sphere.getSurroundSphere().reSetCenter(sphere.m_pos);
    sphere.getSurroundSphere().reSetRadius(sphere.m_radius);
    integration::SurroundSphere ss_ds(sphere.m_pos + sphere.m_ds,
                                      sphere.m_radius);
    sphere.getSurroundSphere() += ss_ds;
  }


}   // namespace rigidbodyaspect::algorithms::movingsphere
