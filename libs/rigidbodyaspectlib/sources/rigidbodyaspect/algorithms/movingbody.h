#ifndef RIGIDBODYASPECT_ALGORITHMS_MOVINGBODY_H
#define RIGIDBODYASPECT_ALGORITHMS_MOVINGBODY_H

#include "../geometry/rigidbodycontainer.h"
#include "../geometry/movingbody.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void computeMovingBodyTrajectoryCache(MovingBody& body, const GM2Vector& Fc);
  void
  simulateMovingBody(rbtypes::Sphere& body, seconds_type sub_dt,
                     seconds_type                               dt,
                     const RigidBodyContainer::FixedPlaneQHash& fixed_planes,
                     const bool state_check = false);

}   // namespace rigidbodyaspect::algorithms::simulation


#endif   // RIGIDBODYASPECT_ALGORITHMS_MOVINGBODY_H
