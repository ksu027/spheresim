#include "sphere_sphere.h"
#include "../backend/rigidbodyaspect.h"

// stl
#include <iostream>
#include <chrono>
using namespace std::chrono_literals;



namespace rigidbodyaspect::algorithms
{

  namespace collision
  {
    detail::SphereSurfColliderReturnType
    detectCollisionSurf(const rbtypes::Sphere&        sphere,
                        const rbtypes::Surf&          surf,
                        [[maybe_unused]] seconds_type dt)
    {
      GM2Vector  n_fake{0.0, 0.0, 0.0};
      const auto dt_rem = dt - sphere.m_dt_tp;
      if (dt_rem.count() <= 0)
        return {0ms, detail::SphereFixedPlaneCollisionStatus::NoCollision,
                n_fake};

      const GM2Vector ds = sphere.m_ds;

      const auto [q, n_collision, t]
        = helpers::intersectDsSurf(sphere, ds, surf);

      if (t < constants::Zero or t > 1.0)
        return {0ms, detail::SphereFixedPlaneCollisionStatus::NoCollision,
                n_fake};

      return {sphere.m_dt_tp + (t * dt_rem),
              detail::SphereFixedPlaneCollisionStatus::Collision, n_collision};
    }
  }   // namespace collision

  namespace statechange
  {



  }   // namespace statechange


  namespace simulation
  {

  }

}   // namespace rigidbodyaspect::algorithms
