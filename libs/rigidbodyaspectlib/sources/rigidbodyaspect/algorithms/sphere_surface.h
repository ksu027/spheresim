#ifndef RIGIDBODYASPECT_ALGORITHMS_SPHERE_SURFACE_H
#define RIGIDBODYASPECT_ALGORITHMS_SPHERE_SURFACE_H

#include "../types.h"
#include "../geometry/rbtypes.h"
#include "../geometry/rigidbodycontainer.h"
#include "../utils.h"
#include "helpers.h"

// stl
#include <variant>

namespace rigidbodyaspect::algorithms
{

  namespace collision
  {
    detail::SphereSurfColliderReturnType
    detectCollisionSurf(const rbtypes::Sphere& sphere,
                        const rbtypes::Surf& surf, seconds_type dt);
  }   // namespace collision


  namespace statechange
  {

  }   // namespace statechange


  namespace simulation
  {

  }   // namespace simulation


  namespace singularities
  {



  }   // namespace singularities

}   // namespace rigidbodyaspect::algorithms


#endif   // RIGIDBODYASPECT_ALGORITHMS_SPHERE_SURFACE_H
