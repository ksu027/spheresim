#include "SSTree.h"
#include "../backend/rigidbodyaspect.h"
#include "sphere_surface.h"
#include "sphere_sphere.h"

// stl
#include <iostream>
#include <chrono>
using namespace std::chrono_literals;



namespace rigidbodyaspect::algorithms
{

  namespace contact
  {

    void
    TreeNode::getPossibleContacts(std::vector<PossibleContact>& contacts) const
    {
      if (isLeaf()) return;
      children.first->getPossibleContactsWith(children.second, contacts);
      children.first->getPossibleContacts(contacts);
      children.second->getPossibleContacts(contacts);
    }

    TreeNode* TreeNode::insert(Qt3DCore::QNodeId                  new_node_id,
                               const integration::SurroundSphere& new_sphere,
                               const bool                         moving,
                               collision::types::ColType          object_type)
    {
      // if we are a leaf -> insert here, make us with another guy children
      if (this->isLeaf()) {
        this->children.first = new TreeNode(this, this->sphere, this->node_id,
                                            this->moving_body, this->node_type);
        this->children.second
          = new TreeNode(this, new_sphere, new_node_id, moving, object_type);
        this->node_id = Qt3DCore::QNodeId();
        this->recalculateSurroundSphere();
        return this;
      }
      // otherwise, search which sphere would grow less and push somewhere
      // deeper there a new one
      else {
        if (children.first->sphere.getGrowth(new_sphere)
            < children.second->sphere.getGrowth(new_sphere)) {
          children.first->insert(new_node_id, new_sphere, moving, object_type);
        }
        else {
          children.second->insert(new_node_id, new_sphere, moving, object_type);
        }
      }
    }   // the return will always be called according to logic

    TreeNode::~TreeNode()
    {
      if (this->parent) {
        TreeNode* sibling = (parent->children.first == this)
                              ? parent->children.second
                              : parent->children.first;

        // parent should have the parameters of sibling
        parent->sphere      = sibling->sphere;
        parent->node_id     = sibling->node_id;
        parent->children    = sibling->children;
        parent->node_type   = sibling->node_type;
        parent->moving_body = sibling->moving_body;
        // children of the sibling should now point to the parent
        if (sibling->children.first)
          sibling->children.first->parent = this->parent;
        if (sibling->children.second)
          sibling->children.second->parent = this->parent;
        // clear sibling
        sibling->parent   = nullptr;
        sibling->children = {nullptr, nullptr};
        delete sibling;
        // recalc the parent's sphere
        parent->recalculateSurroundSphere();
      }
      if (children.first) {
        children.first->parent = nullptr;
        delete children.first;
      }
      if (children.second) {
        children.second->parent = nullptr;
        delete children.second;
      }
    }

    bool TreeNode::overlaps(const TreeNode* other) const
    {
      return this->sphere.intersectWith(other->sphere);
    }

    void TreeNode::getPossibleContactsWith(
      const TreeNode* other, std::vector<PossibleContact>& contacts) const
    {
      // we return number of possible contacts
      // if both are fixed bodies - > exit. We are not interested in the
      // intersections of the fixed bodies
      if (isLeaf() and other->isLeaf() and !this->moving_body
          and !this->moving_body)
        return;
      // if no intersection - > exit
      if (not this->overlaps(other)) return;

      // if both leafs - add both
      if (isLeaf() and other->isLeaf()) {
        contacts.push_back({{this->node_id, this->node_type},
                            {other->node_id, other->node_type}});
        return;
      }

      // go deeper in one which is not a leaf. If both are leafs - go to one
      // which is bigger
      if (other->isLeaf()
          or (!this->isLeaf()
              and this->sphere.getSize() >= other->sphere.getSize())) {
        children.first->getPossibleContactsWith(other, contacts);
        children.second->getPossibleContactsWith(other, contacts);
        return;
      }
      else {
        this->getPossibleContactsWith(other->children.first, contacts);
        this->getPossibleContactsWith(other->children.second, contacts);
        return;
      }
    }

    void TreeNode::printTree(int level)
    {
      int               child_num = 0;
      Qt3DCore::QNodeId id;
      if (this->children.first != nullptr) child_num++;
      if (this->children.second != nullptr) child_num++;

      if (this->isLeaf()) {
        id = this->node_id;
        qDebug() << "level" << level << "children" << child_num << "NodeId"
                 << id << "pos" << this->sphere.getCenter()[0]
                 << this->sphere.getCenter()[1] << this->sphere.getCenter()[2];
      }
      else
        qDebug() << "level" << level << "children" << child_num;

      level++;
      if (!this->isLeaf()) {
        if (this->children.first != nullptr)
          this->children.first->printTree(level);
        if (this->children.second != nullptr)
          this->children.second->printTree(level);
      }
      else
        return;
    }

    void TreeNode::recalculateSurroundSphere(bool recurse)
    {
      if (this->isLeaf()) return;

      this->sphere = children.first->sphere + children.second->sphere;

      if (recurse && parent) parent->recalculateSurroundSphere(true);
    }

    SpheresTree::SpheresTree(RigidBodyContainer& all_bodies)
    {
      auto&       spheres = all_bodies.spheres();
      const auto& planes  = all_bodies.fixedPlanes();
      const auto& surfs   = all_bodies.surfs();

      // The order of tree creation could affect the time spent on construction
      // especially with the multispheres
      // This is something could be checked
      // insert spheres
      for (auto i = spheres.begin(); i != spheres.end(); ++i) {
        auto  q_node_id = i.key();
        auto& sphere    = i.value();
        // we create a sphere surround the current sphere of the sphere and its
        // disposition
        integration::SurroundSphere ss = sphere.getSurroundSphere();
        integration::SurroundSphere ss_1(ss.getCenter() + sphere.m_ds,
                                         ss.getRadius());
        ss += ss_1;
        this->insert(q_node_id, ss, true, collision::types::ColType::Sp);
      }
      // insert planes
      for (auto i = planes.begin(); i != planes.end(); ++i) {
        auto        q_node_id = i.key();
        const auto& plane     = i.value();
        if (!this->multy_spheres) {
          integration::SurroundSphere ss = plane.getSurroundSphere();
          this->insert(q_node_id, ss, false, collision::types::ColType::Pl);
        }
        else {
          std::vector<integration::SurroundSphere> ssv
            = plane.getSurroundSphereS();
          for (auto& ss : ssv)
            this->insert(q_node_id, ss, false, collision::types::ColType::Pl);
        }
      }
      // insert surfs
      for (auto i = surfs.begin(); i != surfs.end(); ++i) {
        auto        q_node_id = i.key();
        const auto& surf      = i.value();
        if (!this->multy_spheres) {
          integration::SurroundSphere ss = surf.getSurroundSphere();
          this->insert(q_node_id, ss, false, collision::types::ColType::Su);
        }
        else {
          std::vector<integration::SurroundSphere> ssv
            = surf.getSurroundSphereS();
          for (auto& ss : ssv)
            this->insert(q_node_id, ss, false, collision::types::ColType::Su);
        }
      }
    }

    void SpheresTree::insert(Qt3DCore::QNodeId                  new_node_id,
                             const integration::SurroundSphere& new_sphere,
                             const bool                         moving,
                             collision::types::ColType          object_type)
    {
      if (root == nullptr) {
        root
          = new TreeNode(nullptr, new_sphere, new_node_id, moving, object_type);
        leafs.insert(new_node_id, root);
      }
      else {
        auto new_node
          = root->insert(new_node_id, new_sphere, moving, object_type);
        leafs.insert(new_node->children.first->node_id,
                     new_node->children.first);
        leafs.insert(new_node->children.second->node_id,
                     new_node->children.second);
      }
    }

    std::vector<PossibleContact> SpheresTree::getContactsRoot() const
    {
      // parse tree from root to leafs and create a list of possible contacts
      std::vector<PossibleContact> possible_contacts;
      root->getPossibleContacts(possible_contacts);
      return possible_contacts;
    }

    std::vector<PossibleContact> SpheresTree::getContactsOneByOne() const
    {
      // check each leaf with a root and then deeper
      std::vector<PossibleContact> possible_contacts;
      for (auto& leaf : leafs) {
        leaf->getPossibleContactsWith(root, possible_contacts);
      }
      return possible_contacts;
    }

    void SpheresTree::removeLeaf(Qt3DCore::QNodeId node_id_remove)
    {
      // remove node as in removing node but also change refferenceto the
      // sibling
      auto& delete_node = leafs[node_id_remove];

      // change the pointers in the qhash table
      if (delete_node->parent) {
        TreeNode* sibling = (delete_node->parent->children.first == delete_node)
                              ? delete_node->parent->children.second
                              : delete_node->parent->children.first;
        leafs[sibling->node_id] = sibling->parent;
      }
      delete delete_node;
      leafs.remove(node_id_remove);
    }


    SpheresTree::~SpheresTree() { delete root; }


    void predictCollisions(SpheresTree&                  tree,
                           std::vector<PossibleContact>& possible_collisions,
                           bool                          from_root)
    {
      if (from_root)
        possible_collisions = tree.getContactsRoot();
      else
        possible_collisions = tree.getContactsOneByOne();
    }

    void detectCollisionsTree(
      SpheresTree& tree,
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                collision_sets,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      std::vector<PossibleContact> possible_collisions;
      algorithms::contact::predictCollisions(tree, possible_collisions, false);
      detectCollisionsBase(collision_sets, possible_collisions, all_bodies,
                           contact_normals, dt);
    }

    void detectCollisionsTreeInner(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                     collision_sets,
      const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids, SpheresTree& current_tree,
      RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      std::vector<PossibleContact> new_possible_collisions;
      // first we delete references to current ids
      for (auto sp_id : curr_col_sp_ids) {
        current_tree.removeLeaf(sp_id);
      }
      // second we insert new surround spheres in the tree
      for (auto sp_id : curr_col_sp_ids) {
        integration::SurroundSphere ss
          = all_bodies.spheres()[sp_id].getSurroundSphere();
        integration::SurroundSphere ss_ds(all_bodies.spheres()[sp_id].m_pos
                                            + all_bodies.spheres()[sp_id].m_ds,
                                          all_bodies.spheres()[sp_id].m_radius);
        ss += ss_ds;
        current_tree.insert(sp_id, ss, true, collision::types::ColType::Sp);
      }
      // now search for possible collisions of the spheres of the current
      // collison
      for (auto sp_id : curr_col_sp_ids) {
        current_tree.leafs[sp_id]->getPossibleContactsWith(
          current_tree.root, new_possible_collisions);
      }
      detectCollisionsBase(collision_sets, new_possible_collisions, all_bodies,
                           contact_normals, dt);
    }

    void detectCollisionsBase(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                    collision_sets,
      std::vector<PossibleContact>& possible_collisions,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      auto& spheres = all_bodies.spheres();
      auto& planes  = all_bodies.fixedPlanes();
      auto& surfs   = all_bodies.surfs();

      for (auto possible_collision : possible_collisions) {
        QSet<Qt3DCore::QNodeId> touching_objects;
        seconds_type            min_col_time = 300ms;

        auto [sp_id, obj_id, col_type]
          = possible_collision.first.second == collision::types::ColType::Sp
              ? std::make_tuple(possible_collision.first.first,
                                possible_collision.second.first,
                                possible_collision.second.second)
              : std::make_tuple(possible_collision.second.first,
                                possible_collision.first.first,
                                possible_collision.first.second);
        if (spheres[sp_id].m_attached_objects.contains(obj_id)) continue;
        switch (col_type) {
          case collision::types::ColType::Pl: {
            const auto [col_dt_tp, col_status]
              = algorithms::collision::detectCollision(spheres[sp_id],
                                                       planes[obj_id], dt);
            if (col_status == collision::types::SpFPlStatus::Collision) {
              singularities::objToColTouchObjs(col_dt_tp, min_col_time, obj_id,
                                               touching_objects);
            }
            break;
          }
          case collision::types::ColType::Su: {
            const auto [col_dt_tp, col_status, normal]
              = algorithms::collision::detectCollisionSurf(spheres[sp_id],
                                                           surfs[obj_id], dt);
            if (col_status == collision::types::SpFPlStatus::Collision) {
              singularities::objToColTouchObjs(col_dt_tp, min_col_time, obj_id,
                                               touching_objects);
              contact_normals.insert({sp_id, obj_id}, normal);
            }
            break;
          }
          case collision::types::ColType::Sp: {
            if (spheres[obj_id].m_attached_objects.contains(sp_id)) continue;
            const auto [col_dt_tp, col_status]
              = algorithms::collision::detectCollisionSp(spheres[sp_id],
                                                         spheres[obj_id], dt);
            if (col_status == collision::types::SpFPlStatus::Collision) {
              singularities::objToColTouchObjs(col_dt_tp, min_col_time, obj_id,
                                               touching_objects);
            }
            break;
          }
        }
        if (touching_objects.size() > 0) {
          touching_objects.insert(sp_id);
          collision_sets.emplace_back(min_col_time, touching_objects);
        }
      }
    }

    void detectCollisionsBruteSphere(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                collision_sets,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      const auto& spheres = all_bodies.spheres();
      const auto& planes  = all_bodies.fixedPlanes();
      const auto& surfs   = all_bodies.surfs();
      auto        n       = 0;
      for (auto sit = spheres.begin(); sit != spheres.end(); ++sit) {
        const auto&             sid    = sit.key();
        const auto&             sphere = sit.value();
        QSet<Qt3DCore::QNodeId> touching_objects;
        seconds_type            min_col_time = 300ms;
        // check the planes
        for (auto fpit = planes.begin(); fpit != planes.end(); ++fpit) {
          const auto& fpid        = fpit.key();
          const auto& fixed_plane = fpit.value();
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(fpid)) continue;
          // Skip planes with the surrounding sphere not intersecting with our
          // sphere
          if (blaze::length(sphere.m_pos
                            - fixed_plane.getSurroundSphere().getCenter())
                - (sphere.m_radius
                   + fixed_plane.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          n++;
          // Make exact collision detection
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollision(sphere, fixed_plane, dt);
          // if collision -> add to the set of touch_objects
          if (col_status == collision::types::SpFPlStatus::Collision) {
            // insert only those with the smaller time
            singularities::objToColTouchObjs(col_dt_tp, min_col_time, fpid,
                                             touching_objects);
          }
        }
        // check the SURFS
        for (auto sfit = surfs.begin(); sfit != surfs.end(); ++sfit) {
          const auto& sfid = sfit.key();
          const auto& surf = sfit.value();
          // Skip attached fixed surfs
          if (sphere.m_attached_objects.contains(sfid)) continue;
          // Skip surfs with the surrounding sphere not intersecting with our
          // sphere
          if (blaze::length(sphere.m_pos - surf.getSurroundSphere().getCenter())
                - (sphere.m_radius + surf.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          n++;
          // Make exact collision detection
          const auto [col_dt_tp, col_status, normal]
            = algorithms::collision::detectCollisionSurf(sphere, surf, dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            // insert only those with the smaller time
            singularities::objToColTouchObjs(col_dt_tp, min_col_time, sfid,
                                             touching_objects);
            contact_normals.insert({sid, sfid}, normal);
          }
        }
        // check the SPHERES
        for (auto spit = spheres.begin(); spit != spheres.end(); ++spit) {
          const auto& spid           = spit.key();
          const auto& sphere_passive = spit.value();
          // skip itself
          if (sid == spid) continue;
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(spid)) continue;
          if (sphere_passive.m_attached_objects.contains(sid)) continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollisionSp(sphere, sphere_passive,
                                                       dt);
          // if collision -> add to the set of touch_objects
          if (col_status == collision::types::SpFPlStatus::Collision) {
            // insert only those with the smaller time
            singularities::objToColTouchObjs(col_dt_tp, min_col_time, spid,
                                             touching_objects);
          }
        }
        // if there were found collision points - add the collision to the
        // vector
        if (touching_objects.size() > 0) {
          touching_objects.insert(sid);
          collision_sets.emplace_back(min_col_time, touching_objects);
        }
      }
    }

    void detectCollisionsBruteSphereInner(
      std::vector<std::pair<seconds_type, QSet<Qt3DCore::QNodeId>>>&
                                     collision_sets,
      const QSet<Qt3DCore::QNodeId>& curr_col_sp_ids,
      const RigidBodyContainer& all_bodies, ContactNTtype& contact_normals,
      seconds_type dt)
    {
      const auto& all_spheres = all_bodies.spheres();
      const auto& all_planes  = all_bodies.fixedPlanes();
      const auto& all_surfs   = all_bodies.surfs();
      // search for the collisions for all the spheres in the givven collision
      for (auto& sp_id : curr_col_sp_ids) {
        QSet<Qt3DCore::QNodeId> touching_objects;
        seconds_type            min_col_time = 300ms;
        const auto&             sphere       = all_spheres[sp_id];
        // check with all the planes
        for (auto fpit = all_planes.begin(); fpit != all_planes.end(); ++fpit) {
          const auto& fpid        = fpit.key();
          const auto& fixed_plane = fpit.value();
          // Skip attached fixed planes
          if (sphere.m_attached_objects.contains(fpid)) continue;
          // Skip planes with the surrounding sphere not intersecting with our
          // sphere
          if (blaze::length(sphere.m_pos
                            - fixed_plane.getSurroundSphere().getCenter())
                - (sphere.m_radius
                   + fixed_plane.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollision(sphere, fixed_plane, dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            singularities::objToColTouchObjs(col_dt_tp, min_col_time, fpid,
                                             touching_objects);
          }
        }
        // check the surfs
        for (auto suit = all_surfs.begin(); suit != all_surfs.end(); ++suit) {
          const auto& sfid = suit.key();
          const auto& surf = suit.value();
          // Skip attached surfs
          if (sphere.m_attached_objects.contains(sfid)) continue;
          // Skip surfs with the surrounding sphere not intersecting with our
          // sphere
          if (blaze::length(sphere.m_pos - surf.getSurroundSphere().getCenter())
                - (sphere.m_radius + surf.getSurroundSphere().getRadius())
              > constants::Zero)
            continue;
          const auto [col_dt_tp, col_status, normal]
            = algorithms::collision::detectCollisionSurf(sphere, surf, dt);
          if (col_status == collision::types::SpFPlStatus::Collision) {
            singularities::objToColTouchObjs(col_dt_tp, min_col_time, sfid,
                                             touching_objects);
            contact_normals.insert({sp_id, sfid}, normal);
          }
        }
        // check with all the spheres
        for (auto spit = all_spheres.begin(); spit != all_spheres.end();
             ++spit) {

          const auto& spp_id         = spit.key();
          const auto& sphere_passive = spit.value();
          // skip itself
          if (sp_id == spp_id) continue;
          // Skip attached fixed spheres
          if (sphere.m_attached_objects.contains(spp_id)) continue;
          if (sphere_passive.m_attached_objects.contains(sp_id)) continue;
          const auto [col_dt_tp, col_status]
            = algorithms::collision::detectCollisionSp(sphere, sphere_passive,
                                                       dt);
          // if collision -> add to the set of touch_objects
          if (col_status == collision::types::SpFPlStatus::Collision) {
            singularities::objToColTouchObjs(col_dt_tp, min_col_time, spp_id,
                                             touching_objects);
          }
        }
        // if there were found collision points - add the collision to the
        // vector
        if (touching_objects.size() > 0) {
          touching_objects.insert(sp_id);
          collision_sets.emplace_back(min_col_time, touching_objects);
        }
      }
    }

  }   // namespace contact

}   // namespace rigidbodyaspect::algorithms
