
import Qt3D.Core 2.0

import com.uit.GMlib2QtIntegration 1.0 as GM2
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects


GM2.SceneObject {


    RB.Environment {
      id: rba_environment
    }

    //surface
    Objects.MySurf {
    environment: rba_environment
    scale:  Qt.vector2d(3,3)
    translateGlobal: Qt.vector3d(0,0,0)
    }

    //sides
    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(4,0,30)
      uAxis: Qt.vector3d(28,40,0)
      vAxis: Qt.vector3d(0,0,-60)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-4,0,-30)
      uAxis: Qt.vector3d(-28,40,0)
      vAxis: Qt.vector3d(0,0,60)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-30,0,4)
      uAxis: Qt.vector3d(0,40,32)
      vAxis: Qt.vector3d(60,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(30,0,-6)
      uAxis: Qt.vector3d(0,40,-32)
      vAxis: Qt.vector3d(-60,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    // ground
    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-8,3,-8)
      uAxis:  Qt.vector3d(0,0,16)
      vAxis:  Qt.vector3d(16,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    // spheres
    // first raw
    NodeInstantiator {
        id: sphere_grid

        property int rows: 30
        property int columns: 30

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-16.45 + x*(1.1),20,-16.45 + y*(1.1))
        }
    }

    // second raw
    NodeInstantiator {
        id: sphere_grid_2

        property int rows: 30
        property int columns: 30

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_2.columns
          property int y: index % sphere_grid_2.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-16.45 + x*(1.1),21.5,-16.45 + y*(1.1))
        }
    }

    // third raw
    NodeInstantiator {
        id: sphere_grid_3

        property int rows: 30
        property int columns: 30

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_3.columns
          property int y: index % sphere_grid_3.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-16.45 + x*(1.1),23,-16.45 + y*(1.1))
        }
    }

    // fourth raw
    NodeInstantiator {
        id: sphere_grid_4

        property int rows: 30
        property int columns: 30

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_4.columns
          property int y: index % sphere_grid_4.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-16.45 + x*(1.1),24.5,-16.45 + y*(1.1))
        }
    }

    // fifth raw
    NodeInstantiator {
        id: sphere_grid_5

        property int rows: 30
        property int columns: 30

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_5.columns
          property int y: index % sphere_grid_5.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-16.45 + x*(1.1),26,-16.45 + y*(1.1))
        }
    }

    // sixth raw
    NodeInstantiator {
        id: sphere_grid_6

        property int rows: 20
        property int columns: 20

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_6.columns
          property int y: index % sphere_grid_6.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-10.95 + x*(1.1),27.5,-10.95 + y*(1.1))
        }
    }

    // seventh raw
    NodeInstantiator {
        id: sphere_grid_7

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_7.columns
          property int y: index % sphere_grid_7.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-5.45 + x*(1.1),29,-5.45 + y*(1.1))
        }
    }

}
