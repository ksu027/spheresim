
import Qt3D.Core 2.0

import com.uit.GMlib2QtIntegration 1.0 as GM2
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects


GM2.SceneObject {


    RB.Environment {
      id: rba_environment
    }

    //surface
    Objects.MySurf {
    environment: rba_environment
    scale:  Qt.vector2d(3,3)
    translateGlobal: Qt.vector3d(0,0,0)
    }

    //sides
    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(4,0,20)
      uAxis: Qt.vector3d(14,20,0)
      vAxis: Qt.vector3d(0,0,-40)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-4,0,-20)
      uAxis: Qt.vector3d(-14,20,0)
      vAxis: Qt.vector3d(0,0,40)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-20,0,4)
      uAxis: Qt.vector3d(0,20,16)
      vAxis: Qt.vector3d(40,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(20,0,-6)
      uAxis: Qt.vector3d(0,20,-16)
      vAxis: Qt.vector3d(-40,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    // ground
    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-8,3,-8)
      uAxis:  Qt.vector3d(0,0,16)
      vAxis:  Qt.vector3d(16,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    // spheres
    // first raw
    NodeInstantiator {
        id: sphere_grid

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-7.25 + x*(1.5),8,-7.25 + y*(1.5))
        }
    }
    // second raw
    NodeInstantiator {
        id: sphere_grid_2

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_2.columns
          property int y: index % sphere_grid_2.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-7.25 + x*(1.5),10,-7.25 + y*(1.5))
        }
    }

    // third raw
    NodeInstantiator {
        id: sphere_grid_3

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_3.columns
          property int y: index % sphere_grid_3.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-7.25 + x*(1.5),12,-7.25 + y*(1.5))
        }
    }

    // fourth raw
    NodeInstantiator {
        id: sphere_grid_4

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_4.columns
          property int y: index % sphere_grid_4.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-7.25 + x*(1.5),14,-7.25 + y*(1.5))
        }
    }

    // fifth raw
    NodeInstantiator {
        id: sphere_grid_5

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_5.columns
          property int y: index % sphere_grid_5.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-7.25 + x*(1.5),16,-7.25 + y*(1.5))
        }
    }
}
