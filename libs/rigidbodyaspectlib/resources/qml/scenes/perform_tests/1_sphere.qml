
import Qt3D.Core 2.0

import com.uit.GMlib2QtIntegration 1.0 as GM2
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects


GM2.SceneObject {

    RB.Environment {
      id: rba_environment
    }

    //surface
    Objects.MySurf {
    environment: rba_environment
    scale:  Qt.vector2d(3,3)
    translateGlobal: Qt.vector3d(0,0,0)
    }

    //sides
    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(4,0,20)
      uAxis: Qt.vector3d(14,20,0)
      vAxis: Qt.vector3d(0,0,-40)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-4,0,-20)
      uAxis: Qt.vector3d(-14,20,0)
      vAxis: Qt.vector3d(0,0,40)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-20,0,4)
      uAxis: Qt.vector3d(0,20,16)
      vAxis: Qt.vector3d(40,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(20,0,-6)
      uAxis: Qt.vector3d(0,20,-16)
      vAxis: Qt.vector3d(-40,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    // ground
    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-8,3,-8)
      uAxis:  Qt.vector3d(0,0,16)
      vAxis:  Qt.vector3d(16,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    //spheres
    Objects.MyBall {
      environment: rba_environment
      radius: 0.5
      initialMass: 0.5
      translateGlobal: Qt.vector3d(-0.5,10,-0.5)
    }
}
