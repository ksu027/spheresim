
import Qt3D.Core 2.0

import com.uit.GMlib2QtIntegration 1.0 as GM2
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects


GM2.SceneObject {


    RB.Environment {
      id: rba_environment
    }

    // ground
    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-10,0,-10)
      uAxis:  Qt.vector3d(0,0,20)
      vAxis:  Qt.vector3d(20,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    // spheres
    // first raw
    NodeInstantiator {
        id: sphere_grid

        property int rows: 25
        property int columns: 40

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.2
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-6.2 + x*(0.5),35,-9.75 + y*(0.5))
        }
    }


}
