
import Qt3D.Core 2.0

import com.uit.GMlib2QtIntegration 1.0 as GM2
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects


GM2.SceneObject {


    RB.Environment {
      id: rba_environment
    }

    // ground
    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-10,0,-10)
      uAxis:  Qt.vector3d(0,0,20)
      vAxis:  Qt.vector3d(20,0,0)
      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    // spheres
    // first raw
    NodeInstantiator {
        id: sphere_grid

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-5.45 + x*(1.1),30,-5.45 + y*(1.1))
        }
    }
    // second raw
    NodeInstantiator {
        id: sphere_grid_2

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_2.columns
          property int y: index % sphere_grid_2.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-5.45 + x*(1.1),32,-5.45 + y*(1.1))
        }
    }

    // third raw
    NodeInstantiator {
        id: sphere_grid_3

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_3.columns
          property int y: index % sphere_grid_3.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-5.45 + x*(1.1),34,-5.45 + y*(1.1))
        }
    }

    // fourth raw
    NodeInstantiator {
        id: sphere_grid_4

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_4.columns
          property int y: index % sphere_grid_4.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-5.45 + x*(1.1),36,-5.45 + y*(1.1))
        }
    }

    // fifth raw
    NodeInstantiator {
        id: sphere_grid_5

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid_5.columns
          property int y: index % sphere_grid_5.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-5.45 + x*(1.1),38,-5.45 + y*(1.1))
        }
    }
}
