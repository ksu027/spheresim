
import Qt3D.Core 2.0

import com.uit.GMlib2QtIntegration 1.0 as GM2
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects


GM2.SceneObject {

    RB.Environment {
      id: rba_environment
    }

    //surface
    Objects.MySurf {
    environment: rba_environment
    scale:  Qt.vector2d(3,3)
    translateGlobal: Qt.vector3d(0,0,0)
    }


    //spheres
    Objects.MyBall {
      environment: rba_environment
      radius: 0.2
      initialMass: 0.5
      translateGlobal: Qt.vector3d(0,35,0)
    }
}
