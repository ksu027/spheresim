import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10

import com.uit.GMlib2QtIntegration 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects

SceneObject {

  property var activeEnvironment : null

  Objects.MyGround {
    environment: activeEnvironment

    origin: Qt.vector3d(-10,0,-10)
    uAxis: Qt.vector3d(0,0,20)
    vAxis: Qt.vector3d(20,0,0)

    initialFriction: 0.5

    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }

  Objects.MyBall {
    environment: activeEnvironment

    radius: 0.5

    initialMass: 1
    initialFriction: 1
    initialVelocity: Qt.vector3d(0,0,0)

    translateGlobal: Qt.vector3d(-3,5,0)
  }

  Objects.MyBall {
    environment: activeEnvironment

    radius: 1.0

    initialMass: 2
    initialFriction: 2
    initialVelocity: Qt.vector3d(0,0,0)

    translateGlobal: Qt.vector3d(2.5,5,-2.5)
  }


  Objects.MyBall {
    environment: activeEnvironment

    radius: 1.5

    initialMass: 3
    initialFriction: 3
    initialVelocity: Qt.vector3d(0,0,0)

    translateGlobal: Qt.vector3d(2.5,5,2.5)
  }


  Objects.MyBall {
    environment: activeEnvironment

    radius: 2.0

    initialMass: 4
    initialFriction: 4
    initialVelocity: Qt.vector3d(0,0,0)

    translateGlobal: Qt.vector3d(-2.5,5,2.5)
  }
}
