import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10

//import com.uit.GMlib2Qt 1.0 as GM2Qt
//import com.uit.STE6245 1.0 as App
import com.uit.GMlib2QtIntegration 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects

SceneObject {

  RB.Environment {
    id: env
    gravity: Qt.vector3d(0,0,0)
  }

  // inner walls
  Objects.MyGround {
    environment: env
    origin: Qt.vector3d(-5,0,5)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(10,0,0)
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }
  Objects.MyGround {
    environment: env
    origin: Qt.vector3d(5,0,0)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(0,0,-10)
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }
  Objects.MyGround {
    environment: env
    origin: Qt.vector3d(5,0,-5)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(0,0,-10)
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }
  Objects.MyGround {
    environment: env
    origin: Qt.vector3d(-5,0,5)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(0,0,10)
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }

  // outer walls
  Objects.MyGround {
    environment: env
    origin: Qt.vector3d(-10,0,10)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(20,0,0)
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }
  Objects.MyGround {
    environment: env
    origin: Qt.vector3d(10,0,10)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(0,0,-20)
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }
  Objects.MyGround {
    environment: env
    origin: Qt.vector3d(10,0,-10)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(-20,0,0)
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }
  Objects.MyGround {
    environment: env
    origin: Qt.vector3d(-10,0,-10)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(0,0,20)
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }


  Objects.MyBall {
    environment: env
    initialVelocity: Qt.vector3d(0,0,5)
    translateGlobal: Qt.vector3d(2,2,2)
  }

}
