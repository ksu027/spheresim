
import Qt3D.Core 2.0

import com.uit.GMlib2QtIntegration 1.0 as GM2
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects

GM2.SceneObject {

    RB.Environment {
      id: rba_environment
    }

    Objects.MySurf {
    environment: rba_environment

    scale:  Qt.vector2d(3,3)
    //translateGlobal: Qt.vector3d(-5,0,-5)
    //fake translation to fix location in the backend
    //this translation is only of the visual part
    translateGlobal: Qt.vector3d(0,0,0)
    }


    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(0,0,10)
      uAxis: Qt.vector3d(9,10,0)
      vAxis: Qt.vector3d(0,0,-20)

      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }


    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(0,0,-10)
      uAxis: Qt.vector3d(-9,10,0)
      vAxis: Qt.vector3d(0,0,20)

      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }


    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(-10,0,0)
      uAxis: Qt.vector3d(0,10,11)
      vAxis: Qt.vector3d(20,0,0)

      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }

    Objects.MyGround {
      environment: rba_environment
      origin: Qt.vector3d(10,0,0)
      uAxis: Qt.vector3d(0,10,-11)
      vAxis: Qt.vector3d(-20,0,0)

      //fake translation to fix location in the backend
      translateGlobal: Qt.vector3d(0,0,0)
    }


    Objects.MyBall {
      environment: rba_environment

      radius: 0.5
      initialMass: 0.5
      initialVelocity: Qt.vector3d(0.0,0.0,5)
      translateGlobal: Qt.vector3d(1,8,0)
    }

}
