import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10

import com.uit.GMlib2QtIntegration 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects

SceneObject {

  RB.Environment {
    id: neg_grav_env
    gravity: Qt.vector3d(0,-1,0)
  }

  Objects.MyGround {
    environment: neg_grav_env

    origin: Qt.vector3d(-10,0,-10)
    uAxis: Qt.vector3d(0,0,0)
    vAxis: Qt.vector3d(20,4,0)

    initialFriction: 0.5
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }


  Objects.MyGround {

    origin: Qt.vector3d(8,-10,-10)
    uAxis: Qt.vector3d(0,0,20)
    vAxis: Qt.vector3d(20,0,0)

    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }

  Objects.MyBall {
    environment: neg_grav_env

    radius: 0.3

    initialVelocity: Qt.vector3d(0,0,0)

    translateGlobal: Qt.vector3d(8,4.1,0)
  }
}
