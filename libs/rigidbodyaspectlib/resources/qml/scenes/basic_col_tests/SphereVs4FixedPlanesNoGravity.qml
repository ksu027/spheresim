import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10

import com.uit.GMlib2QtIntegration 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "../../objects" as Objects

SceneObject {

  RB.Environment {
    id: env
    gravity: Qt.vector3d(0,0,0)
  }

  Objects.MyGround {
    environment: env

    origin: Qt.vector3d(-10,0,10)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(20,0,0)

    initialFriction: 0.5
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }

  Objects.MyGround {
    environment: env

    origin: Qt.vector3d(1,0,10)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(0,0,-20)

    initialFriction: 0.5
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }

  Objects.MyGround {
    environment: env

    origin: Qt.vector3d(10,0,-10)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(-20,0,0)

    initialFriction: 0.9
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }

  Objects.MyGround {
    environment: env

    origin: Qt.vector3d(-1,0,-10)
    uAxis: Qt.vector3d(0,4,0)
    vAxis: Qt.vector3d(0,0,20)

    initialFriction: 0.9
    //fake translation to fix location in the backend
    translateGlobal: Qt.vector3d(0,0,0)
  }

  Objects.MyBall {
    environment: env

    radius: 0.5

    initialMass: 1
    initialFriction: 0.1
    initialVelocity: Qt.vector3d(1700,0,0)

    translateGlobal: Qt.vector3d(0,2,2)
  }

}
