
import Qt3D.Core 2.0

import com.uit.GMlib2QtIntegration 1.0 as GM2
import com.uit.STE6245.RigidBody 1.0 as RB

import "../objects" as Objects


GM2.SceneObject {


    RB.Environment {
      id: rba_environment
      gravity: Qt.vector3d(0,0,0)
    }


    Objects.MyBall {
      environment: rba_environment
      radius: 1.5
      initialMass: 0.5
      initialVelocity: Qt.vector3d(-20.0,0,0.0)
      translateGlobal: Qt.vector3d(10,5,0)
    }




    // spheres
    // first raw
    NodeInstantiator {
        id: sphere_grid

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),0,-4.95 + y*(1.1))
        }
    }


    NodeInstantiator {
        id: sphere_grid_1

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),1.1,-4.95 + y*(1.1))
        }
    }

    NodeInstantiator {
        id: sphere_grid_2

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),2.2,-4.95 + y*(1.1))
        }
    }

    NodeInstantiator {
        id: sphere_grid_3

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),3.3,-4.95 + y*(1.1))
        }
    }

    NodeInstantiator {
        id: sphere_grid_4

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),4.4,-4.95 + y*(1.1))
        }
    }

    NodeInstantiator {
        id: sphere_grid_5

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),5.5,-4.95 + y*(1.1))
        }
    }

    NodeInstantiator {
        id: sphere_grid_6

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),6.6,-4.95 + y*(1.1))
        }
    }

    NodeInstantiator {
        id: sphere_grid_7

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),7.7,-4.95 + y*(1.1))
        }
    }

    NodeInstantiator {
        id: sphere_grid_8

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),8.8,-4.95 + y*(1.1))
        }
    }

    NodeInstantiator {
        id: sphere_grid_9

        property int rows: 10
        property int columns: 10

        model: rows * columns

        Objects.MyBall {
          property int x: index / sphere_grid.columns
          property int y: index % sphere_grid.columns

          environment: rba_environment
          radius: 0.5
          initialMass: 0.5
          translateGlobal: Qt.vector3d(-4.95 + x*(1.1),9.9,-4.95 + y*(1.1))
        }
    }



}
