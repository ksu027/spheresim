import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

import QtQuick.Window 2.12

// Qt3D
import Qt3D.Core 2.12 as Q3DCore

import com.uit.STE6245.RigidBody 1.0 as RB

// Scenarios examples
import "qrc:///lib_examples_qml/scenarios/SingleCameraWithOverlay"
import "qrc:///lib_rb_qml/scenarios"


ApplicationWindow { id: app_window

  default property alias content : scenario.content
  signal toggleGroundGridOverlay;
  property var app_title : "Simulator"

  visible: true

  title: app_title

  function initResolution(screen) {
    if(screen.desktopAvailableWidth < 800 ||
        screen.desktopAvailableHeight < 600 )
      return Qt.size(640,480)
    else if(screen.desktopAvailableWidth < 1024 ||
            screen.desktopAvailableHeight < 768 )
      return Qt.size(800,600)
    else
      return Qt.size(1024,768)
  }

  function initGeometry() {

    var s = Screen
    s.x = 0
    s.y = 0

    var init_res = initResolution(s)

    var rect = Qt.rect(
          (s.virtualX + (s.desktopAvailableWidth - init_res.width)) / 2,
          (s.virtualY + (s.desktopAvailableHeight - init_res.height)) / 2,
          init_res.width, init_res.height);
    console.debug("init rect: " + rect)
    return rect
  }

  function updateGravity() {
    var g = Qt.vector3d(g_x.value,g_y.value,g_z.value)
    scenario.rba_environment.gravity = g
  }

  Component.onCompleted: {
    var geom = initGeometry()
    x      = geom.x
    y      = geom.y
    width  = geom.width
    height = geom.height
  }

  header: ToolBar {
    RowLayout {
      anchors.fill: parent
      Item { Layout.fillHeight: true; Layout.fillWidth: true}

      Text { font.bold: true; text: "Scenes" }
      ComboBox {
        Layout.leftMargin: 10; Layout.rightMargin: 5
        Layout.minimumWidth: 500
        model: ListModel {
            ListElement { text: "-- clear --" }
            ListElement { text: "GroundBall" }
// visualization test
            ListElement { text: "shoot_1000_spheres" }
            ListElement { text: "basic_col_tests/CheckRotation" }
            ListElement { text: "basic_col_tests/MultiCollide" }
            ListElement { text: "basic_col_tests/MultiSlide" }
// performance tests
            ListElement { text: "perform_tests/1_sphere" }
            ListElement { text: "perform_tests/10_spheres" }
            ListElement { text: "perform_tests/100_spheres" }
            ListElement { text: "perform_tests/500_spheres" }
            ListElement { text: "perform_tests/1000_spheres" }
            ListElement { text: "perform_tests/5000_spheres" }
            ListElement { text: "perform_tests/10000_spheres" }
// surround sphere tests
            ListElement { text: "surround_sphere_test/plane_1_sphere" }
            ListElement { text: "surround_sphere_test/plane_100_spheres" }
            ListElement { text: "surround_sphere_test/plane_500_spheres" }
            ListElement { text: "surround_sphere_test/plane_1000_spheres" }
            ListElement { text: "surround_sphere_test/surf_1_sphere" }
            ListElement { text: "surround_sphere_test/surf_100_spheres" }
            ListElement { text: "surround_sphere_test/surf_500_spheres" }
            ListElement { text: "surround_sphere_test/surf_1000_spheres" }

        }

        onCurrentTextChanged: {
          if(currentText === "-- clear --")
            dynamic_scene_loader.source = ""
          else {
            dynamic_scene_loader.source =
              "qrc:///lib_rb_qml/scenes/" + currentText + ".qml"

          }
        }
      }

      ToolButton{
        Layout.leftMargin: 5; font.bold: true;
        text: "Reload!"; onClicked: dynamic_scene_loader.reload() }
    }
  }


  menuBar: MenuBar {
    Menu {
      title: "File"
      MenuItem {
        text: "Toggle simulator";
        checkable: true; checked: scenario.simulator_run_status;
        onTriggered: scenario.simulator_run_status = !scenario.simulator_run_status
      }
      MenuSeparator{}
      MenuItem { text: "Quit"; onTriggered: Qt.quit() }
    }
  }



  RigidBodyScenario { id: scenario
    anchors.fill: parent


    RB.FpsMonitor {
        rollingMeanFrameCount: 20
        onFramesPerSecondChanged: {
            var fpsString = parseFloat(Math.round(framesPerSecond * 100) / 100).toFixed(2);
            app_window.title = app_title + " FPS = " + fpsString
                    + " (" + rollingMeanFrameCount + " frame average)"
        }
    }

    RB.Environment { id: default_environment }

    Q3DCore.EntityLoader { id: dynamic_scene_loader
      function reload() {
        var current_source = source
        source = ""
        source = current_source
      }

      onStatusChanged: {
        if(status !== Q3DCore.EntityLoader.Ready )
          return


        if(entity === null || entity.activeEnvironment === undefined)
          return

        entity.activeEnvironment = default_environment
      }
    }
  }
}
