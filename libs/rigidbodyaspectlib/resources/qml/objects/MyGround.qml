import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2QtIntegration 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

Ground {
  id: ground

  property alias rbc : rbc
  property alias environment: rbc.environment
  property alias constraints: rbc.constraints

  property alias initialFriction : rbc.initialFriction

  defaultMesh.samples: Qt.size(2,2)
  origin: Qt.vector3d(0,0,0)
  uAxis: Qt.vector3d(0,0,10)
  vAxis: Qt.vector3d(10,0,0)
  //defaultMaterialOptions: SceneObject.GoochMaterialOption
  defaultMaterialOptions: SceneObject.NoMaterialOption
  defaultMeshOptions: Qt.size(20,20)

  Texture2D{
  id:texture
  TextureImage {

          //source: "fireballpng4.png"
          //source: "qrc:///gfx/objects/small_texture.png"
          source: "qrc:///gfx/objects/black_white.png"
  //        format: Texture.SRGB8_Alpha8
  //        mirrored: false
        }
  wrapMode {
      x: WrapMode.Repeat
      y: WrapMode.Repeat
  }
  minificationFilter: Texture.Nearest
  magnificationFilter: Texture.Nearest
//  generateMipMaps: true
//  maximumAnisotropy: 16.0
  }

  DiffuseSpecularMaterial {
      id: g_mat
      ambient: "white"
               //Qt.rgba(0.05,0.05,0.05,1.0)

      diffuse: texture/*"red"*/
//      diffuse: TextureLoader {
//          source: "qrc:///gfx/objects/small_texture.png"
//          /*mirrored: true*/}
      specular: Qt.rgba(0.2,0.2,0.2,1.0)
      shininess: 10.0
      textureScale: 4.0
  }

//  TextureMaterial {
//      id: g_mat
//      texture: TextureLoader {
//          id: textureLoader
//          source: "qrc:///gfx/objects/squares.jpg"
//          mirrored: false
//      }
//      //textureTransform: spriteGrid.textureTransform
//  }

  components: [g_mat]

  RB.PlaneController{
    id: rbc

    constraints: RB.Fixed

    origin: ground.origin
    uAxis: ground.uAxis
    vAxis: ground.vAxis
  }

  onPSpaceFrameParentChanged: {
    rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
  }
}
