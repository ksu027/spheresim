import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2QtIntegration 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

Ball {
  id: ball

  property alias environment: rbc.environment
  property alias initialMass: rbc.initialMass
  property alias initialFriction: rbc.initialFriction
  property alias initialVelocity: rbc.initialVelocity

  radius: 0.5
  defaultMesh.samples: Qt.size(20,20)
  defaultMaterialOptions: SceneObject.NoMaterialOption
  defaultMeshOptions: Qt.size(20,20)



  Texture2D{
  id:texture
  TextureImage {

//          source: "fireballpng4.png"
          source: "qrc:///gfx/objects/small_texture.png"
//          source: "qrc:///gfx/objects/squares.jpg"
//          source: "qrc:///gfx/objects/fireballpng4.png"
//          source: "qrc:///gfx/objects/tr.png"
//          format: Texture.SRGB8_Alpha8
//          mirrored: false
        }
  wrapMode {
      x: WrapMode.MirroredRepeat
      y: WrapMode.MirroredRepeat
  }
//  minificationFilter: Texture.LinearMipMapLinear
//  magnificationFilter: Texture.Linear
//  generateMipMaps: true
//  maximumAnisotropy: 16.0
  }

  DiffuseSpecularMaterial {
      id: g_mat
      ambient: "white"
               //Qt.rgba(0.05,0.05,0.05,0.1)

      diffuse: texture/*"red"*/
//      diffuse: TextureLoader {
//          source: "qrc:///gfx/objects/small_texture.png"
//          /*mirrored: true*/}
//      diffuse: Qt.rgba(0.5,0.5,0.5,0.1)
      specular: Qt.rgba(0.2,0.2,0.2,1.0)
      shininess: 10.0
//      textureScale: 100.0
//      alphaBlending: true
  }

//  PhongAlphaMaterial{
//      id: g_mat
//          ambient: Qt.rgba( 1, 0, 0, 1.0 )
//          diffuse: Qt.rgba( 1, 0, 0, 1.0 )
//          specular: Qt.rgba(1, 0, 0, 1.0 )
//          shininess: 1.0
//          alpha: 0.4
//      }

//  TextureMaterial {
//      id: g_mat
//      texture: TextureLoader {
//          id: textureLoader
//          source: "qrc:///gfx/objects/squares.jpg"
//          mirrored: false
//      }
//      //textureTransform: spriteGrid.textureTransform
//  }

  components: [g_mat]


  RB.SphereController{
    id: rbc

    radius: ball.radius
    initialMass: 1
    initialVelocity: Qt.vector3d(0,0,0)

    onFrameComputed: parent.setFrameParentQt(dir,up,pos)
  }

  onPSpaceFrameParentChanged: {
    rbc.resetFrameByDup(directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
  }
}
