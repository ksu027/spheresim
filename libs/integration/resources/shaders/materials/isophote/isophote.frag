#version 330 core

uniform struct LightInfo {
    vec4 position;
    vec3 intensity;
} light;

uniform struct IsophoteInfo {
  vec3 eye_vector;
  vec3 color1;
  vec3 color2;
  int  resolution;
  bool local;
} isophote;

in EyeSpaceVertex {
    vec3 position;
    vec3 normal;
} fs_in;


uniform mat3 modelViewNormal;

out vec4 fragColor;

vec3 adsModel( const in vec3 pos, const in vec3 n, const in vec3 color )
{
    // Calculate the vector from the light to the fragment
    vec3 s = normalize( vec3( light.position ) - pos );

    // Calculate the vector from the fragment to the eye position (the
    // origin since this is in "eye" or "camera" space
    vec3 v = normalize( -pos );

    // Refleft the light beam using the normal at this fragment
    vec3 r = reflect( -s, n );

    // Calculate the diffus component
    vec3 diffuse = vec3( max( dot( s, n ), 0.0 ) );

    // Calculate the specular component
//    vec3 specular = vec3( pow( max( dot( r, v ), 0.0 ), shininess ) );

    // Combine the ambient, diffuse and specular contributions
    return light.intensity * ( color + color * diffuse);
//    return light.intensity * ( color + color * diffuse + color * specular );
}

void main()
{
  vec3 e = normalize( isophote.eye_vector);
  vec3 n = normalize( fs_in.normal );

  // compute smallest angle
  float ang = acos( dot(e,n) / (length(e) * length(n)));

  // alternating coloring of isophote families
  vec3 draw_color;
  if( int(((ang + 1)/2.0)*isophote.resolution) % 2 == 0)
    draw_color = isophote.color1;
  else
    draw_color = isophote.color2;

  fragColor = vec4(adsModel(fs_in.position, normalize(fs_in.normal), draw_color),1.0);
}
