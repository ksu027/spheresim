#version 150 core
in vec4 vertexPosition;
in vec2 vertexTexCoord;

uniform mat4 mvp;

out vec2 texCoord;

void main(void)
{
    gl_Position = mvp * vertexPosition;
    texCoord = vec2(vertexTexCoord.s,1-vertexTexCoord.t);
}
