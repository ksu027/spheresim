#version 150 core
uniform sampler2DMS render_texture;
uniform sampler2D overlay_texture;
uniform sampler2D select_depth_texture;
uniform sampler2D active_depth_texture;
uniform float tex_width;
uniform float tex_height;
uniform int aa_level;


in vec2 texCoord;

out vec4 fragColor;


float depthEdgeDetect( sampler2D ed_tex ) {

  // x and y sobel filters\n"
  mat3 sx =  mat3(
     1.0,  2.0,  1.0,
     0.0,  0.0,  0.0,
    -1.0, -2.0, -1.0
    );

  mat3 sy = mat3(
    1.0, 0.0, -1.0,
    2.0, 0.0, -2.0,
    1.0, 0.0, -1.0
    );

  mat3 I;
  float dx = 1.5 / float(tex_width);
  float dy = 1.5 / float(tex_height);
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {

      // Sample depth
      float sample = 1.0-texture(ed_tex, texCoord.xy + vec2(dx * (i-1), dy * (j-1))).r;
      I[i][j] = sample > 0 ? 1 : 0;
    }
  }

  float gx = dot(sx[0], I[0]) + dot(sx[1], I[1]) + dot(sx[2], I[2]);
  float gy = dot(sy[0], I[0]) + dot(sy[1], I[1]) + dot(sy[2], I[2]);


  float g = sqrt(pow(gx, 2.0)+pow(gy, 2.0));


  return g;
}


vec3 renderColor(vec2 tex_coord) {

  ivec2 tc = ivec2(floor(textureSize(render_texture) * tex_coord));
  vec4 c;
  for( int i = 0; i < aa_level; ++i )
    c += texelFetch(render_texture, tc, i);

  return (c / float(aa_level)).rgb;
}

void main(void)
{

  float g_sel = depthEdgeDetect(select_depth_texture);
  g_sel = smoothstep(0,1,g_sel);

  float g_active = depthEdgeDetect(active_depth_texture);
  g_active = smoothstep(0,1,g_active);

  vec3 edge_color = vec3(0,0,0);
  vec3 active_edge_color = vec3(0.5,0,0);
  vec3 render_color  = renderColor(texCoord);
  vec3 result_color = mix(render_color,edge_color,vec3(g_sel));
  vec3 result_color2 = mix(result_color,active_edge_color,vec3(g_active));

  fragColor = vec4(result_color2,1.0);
}
