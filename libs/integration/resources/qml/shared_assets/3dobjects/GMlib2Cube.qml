import QtQuick 2.12

// Qt3D
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12

Entity {

  property alias oc_trans: oc_transform


  OrientationCube {
    id: oc

    components: [
      Transform {
        id: oc_transform
      }
    ]

  }

  // Spining GM2 "logo"
  Entity {

    MetalRoughMaterial{
      id: gm_text_mat
      baseColor: "#0000cc"
    }

//    Entity {
//      ExtrudedTextMesh {id: gm_text_mesh; depth: 0.7; text: "GMlib"; font.family: "Monospace"; font.pointSize: 34; font.bold: true}
//      Transform{ id: gm_text_transform; scale: 0.2; translation: Qt.vector3d(-0.45,-0.05,-0.1) }
//      components: [gm_text_mesh,gm_text_mat,gm_text_transform]
//    }

//    Entity {
//      ExtrudedTextMesh {id: gm2_text_mesh; depth: 0.7; text: "2"; font.family: "Monospace"; font.pointSize: 34; font.bold: true}
//      Transform{ id: gm2_text_transform; scale: 0.2; translation: Qt.vector3d(-0.1,-0.35,-0.1) }
//      components: [gm2_text_mesh,gm_text_mat,gm2_text_transform]
//    }

    components: [
      Transform{ id: gm_text_top_transform }
    ]

    SequentialAnimation {
      running: true
      loops: Animation.Infinite

      NumberAnimation {
        target: gm_text_top_transform
        property: "rotationY"
        duration: 5000
        from: 0
        to: 359
      }
    }
  }
}
