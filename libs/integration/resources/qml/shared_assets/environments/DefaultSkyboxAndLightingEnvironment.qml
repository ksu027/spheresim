import QtQuick 2.12

// Qt3D
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12

Entity {
  id: environment

  property alias skyboxEnabled:                 skybox.enabled
  property alias skyboxEnvironmentLightEnabled: skybox_env_light.enabled

  SkyboxEntity {
    id: skybox
    baseName: "qrc:///lib_examples_envmaps/cedar_bridge/cedar_bridge_irradiance"
    extension: ".dds"
    gammaCorrect: true
  }

  Entity {
    id: skybox_env_light
    components: [
      EnvironmentLight {
        irradiance: TextureLoader {
          source: "qrc:///lib_examples_envmaps/cedar_bridge/cedar_bridge_irradiance.dds"

          minificationFilter: Texture.LinearMipMapLinear
          magnificationFilter: Texture.Linear
          wrapMode {
            x: WrapMode.ClampToEdge
            y: WrapMode.ClampToEdge
          }
          generateMipMaps: false
        }
        specular: TextureLoader {
          source: "qrc:///lib_examples_envmaps/cedar_bridge/cedar_bridge_specular.dds"

          minificationFilter: Texture.LinearMipMapLinear
          magnificationFilter: Texture.Linear
          wrapMode {
            x: WrapMode.ClampToEdge
            y: WrapMode.ClampToEdge
          }
          generateMipMaps: false
        }
      }
    ]
  }
}
