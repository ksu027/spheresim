import QtQuick 2.12

// Qt3D
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12

Entity {
  id: environment

  Entity {
    id: myPointLight

    // Qt3D ignores everything but the default light position (0,0,0) with gooch shading
    // So this represents what we can control with lighting
    components: [ PointLight {} ]
  }
}
