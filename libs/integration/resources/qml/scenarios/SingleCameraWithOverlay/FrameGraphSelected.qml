import QtQuick 2.12
import QtQuick.Scene3D 2.12

// Qt3D
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12


FrameGraphNode {

  SortPolicy {
    sortTypes: [ SortPolicy.FrontToBack ]

    RenderStateSet {
      renderStates: [
        NoDepthMask { enabled: false},
        DepthTest{ depthFunction: DepthTest.LessOrEqual}
//        ,CullFace{ mode:CullFace.NoCulling}
      ]
    }
  }
}
