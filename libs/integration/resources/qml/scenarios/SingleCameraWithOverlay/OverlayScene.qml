import QtQuick 2.12

// Qt3D
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12

// Local
import "../../shared_assets/cameras"
import "../../shared_assets/3dobjects"

Entity {
  id: scene_overlay

  property var camera : null

  GMlib2Cube {
    id: gmlib2cube

    Component.onCompleted: {
      oc_trans.matrix = Qt.binding(function(){

        var A = scene_overlay.camera.viewMatrix
        var d = A.column(0).normalized()
        var s = A.column(1).normalized()
        var u = A.column(2).normalized()

        var M = Qt.matrix4x4(
              d.x, s.x, u.x, 0,
              d.y, s.y, u.y, 0,
              d.z, s.z, u.z, -2.5,
              0, 0, 0, 1
              )
        return M
      })
    }
  }

  Entity {
    components: [
      DirectionalLight {
        worldDirection: Qt.vector3d(0.3, -3.0, 0.0).normalized();
        color: "#fbf9ce"
        intensity: 0.3
      },
        Transform {
                    id: lightpostransform
                    translation: Qt.vector3d(0.0, 10.0, 0.0)
                }
    ]
  }

  Entity {
    components: [
      DirectionalLight {
        worldDirection: Qt.vector3d(-0.3, -0.3, 0.0).normalized();
//        color: "#9cdaef"
        color: "#9cdaef"
        intensity: 0.15
      }
    ]
  }

  Entity {
      PointLight {
          id: pointL
          color: "#fff2a3"
      }
      Transform{
          id: plightpostransform
          translation: Qt.vector3d(0.0, 4.0, 15.0)
      }
      components: [plightpostransform, pointL]
  }
}
