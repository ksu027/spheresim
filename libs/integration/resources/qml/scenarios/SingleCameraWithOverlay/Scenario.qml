import QtQuick 2.12
import QtQuick.Scene3D 2.12
import QtQuick.Controls 2.5

import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12
import Qt3D.Input 2.12

import com.uit.GMlib2QtIntegration 1.0

import "../../shared_assets/cameras"
import "../../shared_assets/environments"

Item {
  id: single_camera_scene

  anchors.leftMargin: 1
  anchors.topMargin: 1
  anchors.rightMargin: 1
  anchors.bottomMargin: 1


  default property alias content: scenegraph.childNodes
  property string scene_element_name: "scene_root_entity"
  property alias scenegraph: scenegraph
  property alias scenegraphModel: scenegraph.scenegraphModel
  property alias overlay_scene: overlay_scene
  property alias camera : camera
  property alias scene3d : scene3d
  property alias clearColor : fg.clear_color
  property alias skyboxEnabled : skybox.enabled
  property alias antialiasing_level : fg.antialiasing_level

  signal saveCameraSettings(vector3d position,vector3d viewCenter,vector3d upVector)

  function setRenderSize(size) {
    console.debug("scenario setRenderSize: " + size)
    fg.setRenderSize(size)
  }

  function requestRenderCapture() {
    return fg.render_capture.requestCapture()
  }




  Rectangle {
    anchors.fill: scene3d
    z: 2
    visible: !scene3d.focus
    color: "transparent"
    border.color: "gray"
    border.width: 2

    MouseArea {
      anchors.fill: parent
      onClicked: {
        scene3d.forceActiveFocus(Qt.WheelFocus)
      }
    }
  }

  Scene3D {
    id: scene3d
    z: 1
    objectName: "scene3d"
    anchors.fill: parent
    focus: true
    hoverEnabled: true
    aspects: ["input","logic"]
    cameraAspectRatioMode: Scene3D.UserAspectRatio



    Entity {
      id: root_entity

      components: [ input_settings, render_settings]

      RenderSettings {
        id: render_settings
        activeFrameGraph: FrameGraph { id: fg
          camera: camera
          overlay_camera: overlay_camera
          s3d_size: Qt.size(scene3d.width, scene3d.height)
//          antialiasing_level: 4
          onAntialiasing_levelChanged: single_camera_scene.update()
        }
        pickingSettings.pickMethod: PickingSettings.BoundingVolumePicking
        pickingSettings.faceOrientationPickingMode: PickingSettings.FrontFace
      }

      InputSettings { id: input_settings }

      Camera {
        id: camera

        readonly property vector3d default_pos        : Qt.vector3d(15,10,25)
        readonly property vector3d default_viewCenter : Qt.vector3d(0,0,0)
        readonly property vector3d default_up         : Qt.vector3d(0,1,0)


        projectionType: CameraLens.PerspectiveProjection
        fieldOfView: 45
        nearPlane : 0.1
        farPlane : 1000.0
        aspectRatio: scene3d.width / scene3d.height

        position: default_pos
        viewCenter: default_viewCenter
        upVector: default_up

        onPositionChanged:   saveCameraSettings(position,viewCenter,upVector)
        onViewCenterChanged: saveCameraSettings(position,viewCenter,upVector)
        onUpVectorChanged:   saveCameraSettings(position,viewCenter,upVector)

        function snapToDefault() {
          position = default_pos
          viewCenter = default_viewCenter
          upVector = default_up
        }

        function snapToBack() {
          position = Qt.vector3d(0,0,-default_pos.length())
          viewCenter = Qt.vector3d(0,0,0)
          upVector = Qt.vector3d(0,1,0)
        }

        function snapToFront() {
          position = Qt.vector3d(0,0,default_pos.length())
          viewCenter = Qt.vector3d(0,0,0)
          upVector = Qt.vector3d(0,1,0)
        }

        function snapToLeft() {
          position = Qt.vector3d(-default_pos.length(),0,0)
          viewCenter = Qt.vector3d(0,0,0)
          upVector = Qt.vector3d(0,1,0)
        }

        function snapToRight() {
          position = Qt.vector3d(default_pos.length(),0,0)
          viewCenter = Qt.vector3d(0,0,0)
          upVector = Qt.vector3d(0,1,0)
        }

        function snapToBottom() {
          position = Qt.vector3d(0,-default_pos.length(),0)
          viewCenter = Qt.vector3d(0,0,0)
          upVector = Qt.vector3d(0,0,1)
        }

        function snapToTop() {
          position = Qt.vector3d(0,default_pos.length(),0)
          viewCenter = Qt.vector3d(0,0,0)
          upVector = Qt.vector3d(0,0,-1)
        }

      }

      Camera {
        id: overlay_camera

        projectionType: CameraLens.OrthographicProjection
        fieldOfView: 45
        nearPlane : 0.1
        farPlane : 1000.0
        left: -1
        right: 1
        bottom: -1
        top: 1

        position: Qt.vector3d(0,0,4)
        viewCenter: Qt.vector3d(0.0, 0.0, -1.0)
        upVector: Qt.vector3d(0.0, 1.0, 0.0)
      }


      OrbitCameraController{ camera: camera }

      Entity {
        id: content_layer_entity

        Scenegraph {
          id: scenegraph
          objectName: scene_element_name
        }

        DefaultSkyboxAndLightingEnvironment {
          id: skybox
          skyboxEnabled: true
          skyboxEnvironmentLightEnabled: false
        }
        DefaultGoochLightingEnvironment {}

        Entity {
          enabled: true
          components: [
            PlaneMesh {
              height: 1
              width: height
              meshResolution: Qt.size(2,2)
            },
            Material {
              effect: Effect {

                  parameters: [
                    Parameter { name: "render_texture"; value: fg.render_texture },
                    Parameter { name: "select_depth_texture"; value: fg.select_depth_texture },
                    Parameter { name: "active_depth_texture"; value: fg.active_depth_texture },
                    Parameter { name: "tex_width"; value: fg.render_size.width },
                    Parameter { name: "tex_height"; value: fg.render_size.height },
                    Parameter { name: "aa_level"; value: fg.antialiasing_level }
                  ]

                  techniques: [
                    Technique {
                      graphicsApiFilter {
                        api: GraphicsApiFilter.OpenGL
                        profile: GraphicsApiFilter.CoreProfile
                        majorVersion: 3
                        minorVersion: 2
                      }

                      renderPasses: [
                        RenderPass {
                          filterKeys: [ FilterKey { name: "pass"; value: "compose" } ]
                          shaderProgram: ShaderProgram {
                            vertexShaderCode:   loadSource("qrc:/lib_examples_shaders/materials/compose_render/compose_render.vert")
                            fragmentShaderCode: loadSource("qrc:/lib_examples_shaders/materials/compose_render/compose_render.frag")
                          }
                        }
                      ]
                    }
                  ]
              }
            }

          ]
        }

        components: [fg.scene_layer]
      }

      Entity {
        id: overlay_layer_entity

        OverlayScene {
          id: overlay_scene
          camera: camera
        }

        components: [fg.overlay_layer]
      }

    } // END Entity (id:scene_root)


  } // END Scene3D

}
