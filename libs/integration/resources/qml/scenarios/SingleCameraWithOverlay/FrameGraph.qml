import QtQuick 2.12
import QtQuick.Scene3D 2.12

// Qt3D
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12


FrameGraphNode {
  id: root

  Item {
    id: priv
    readonly property size min_render_size : Qt.size(2,2)
    readonly property size max_render_size : Qt.size(5120,1440)

    property size render_size : min_render_size
    property real ar : render_size.width/render_size.height

//    property color clear_color: "#A6BBC8"
    property color clear_color: "#DE7C00"

//    onRender_sizeChanged: console.debug("render_size: " + render_size)
  }


  onS3d_sizeChanged: setRenderSize(s3d_size)


  function setRenderSize(size) {
    if(size.width < priv.min_render_size.width || size.height < priv.min_render_size.height)
      priv.render_size = priv.min_render_size
    else if(size.width > priv.max_render_size.width || size.height > priv.max_render_size.height)
      priv.render_size = priv.max_render_size.height
    else
      priv.render_size = Qt.size(size.width,size.height)
  }

  property alias camera: cameraselector.camera
  property alias overlay_camera: overlay_cameraselector.camera

  property size s3d_size: Qt.size(100,100)
  property alias render_capture : render_capture

  property alias clear_color : priv.clear_color

  property int antialiasing_level : 4

  readonly property alias scene_layer : scene_layer
  readonly property alias overlay_layer : overlay_layer


  readonly property real aspect_ratio : priv.ar
  readonly property size render_size : priv.render_size


  readonly property Texture2DMultisample render_texture : Texture2DMultisample {
                        id: render_texture
                        width: priv.render_size.width
                        height: priv.render_size.height
                        samples: antialiasing_level
                        format: Texture.RGBA8_UNorm//Format
                        generateMipMaps: false
                        magnificationFilter: Texture.Linear
                        minificationFilter: Texture.Linear
                        wrapMode {
                          x: WrapMode.ClampToBorder
                          y: WrapMode.ClampToBorder
                        }
                        comparisonFunction: Texture.CompareLessEqual
                        comparisonMode: Texture.CompareRefToTexture
                      }

  readonly property Texture2DMultisample render_depth_texture :  Texture2DMultisample {
                      width: priv.render_size.width
                      height: priv.render_size.height
                      samples: antialiasing_level
                      format: Texture.D24S8
                      generateMipMaps: false
                      magnificationFilter: Texture.Linear
                      minificationFilter: Texture.Linear
                      wrapMode { x: WrapMode.ClampToEdge; y: WrapMode.ClampToEdge }
                      comparisonFunction: Texture.CompareLessEqual
                      comparisonMode: Texture.None
                    }

  readonly property Texture2D active_depth_texture :  Texture2D {
                      id: active_depth_texture
                      width: priv.render_size.width
                      height: priv.render_size.height
                      format: Texture.DepthFormat
                      generateMipMaps: false
                      magnificationFilter: Texture.Linear
                      minificationFilter: Texture.Linear
                      wrapMode { x: WrapMode.ClampToEdge; y: WrapMode.ClampToEdge }
                      comparisonFunction: Texture.CompareLessEqual
                      comparisonMode: Texture.None
                    }

  readonly property Texture2D select_depth_texture :  Texture2D {
                      id: select_depth_texture
                      width: priv.render_size.width
                      height: priv.render_size.height
                      format: Texture.DepthFormat
                      generateMipMaps: false
                      magnificationFilter: Texture.Linear
                      minificationFilter: Texture.Linear
                      wrapMode { x: WrapMode.ClampToEdge; y: WrapMode.ClampToEdge }
                      comparisonFunction: Texture.CompareLessEqual
                      comparisonMode: Texture.None
                    }


  Layer {
    id: scene_layer
    recursive: true
  }

  Layer {
    id: overlay_layer
    recursive: true
  }


  RenderSurfaceSelector {
    id:rssel


    // Render scene pass
    RenderTargetSelector {
      target: RenderTarget {
        attachments: [
          RenderTargetOutput {
            attachmentPoint: RenderTargetOutput.Color0
            texture: render_texture
          },
          RenderTargetOutput {
            attachmentPoint: RenderTargetOutput.Depth
            texture: render_depth_texture
          }
        ]
      }

      TechniqueFilter {
        matchAll: [ FilterKey {name: "renderingStyle"; value: "forward"} ]


        Viewport { normalizedRect: Qt.rect(0,0,1,1)


          CameraSelector { id: cameraselector
            camera: root.camera

            // Scene Layer
            LayerFilter {
              layers: [scene_layer]
              filterMode: LayerFilter.AcceptAnyMatchingLayers


              ClearBuffers {
                buffers: ClearBuffers.ColorDepthBuffer
                clearColor: priv.clear_color
                clearDepthValue: 1
                NoDraw{}
              }
              FrameGraphScene {}
            }
          }

        }


        Viewport {
          normalizedRect: Qt.rect(
                            1-150.0/s3d_size.width,
                            40.0/s3d_size.height,
                            150.0/s3d_size.width,
                            150.0/s3d_size.height)

          // Overlay
          LayerFilter {
            layers: [overlay_layer]
            filterMode: LayerFilter.AcceptAnyMatchingLayers

            CameraSelector {
              id: overlay_cameraselector; /*camera: render_pass_cameraselector.camera*/

              FrameGraphOverlay {}
            }
          }
        }

      }
    }


    // Render selected objects pass
    RenderTargetSelector {
      target: RenderTarget {
        attachments: [
          RenderTargetOutput {
            attachmentPoint: RenderTargetOutput.Depth
            texture: active_depth_texture
          }
        ]
      }

      TechniqueFilter {
        matchAll: [ FilterKey {name: "renderingStyle"; value: "forward"} ]

        Viewport { normalizedRect: Qt.rect(0,0,1,1)

          CameraSelector {
            camera: cameraselector.camera

            // Selected Objects Layer
            LayerFilter {
              layers: [scenegraph.activeLayer]
              filterMode: LayerFilter.AcceptAnyMatchingLayers

              ClearBuffers {
                buffers: ClearBuffers.DepthBuffer
                clearDepthValue: 1
                NoDraw{}
              }
              FrameGraphSelected {}
            }
          }
        }
      }
    }

    RenderTargetSelector {
      target: RenderTarget {
        attachments: [
          RenderTargetOutput {
            attachmentPoint: RenderTargetOutput.Depth
            texture: select_depth_texture
          }
        ]
      }

      TechniqueFilter {
        matchAll: [ FilterKey {name: "renderingStyle"; value: "forward"} ]

        Viewport { normalizedRect: Qt.rect(0,0,1,1)

          CameraSelector {
            camera: cameraselector.camera

            // Selected Objects Layer
            LayerFilter {
              layers: [scenegraph.selectLayer]
              filterMode: LayerFilter.AcceptAnyMatchingLayers

//              LayerFilter {
//                layers: [scenegraph.activeLayer]
//                filterMode: LayerFilter.DiscardAnyMatchingLayers

                ClearBuffers {
                  buffers: ClearBuffers.DepthBuffer
                  clearDepthValue: 1
                  NoDraw{}
                }
                FrameGraphSelected {}
//              }
            }
          }

        }

      }
    }


    // Composition pass
    Viewport { normalizedRect: Qt.rect(0,0,1,1)

      RenderPassFilter {
        matchAny: [ FilterKey { name: "pass"; value: "compose" } ]

        CameraSelector {
          camera: Camera {
            projectionType: CameraLens.OrthographicProjection
            position: Qt.vector3d(0.0, 1.0, 0.0)
            viewCenter: Qt.vector3d(0.0, 0.0, 0.0)
            upVector: Qt.vector3d(0.0, 0.0, -1.0)
          }

          ClearBuffers {
            buffers: ClearBuffers.ColorBuffer
            NoDraw{}
          }

          RenderCapture { id: render_capture
            RenderStateSet {
              renderStates: [
                MultiSampleAntiAliasing{},
                NoDepthMask {},
                DepthTest{ enabled: false}
//                ,CullFace{ mode:CullFace.Back}
              ]
            }
          }
        }
      }
    }
  }
}
