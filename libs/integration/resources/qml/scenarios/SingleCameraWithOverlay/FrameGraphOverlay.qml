import QtQuick 2.12
import QtQuick.Scene3D 2.12

// Qt3D
import Qt3D.Core 2.12
import Qt3D.Render 2.12
import Qt3D.Input 2.12
import Qt3D.Extras 2.12


FrameGraphNode {

  LayerFilter {
    layers: [scenegraph.transparencyLayer]
    filterMode: LayerFilter.DiscardAnyMatchingLayers

    SortPolicy {
      sortTypes: [SortPolicy.FrontToBack]

      RenderStateSet {
        renderStates: [
          NoDepthMask { enabled: false},
          DepthTest{ depthFunction: DepthTest.Less}
//          ,CullFace{ mode:CullFace.NoCulling}
        ]
      }
    }
  }

  LayerFilter {
    layers: [scenegraph.transparencyLayer]
    filterMode: LayerFilter.AcceptAnyMatchingLayers

    SortPolicy {
      sortTypes: [ SortPolicy.BackToFront ]

      RenderStateSet {
        renderStates: [
          NoDepthMask {},
          DepthTest{ depthFunction: DepthTest.LessOrEqual},
          BlendEquation{ blendFunction: BlendEquation.Add },
          BlendEquationArguments {
            sourceRgb: BlendEquationArguments.SourceAlpha
            destinationRgb: BlendEquationArguments.OneMinusSourceAlpha
            sourceAlpha: BlendEquationArguments.One
            destinationAlpha: BlendEquationArguments.OneMinusSourceAlpha
          }
//          ,CullFace{ mode:CullFace.NoCulling}
        ]
      }
    }
  }

}
