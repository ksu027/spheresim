#include "scenario.h"

// qt
#include <Qt3DCore>
#include <Qt3DRender>
#include <QtConcurrent/QtConcurrent>

// stl
#include <mutex>




namespace integration {

  Scenario::Scenario(const QString& category, const QString& name,
                     InitFunc initialize_fn, CleanupFunc cleanup_fn,
                     Scenegraph*                    scenegraph)
    : QObject(scenegraph), m_scenegraph{scenegraph}, m_category{category},
      m_name{name}, m_initialize_fn{initialize_fn}, m_cleanup_fn{cleanup_fn}
  {
#if INTEGRATION_SCENARIO_THREAD_DEV
    connect(&m_initialize_watcher, &InitializeFutureWatcher::finished, this,
            &Scenario::handleInitializeDone);
#endif
  }


  Scenario::~Scenario() {}

  void Scenario::setProgress(int progress)
  {
    m_progress = std::clamp( progress, 0, 1000 );
    emit progressChanged(m_progress);
  }

  SceneObject* Scenario::root() { return m_scenario_root; }

  Scenegraph* Scenario::scenegraph() { return m_scenegraph; }

  void Scenario::deleteChildrenOfRootLater()
  {
    for (auto* child : m_scenario_root->children()) {
      child->setParent(reinterpret_cast<Qt3DCore::QNode*>(0x0));
      child->deleteLater();
    }
  }

  void Scenario::initialize()
  {
    std::scoped_lock lock(m_mutex);

    setProgress(Scenario::minProgress());

#if INTEGRATION_SCENARIO_THREAD_DEV
    auto*                 main_thread = QThread::currentThread();
    QFuture<SceneObject*> future = QtConcurrent::run([this, main_thread]() {
      auto* scenario_root = new SceneObject;

      scenario_root->setObjectName(
        QString("%1 (%2)").arg(m_name).arg(m_category));
      m_initialize_fn(*this);

      scenario_root->moveToThread(main_thread);
      return scenario_root;
    });

    m_initialize_watcher.setFuture(future);
    qDebug() << "initialize -- DONE";
#else
    m_scenario_root = new SceneObject;
    m_scenario_root->setObjectName(
      QString("%1 (%2)").arg(m_name).arg(m_category));
    m_initialize_fn(*this);
    m_scenario_root->setParent(m_scenegraph);

    setProgress(Scenario::maxProgress());
#endif
  }

#if INTEGRATION_SCENARIO_THREAD_DEV
  void Scenario::handleInitializeDone()
  {
    qDebug() << "handle initialize done";
    auto* scenario_root = m_initialize_watcher.result();
    scenario_root->setParent(m_scenegraph);
    setProgress(Scenario::maxProgress());
    qDebug() << "handle initialize done -- DONE";
  }
#endif

  void Scenario::cleanup()
  {

    std::scoped_lock lk(m_mutex);
    if (not m_scenario_root) return;

    m_scenario_root->setParent(reinterpret_cast<Qt3DCore::QNode*>(0x0));
    m_cleanup_fn(*this);
    m_scenario_root->deleteLater();
    setProgress(Scenario::minProgress());
  }

}   // namespace integration
