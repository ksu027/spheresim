#ifndef INTEGRATION_SCENEOBJECT_H
#define INTEGRATION_SCENEOBJECT_H


// gmlib2
#include <gmlib2.h>

// stl
#include <optional>

// qt
#include <Qt3DCore>

//
//#include "utils/SurroundSphere.h"


namespace Qt3DRender
{
  class QGeometryRenderer;
  class QObjectPicker;
  class QPickEvent;
  class QLayer;
  class QFilterKey;
  class QMaterial;
}   // namespace Qt3DRender
namespace Qt3DExtras
{
  class QPhongAlphaMaterial;
}

namespace Qt3DRender
{
  class QAbstractTextureProvider;
}


namespace integration
{
  class Scenegraph;
  class SceneObjectModel;
//  class SurroundSphere;

  class SceneObject : public Qt3DCore::QEntity,
                      public gmlib2::ProjectiveSpaceObject<> {
    Q_OBJECT


    Q_PROPERTY(
      bool selected READ selected WRITE setSelected NOTIFY selectedChanged)

    Q_PROPERTY(QVariant defaultMaterialOptions READ defaultMaterialOptions WRITE
                 setDefaultMaterialOptions NOTIFY defaultMaterialOptionsChanged)

    Q_PROPERTY(QVariant defaultMeshOptions READ defaultMeshOptions WRITE
                 setDefaultMeshOptions NOTIFY defaultMeshOptionsChanged)

    Q_PROPERTY(QVector3D translateGlobal READ frameOriginGlobalQt WRITE translateGlobalQt)

    // Friends
    friend class Scenegraph;
    friend class SceneModel;
    friend class ScenegraphModel;
    friend class SceneObjecModel;

  private:
    // Base clases
    using EntityBase = Qt3DCore::QEntity;
    using EmbedBase  = gmlib2::ProjectiveSpaceObject<>;

    // Types
    using SceneObjectVector = QVector<SceneObject*>;

  public:
    // Aggregate dimensions
    static constexpr auto FrameDim   = EmbedSpace::FrameDim;
    static constexpr auto VectorDim  = EmbedSpace::VectorDim;
    static constexpr auto ASFrameDim = EmbedSpace::ASFrameDim;
    static constexpr auto VectorHDim = EmbedSpace::VectorHDim;

    // Aggregate types
    using Unit = EmbedBase::Unit;

    using Point  = EmbedBase::Point;
    using Vector = EmbedBase::Vector;

    using Frame    = EmbedBase::Frame;
    using ASFrame  = EmbedBase::ASFrame;
    using ASFrameH = EmbedBase::ASFrameH;

    using PointH  = EmbedBase::PointH;
    using VectorH = EmbedBase::VectorH;

    // Constructors
    SceneObject(QNode* parent = nullptr);
    ~SceneObject() override;



    // Frame access

    //! Vector space frame wrt. global coordinates
    const Frame vSpaceFrameGlobal() const;

    //! Projective space frame wrt. global coordinates
    const ASFrameH pSpaceFrameGlobal() const;

    //! Parent object's vector space frame wrt. global coordinates
    const Frame parentVSpaceFrameGlobal() const;

    //! Parent object's projective space frame wrt. global coordinates
    const ASFrameH parentPSpaceFrameGlobal() const;



    // Frame-axis access
    const Vector directionAxisGlobal() const;
    const Vector sideAxisGlobal() const;
    const Vector upAxisGlobal() const;
    const Point  frameOriginGlobal() const;

    const VectorH directionAxisGlobalH() const;
    const VectorH sideAxisGlobalH() const;
    const VectorH upAxisGlobalH() const;
    const PointH  frameOriginGlobalH() const;

    bool selected() const;
    void setSelected(bool selected_flag, bool multi = false);
    bool activeSelected() const;
    void toggleSelected();

    bool collapsed() const;
    void setCollapsed(bool selected_flag);
    void toggleCollapsed();


    Q_INVOKABLE QVector3D directionAxisGlobalQt() const;
    Q_INVOKABLE QVector3D sideAxisGlobalQt() const;
    Q_INVOKABLE QVector3D upAxisGlobalQt() const;
    Q_INVOKABLE QVector3D frameOriginGlobalQt() const;


    enum DefaultMaterialOptions : size_t {
      NoMaterialOption = 0,
      GoochMaterialOption,
      DiffuseSpecularMaterialOption,
      TransparentDiffuseSpecularMaterialOption,
      RobustWireframeMaterialOption
    };
    Q_ENUM(DefaultMaterialOptions)



  public slots:
    void rotateLocalQt(float angle_in_radians, const QVector3D& axis);
    void rotateParentQt(float angle_in_radians, const QVector3D& axis);
    void rotateGlobalQt(float angle_in_radians, const QVector3D& axis);
    void translateLocalQt(const QVector3D& v);
    void translateParentQt(const QVector3D& v);
    void translateGlobalQt(const QVector3D& v);
    void setFrameParentQt(const QVector3D& dir, const QVector3D& up,
                          const QVector3D& pos);
    void setFrameGlobalQt(const QVector3D& dir, const QVector3D& up,
                          const QVector3D& pos);

  public:
    void rotateLocal(double angle_in_radians, const Vector& axis) override;
    void rotateParent(double angle_in_radians, const Vector& axis) override;
    void rotateGlobal(double angle_in_radians, const Vector& axis);
    void translateLocal(const Vector& v) override;
    void translateParent(const Vector& v) override;
    void translateGlobal(const Vector& v);
    void setFrameParent(const Vector& dir, const Vector& up,
                        const Point& origin) override;
    void setFrameGlobal(const Vector& dir, const Vector& up,
                        const Point& origin);

    // Q_Property stuff
    QVariant defaultMaterialOptions() const
    {
      return m_default_material_options;
    }

    QVariant defaultMeshOptions() const
    {
      return m_default_mesh_options;
    }

    void setDefaultMaterialOptions(const QVariant default_material_options)
    {
      m_default_material_options = default_material_options;
      if (default_material_options.canConvert<DefaultMaterialOptions>()) {
        initDefaultComponents(m_default_material_options.value<DefaultMaterialOptions>());
      }
      emit defaultMaterialOptionsChanged(m_default_material_options);
    }

    virtual void setDefaultMeshOptions(const QVariant default_mesh_options)
    {
      m_default_mesh_options = default_mesh_options;
      emit defaultMeshOptionsChanged(default_mesh_options);
    }

    //SurroundSphere* getSurroundSphere(){return m_surround_sphere;}
//    SurroundSphere getSurroundSphere(){return m_surround_sphere;}

  signals:
    void selectedChanged(bool selected) const;
    void collapsedChanged(bool collapsed) const;
    void pSpaceFrameParentChanged(const QMatrix4x4&) const;
    void pSpaceFrameGlobalChanged(const QMatrix4x4&) const;
    void defaultMaterialOptionsChanged(QVariant material_options) const;
    void defaultMeshOptionsChanged(QVariant mesh_options) const;

  protected:

//    SurroundSphere                      m_surround_sphere;
//    void setSurroundSphere(const SurroundSphere& sp)
//    {
//      m_surround_sphere = sp;
//    }
  protected slots:
    void handleParentChanged(QObject* parent);

  private:
    bool                                m_selected_flag{false};
    QPointer<Qt3DRender::QObjectPicker> m_objectpicker;
    QVariant                            m_default_material_options;
    QVariant                            m_default_mesh_options;


    //! Parent object's projective space frame wrt. global coordinates
    mutable ASFrameH m_parent_pspace_frame_global{
      gmlib2::spaces::projectivespace::identityFrame<EmbedBase::EmbedSpace>()};

    //! Projective space frame wrt. global coordinates
    mutable ASFrameH m_pspace_frame_global{
      gmlib2::spaces::projectivespace::identityFrame<EmbedBase::EmbedSpace>()};

    bool m_global_frames_dirty_flag{true};

    //    SceneObjectVector m_sceneobject_children;
    qint64 m_scene_depth_pos{0};

    QPointer<Qt3DCore::QTransform> m_transform;

    void updateGlobalFrames(SceneObject* parent);
    void updateGlobalFrames(Scenegraph* parent);
    void endGlobalFramesUpdate();
    void endTransform();
    void markGlobalFrameDirty();
    void updateSceneDepthInfo(qint64 parent_scene_depth_pos);
    void lazyPrepare() const;

    SceneObjectVector sceneObjectChildren() const;
    Scenegraph* getSceneGraphFromParent(QObject* parent);


  private slots:
    void pick(Qt3DRender::QPickEvent* pick);


    // Node construction
  private:
    QPointer<Scenegraph>                    m_scenegraph;
    QPointer<Qt3DRender::QGeometryRenderer> m_geometry_renderer;

    void        setSceneGraph(Scenegraph* scenegraph);



    // Default Components stuff
  public:
    void initDefaultComponents(DefaultMaterialOptions material_option
                               = GoochMaterialOption,
                               bool selectable = true)
    {
      if (material_option == GoochMaterialOption)
        initDefaultComponents<GoochMaterialOption>(selectable);
      else if (material_option == DiffuseSpecularMaterialOption)
        initDefaultComponents<DiffuseSpecularMaterialOption>(selectable);
      else if (material_option == TransparentDiffuseSpecularMaterialOption)
        initDefaultComponents<TransparentDiffuseSpecularMaterialOption>(
          selectable);
      else if (material_option == RobustWireframeMaterialOption)
        initDefaultComponents<RobustWireframeMaterialOption>(
          selectable);
    }


    template <size_t DefaultMaterialOption_T,
              typename
              = std::enable_if_t<DefaultMaterialOption_T == NoMaterialOption>>
    void initDefaultComponents(bool selectable = false)
    {
      initDefaultComponents_Impl<NoMaterialOption>(
        std::tuple(), selectable, std::make_index_sequence<0>{});
    }

    template <size_t DefaultMaterialOption_T,
              typename = std::enable_if_t<DefaultMaterialOption_T
                                          == GoochMaterialOption>>
    void initDefaultComponents(bool          selectable = true,
                               const QColor& diffuse    = {"orange"},
                               const QColor& specular   = {"white"})
    {
      initDefaultComponents_Impl<GoochMaterialOption>(
        std::tuple(diffuse, specular), selectable,
        std::make_index_sequence<2>{});
    }

    template <size_t DefaultMaterialOption_T,
              typename = std::enable_if_t<DefaultMaterialOption_T
                                          == DiffuseSpecularMaterialOption>>
    void initDefaultComponents(bool          selectable = true,
                               const QVariant & diffuse    = QColor("orange"),
                               const QColor & ambient    = QColor("gray"),
                               const QVariant & specular   = QColor("white"))
    {
      initDefaultComponents_Impl<DiffuseSpecularMaterialOption>(
        std::tuple(diffuse, ambient, specular), selectable,
        std::make_index_sequence<3>{});
    }


    template <size_t DefaultMaterialOption_T,
              typename
              = std::enable_if_t<DefaultMaterialOption_T
                                 == TransparentDiffuseSpecularMaterialOption>>
    void initDefaultComponents(bool selectable = true, qreal opacity = 0.5,
                               const QColor& diffuse  = QColor("orange"),
                               const QColor& ambient  = QColor("gray"),
                               const QColor& specular = QColor("white"))
    {
      initDefaultComponents_Impl<TransparentDiffuseSpecularMaterialOption>(
        std::tuple(diffuse, ambient, specular, opacity), selectable,
        std::make_index_sequence<4>{});
    }

    template <size_t DefaultMaterialOption_T,
              typename = std::enable_if_t<DefaultMaterialOption_T
                                          == RobustWireframeMaterialOption>>
    void initDefaultComponents(bool          selectable  = true, qreal opacity = 0.5,
                               const QColor& diffuse     = {"orange"},
                               const QColor& ambient     = {"gray"},
                               const QVector3D& lightpos = QVector3D{10.0,10.0,0.0},
                               const QColor& specular    = {"white"})
    {
      initDefaultComponents_Impl<RobustWireframeMaterialOption>(
        std::tuple(diffuse,ambient,specular,lightpos,opacity), selectable,
        std::make_index_sequence<5>{});
    }


  private:
    template <size_t DefaultMaterialOption_T, typename MaterialParamTuple_T,
              size_t... Is>
    void initDefaultComponents_Impl(MaterialParamTuple_T material_params,
                                    bool selectable, std::index_sequence<Is...>)
    {
      // Init default mesh (overloaded in potential "super/sub-class")
      initDefaultMesh();

      // Init selection component
      if (selectable) initDefaultSelectionComponents();

      // Init Default Material
      if constexpr (DefaultMaterialOption_T == GoochMaterialOption)
        initGoochMaterial(std::get<Is>(material_params)...);
      else if constexpr (DefaultMaterialOption_T
                         == DiffuseSpecularMaterialOption)
        initDiffuseSpecularMaterial(std::get<Is>(material_params)...);
      else if constexpr (DefaultMaterialOption_T
                         == TransparentDiffuseSpecularMaterialOption)
        initTransparentDiffuseSpecularMaterial(
          std::get<Is>(material_params)...);
      else if constexpr (DefaultMaterialOption_T
                         == RobustWireframeMaterialOption)
        initRobustWireframeMaterial(
            std::get<Is>(material_params)...);
    }


    void initGoochMaterial(const QColor& diffuse, const QColor& specular);
    void initDiffuseSpecularMaterial/*(Qt3DRender::QAbstractTextureProvider * diffuse,
                                     const QColor& ambient,
                                     Qt3DRender::QAbstractTextureProvider * specular);*/
                                        (const QVariant & diffuse,
                                         const QColor& ambient,
                                         const QVariant & specular);
    void initTransparentDiffuseSpecularMaterial(const QColor& diffuse,
                                                const QColor& ambient,
                                                const QColor& specular,
                                                qreal         opacity);

    void initRobustWireframeMaterial(const QColor&    diffuse,
                                     const QColor&    ambient,
                                     const QColor&    specular,
                                     const QVector3D& lightpos,
                                     qreal            opacity);





  public:
    Scenegraph*       scenegraph();
    const Scenegraph* scenegraph() const;





  protected:
    virtual void initDefaultSelectionComponents();
    virtual void initDefaultMesh() {}

    const Qt3DRender::QGeometryRenderer* geometryRenderer() const;
    void setGeometryRenderer(Qt3DRender::QGeometryRenderer* geometry_renderer);


    void clearActiveGeometryRenderer();
    void
    resetActiveGeometryRenderer(Qt3DRender::QGeometryRenderer* geometry_renderer
                                = nullptr);

  public:
    QPointer<Qt3DRender::QMaterial> m_default_material;

    template <typename DefaultMaterial_T>
    std::optional<QPointer<DefaultMaterial_T>> defaultMaterial() const
    {
      if(not m_default_material) return {};

      if (auto* obj = qobject_cast<DefaultMaterial_T*>(m_default_material); obj)
        return QPointer<DefaultMaterial_T>(obj);

      return {};
    }
  };

}   // namespace integration

#endif   // INTEGRATION_SCENEOBJECT_H
