#ifndef INTEGRATION_INITIALIZATION_H
#define INTEGRATION_INITIALIZATION_H

#include "integration/parametric/components/curvegeometry.h"
#include "integration/parametric/components/surfacegeometry.h"
#include "integration/models/scenegraphmodel.h"
#include "integration/scenegraph.h"
#include "integration/sceneobject.h"

#include "examples/utils/traditionalgmlibcameracontroller.h"
#include "examples/objects/parametric/parametricobjects.h"

// qt
#include <QQmlEngine>
#include <QMetaType>

namespace initialization
{


  namespace init
  {
    namespace detail
    {
      constexpr auto qt_registertype_uri_integration
        = "com.uit.GMlib2QtIntegration";
      constexpr auto qt_registertype_uri_examples  = "com.uit.GMlib2QtExamples";
      constexpr auto qt_registertype_major_version = 1;
      constexpr auto qt_registertype_minor_version = 0;
    }   // namespace detail




    inline void registerQmlTypes()
    {

      // Createable types
      qmlRegisterType<integration::Scenegraph>(
        detail::qt_registertype_uri_integration,
        detail::qt_registertype_major_version,
        detail::qt_registertype_minor_version, "Scenegraph");

      qmlRegisterType<integration::SceneObject>(
        detail::qt_registertype_uri_integration,
        detail::qt_registertype_major_version,
        detail::qt_registertype_minor_version, "SceneObject");


      // UnCreateable types
      qRegisterMetaType<integration::ScenegraphModel*>("ScenegraphModel");
      qmlRegisterUncreatableType<integration::ScenegraphModel>(
        detail::qt_registertype_uri_integration,
        detail::qt_registertype_major_version,
        detail::qt_registertype_minor_version, "ScenegraphModel",
        "The scenegraph owns this");

      qmlRegisterUncreatableType<integration::CurveMesh>(
        detail::qt_registertype_uri_integration,
        detail::qt_registertype_major_version,
        detail::qt_registertype_minor_version, "CurveMesh",
        "Provides a 'view'...");

      qmlRegisterUncreatableType<integration::SurfaceMesh>(
        detail::qt_registertype_uri_integration,
        detail::qt_registertype_major_version,
        detail::qt_registertype_minor_version, "SurfaceMesh",
        "Provides a 'view'...");

      // Createable types
      qmlRegisterType<examples::TraditionalGMlibCameraController>(
        detail::qt_registertype_uri_examples, 1, 0,
        "TraditionalGMlibCameraController");
    }

    inline void registerObjectsAsQmlTypes()
    {
      // parametric point
      qmlRegisterType<examples::APPoint>(detail::qt_registertype_uri_examples,
                                         1, 0, "PPoint");

      // parametric curves
      qmlRegisterType<examples::Circle>(detail::qt_registertype_uri_examples,
                                         1, 0, "Circle");
      qmlRegisterType<examples::Line>(detail::qt_registertype_uri_examples, 1,
                                       0, "Line");

      // parametric surfaces
      qmlRegisterType<examples::Plane>(detail::qt_registertype_uri_examples, 1,
                                        0, "Plane");
      qmlRegisterType<examples::Sphere>(detail::qt_registertype_uri_examples,
                                         1, 0, "Sphere");
      qmlRegisterType<examples::Torus>(detail::qt_registertype_uri_examples, 1,
                                        0, "Torus");
    }

  }   // namespace init

}   // namespace initialization

#endif   // INTEGRATION_INITIALIZATION_H
