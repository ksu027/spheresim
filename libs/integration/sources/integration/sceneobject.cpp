#include "sceneobject.h"

#include "scenegraph.h"
#include "utils/typeconversion.h"
#include "qt3dexamples/materials/robustwireframematerial.h"

// qt
#include <QCoreApplication>
#include <Qt3DRender>
#include <Qt3DExtras>
#include <QQmlProperty>

// stl
#include <sstream>
#include <iostream>


namespace integration
{



  SceneObject::SceneObject(Qt3DCore::QNode* parent)
    : EntityBase(parent), EmbedBase()
  {
    // "try to" get scenegraph from parent
    setSceneGraph(getSceneGraphFromParent(parent));
    markGlobalFrameDirty();

    connect(this, &EntityBase::parentChanged, this,
            &SceneObject::handleParentChanged);

    m_transform = new Qt3DCore::QTransform(this);

//    QQmlProperty::write(m_transform, "ComponentType", "SceneObjectTransform");
//    m_transform->setProperty("ComponentType", QString("SceneObjectTransform"));
    connect(this, &SceneObject::pSpaceFrameParentChanged, m_transform,
            &Qt3DCore::QTransform::setMatrix);
    addComponent(m_transform);
  }

  SceneObject::~SceneObject()
  {
    const auto& gtli = scenegraph()->globalTransparencyLayerInstance();
    if (gtli) removeComponent(gtli);
  }

  void SceneObject::initDefaultSelectionComponents()
  {
    // Object picker
    m_objectpicker = new Qt3DRender::QObjectPicker(this);
    connect(m_objectpicker, &Qt3DRender::QObjectPicker::clicked, this,
            &SceneObject::pick);
    addComponent(m_objectpicker);
  }

  const Qt3DRender::QGeometryRenderer* SceneObject::geometryRenderer() const
  {
    return m_geometry_renderer;
  }

  void SceneObject::setGeometryRenderer(Qt3DRender::QGeometryRenderer* geometry_renderer)
  {
    if (m_geometry_renderer) removeComponent(m_geometry_renderer);

    m_geometry_renderer = geometry_renderer;

    if(not m_geometry_renderer) {
      setSelected(false,true);
    }
    else {
      addComponent(m_geometry_renderer);
    }
  }

  void SceneObject::clearActiveGeometryRenderer()
  {
    if (scenegraph()->selected(this)) scenegraph()->setDeSelected(this, true);

    for (auto* comp : components())
      if (auto* geom_rend = qobject_cast<Qt3DRender::QGeometryRenderer*>(comp);
          geom_rend)
        removeComponent(comp);
  }

  void SceneObject::resetActiveGeometryRenderer(Qt3DRender::QGeometryRenderer* geometry_renderer)
  {
    clearActiveGeometryRenderer();

    if (geometry_renderer)
      addComponent(geometry_renderer);
    else if (m_geometry_renderer)
      addComponent(m_geometry_renderer);
  }

  bool SceneObject::selected() const
  {
    return m_scenegraph ? m_scenegraph->selected(this) : false;
  }

  bool SceneObject::activeSelected() const
  {
    return m_scenegraph ? m_scenegraph->activeSelected(this) : false;
  }

  void SceneObject::setSelected(bool selected_flag, bool multi)
  {
    if (m_scenegraph) {

      if (selected_flag)
        m_scenegraph->setSelected(this, m_geometry_renderer, multi);
      else
        m_scenegraph->setDeSelected(this, multi);
    }

    emit selectedChanged(selected_flag);
  }

  void SceneObject::toggleSelected() { setSelected(not selected()); }

  bool SceneObject::collapsed() const
  {
    return m_scenegraph ? m_scenegraph->collapsed(this) : false;
  }

  void SceneObject::setCollapsed(bool collapsed_flag)
  {
    if (m_scenegraph)
    m_scenegraph->setCollapsed(this, collapsed_flag);

    emit collapsedChanged(collapsed_flag);
  }

  void SceneObject::toggleCollapsed() { setCollapsed(not collapsed()); }


  QVector3D SceneObject::directionAxisGlobalQt() const
  {
    const auto& d = directionAxisGlobal();
    return {float(d[0]), float(d[1]), float(d[2])};
  }

  QVector3D SceneObject::sideAxisGlobalQt() const
  {
    const auto& s = sideAxisGlobal();
    return {float(s[0]), float(s[1]), float(s[2])};
  }

  QVector3D SceneObject::upAxisGlobalQt() const
  {
    const auto& u = upAxisGlobal();
    return {float(u[0]), float(u[1]), float(u[2])};
  }

  QVector3D SceneObject::frameOriginGlobalQt() const
  {
    const auto& p = frameOriginGlobal();
    return {float(p[0]), float(p[1]), float(p[2])};
  }

  void SceneObject::pick(Qt3DRender::QPickEvent* pick)
  {
    pick->setAccepted(true);
    toggleSelected();
    if(m_scenegraph)
      m_scenegraph->scenegraphModel()->update(this);
  }

  Scenegraph*       SceneObject::scenegraph() { return m_scenegraph; }
  const Scenegraph* SceneObject::scenegraph() const { return m_scenegraph; }

  void SceneObject::setSceneGraph(Scenegraph* scenegraph)
  {
    if(m_scenegraph) m_scenegraph->scenegraphModel()->update();

    m_scenegraph = scenegraph;
    for (auto* child : sceneObjectChildren())
      child->setSceneGraph(m_scenegraph);
    markGlobalFrameDirty();

    if(m_scenegraph) m_scenegraph->scenegraphModel()->update();
  }

  void SceneObject::initGoochMaterial(const QColor &diffuse, const QColor &specular)
  {
    auto* material = new Qt3DExtras::QGoochMaterial(this);
    material->setAlpha(0.25f);
    material->setBeta(0.5f);
    material->setShininess(100.0f);
    material->setCool(QColor("blue"));
    material->setWarm(QColor("orange"));
    material->setDiffuse(diffuse);
    material->setSpecular(specular);
    this->addComponent(material);
    m_default_material = material;
  }

  void SceneObject::initDiffuseSpecularMaterial/*(Qt3DRender::QAbstractTextureProvider * diffuse, const QColor &ambient, Qt3DRender::QAbstractTextureProvider *specular)*/
  (const QVariant & diffuse,
   const QColor& ambient,
   const QVariant & specular)
  {
    auto* material = new Qt3DExtras::QDiffuseSpecularMaterial(this);
    material->setDiffuse(diffuse);
    material->setAmbient(ambient);
    material->setSpecular(specular);
    material->setTextureScale(5.0);
    //material->setTextureScale(50.0);
    this->addComponent(material);
    m_default_material = material;
  }

  void SceneObject::initTransparentDiffuseSpecularMaterial(
    const QColor& diffuse, const QColor& ambient, const QColor& specular,
    qreal opacity)
  {
    auto c = diffuse;
    c.setAlphaF(opacity);
    auto* material = new Qt3DExtras::QDiffuseSpecularMaterial(this);
    material->setDiffuse(c);
    material->setAmbient(ambient);
    material->setSpecular(specular);
    this->addComponent(material);
    this->addComponent(scenegraph()->globalTransparencyLayerInstance());
    m_default_material = material;
  }

  void SceneObject::initRobustWireframeMaterial(
    const QColor& diffuse, const QColor& ambient, const QColor& specular, const QVector3D& lightpos,
    qreal opacity)
  {
    auto c = diffuse;
    c.setAlphaF(opacity);
    auto* material = new qt3dexamples::RobustWireframeMaterial(this);
    material->setDiffuse(c);
    material->setAmbient(ambient);
    material->setSpecular(specular);
    material->setLightPosition(lightpos);
    this->addComponent(material);
    this->addComponent(scenegraph()->globalTransparencyLayerInstance());
    m_default_material = material;
  }

//  void SceneObject::initDefaultComponents(
//    SceneObject::DefaultMaterialOptions material_option, const QColor& diffuse,
//    const QColor& specular, bool selectable)
//  {
//    initDefaultMesh();
//    initDefaultMaterial(material_option, diffuse, specular);
//    if (selectable) initDefaultSelectionComponents();
//  }

//  void SceneObject::initDefaultComponents(
//    SceneObject::DefaultMaterialOptions material_option, bool selectable)
//  {
//    initDefaultComponents(material_option, QColor("orange"), QColor("white"),
//                          selectable);
//  }

  void SceneObject::rotateLocalQt(float angle_in_radians, const QVector3D& a)
  {
    rotateLocal(double(angle_in_radians),
                Vector{double(a[0]), double(a[1]), double(a[2])});
  }

  void SceneObject::rotateParentQt(float angle_in_radians, const QVector3D& a)
  {
    rotateParent(double(angle_in_radians),
                 Vector{double(a[0]), double(a[1]), double(a[2])});
  }

  void SceneObject::rotateGlobalQt(float angle_in_radians, const QVector3D& a)
  {
    rotateGlobal(double(angle_in_radians),
                 Vector{double(a[0]), double(a[1]), double(a[2])});
  }

  void SceneObject::translateLocalQt(const QVector3D& v)
  {
    const Vector vec{double(v[0]), double(v[1]), double(v[2])};
    translateLocal(vec);
  }

  void SceneObject::translateParentQt(const QVector3D& v)
  {
    const Vector vec{double(v[0]), double(v[1]), double(v[2])};
    translateParent(vec);
  }

  void SceneObject::translateGlobalQt(const QVector3D& v)
  {
    const Vector vec{double(v[0]), double(v[1]), double(v[2])};
    translateGlobal(vec);
  }

  void SceneObject::setFrameParent(
    const Vector& dir, const Vector& up, const Point& origin)
  {
    EmbedBase::setFrameParent(dir,up,origin);
    endTransform();
  }


  void SceneObject::setFrameParentQt(const QVector3D& dir, const QVector3D& up,
                                     const QVector3D& pos)
  {
    auto q3ToVec = [](const auto& qv) {
      using U = Unit;
      return Vector{U(qv.x()), U(qv.y()), U(qv.z())};
    };

    setFrameParent(q3ToVec(dir), q3ToVec(up), q3ToVec(pos));
    endTransform();
  }

  void SceneObject::setFrameGlobalQt(const QVector3D &dir, const QVector3D &up, const QVector3D &pos)
  {
    auto q3ToVec = [](const auto& qv) {
      using U = Unit;
      return Vector{U(qv.x()), U(qv.y()), U(qv.z())};
    };

    setFrameGlobal(q3ToVec(dir), q3ToVec(up), q3ToVec(pos));
    endTransform();
  }

  void SceneObject::translateLocal(const Vector& v)
  {
    EmbedBase::translateLocal(v);
    endTransform();
  }

  void SceneObject::translateParent(const SceneObject::Vector& v)
  {
    EmbedBase::translateParent(v);
    endTransform();
  }

  void SceneObject::translateGlobal(const SceneObject::Vector& v)
  {
    translateParent(Vector{blaze::inv(vSpaceFrameGlobal()) * v});
  }

  void SceneObject::handleParentChanged(QObject* parent)
  {
    setSceneGraph(getSceneGraphFromParent(parent));
  }

  void SceneObject::rotateLocal(double angle_in_radians, const Vector& axis)
  {
    EmbedBase::rotateLocal(angle_in_radians, axis);
    endTransform();
  }

  void SceneObject::rotateParent(double angle_in_radians, const Vector& axis)
  {
    EmbedBase::rotateParent(angle_in_radians, axis);
    endTransform();
  }

  void SceneObject::rotateGlobal(double angle_in_radians, const Vector& axis)
  {
    rotateParent(angle_in_radians, Vector{blaze::inv(vSpaceFrameParent()) * axis});
  }

  const SceneObject::Vector SceneObject::directionAxisGlobal() const
  {
    return parentVSpaceFrameGlobal() * this->directionAxisParent();
  }

  const SceneObject::Vector SceneObject::sideAxisGlobal() const
  {
    return parentVSpaceFrameGlobal() * this->sideAxisParent();
  }

  const SceneObject::Vector SceneObject::upAxisGlobal() const
  {
    return parentVSpaceFrameGlobal() * this->upAxisParent();
  }

  const SceneObject::Point SceneObject::frameOriginGlobal() const
  {
    return blaze::subvector<0UL, VectorDim>(parentPSpaceFrameGlobal()
                                            * this->frameOriginParentH());
  }

  const SceneObject::VectorH SceneObject::directionAxisGlobalH() const
  {
    return parentPSpaceFrameGlobal() * this->directionAxisParentH();
  }

  const SceneObject::VectorH SceneObject::sideAxisGlobalH() const
  {
    return parentPSpaceFrameGlobal() * this->sideAxisParentH();
  }

  const SceneObject::VectorH SceneObject::upAxisGlobalH() const
  {
    return parentPSpaceFrameGlobal() * this->upAxisParentH();
  }

  const SceneObject::PointH SceneObject::frameOriginGlobalH() const
  {
    return parentPSpaceFrameGlobal() * this->frameOriginParentH();
  }

  void SceneObject::setFrameGlobal(const Vector& dir, const Vector& up,
                                   const Point& origin)
  {
    // Invert parent's frame wrt. global space
    const auto parent_pspace_frame_global_inv
      = ASFrameH(blaze::inv(m_parent_pspace_frame_global));

    // Move global frame components into wrt. parent space
    const auto gdirh = parent_pspace_frame_global_inv
                       * VectorH{dir[0], dir[1], dir[2], Unit(0)};
    const auto guph = parent_pspace_frame_global_inv
                      * VectorH{up[0], up[1], up[2], Unit(0)};
    const auto goriginh
      = parent_pspace_frame_global_inv
        * VectorH{origin[0], origin[1], origin[2], Unit(1)};

    // Set frame wrt. parent space
    setFrameParent(blaze::subvector<0UL, 3UL>(gdirh),
                   blaze::subvector<0UL, 3UL>(guph),
                   blaze::subvector<0UL, 3UL>(goriginh));
  }


  const SceneObject::Frame SceneObject::parentVSpaceFrameGlobal() const
  {
    using ReturnType = const SceneObject::Frame;
    return ReturnType(
      blaze::eval(blaze::submatrix<0UL, 0UL, VectorDim, FrameDim>(
        parentPSpaceFrameGlobal())));
  }

  const SceneObject::Frame SceneObject::vSpaceFrameGlobal() const
  {
    using ReturnType = const SceneObject::Frame;
    return ReturnType(blaze::eval(
      blaze::submatrix<0UL, 0UL, VectorDim, FrameDim>(pSpaceFrameGlobal())));
  }

  const SceneObject::ASFrameH SceneObject::parentPSpaceFrameGlobal() const
  {
    lazyPrepare();
    return m_parent_pspace_frame_global;
  }

  const SceneObject::ASFrameH SceneObject::pSpaceFrameGlobal() const
  {
    lazyPrepare();
    return m_pspace_frame_global;
  }

  void SceneObject::endTransform()
  {
    markGlobalFrameDirty();
    emit pSpaceFrameParentChanged(toQMatrix4x4(pSpaceFrameParent()));
  }

  void SceneObject::markGlobalFrameDirty()
  {
    m_global_frames_dirty_flag = {true};
    if (m_scenegraph) m_scenegraph->markGlobalMatrixDirty(this);
  }

  void SceneObject::updateSceneDepthInfo(qint64 parent_scene_depth_pos)
  {
    m_scene_depth_pos = parent_scene_depth_pos + 1;
    for (auto* child : sceneObjectChildren())
      child->updateSceneDepthInfo(m_scene_depth_pos);
  }

  void SceneObject::lazyPrepare() const
  {
    if (m_scenegraph) m_scenegraph->lazyPrepare();
  }

  void SceneObject::updateGlobalFrames(SceneObject* parent)
  {
    m_pspace_frame_global = m_parent_pspace_frame_global = parent->m_pspace_frame_global;
    m_pspace_frame_global = m_pspace_frame_global * pSpaceFrameParent();

    endGlobalFramesUpdate();
    for (auto* child : sceneObjectChildren()) child->updateGlobalFrames(this);
  }

  void SceneObject::updateGlobalFrames(Scenegraph* /*parent*/)
  {
    m_pspace_frame_global
      = gmlib2::spaces::projectivespace::identityFrame<EmbedBase::EmbedSpace>()
        * pSpaceFrameParent();
    endGlobalFramesUpdate();
    for (auto* child : sceneObjectChildren()) child->updateGlobalFrames(this);
  }

  SceneObject::SceneObjectVector SceneObject::sceneObjectChildren() const
  {
    SceneObjectVector sceneobject_child_list;
    const auto&       child_list = childNodes();
    sceneobject_child_list.reserve(child_list.size());
    for (auto* c : child_list) {
      if (auto* so = qobject_cast<SceneObject*>(c); so)
        sceneobject_child_list.push_back(so);
    }
    return sceneobject_child_list;
  }

  Scenegraph* SceneObject::getSceneGraphFromParent(QObject* parent)
  {
    // If parent is not, so are also not its scenegraph
    if (not parent) return nullptr;

    // If parent is scenegraph - voala
    if (auto* scenegraph = dynamic_cast<Scenegraph*>(parent); scenegraph)
      return scenegraph;

    // If parent is scenegraph object ask for its parent
    if (auto* sceneobject = dynamic_cast<SceneObject*>(parent); sceneobject)
      return sceneobject->scenegraph();

    // else recurse up
    return getSceneGraphFromParent(parent->parent());
  }

  void SceneObject::endGlobalFramesUpdate()
  {
    m_global_frames_dirty_flag = {false};
    emit pSpaceFrameGlobalChanged(toQMatrix4x4(m_pspace_frame_global));
  }

}   // namespace gmlib2::qt::integration
