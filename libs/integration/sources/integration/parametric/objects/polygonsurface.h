#ifndef INTEGRATION_PARAMETRIC_OBJECTS_PPOLYGONSURFACE_H
#define INTEGRATION_PARAMETRIC_OBJECTS_PPOLYGONSURFACE_H

#include "../components/polygonsurfacegeometry.h"

// qt
#include <Qt3DExtras>


namespace integration
{

  template <typename PolygonSurface_T>
  class PolygonSurface : public PolygonSurface_T {
    using Base = PolygonSurface_T;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult evaluateGlobal(const PSpacePoint&     par,
                                    const PSpaceSizeArray& no_derivatives,
                                    const PSpaceBoolArray& from_left
                                    = {{true, true}}) const;

    // Default geometry renderer
    PolygonSurfaceMesh* defaultMesh() const;

    // Default mesh operations
    void sampleDefaultMesh(int sampling_level = 3);

  protected:
    void initDefaultMesh() override;
    void setDefaultMeshOptions(const QVariant default_mesh_options) override;

  private:
    QPointer<PolygonSurfaceMesh> m_mesh{nullptr};
  };




  template <typename PolygonSurface_T>
  typename PolygonSurface<PolygonSurface_T>::EvaluationResult
  PolygonSurface<PolygonSurface_T>::evaluateGlobal(
    const PSpacePoint& par, const PSpaceSizeArray& no_derivatives,
    const PSpaceBoolArray& from_left) const
  {
    const auto& global_frame = this->pSpaceFrameGlobal();
    return blaze::map(
      this->evaluate(par, no_derivatives, from_left),
      [global_frame](const auto& ele) { return global_frame * ele; });
  }

  template <typename PolygonSurface_T>
  PolygonSurfaceMesh* PolygonSurface<PolygonSurface_T>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename PolygonSurface_T>
  void PolygonSurface<PolygonSurface_T>::sampleDefaultMesh(int sampling_level)
  {
    if (not m_mesh) return;
    m_mesh->setSamplingLevel(sampling_level);
    m_mesh->sample();
  }

  template <typename PolygonSurface_T>
  void PolygonSurface<PolygonSurface_T>::initDefaultMesh()
  {
    m_mesh = new PolygonSurfaceMesh(this);
    this->setGeometryRenderer(m_mesh);
  }

  template <typename PolygonSurface_T>
  void PolygonSurface<PolygonSurface_T>::setDefaultMeshOptions(
    const QVariant default_mesh_options)
  {
    if (default_mesh_options.canConvert<int>()) {
      sampleDefaultMesh(default_mesh_options.toInt());
    }

    SceneObject::setDefaultMeshOptions(default_mesh_options);
  }

}   // namespace integration

#endif   // INTEGRATION_PARAMETRIC_OBJECTS_PPOLYGONSURFACE_H
