#ifndef INTEGRATION_PARAMETRIC_OBJECTS_PCURVE_H
#define INTEGRATION_PARAMETRIC_OBJECTS_PCURVE_H

#include "../components/curvegeometry.h"

// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DExtras>

namespace integration
{

  template <typename Curve_T>
  class Curve : public Curve_T {
    using Base = Curve_T;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult
    evaluateGlobal(const PSpacePoint&     par,
                   const PSpaceSizeArray& no_derivatives = {{1}},
                   const PSpaceBoolArray& from_left      = {{true}}) const;

    // Default geometry renderer
    integration::CurveMesh* defaultMesh() const;

    // Default mesh operations
    void sampleDefaultMesh(int samples = 50, int slices = 10,
                           float radius = 0.1f);

  protected:
    void initDefaultMesh() override;
    void setDefaultMeshOptions(const QVariant default_mesh_options) override;

  private:
    QPointer<integration::CurveMesh> m_mesh{nullptr};
  };



  template <typename Curve_T>
  typename Curve<Curve_T>::EvaluationResult
  Curve<Curve_T>::evaluateGlobal(const PSpacePoint&     par,
                                 const PSpaceSizeArray& no_derivatives,
                                 const PSpaceBoolArray& from_left) const
  {
    const auto& global_frame = this->pSpaceFrameGlobal();
    return blaze::map(
      this->evaluate(par, no_derivatives, from_left),
      [global_frame](const auto& ele) { return global_frame * ele; });
  }

  template <typename Curve_T>
  integration::CurveMesh* Curve<Curve_T>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename Curve_T>
  void Curve<Curve_T>::sampleDefaultMesh(int samples, int slices, float radius)
  {
    if (not m_mesh) return;
    m_mesh->setSamples(samples);
    m_mesh->setSlices(slices);
    m_mesh->setRadius(radius);
    m_mesh->sample();
  }

  template <typename Curve_T>
  void Curve<Curve_T>::initDefaultMesh()
  {
    m_mesh = new integration::CurveMesh(this);
    this->setGeometryRenderer(m_mesh);
  }

  template <typename Curve_T>
  void Curve<Curve_T>::setDefaultMeshOptions(const QVariant default_mesh_options)
  {
    if (default_mesh_options.canConvert<QStringList>()) {
      QStringList lst = default_mesh_options.toStringList();

      if (lst.size() == 3) {
        sampleDefaultMesh(lst.at(0).toInt(), lst.at(1).toInt(),
                          lst.at(2).toFloat());
      }
    }

    SceneObject::setDefaultMeshOptions(default_mesh_options);
  }
}   // namespace integration


#endif   // INTEGRATION_PARAMETRIC_OBJECTS_PCURVE_H
