#ifndef INTEGRATION_PARAMETRIC_OBJECTS_POINT_H
#define INTEGRATION_PARAMETRIC_OBJECTS_POINT_H

#include "../../sceneobject.h"

// qt
#include <Qt3DExtras>

namespace integration
{

  template <typename Point_T>
  class PPoint : public Point_T {
    using Base = Point_T;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult evaluateGlobal(const PSpacePoint&     par,
                                    const PSpaceSizeArray& no_derivatives,
                                    const PSpaceBoolArray& from_left
                                    = {{true, true}}) const;

    // Default geometry renderer
    Qt3DExtras::QSphereMesh* defaultMesh() const;

    // Default mesh operations
    void sampleDefaultMesh(int rings = 5, int slices = 5, float radius = 1.0f);

  protected:
    void initDefaultMesh() override;
    void setDefaultMeshOptions(const QVariant default_mesh_options) override;

  private:
    Qt3DExtras::QSphereMesh* m_mesh{nullptr};
  };



  template <typename Point_T>
  typename PPoint<Point_T>::EvaluationResult
  PPoint<Point_T>::evaluateGlobal(const PSpacePoint&     par,
                                  const PSpaceSizeArray& no_derivatives,
                                  const PSpaceBoolArray& from_left) const
  {
    const auto& global_frame = this->pSpaceFrameGlobal();
    return blaze::map(
      this->evaluate(par, no_derivatives, from_left),
      [global_frame](const auto& ele) { return global_frame * ele; });
  }

  template <typename Point_T>
  Qt3DExtras::QSphereMesh* PPoint<Point_T>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename Point_T>
  void PPoint<Point_T>::sampleDefaultMesh(int rings, int slices, float radius)
  {
    if (not m_mesh) return;
    m_mesh->setRings(rings);
    m_mesh->setSlices(slices);
    m_mesh->setRadius(radius);
  }

  template <typename Point_T>
  void PPoint<Point_T>::initDefaultMesh()
  {
    m_mesh = new Qt3DExtras::QSphereMesh(this);
    this->setGeometryRenderer(m_mesh);
  }

  template <typename PPoint_T>
  void PPoint<PPoint_T>::setDefaultMeshOptions(const QVariant default_mesh_options)
  {
    if (default_mesh_options.canConvert<QStringList>()) {
      QStringList lst = default_mesh_options.toStringList();

      if (lst.size() == 3) {
        sampleDefaultMesh(lst.at(0).toInt(), lst.at(1).toInt(),
                          lst.at(2).toFloat());
      }
    }

    SceneObject::setDefaultMeshOptions(default_mesh_options);
  }
}   // namespace integration


#endif   // INTEGRATION_PARAMETRIC_OBJECTS_POINT_H
