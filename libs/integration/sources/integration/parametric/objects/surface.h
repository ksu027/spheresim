#ifndef INTEGRATION_PARAMETRIC_OBJECTS_SURFACE_H
#define INTEGRATION_PARAMETRIC_OBJECTS_SURFACE_H

#include "../components/surfacegeometry.h"

// qt
#include <Qt3DExtras>

namespace integration
{

  template <typename Surface_T>
  class Surface : public Surface_T {
    using Base = Surface_T;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult evaluateGlobal(const PSpacePoint&     par,
                                    const PSpaceSizeArray& no_derivatives,
                                    const PSpaceBoolArray& from_left
                                    = {{true, true}}) const;

    // Default geometry renderer
    integration::SurfaceMesh* defaultMesh() const;

    // Default mesh operations
    void sampleDefaultMesh(const QSize& size = QSize{20, 20});
    void setDefaultMeshOptions(const QVariant default_mesh_options) override;

  protected:
    void initDefaultMesh() override;

  private:
    QPointer<integration::SurfaceMesh> m_mesh{nullptr};
  };




  template <typename Surface_T>
  typename Surface<Surface_T>::EvaluationResult
  Surface<Surface_T>::evaluateGlobal(const PSpacePoint&     par,
                                     const PSpaceSizeArray& no_derivatives,
                                     const PSpaceBoolArray& from_left) const
  {
    auto eval_res      = this->evaluate(par, no_derivatives, from_left);
    auto global_hframe = this->pSpaceFrameGlobal();

    EvaluationResult res
      = blaze::map(eval_res, [&global_hframe](const auto& ele) {
          return blaze::evaluate(global_hframe * ele);
        });
    return res;
  }

  template <typename Surface_T>
  integration::SurfaceMesh* Surface<Surface_T>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename Surface_T>
  void Surface<Surface_T>::sampleDefaultMesh(const QSize& size)
  {
    if (not m_mesh) return;

    m_mesh->setSamples(size);
    m_mesh->sample();
  }

  template <typename Surface_T>
  void Surface<Surface_T>::initDefaultMesh()
  {
    m_mesh = new integration::SurfaceMesh(this);
    this->setGeometryRenderer(m_mesh);
  }

  template <typename Surface_T>
  void
  Surface<Surface_T>::setDefaultMeshOptions(const QVariant default_mesh_options)
  {
    if (default_mesh_options.canConvert<QSize>()) {
      sampleDefaultMesh(default_mesh_options.toSize());
    }

    SceneObject::setDefaultMeshOptions(default_mesh_options);
  }

}   // namespace integration

#endif   // INTEGRATION_PARAMETRIC_OBJECTS_SURFACE_H
