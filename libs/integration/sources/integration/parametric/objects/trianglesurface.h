#ifndef INTEGRATION_PARAMETRIC_OBJECTS_TRIANGLESURFACE_H
#define INTEGRATION_PARAMETRIC_OBJECTS_TRIANGLESURFACE_H

#include "../components/trianglesurfacegeometry.h"

// qt
#include <Qt3DExtras>


namespace integration
{

  template <typename TriangleSurface_T>
  class TriangleSurface : public TriangleSurface_T {
    using Base = TriangleSurface_T;

  public:
    GM2_DEFINE_DEFAULT_PARAMETRIC_OBJECT_TYPES

    // Constructor(s)
    using Base::Base;

    // Members
    EvaluationResult evaluateGlobal(const PSpacePoint&     par,
                                    const PSpaceSizeArray& no_derivatives,
                                    const PSpaceBoolArray& from_left
                                    = {{true, true}}) const;

    // Default geometry renderer
    TriangleSurfaceMesh* defaultMesh() const;

    // Default mesh operations
    void sampleDefaultMesh(int sampling_level = 4);

  protected:
    void initDefaultMesh() override;
    void setDefaultMeshOptions(const QVariant default_mesh_options) override;

  private:
    QPointer<TriangleSurfaceMesh> m_mesh{nullptr};
  };




  template <typename TriangleSurfaceConstruction_T>
  typename TriangleSurface<TriangleSurfaceConstruction_T>::EvaluationResult
  TriangleSurface<TriangleSurfaceConstruction_T>::evaluateGlobal(
    const PSpacePoint& par, const PSpaceSizeArray& no_derivatives,
    const PSpaceBoolArray& from_left) const
  {
    const auto& global_frame = this->pSpaceFrameGlobal();
    return blaze::map(
      this->evaluate(par, no_derivatives, from_left),
      [global_frame](const auto& ele) { return global_frame * ele; });
  }

  template <typename TriangleSurfaceConstruction_T>
  TriangleSurfaceMesh*
  TriangleSurface<TriangleSurfaceConstruction_T>::defaultMesh() const
  {
    return m_mesh;
  }

  template <typename TriangleSurface_T>
  void TriangleSurface<TriangleSurface_T>::sampleDefaultMesh(int sampling_level)
  {
    if (not m_mesh) return;

    m_mesh->setSamplingLevel(sampling_level);
    m_mesh->sample();
  }

  template <typename TriangleSurfaceConstruction_T>
  void TriangleSurface<TriangleSurfaceConstruction_T>::initDefaultMesh()
  {
    m_mesh = new TriangleSurfaceMesh(this);
    this->setGeometryRenderer(m_mesh);
  }

  template <typename TriangleSurface_T>
  void TriangleSurface<TriangleSurface_T>::setDefaultMeshOptions(
    const QVariant default_mesh_options)
  {
    if (default_mesh_options.canConvert<int>()) {
      sampleDefaultMesh(default_mesh_options.toInt());
    }

    SceneObject::setDefaultMeshOptions(default_mesh_options);
  }

}   // namespace integration

#endif   // INTEGRATION_PARAMETRIC_OBJECTS_TRIANGLESURFACE_H
