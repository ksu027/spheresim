#include "polygonsurfacegeometry.h"


// gmlib2
#include <gmlib2.h>

// stl
#include <functional>
#include <numeric>

namespace integration
{

  int PolygonSurfaceGeometry::samplingLevel() const { return m_samp_lvl; }

  void PolygonSurfaceGeometry::setSamplingLevel(int level)
  {
    if( m_samp_lvl == level) return;
    m_samp_lvl = level;
  }

  void PolygonSurfaceGeometry::sample()
  {
    if( m_samp_lvl < 2) return;
    doSample();
  }

  void PolygonSurfaceGeometry::doSample()
  {
    updateVertices();
    updateIndices();
    emit samplingLevelChanged(m_samp_lvl);
  }


  void PolygonSurfaceGeometry::updateVertices()
  {
    if (not m_data.m_initialized) return;

    assert(m_samp_lvl > 1);

    auto*      polygon                   = m_surface_sampler.get();
    const auto sampling_pspace_positions = vertexSamplePositions();
    auto       sample_set
      = polygon->sample(sampling_pspace_positions);   // size_t(resolution));


    const auto no_verts = vertexCount();

    m_data.m_position_attribute->setCount(quint32(no_verts));
    m_data.m_normal_attribute->setCount(quint32(no_verts));
    m_data.m_tangent_attribute->setCount(quint32(no_verts));

    QByteArray buffer_bytes;
    buffer_bytes.resize(no_verts * int(sizeof(VertexElement)));
    VertexElement* ptr = reinterpret_cast<VertexElement*>(buffer_bytes.data());

    for (auto i = 0UL; i < sample_set.size(); ++i) {

      const auto& sample = sample_set[i];

      ptr[i].p[0UL] = float(sample(0UL, 0UL)[0UL]);
      ptr[i].p[1UL] = float(sample(0UL, 0UL)[1UL]);
      ptr[i].p[2UL] = float(sample(0UL, 0UL)[2UL]);

      if (sample.rows() > 1) {
        const auto du = sample(1, 0);
        ptr[i].t[0UL] = float(du[0UL]);
        ptr[i].t[1UL] = float(du[1UL]);
        ptr[i].t[2UL] = float(du[2UL]);
        ptr[i].t[3UL] = float(du[3UL]);
      }

      if (sample.rows() > 1 and sample.columns() > 1) {

        const auto du
          = blaze::normalize(blaze::subvector<0UL, 3UL>(sample(1, 0)));
        const auto dv
          = blaze::normalize(blaze::subvector<0UL, 3UL>(sample(0, 1)));
        const auto c_norm = blaze::normalize(blaze::cross(du, dv));
        ptr[i].n[0UL]     = float(c_norm[0UL]);
        ptr[i].n[1UL]     = float(c_norm[1UL]);
        ptr[i].n[2UL]     = float(c_norm[2UL]);
      }

    }

    m_data.m_vertex_buffer->setData(buffer_bytes);
  }

  void PolygonSurfaceGeometry::updateIndices()
  {
    if (not m_data.m_initialized) return;

    const auto no_faces = faceCount();

    m_data.m_index_attribute->setCount(quint32(no_faces * 3));

    const auto no_indices = no_faces;
    QByteArray indexBytes;
    indexBytes.resize(no_indices * int(sizeof(IndexElement)));
    IndexElement* iptr = reinterpret_cast<IndexElement*>(indexBytes.data());

    const auto polygon_tris = faceIndices();

    for(auto i = 0UL; i < polygon_tris.size(); ++i ) {
      const auto& tri = polygon_tris[i];
      iptr[i] = {quint32(tri[0]),quint32(tri[1]),quint32(tri[2])};
    }

    m_data.m_index_buffer->setData(indexBytes);
  }

  void PolygonSurfaceGeometry::initRenderData()
  {
    m_data.m_vertex_buffer = new Qt3DRender::QBuffer(this);
    m_data.m_index_buffer  = new Qt3DRender::QBuffer(this);

    const int stride = sizeof(VertexElement);

    m_data.m_position_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_position_attribute->setName(
      Qt3DRender::QAttribute::defaultPositionAttributeName());
    m_data.m_position_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_position_attribute->setVertexSize(3);
    m_data.m_position_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_position_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_position_attribute->setByteStride(stride);

    m_data.m_normal_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_normal_attribute->setName(
      Qt3DRender::QAttribute::defaultNormalAttributeName());
    m_data.m_normal_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_normal_attribute->setVertexSize(3);
    m_data.m_normal_attribute->setByteOffset(3 * sizeof(float));
    m_data.m_normal_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_normal_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_normal_attribute->setByteStride(stride);


    m_data.m_tangent_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_tangent_attribute->setName(
      Qt3DRender::QAttribute::defaultTangentAttributeName());
    m_data.m_tangent_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_tangent_attribute->setVertexSize(4);
    m_data.m_tangent_attribute->setByteOffset(6 * sizeof(float));
    m_data.m_tangent_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_tangent_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_tangent_attribute->setByteStride(stride);

    m_data.m_index_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_index_attribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    m_data.m_index_attribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    m_data.m_index_attribute->setBuffer(m_data.m_index_buffer);


    addAttribute(m_data.m_position_attribute);
    addAttribute(m_data.m_normal_attribute);
    addAttribute(m_data.m_tangent_attribute);
    addAttribute(m_data.m_index_attribute);

    m_data.m_initialized = true;
  }

  int PolygonSurfaceGeometry::vertexCount() const
  {
    assert(m_samp_lvl > 1);

    auto* polygon = m_surface_sampler.get();
    return int(gmlib2::parametric::polygonsurface::convex_algorithms::
                 countTriSamplingPSpacePositions(size_t(m_samp_lvl),
                                                 polygon->polygon2D()));
  }

  std::vector<gmlib2::VectorT<double, 2UL>>
  PolygonSurfaceGeometry::vertexSamplePositions() const
  {
    assert(m_samp_lvl > 1);

    auto* polygon = m_surface_sampler.get();
    return gmlib2::parametric::polygonsurface::convex_algorithms::
      generateTriSamplingPSpacePositions(size_t(m_samp_lvl),
                                         polygon->polygon2D());
  }

  int PolygonSurfaceGeometry::faceCount() const
  {
    assert(m_samp_lvl > 1);

    auto* polygon = m_surface_sampler.get();
    return int(
      gmlib2::parametric::polygonsurface::convex_algorithms::
        countTriSamplingFaceIndices(size_t(m_samp_lvl), polygon->polygon2D()));
  }


  std::vector<std::array<size_t, 3>>
  PolygonSurfaceGeometry::faceIndices() const
  {
    assert(m_samp_lvl > 1);

    auto* polygon = m_surface_sampler.get();
    return gmlib2::parametric::polygonsurface::convex_algorithms::
      generateTriSamplingFaceIndices(size_t(m_samp_lvl), polygon->polygon2D());
  }

  int PolygonSurfaceMesh::samplingLevel() const
  {
    return m_geometry->samplingLevel();
  }

  void PolygonSurfaceMesh::sample()
  {
    m_geometry->sample();
    emit meshSampled();
  }

  void PolygonSurfaceMesh::setSamplingLevel(int level)
  {
    m_geometry->setSamplingLevel(level);
    emit samplingLevelChanged(level);
  }

}   // namespace integration
