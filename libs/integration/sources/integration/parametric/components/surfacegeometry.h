#ifndef INTEGRATION_PARAMETRIC_COMPONENTS_PSURFACEGEOMETRY_H
#define INTEGRATION_PARAMETRIC_COMPONENTS_PSURFACEGEOMETRY_H


#include "../../sceneobject.h"

// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DRender>
#include <QFutureWatcher>

// stl
#include <memory>



namespace integration
{


  namespace detail
  {

    class SurfaceSamplerBase {
    public:
      SurfaceSamplerBase()          = default;
      virtual ~SurfaceSamplerBase() = default;

      using EvaluationResult
        = gmlib2::datastructures::parametrics::surface::EvaluationResult<>;
      using SamplingResult
        = gmlib2::datastructures::parametrics::surface::SamplingResult<>;

      virtual SamplingResult   sample(const QSize& samples,
                                      const QSize& derivatives) const      = 0;
      virtual EvaluationResult samplePart(const QSize&  samples,
                                          const QPoint& which,
                                          const QSize&  derivatives) const = 0;
    };

    template <typename Surface_T>
    class SurfaceSampler : public SurfaceSamplerBase {
    public:
      using PSpaceSizeArray = typename Surface_T::PSpaceSizeArray;

      SurfaceSampler(Surface_T* surface) : m_surface{surface}
      {
        static_assert(std::is_same<SurfaceSamplerBase::SamplingResult,
                                   typename Surface_T::SamplingResult>::value,
                      "Surface does not have expected SamplingResult type!");
      }
      ~SurfaceSampler() override = default;


    public:
      SamplingResult sample(const QSize& samples,
                            const QSize& derivatives) const override
      {
        if( not m_surface ) return SamplingResult();

        return gmlib2::parametric::surface::sample(
          m_surface, m_surface->startParameters(), m_surface->endParameters(),
          PSpaceSizeArray{{size_t(samples.width()), size_t(samples.height())}},
          PSpaceSizeArray{
            {size_t(derivatives.width()), size_t(derivatives.height())}});
      }

      EvaluationResult samplePart(const QSize& samples, const QPoint& which,
                                  const QSize& derivatives) const override
      {
        if( not m_surface ) return EvaluationResult{};

        return gmlib2::parametric::surface::samplePart(
          m_surface, PSpaceSizeArray{size_t(which.x()), size_t(which.y())},
          m_surface->startParameters(), m_surface->endParameters(),
          PSpaceSizeArray{{size_t(samples.width()), size_t(samples.height())}},
          PSpaceSizeArray{
            {size_t(derivatives.width()), size_t(derivatives.height())}});
      }


      Surface_T* m_surface{nullptr};
    };


  }   // namespace detail





  class SurfaceGeometry : public Qt3DRender::QGeometry {
    Q_OBJECT

    struct RenderData {
      Qt3DRender::QBuffer*    m_vertex_buffer;
      Qt3DRender::QBuffer*    m_index_buffer;
      Qt3DRender::QAttribute* m_position_attribute;
      Qt3DRender::QAttribute* m_normal_attribute;
      Qt3DRender::QAttribute* m_index_attribute;
      Qt3DRender::QAttribute* m_texture_coordinate_attribute;
      Qt3DRender::QAttribute* m_tangent_attribute;
      bool                    m_initialized{false};
    };


  public:
    explicit SurfaceGeometry(QNode* parent) : Qt3DRender::QGeometry{parent} {}

    template <typename Surface_T>
    SurfaceGeometry(Surface_T* surface, QNode* parent)
      : SurfaceGeometry(parent)
    {
      set<detail::SurfaceSampler>(surface);
    }

    ~SurfaceGeometry() override = default;


    template <template <typename> typename SurfaceSampler_T,
              typename Surface_T>
    void set(Surface_T* surface)
    {
      m_surface_sampler
        = std::make_unique<SurfaceSampler_T<Surface_T>>(surface);
      initRenderData();
      connect(&m_sample_watcher, &SamplingWatcher::finished, this,
              &SurfaceGeometry::handleDoSampleDone);
    }

    QSize samples() const;

    struct VertexElement {
      std::array<float, 3> p;
      std::array<float, 3> n;
      std::array<float, 4> t;
      std::array<float, 2> uv;
    };

    struct IndexElement {
      std::array<quint32, 3> i;

      void print() const
      {
        std::cout << i[0] << ", " << i[1] << ", " << i[2] << std::endl;
        ;
      }
    };


    int vertexCount() const;
    int faceCount() const;


  private:
    using SamplingResult  = detail::SurfaceSamplerBase::SamplingResult;
//    using SamplingWatcher  = QFutureWatcher<SamplingResult>;
    using EvaluationResult = detail::SurfaceSamplerBase::EvaluationResult;
//    using FutureResult = std::pair<std::pair<size_t, size_t>, EvaluationResult>;
    using FutureResult = SamplingResult;
    using SamplingWatcher = QFutureWatcher<FutureResult>;
    //    using SamplingWatcher = QFutureWatcher<void>;
    std::unique_ptr<detail::SurfaceSamplerBase> m_surface_sampler{nullptr};
    RenderData                                   m_data;
    QSize                                        m_samples{0,0};
    SamplingResult                               m_sample_set;
    SamplingWatcher                              m_sample_watcher;
    QList<QPoint>                                m_sample_nrs;

    void initRenderData();
    void doSample();
    void updateVertices();
    void updateIndices();


  private slots:
    void handleDoSampleDone();

  public slots:
    void sample();
    void setSamples(const QSize& samples);

  signals:
    void samplesChanged(const QSize& samples);

  };




  class SurfaceMesh : public Qt3DRender::QGeometryRenderer {
    Q_OBJECT

    Q_PROPERTY(
      QSize samples READ samples WRITE setSamples NOTIFY samplesChanged)

  public:
    SurfaceMesh() = delete;

    template <typename Surface_T>
    SurfaceMesh(Surface_T* psurface);

    QSize samples() const;

    QPointer<SurfaceGeometry> m_geometry{nullptr};


  public slots:
    void sample();
    void setSamples(const QSize& samples = QSize(20, 20));

  signals:
    void samplesChanged(const QSize& samples);
    void meshSampled();
  };



  template <typename Surface_T>
  SurfaceMesh::SurfaceMesh(Surface_T* psurface)
    : Qt3DRender::QGeometryRenderer(psurface)
  {
    static_assert(std::is_base_of<SceneObject, Surface_T>::value, "...");

    m_geometry = new SurfaceGeometry(psurface, this);

    connect(m_geometry, &SurfaceGeometry::samplesChanged, this,
            &SurfaceMesh::samplesChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }

}   // namespace integration


#endif   // INTEGRATION_PARAMETRIC_COMPONENTS_PSURFACEGEOMETRY_H
