#include "trianglesurfacegeometry.h"


// gmlib2
#include <gmlib2.h>

// stl
#include <functional>
#include <numeric>

namespace integration
{

  int TriangleSurfaceGeometry::samplingLevel() const { return m_samp_lvl; }

  void TriangleSurfaceGeometry::updateVertices()
  {
    if(not m_data.m_initialized) return;

    assert(m_samp_lvl > 0);

    auto* polygon = m_triangle_sampler.get();

    auto sample_set = polygon->sample(size_t(m_samp_lvl));   // size_t(resolution));


    const auto no_verts = vertexCount();

    m_data.m_position_attribute->setCount(quint32(no_verts));
    m_data.m_normal_attribute->setCount(quint32(no_verts));
    m_data.m_tangent_attribute->setCount(quint32(no_verts));

    QByteArray buffer_bytes;
    buffer_bytes.resize(no_verts * int(sizeof(VertexElement)));
    VertexElement* ptr = reinterpret_cast<VertexElement*>(buffer_bytes.data());

    for (auto i = 0UL; i < sample_set.size(); ++i) {

      const auto& sample = sample_set[i];

      // Position
      const auto p = sample[0UL];
      ptr->p[0UL] = float(p[0UL]);
      ptr->p[1UL] = float(p[1UL]);
      ptr->p[2UL] = float(p[2UL]);

      // Tangent
      if (sample.size() > 1) {

        const auto Dp0 = sample[1UL];
        const auto Dp1 = sample[2UL];
        const auto Du  = Dp1 - Dp0;
        ptr->t[0UL]    = float(Du[0UL]);
        ptr->t[1UL]    = float(Du[1UL]);
        ptr->t[2UL]    = float(Du[2UL]);
        ptr->t[3UL]    = float(Du[3UL]);
      }

      // Normal
      if (sample.size() > 3) {

        const auto Dp0 = sample[1UL];
        const auto Dp1 = sample[2UL];
        const auto Dp2 = sample[3UL];

        const auto Du = Dp1 - Dp0;
        const auto Dv = Dp2 - Dp0;

        const auto Du3 = blaze::subvector<0ul, 3ul>(Du);
        const auto Dv3 = blaze::subvector<0ul, 3ul>(Dv);

        const auto c_norm = blaze::normalize(blaze::cross(Du3, Dv3));
        ptr->n[0UL]       = float(c_norm[0UL]);
        ptr->n[1UL]       = float(c_norm[1UL]);
        ptr->n[2UL]       = float(c_norm[2UL]);
      }

      ptr++;
    }

    m_data.m_vertex_buffer->setData(buffer_bytes);
  }

  void TriangleSurfaceGeometry::updateIndices()
  {
    if (not m_data.m_initialized) return;

    const auto no_faces   = faceCount();
    const auto no_indices = no_faces * 3;

    m_data.m_index_attribute->setCount(quint32(no_indices));

    //    const auto no_indices = no_faces;
    QByteArray indexBytes;
    indexBytes.resize(no_faces * int(sizeof(IndexElement)));

//    for(auto& ele : indexBytes)
//      ele = 0;
//    qDebug() << "No. Faces: "   << no_indices;
//    qDebug() << "No. Indices: " << no_indices;
//    qDebug() << "Index Bytes A: " << indexBytes;

    IndexElement* iptr = reinterpret_cast<IndexElement*>(indexBytes.data());

    const auto polygon_tris = faceIndices();
//    qDebug() << "No. Polygon Tris.: "   << polygon_tris.size();

    for(auto i = 0UL; i < polygon_tris.size(); ++i ) {

      const auto& tri = polygon_tris[i];
//      qDebug() << "i: " << i << " -- " << tri[0] << ", " << tri[1] << ", " << tri[2];
      iptr->i[0] = quint32(tri[0]);
      iptr->i[1] = quint32(tri[1]);
      iptr->i[2] = quint32(tri[2]);
      iptr++;
    }

//    qDebug() << "Index Bytes B: " << indexBytes;


    m_data.m_index_buffer->setData(indexBytes);
  }

  void TriangleSurfaceGeometry::initRenderData()
  {
    m_data.m_vertex_buffer = new Qt3DRender::QBuffer(this);
    m_data.m_index_buffer  = new Qt3DRender::QBuffer(this);

    const int stride = sizeof(VertexElement);

    m_data.m_position_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_position_attribute->setName(
      Qt3DRender::QAttribute::defaultPositionAttributeName());
    m_data.m_position_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_position_attribute->setVertexSize(3);
    m_data.m_position_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_position_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_position_attribute->setByteStride(stride);

    m_data.m_normal_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_normal_attribute->setName(
      Qt3DRender::QAttribute::defaultNormalAttributeName());
    m_data.m_normal_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_normal_attribute->setVertexSize(3);
    m_data.m_normal_attribute->setByteOffset(3 * sizeof(float));
    m_data.m_normal_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_normal_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_normal_attribute->setByteStride(stride);


    m_data.m_tangent_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_tangent_attribute->setName(
      Qt3DRender::QAttribute::defaultTangentAttributeName());
    m_data.m_tangent_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_tangent_attribute->setVertexSize(4);
    m_data.m_tangent_attribute->setByteOffset(6 * sizeof(float));
    m_data.m_tangent_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_tangent_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_tangent_attribute->setByteStride(stride);

    m_data.m_index_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_index_attribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    m_data.m_index_attribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    m_data.m_index_attribute->setBuffer(m_data.m_index_buffer);

    addAttribute(m_data.m_position_attribute);
    addAttribute(m_data.m_normal_attribute);
    addAttribute(m_data.m_tangent_attribute);
    addAttribute(m_data.m_index_attribute);

    m_data.m_initialized = true;
  }
  void TriangleSurfaceGeometry::setSamplingLevel(int level)
  {
    if (m_samp_lvl == level) return;

    m_samp_lvl = level;
  }

  void TriangleSurfaceGeometry::sample()
  {
    if (m_samp_lvl < 1) return;
    doSample();
  }

  void TriangleSurfaceGeometry::doSample()
  {
    updateVertices();
    updateIndices();
    emit samplingLevelChanged(m_samp_lvl);
  }

  int TriangleSurfaceGeometry::vertexCount() const
  {
    assert(m_samp_lvl > 0);
    return int(gmlib2::parametric::trianglesurface::algorithms::
                 countTriSamplingPSpacePositions(size_t(m_samp_lvl)));
  }


  int TriangleSurfaceGeometry::faceCount() const
  {

    assert(m_samp_lvl > 0);
    return int(
      gmlib2::parametric::trianglesurface::algorithms::countTriSamplingFaces(
        size_t(m_samp_lvl)));
  }


  std::vector<std::array<size_t, 3>>
  TriangleSurfaceGeometry::faceIndices() const
  {
    assert(m_samp_lvl > 0);

    return gmlib2::parametric::trianglesurface::algorithms::
      generateTriSamplingFaceIndices(size_t(m_samp_lvl));
  }

  int TriangleSurfaceMesh::samplingLevel() const
  {
    return m_geometry->samplingLevel();
  }

  void TriangleSurfaceMesh::sample()
  {
    m_geometry->sample();
    emit meshSampled();
  }

  void TriangleSurfaceMesh::setSamplingLevel(int level)
  {
    m_geometry->setSamplingLevel(level);
    emit samplingLevelChanged(level);
  }

}   // namespace integration
