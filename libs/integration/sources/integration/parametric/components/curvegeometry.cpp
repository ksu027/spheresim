#include "curvegeometry.h"

// stl
#include <iostream>

namespace integration
{


  void CurveGeometry::updateVertices()
  {
    if(not m_data.m_initialized) return;

    auto* curve      = m_curve_sampler.get();
    auto  sample_set = curve->sample(size_t(m_samples), 1);


    const int no_verts = vertexCount();
    const int stride   = sizeof(VertexElement);


    m_data.m_position_attribute->setCount(quint32(no_verts));
    m_data.m_normal_attribute->setCount(quint32(no_verts));
    m_data.m_tangent_attribute->setCount(quint32(no_verts));



    QByteArray buffer_bytes;
    buffer_bytes.resize(stride * no_verts);
    VertexElement* ptr = reinterpret_cast<VertexElement*>(buffer_bytes.data());


    // Slice tube sampling
    auto slice_samples
      = gmlib2::DVectorT<gmlib2::DVectorT<gmlib2::VectorT<double, 4UL>>>(
        size_t(m_slices));
    {
      double ps = 0.0;
      double pe = 2.0 * M_PI;
      double pd = pe - ps;
      double dt = pd / double(m_slices);

      for (int i = 0; i < m_slices; ++i) {

        const double c_t  = ps + dt * i;
        const auto   c_r  = double(m_radius);
        const auto   c_ct = c_r * std::cos(c_t);
        const auto   c_st = c_r * std::sin(c_t);

        slice_samples[size_t(i)]
          = {gmlib2::VectorT<double, 4UL>{0.0, c_ct, c_st, 1.0},
             gmlib2::VectorT<double, 4UL>{0.0, -c_st, c_ct, 0.0}};
      }
    }

    // Helper auto function
    auto fillElementPtr
      = [&ptr](const auto& frame, const auto& slice_set, const auto& pos) {
          for (auto& slice : slice_set) {

            const auto p = blaze::evaluate(pos + frame * slice[0]);
            const auto u = blaze::evaluate(
              frame * gmlib2::VectorT<double, 4>{1.0, 0.0, 0.0, 0.0});
            const auto v = blaze::evaluate(frame * slice[1]);
            const auto n = blaze::evaluate(blaze::normalize(blaze::cross(
              blaze::subvector(u, 0UL, 3UL), blaze::subvector(v, 0UL, 3UL))));

            ptr->p[0] = float(p[0]);
            ptr->p[1] = float(p[1]);
            ptr->p[2] = float(p[2]);

            ptr->n[0] = float(n[0]);
            ptr->n[1] = float(n[1]);
            ptr->n[2] = float(n[2]);

            // Tangent (u)
            ptr->t[0] = float(u[0]);
            ptr->t[1] = float(u[1]);
            ptr->t[2] = float(u[2]);
            ptr->t[3] = float(u[3]);
            ptr++;
          }
        };

    // Frame & view
//    auto frame = gmlib2::spaces::projectivespace::identityFrame<
//      gmlib2::parametric::Curve<SceneObject>::EmbedSpace>();
    auto frame = gmlib2::spaces::projectivespace::identityFrame<
      gmlib2::spaces::projectivespace::ProjectiveSpace<gmlib2::spaces::D3R3SpaceInfo<>>>();
    auto rframe = blaze::submatrix(frame, 0UL, 0UL, 3UL, 3UL);

    {
      rframe = gmlib2::algorithms::linearIndependetFrameTo(
        static_cast<gmlib2::DVectorT<double>>(
          blaze::subvector(sample_set[0UL][1UL], 0UL, 3UL)));
      fillElementPtr(frame, slice_samples, sample_set[0UL][0UL]);
    }

    for (size_t i = 1; i < size_t(m_samples); ++i) {

      // RMF
      rframe = gmlib2::algorithms::rotationMinimizingFrameMSDR<double, 3>(
        static_cast<gmlib2::DVectorT<double>>(blaze::column(rframe, 2UL)),
        static_cast<gmlib2::DVectorT<double>>(
          blaze::subvector(sample_set[i - 1UL][0UL], 0UL, 3UL)),
        static_cast<gmlib2::DVectorT<double>>(
          blaze::subvector(sample_set[i][0UL], 0UL, 3UL)),
        static_cast<gmlib2::DVectorT<double>>(blaze::column(rframe, 0UL)),
        static_cast<gmlib2::DVectorT<double>>(
          blaze::subvector(sample_set[i][1UL], 0UL, 3UL)));

      // Fill slice
      fillElementPtr(frame, slice_samples, sample_set[size_t(i)][0UL]);
    }
    m_data.m_vertex_buffer->setData(buffer_bytes);
  }

  void CurveGeometry::updateIndices()
  {
    if (not m_data.m_initialized) return;

    const int faces = faceCount();

    m_data.m_index_attribute->setCount(quint32(faces * 3));


    QByteArray indexBytes;
    indexBytes.resize(faces * int(sizeof(IndexElement)));
    IndexElement* iptr = reinterpret_cast<IndexElement*>(indexBytes.data());

    for (int i = 0; i < m_samples - 1; ++i) {

      const auto row      = i * m_slices;
      const auto next_row = (i + 1) * m_slices;

      for (int j = 0; j < m_slices - 1; ++j) {

        // tri #1
        iptr->i[0] = quint32(row + j);
        iptr->i[1] = quint32(next_row + j);
        iptr->i[2] = quint32(row + j + 1);
        iptr++;

        // tri #2
        iptr->i[0] = quint32(next_row + j);
        iptr->i[1] = quint32(next_row + j + 1);
        iptr->i[2] = quint32(row + j + 1);
        iptr++;
      }

      // closed rim
      {
        // tri #1
        iptr->i[0] = quint32(row + m_slices - 1);
        iptr->i[1] = quint32(next_row + m_slices - 1);
        iptr->i[2] = quint32(row);
        iptr++;

        // tri #2
        iptr->i[0] = quint32(next_row + m_slices - 1);
        iptr->i[1] = quint32(next_row);
        iptr->i[2] = quint32(row);
        iptr++;
      }
    }


    m_data.m_index_buffer->setData(indexBytes);
  }

  int CurveGeometry::samples() const { return m_samples; }

  int CurveGeometry::slices() const { return m_slices; }

  float CurveGeometry::radius() const { return m_radius; }

  void CurveGeometry::setSamples(int samples)
  {
    if (samples == m_samples) return;
    m_samples = samples;
  }

  void CurveGeometry::sample()
  {
    if (m_samples < 2) return;
    if (m_slices < 2) return;

    doSample();
  }

  void CurveGeometry::doSample()
  {
    updateVertices();
    updateIndices();
    emit samplesChanged(m_samples);
  }

  void CurveGeometry::setSlices(int slices)
  {
    if (m_slices not_eq slices) {
      m_slices = slices;
      emit slicesChanged(slices);
    }
  }

  void CurveGeometry::setRadius(float radius)
  {
    if (not qFuzzyCompare(m_radius, radius)) {
      m_radius = radius;
      emit radiusChanged(radius);
    }
  }



  void CurveGeometry::initRenderData()
  {
    m_data.m_vertex_buffer = new Qt3DRender::QBuffer(this);
    m_data.m_index_buffer  = new Qt3DRender::QBuffer(this);

    const int stride = sizeof(VertexElement);   //  [ position, normals,
                                                //    tangents ]

    m_data.m_position_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_position_attribute->setName(
      Qt3DRender::QAttribute::defaultPositionAttributeName());
    m_data.m_position_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_position_attribute->setVertexSize(3);
    m_data.m_position_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_position_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_position_attribute->setByteStride(stride);

    m_data.m_normal_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_normal_attribute->setName(
      Qt3DRender::QAttribute::defaultNormalAttributeName());
    m_data.m_normal_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_normal_attribute->setVertexSize(3);
    m_data.m_normal_attribute->setByteOffset(3 * sizeof(float));
    m_data.m_normal_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_normal_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_normal_attribute->setByteStride(stride);

    m_data.m_tangent_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_tangent_attribute->setName(
      Qt3DRender::QAttribute::defaultTangentAttributeName());
    m_data.m_tangent_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_tangent_attribute->setVertexSize(4);
    m_data.m_tangent_attribute->setByteOffset(6 * sizeof(float));
    m_data.m_tangent_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_tangent_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_tangent_attribute->setByteStride(stride);


    m_data.m_index_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_index_attribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    m_data.m_index_attribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    m_data.m_index_attribute->setBuffer(m_data.m_index_buffer);


    addAttribute(m_data.m_position_attribute);
    addAttribute(m_data.m_normal_attribute);
    addAttribute(m_data.m_tangent_attribute);
    addAttribute(m_data.m_index_attribute);

    m_data.m_initialized = true;
  }

  int CurveGeometry::vertexCount() const { return m_samples * m_slices; }

  int CurveGeometry::faceCount() const
  {
    return 2 * (m_samples - 1) * (m_slices);
  }



  int CurveMesh::samples() const { return m_geometry->samples(); }

  int CurveMesh::slices() const { return m_geometry->slices(); }

  float CurveMesh::radius() const { return m_geometry->radius(); }

  void CurveMesh::sample() {
    m_geometry->sample();
    emit meshSampled();
  }

  void CurveMesh::setSamples(int samples) {
    m_geometry->setSamples(samples);
    emit samplesChanged(samples);
  }

  void CurveMesh::setSlices(int slices) {
    m_geometry->setSlices(slices);
    emit slicesChanged(slices);
  }

  void CurveMesh::setRadius(float radius) {
    m_geometry->setRadius(radius);
    emit radiusChanged(radius);
  }

}   // namespace integration
