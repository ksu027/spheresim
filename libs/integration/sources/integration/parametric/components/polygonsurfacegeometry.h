#ifndef INTEGRATION_PARAMETRIC_COMPONENTS_PPOLYGONSURFACEGEOMETRY_H
#define INTEGRATION_PARAMETRIC_COMPONENTS_PPOLYGONSURFACEGEOMETRY_H


#include "../../sceneobject.h"

// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DExtras>

namespace integration
{


  namespace detail
  {

    class PolygonSurfaceSamplerBase {
    public:
      PolygonSurfaceSamplerBase()          = default;
      virtual ~PolygonSurfaceSamplerBase() = default;

      using EvaluationResult
        = gmlib2::datastructures::parametrics::polygonsurface::EvaluationResult<>;

      using SamplingResult
        = gmlib2::datastructures::parametrics::polygonsurface::SamplingResult<>;

      using SamplingPoints
        = gmlib2::datastructures::parametrics::polygonsurface::SamplingPoints<>;

      using Polygon2D = gmlib2::DVectorT<gmlib2::VectorT<double, 2>>;

      virtual size_t         sides() const     = 0;
      virtual Polygon2D      polygon2D() const = 0;
      virtual SamplingResult
      sample(const SamplingPoints& sampling_pspace_positions) const = 0;
    };

    template <typename PolygonSurface_T>
    class PolygonSurfaceSampler : public PolygonSurfaceSamplerBase {
    public:
      using PSpaceSizeArray = typename PolygonSurface_T::PSpaceSizeArray;
      using PSpaceUnit      = typename PolygonSurface_T::PSpaceUnit;
      using SamplingPoints
        = gmlib2::datastructures::parametrics::polygonsurface::SamplingPoints<>;

      PolygonSurfaceSampler(PolygonSurface_T* polygon)
        : m_polygonsurface{polygon}
      {
        static_assert(
          std::is_same<PolygonSurfaceSamplerBase::SamplingResult,
                       typename PolygonSurface_T::SamplingResult>::value,
          "PolygonSurface does not have expected SamplingResult "
          "type!");
      }
      ~PolygonSurfaceSampler() override = default;


      // PolygonSurfaceConstructionWrapperInterface interface
      size_t sides() const override
      {
        if (m_polygonsurface)
          return m_polygonsurface->sides();
        else
          return 0;
      }

      Polygon2D polygon2D() const override
      {
        if (m_polygonsurface)
          return m_polygonsurface->polygon2D();
        else
          return Polygon2D();
      }

      SamplingResult
      sample(const SamplingPoints& sampling_pspace_positions) const override
      {
        if(not m_polygonsurface) return SamplingResult{};

        return gmlib2::parametric::polygonsurface::sample(
          m_polygonsurface, sampling_pspace_positions, PSpaceSizeArray{1, 1});
      }

      PolygonSurface_T* m_polygonsurface{nullptr};
    };
  }   // namespace detail





  class PolygonSurfaceGeometry : public Qt3DRender::QGeometry {
    Q_OBJECT

    struct RenderData {
      QPointer<Qt3DRender::QBuffer>    m_vertex_buffer{nullptr};
      QPointer<Qt3DRender::QBuffer>    m_index_buffer{nullptr};
      QPointer<Qt3DRender::QAttribute> m_position_attribute{nullptr};
      QPointer<Qt3DRender::QAttribute> m_normal_attribute{nullptr};
      QPointer<Qt3DRender::QAttribute> m_index_attribute{nullptr};
      QPointer<Qt3DRender::QAttribute> m_tangent_attribute{nullptr};
      bool                             m_initialized{false};
    };

  public:
    explicit PolygonSurfaceGeometry(QNode* parent)
      : Qt3DRender::QGeometry(parent)
    {
    }

    template <typename PolygonSurface_T>
    PolygonSurfaceGeometry(PolygonSurface_T* surface, QNode* parent)
      : Qt3DRender::QGeometry(parent)
    {
      set<detail::PolygonSurfaceSampler>(surface);
    }

    ~PolygonSurfaceGeometry() override = default;



    template <template <typename> typename PolygonSurfaceSampler_T,
              typename PolygonSurface_T>
    void set(PolygonSurface_T* surface)
    {
      m_surface_sampler
        = std::make_unique<PolygonSurfaceSampler_T<PolygonSurface_T>>(
          surface);
      initRenderData();
    }


    int samplingLevel() const;


    struct VertexElement {
      std::array<float, 3> p;
      std::array<float, 3> n;
      std::array<float, 4> t;


      void printInline() const
      {
        std::cout << "p: " << p[0] << ", " << p[1] << ", " << p[2];
        std::cout << ", n: " << n[0] << ", " << n[1] << ", " << n[2];
        std::cout << ", t: " << t[0] << ", " << t[1] << ", " << t[2] << ", "
                  << t[3];
      }

      void print() const
      {
        printInline();
        std::cout << std::endl;
      }
    };

    struct IndexElement {
      std::array<quint32, 3> i;

      void print() const
      {
        std::cout << i[0] << ", " << i[1] << ", " << i[2] << std::endl;
      }
    };



    void updateVertices();
    void updateIndices();

    virtual int vertexCount() const;
    virtual int faceCount() const;

    virtual std::vector<gmlib2::VectorT<double, 2UL>>
    vertexSamplePositions() const;
    virtual std::vector<std::array<size_t, 3>> faceIndices() const;



  protected:

    // Default properties
    int                                                m_samp_lvl{0};
    std::unique_ptr<detail::PolygonSurfaceSamplerBase> m_surface_sampler{
      nullptr};
    RenderData m_data;

    void initRenderData();
    void doSample();

  public slots:
    void         sample();
    virtual void setSamplingLevel(int level = 3);

  signals:
    void samplingLevelChanged(int level);
  };





  class PolygonSurfaceMesh : public Qt3DRender::QGeometryRenderer {
    Q_OBJECT

    Q_PROPERTY(int samplingLevel READ samplingLevel WRITE setSamplingLevel
                 NOTIFY samplingLevelChanged)

  public:
    PolygonSurfaceMesh() = delete;

    template <typename PolygonSurface_T>
    PolygonSurfaceMesh(PolygonSurface_T* surface);

    template <typename PolygonSurface_T, typename PolygonSurfaceGeometry_T>
    PolygonSurfaceMesh(PolygonSurface_T*         surface,
                       PolygonSurfaceGeometry_T* geometry);

    int samplingLevel() const;

    QPointer<PolygonSurfaceGeometry> m_geometry{nullptr};

  public slots:
    void sample();
    void setSamplingLevel(int level = 3);

  signals:
    void samplingLevelChanged(int level);
    void meshSampled();
  };



  template <typename PolygonSurface_T>
  PolygonSurfaceMesh::PolygonSurfaceMesh(PolygonSurface_T* surface)
    : Qt3DRender::QGeometryRenderer(surface)
  {
    static_assert(std::is_base_of<SceneObject, PolygonSurface_T>::value, "...");
    m_geometry = new PolygonSurfaceGeometry(surface, this);

    connect(m_geometry, &PolygonSurfaceGeometry::samplingLevelChanged, this,
            &PolygonSurfaceMesh::samplingLevelChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }

  template <typename PolygonSurface_T, typename PolygonSurfaceGeometry_T>
  PolygonSurfaceMesh::PolygonSurfaceMesh(PolygonSurface_T*         surface,
                                         PolygonSurfaceGeometry_T* geometry)
    : Qt3DRender::QGeometryRenderer(surface)
  {
    static_assert(std::is_base_of<SceneObject, PolygonSurface_T>::value, "...");
    static_assert(
      std::is_base_of<PolygonSurfaceGeometry, PolygonSurfaceGeometry_T>::value,
      "...");

    m_geometry = geometry;
    m_geometry->setParent(this);

    connect(m_geometry, &PolygonSurfaceGeometry_T::samplingLevelChanged, this,
            &PolygonSurfaceMesh::samplingLevelChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }


}   // namespace integration

#endif   // INTEGRATION_PARAMETRIC_COMPONENTS_PPOLYGONSURFACEGEOMETRY_H
