#include "surfacegeometry.h"

// qt
#include <QtConcurrent/QtConcurrent>
#include <QDebug>

// stl
#include <iostream>
#include <iomanip>


namespace integration
{


  void SurfaceGeometry::updateVertices()
  {
    if(not m_data.m_initialized) return;

//    auto* surface    = m_surface_sampler.get();
    auto  sample_set = m_sample_set;//surface->sample(m_samples, QSize(1, 1));

    const int M1 = m_sample_set.rows();
    const int M2 = m_sample_set.columns();
    const int stride = sizeof(VertexElement);
    const int no_verts = vertexCount();

    m_data.m_position_attribute->setCount(quint32(no_verts));
    m_data.m_normal_attribute->setCount(quint32(no_verts));
    m_data.m_tangent_attribute->setCount(quint32(no_verts));


    QByteArray buffer_bytes;
    buffer_bytes.resize(int(stride * no_verts));
    VertexElement* ptr = reinterpret_cast<VertexElement*>(buffer_bytes.data());

    for (int i = 0; i < M1; ++i) {
      for (int j = 0; j < M2; ++j) {

        const auto& sample = sample_set(size_t(i),size_t(j));

        // Position
        const auto& func = blaze::eval(sample(0, 0));
        ptr->p[0] = float(func[0]);
        ptr->p[1] = float(func[1]);
        ptr->p[2] = float(func[2]);

        // "some" u_der tangent
        if (sample.rows() > 0) {
          const auto& der_u = blaze::eval(sample(1, 0));
          ptr->t[0]         = float(der_u[0]);
          ptr->t[1]         = float(der_u[1]);
          ptr->t[2]         = float(der_u[2]);
          ptr->t[3]         = 1.0f; // Bi-vector direction component
        }

        // Normal
        if (sample.rows() > 0 and sample.columns() > 0) {
          const auto& der_u  = blaze::eval(sample(1, 0));
          const auto& der_v  = blaze::eval(sample(0, 1));
          const auto normal = blaze::cross(blaze::subvector(der_u, 0UL, 3UL),
                                           blaze::subvector(der_v, 0UL, 3UL));
          ptr->n[0]         = float(normal[0]);
          ptr->n[1]         = float(normal[1]);
          ptr->n[2]         = float(normal[2]);
        }

        // Texture coordiantes
        ptr->uv[0] = float((i) / float(M1-1) );
        ptr->uv[1] = float((j) / float(M2-1) );

        // Iterate
        ptr++;
      }
    }
    m_data.m_vertex_buffer->setData(buffer_bytes);
  }

  void SurfaceGeometry::updateIndices() {

    if(not m_data.m_initialized) return;

    const int faces = faceCount();

    m_data.m_index_attribute->setCount(quint32(faces * 3));

    const int M1 = m_sample_set.rows();
    const int M2 = m_sample_set.columns();

    QByteArray indexBytes;
    indexBytes.resize(faces * int(sizeof(IndexElement)));
    IndexElement* iptr = reinterpret_cast<IndexElement*>(indexBytes.data());

    for (int i = 0; i < M1 - 1; ++i) {

      const auto row      = i * M2;
      const auto next_row = (i + 1) * M2;

      for (int j = 0; j < M2 - 1; ++j) {

        // tri #1
        iptr->i[0] = quint32(row + j);
        iptr->i[1] = quint32(next_row + j);
        iptr->i[2] = quint32(row + j + 1);
        iptr++;

        // tri #2
        iptr->i[0] = quint32(next_row + j);
        iptr->i[1] = quint32(next_row + j + 1);
        iptr->i[2] = quint32(row + j + 1);
        iptr++;
      }
    }
    m_data.m_index_buffer->setData(indexBytes);
  }

  QSize SurfaceGeometry::samples() const
  {
    return m_samples;
  }

  void SurfaceGeometry::setSamples(const QSize &samples)
  {
    if(m_samples == samples) return;
    m_samples = samples;
  }

  void SurfaceGeometry::sample() {

    if (m_samples.height() < 2 or m_samples.width() < 2) return;

    doSample();
  }


  void SurfaceGeometry::doSample() {

    m_sample_nrs.clear();
    for( int i = 0; i < m_samples.width(); ++i)
      for (int j = 0; j < m_samples.height(); ++j) m_sample_nrs.append({i, j});

    m_sample_set.resize(size_t(m_samples.width()), size_t(m_samples.height()));

#if 0
    std::function<FutureResult(const QPoint&)> eval_core
      = [this](const QPoint& what) -> FutureResult {
      qDebug() << "what " << what;
      auto* surface = this->m_surface_sampler.get();
      FutureResult res;
      res.first  = std::pair(size_t(what.x()), size_t(what.y()));
      res.second = surface->samplePart(this->m_samples, what, QSize(1, 1));
      return res;
    };
    QFuture<FutureResult> future
      = QtConcurrent::mapped(m_sample_nrs, eval_core);
#elif 0

    QFuture<FutureResult> future = QtConcurrent::run([this]() {
      auto* surface = this->m_surface_sampler.get();
      return surface->sample(this->m_samples, QSize(1, 1));
    });

#elif 1
    m_sample_set = m_surface_sampler->sample(this->m_samples, QSize(1, 1));
    handleDoSampleDone();
#else

    std::function<void(QPoint&)> eval_core = [this](QPoint& what) {
      std::cout << "what [" << what.x() << "," << what.y() << "]" << std::endl;
      [[maybe_unused]] auto* surface = this->m_surface_sampler.get();
      this->m_sample_set(size_t(what.x()), size_t(what.y()))
        = surface->samplePart(this->m_samples, what, QSize(1, 1));
    };

    QFuture<void> future = QtConcurrent::map(m_sample_nrs, eval_core);
#endif

//    m_sample_watcher.setFuture(future);
  }

  void SurfaceGeometry::handleDoSampleDone()
  {
#if 0
    for (int i = 0; i < m_samples.width() * m_samples.height(); ++i) {
      const auto [idx, sample] = m_sample_watcher.resultAt(i);
      const auto [r, c]        = idx;
      std::cerr << "idx: [" << r << "," << c << "]" << std::endl;
      m_sample_set(r, c) = blaze::evaluate(sample);
    }
#elif 0
    m_sample_set                 = m_sample_watcher.result();
#endif

    updateVertices();
    updateIndices();
    emit samplesChanged(m_samples);
  }

  void SurfaceGeometry::initRenderData()
  {

    m_data.m_vertex_buffer = new Qt3DRender::QBuffer(this);
    m_data.m_index_buffer  = new Qt3DRender::QBuffer(this);


    const int stride = sizeof(VertexElement);   // [ position, normal,
                                               // "some" tangent,texture coordinate]


    m_data.m_position_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_position_attribute->setName(
      Qt3DRender::QAttribute::defaultPositionAttributeName());
    m_data.m_position_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_position_attribute->setVertexSize(3);
    m_data.m_position_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_position_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_position_attribute->setByteStride(stride);



    m_data.m_normal_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_normal_attribute->setName(
      Qt3DRender::QAttribute::defaultNormalAttributeName());
    m_data.m_normal_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_normal_attribute->setVertexSize(3);
    m_data.m_normal_attribute->setByteOffset(3 * sizeof(float));
    m_data.m_normal_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_normal_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_normal_attribute->setByteStride(stride);



    m_data.m_tangent_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_tangent_attribute->setName(
      Qt3DRender::QAttribute::defaultTangentAttributeName());
    m_data.m_tangent_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_tangent_attribute->setVertexSize(4);
    m_data.m_tangent_attribute->setByteOffset(6 * sizeof(float));
    m_data.m_tangent_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_tangent_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_tangent_attribute->setByteStride(stride);


    m_data.m_texture_coordinate_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_texture_coordinate_attribute->setName(
      Qt3DRender::QAttribute::defaultTextureCoordinateAttributeName());
    m_data.m_texture_coordinate_attribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
    m_data.m_texture_coordinate_attribute->setVertexSize(2);
    m_data.m_texture_coordinate_attribute->setByteOffset(10 * sizeof(float));
    m_data.m_texture_coordinate_attribute->setAttributeType(
      Qt3DRender::QAttribute::VertexAttribute);
    m_data.m_texture_coordinate_attribute->setBuffer(m_data.m_vertex_buffer);
    m_data.m_texture_coordinate_attribute->setByteStride(stride);


    m_data.m_index_attribute = new Qt3DRender::QAttribute(this);
    m_data.m_index_attribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    m_data.m_index_attribute->setVertexBaseType(Qt3DRender::QAttribute::UnsignedInt);
    m_data.m_index_attribute->setBuffer(m_data.m_index_buffer);


    addAttribute(m_data.m_position_attribute);
    addAttribute(m_data.m_normal_attribute);
    addAttribute(m_data.m_tangent_attribute);
    addAttribute(m_data.m_texture_coordinate_attribute);
    addAttribute(m_data.m_index_attribute);

    m_data.m_initialized = true;
  }

  int SurfaceGeometry::vertexCount() const
  {
    return m_samples.width() * m_samples.height();
  }

  int SurfaceGeometry::faceCount() const
  {
    return 2 * (m_samples.width() - 1) * (m_samples.height() - 1);
  }

  QSize SurfaceMesh::samples() const
  {
    return m_geometry->samples();
  }

  void SurfaceMesh::sample() {
    m_geometry->sample();
    emit meshSampled();
  }

  void SurfaceMesh::setSamples(const QSize &samples)
  {
    m_geometry->setSamples(samples);
    emit samplesChanged(samples);
  }

}   // namespace integration
