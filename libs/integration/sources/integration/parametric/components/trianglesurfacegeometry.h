#ifndef INTEGRATION_PARAMETRIC_COMPONENTS_PTRIANGLESURFACEGEOMETRY_H
#define INTEGRATION_PARAMETRIC_COMPONENTS_PTRIANGLESURFACEGEOMETRY_H


#include "../../sceneobject.h"

// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DExtras>

namespace integration
{

  namespace detail
  {

    class TriangleSurfaceSamplerBase {
    public:
      TriangleSurfaceSamplerBase()          = default;
      virtual ~TriangleSurfaceSamplerBase() = default;

      using EvaluationResult = gmlib2::datastructures::parametrics::
        trianglesurface::EvaluationResult<>;

      using SamplingResult = gmlib2::datastructures::parametrics::
        trianglesurface::SamplingResult<>;

      virtual SamplingResult sample(size_t sampling_level) const = 0;
    };

    template <typename TriangleSurface_T>
    class TriangleSurfaceSampler : public TriangleSurfaceSamplerBase {
    public:
      using PSpaceSizeArray = typename TriangleSurface_T::PSpaceSizeArray;
      using PSpaceUnit      = typename TriangleSurface_T::PSpaceUnit;
      using SamplingPSpacePositions
        = std::vector<gmlib2::VectorT<PSpaceUnit, 2UL>>;

      TriangleSurfaceSampler(TriangleSurface_T* triangle)
        : m_trianglesurface{triangle}
      {
        static_assert(
          std::is_same<TriangleSurfaceSamplerBase::SamplingResult,
                       typename TriangleSurface_T::SamplingResult>::value,
          "PolygonSurfaceConstruction does not have expected SamplingResult "
          "type!");
      }
      ~TriangleSurfaceSampler() override = default;



      SamplingResult sample(size_t sampling_level) const override
      {
        if( not m_trianglesurface ) SamplingResult{};

        return gmlib2::parametric::trianglesurface::sample(m_trianglesurface,
                                                           sampling_level, 1ul);
      }


      TriangleSurface_T* m_trianglesurface{nullptr};
    };
  }   // namespace detail





  class TriangleSurfaceGeometry : public Qt3DRender::QGeometry {
    Q_OBJECT


    struct RenderData {
      QPointer<Qt3DRender::QBuffer>    m_vertex_buffer;
      QPointer<Qt3DRender::QBuffer>    m_index_buffer;
      QPointer<Qt3DRender::QAttribute> m_position_attribute;
      QPointer<Qt3DRender::QAttribute> m_normal_attribute;
      QPointer<Qt3DRender::QAttribute> m_index_attribute;
      QPointer<Qt3DRender::QAttribute> m_tangent_attribute;
      bool                             m_initialized{false};
    };



  public:
    explicit TriangleSurfaceGeometry(QNode* parent)
      : Qt3DRender::QGeometry(parent)
    {
    }

    template <typename TriangleSurface_T>
    TriangleSurfaceGeometry(TriangleSurface_T* surface, QNode* parent)
      : Qt3DRender::QGeometry(parent)
    {
      set<detail::TriangleSurfaceSampler>(surface);
    }

    ~TriangleSurfaceGeometry() override = default;

    template <template <typename> typename TriangleSampler_T,
              typename Triangle_T>
    void set(Triangle_T* triangle)
    {
      m_triangle_sampler
        = std::make_unique<TriangleSampler_T<Triangle_T>>(triangle);
      initRenderData();
    }


    int samplingLevel() const;


    struct VertexElement {
      std::array<float, 3> p;
      std::array<float, 3> n;
      std::array<float, 4> t;


      void printInline() const
      {
        std::cout << "p: " << p[0] << ", " << p[1] << ", " << p[2];
        std::cout << ", n: " << n[0] << ", " << n[1] << ", " << n[2];
        std::cout << ", t: " << t[0] << ", " << t[1] << ", " << t[2] << ", "
                  << t[3];
      }

      void print() const
      {
        printInline();
        std::cout << std::endl;
      }
    };

    struct IndexElement {
      std::array<quint32, 3> i;

      void print() const
      {
        std::cout << i[0] << ", " << i[1] << ", " << i[2] << std::endl;
        ;
      }
    };



    void updateVertices();
    void updateIndices();

    virtual int vertexCount() const;
    virtual int faceCount() const;




  private:

    // Default properties
    int                                                  m_samp_lvl{0};
    std::unique_ptr<detail::TriangleSurfaceSamplerBase> m_triangle_sampler{
      nullptr};
    RenderData m_data;

    void                                       initRenderData();
    void                                       doSample();
    virtual std::vector<std::array<size_t, 3>> faceIndices() const;

  public slots:
    void sample();
    void setSamplingLevel(int level = 4);

  signals:
    void samplingLevelChanged(int level);
  };





  class TriangleSurfaceMesh : public Qt3DRender::QGeometryRenderer {
    Q_OBJECT

    Q_PROPERTY(int samplingLevel READ samplingLevel WRITE setSamplingLevel
                 NOTIFY samplingLevelChanged)
  public:
    template <typename TriangleSurface_T>
    TriangleSurfaceMesh(TriangleSurface_T* surface);

    int samplingLevel() const;

    TriangleSurfaceGeometry* m_geometry{nullptr};

  public slots:
    void sample();
    void setSamplingLevel(int level = 4);

  signals:
    void samplingLevelChanged(int level);
    void meshSampled();
  };



  template <typename TriangleSurface_T>
  TriangleSurfaceMesh::TriangleSurfaceMesh(TriangleSurface_T* surface)
    : Qt3DRender::QGeometryRenderer(surface)
  {
    static_assert(
      std::is_base_of<SceneObject, TriangleSurface_T>::value,
      "...");

    m_geometry = new TriangleSurfaceGeometry(surface,this);
    connect(m_geometry, &TriangleSurfaceGeometry::samplingLevelChanged, this,
            &TriangleSurfaceMesh::samplingLevelChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }


}   // namespace integration

#endif   // INTEGRATION_PARAMETRIC_COMPONENTS_PTRIANGLESURFACEGEOMETRY_H
