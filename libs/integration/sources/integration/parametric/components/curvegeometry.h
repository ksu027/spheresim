#ifndef INTEGRATION_PARAMETRIC_COMPONENTS_PCURVEGEOMETRY_H
#define INTEGRATION_PARAMETRIC_COMPONENTS_PCURVEGEOMETRY_H


#include "../../sceneobject.h"


// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DRender>

// stl
#include <memory>


namespace integration
{

  namespace detail
  {

    class CurveSamplerBase {
    public:
      CurveSamplerBase()          = default;
      virtual ~CurveSamplerBase() = default;

      using EvaluationResult
        = gmlib2::datastructures::parametrics::curve::EvaluationResult<>;

      using SamplingResult
        = gmlib2::datastructures::parametrics::curve::SamplingResult<>;

      virtual SamplingResult sample(size_t samples,
                                    size_t derivatives) const = 0;
    };


    template <typename Curve_T>
    class CurveSampler : public CurveSamplerBase {
    public:
      using PSpaceSizeArray = typename Curve_T::PSpaceSizeArray;

      CurveSampler(Curve_T* curve) : m_curve{curve}
      {
        static_assert(std::is_same<CurveSamplerBase::SamplingResult,
                                   typename Curve_T::SamplingResult>::value,
                      "Curve does not have expected SamplingResult type!");
      }
      ~CurveSampler() override = default;


    public:
      SamplingResult sample(size_t no_samples,
                            size_t no_derivatives) const override
      {
        if (not m_curve or no_samples < 2) return SamplingResult{};

        return gmlib2::parametric::curve::sample(
          m_curve, m_curve->startParameters(), m_curve->endParameters(),
          PSpaceSizeArray{{no_samples}}, PSpaceSizeArray{{no_derivatives}});
      }


      Curve_T* m_curve{nullptr};
    };
  }   // namespace detail





  class CurveGeometry : public Qt3DRender::QGeometry {
    Q_OBJECT

    struct RenderData {
      QPointer<Qt3DRender::QBuffer>    m_vertex_buffer{nullptr};
      QPointer<Qt3DRender::QBuffer>    m_index_buffer{nullptr};
      QPointer<Qt3DRender::QAttribute> m_position_attribute{nullptr};
      QPointer<Qt3DRender::QAttribute> m_normal_attribute{nullptr};
      QPointer<Qt3DRender::QAttribute> m_index_attribute{nullptr};
      QPointer<Qt3DRender::QAttribute> m_tangent_attribute{nullptr};
      bool                             m_initialized{false};
    };


  public:
    explicit CurveGeometry(QNode* parent) : Qt3DRender::QGeometry(parent) {}

    template <typename Curve_T>
    CurveGeometry(Curve_T* curve, QNode* parent) : CurveGeometry(parent)
    {
      set<detail::CurveSampler>(curve);
    }

    ~CurveGeometry() override = default;


    template <template <typename> typename CurveSampler_T, typename Curve_T>
    void set(Curve_T* curve)
    {
      m_curve_sampler = std::make_unique<CurveSampler_T<Curve_T>>(curve);
      initRenderData();
    }

    int   samples() const;
    int   slices() const;
    float radius() const;

    struct VertexElement {
      std::array<float, 3> p;
      std::array<float, 3> n;
      std::array<float, 4> t;

      void printInline() const
      {
        std::cout << "p: " << p[0] << ", " << p[1] << ", " << p[2];
        std::cout << ", n: " << n[0] << ", " << n[1] << ", " << n[2];
        std::cout << ", t: " << t[0] << ", " << t[1] << ", " << t[2] << ", "
                  << t[3];
      }
      void print() const
      {
        printInline();
        std::cout << std::endl;
      }
    };

    struct IndexElement {
      std::array<quint32, 3> i;

      void print() const
      {
        std::cout << i[0] << ", " << i[1] << ", " << i[2] << std::endl;
      }
    };

    int vertexCount() const;
    int faceCount() const;



  private:

    // Default properties
    std::unique_ptr<detail::CurveSamplerBase> m_curve_sampler{nullptr};
    RenderData                                 m_data;
    int                                        m_samples{0};
    int                                        m_slices{10};
    float                                      m_radius{0.1f};

    void initRenderData();
    void doSample();
    void updateVertices();
    void updateIndices();


  public slots:
    void sample();
    void setSamples(int rings);
    void setSlices(int slices);
    void setRadius(float radius);

  signals:
    void samplesChanged(int);
    void slicesChanged(int);
    void radiusChanged(float);
  };




  class CurveMesh : public Qt3DRender::QGeometryRenderer {
    Q_OBJECT

    Q_PROPERTY(int samples READ samples WRITE setSamples NOTIFY samplesChanged)
    Q_PROPERTY(int slices READ slices WRITE setSlices NOTIFY slicesChanged)
    Q_PROPERTY(float radius READ radius WRITE setRadius NOTIFY radiusChanged)

  public:
    CurveMesh() = delete;

    template <typename Curve_T>
    CurveMesh(Curve_T* pcurve);

    int   samples() const;
    int   slices() const;
    float radius() const;

    QPointer<CurveGeometry> m_geometry{nullptr};

  public slots:
    void sample();
    void setSamples(int rings = 20);
    void setSlices(int slices = 10);
    void setRadius(float radius = 0.1f);

  signals:
    void samplesChanged(int);
    void slicesChanged(int);
    void radiusChanged(float);
    void meshSampled();
  };



  template <typename Curve_T>
  CurveMesh::CurveMesh(Curve_T* pcurve)
    : Qt3DRender::QGeometryRenderer(pcurve)
  {
    static_assert(std::is_base_of<SceneObject, Curve_T>::value, "...");

    m_geometry = new CurveGeometry(pcurve, this);

    connect(m_geometry, &CurveGeometry::samplesChanged, this,
            &CurveMesh::samplesChanged);
    connect(m_geometry, &CurveGeometry::slicesChanged, this,
            &CurveMesh::slicesChanged);
    connect(m_geometry, &CurveGeometry::radiusChanged, this,
            &CurveMesh::radiusChanged);
    setGeometry(m_geometry);
    setPrimitiveType(PrimitiveType::Triangles);
  }

}   // namespace integration


#endif   // INTEGRATION_PARAMETRIC_COMPONENTS_PCURVEGEOMETRY_H
