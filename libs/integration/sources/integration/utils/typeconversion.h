#ifndef INTEGRATION_UTILS_TYPECONVERSION_H
#define INTEGRATION_UTILS_TYPECONVERSION_H

// gmlib2
#include <gmlib2.h>

// qt

#include <QMatrix4x4>

namespace integration
{

  template <typename Unit_T>
  QVector3D toQVector3D(const gmlib2::VectorT<Unit_T, 3>& v)
  {
    return {float(v[0]), float(v[1]), float(v[2])};
  }

  template <typename Unit_T>
  typename gmlib2::VectorT<Unit_T, 3> toVector3(const QVector3D& p)
  {
    return gmlib2::VectorT<Unit_T, 3>{Unit_T(p[0]), Unit_T(p[1]), Unit_T(p[2])};
  }

  template <typename Unit_T>
  QMatrix4x4 toQMatrix4x4(const gmlib2::MatrixT<Unit_T,4,4>& m)
  {
    // clang-format off
    return QMatrix4x4(
      float(m(0, 0)), float(m(0, 1)), float(m(0, 2)), float(m(0, 3)),
      float(m(1, 0)), float(m(1, 1)), float(m(1, 2)), float(m(1, 3)),
      float(m(2, 0)), float(m(2, 1)), float(m(2, 2)), float(m(2, 3)),
      float(m(3, 0)), float(m(3, 1)), float(m(3, 2)), float(m(3, 3)));
    // clang-format on
  }

}   // namespace integration


#endif   // INTEGRATION_UTILS_TYPECONVERSION_H
