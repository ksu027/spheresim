#include "scenegraph.h"

#include "scenario.h"

namespace integration
{

  QPointer<Qt3DRender::QLayer> Scenegraph::m_transparency_layer = {nullptr};
  QPointer<Qt3DRender::QLayer> Scenegraph::m_select_layer       = {nullptr};
  QPointer<Qt3DRender::QLayer> Scenegraph::m_active_layer       = {nullptr};


  QPointer<Qt3DRender::QLayer> Scenegraph::globalSelectLayerInstance()
  {
    return m_select_layer;
  }

  QPointer<Qt3DRender::QLayer> Scenegraph::globalActiveLayerInstance()
  {
    return m_active_layer;
  }

  QPointer<Qt3DRender::QLayer> Scenegraph::globalTransparencyLayerInstance()
  {
    return m_transparency_layer;
  }


  Scenegraph::Scenegraph(QNode* parent)
    : QEntity(parent), m_scenegraph_model(this)
  {
    // Singletons
    if (not m_transparency_layer)
      m_transparency_layer = new Qt3DRender::QLayer(this);

    if (not m_select_layer) m_select_layer = new Qt3DRender::QLayer(this);
    if (not m_active_layer) m_active_layer = new Qt3DRender::QLayer(this);

    // Select material
    m_select_material = new Qt3DExtras::QDiffuseSpecularMaterial(this);
    auto sel_color = QColor("#FF0000");
    sel_color.setAlphaF(0.3);
    m_select_material->setDiffuse(sel_color);
    m_select_material->setAmbient(sel_color);

    // Active material
    m_active_selected_material = new Qt3DExtras::QDiffuseSpecularMaterial(this);
    auto actsel_color = QColor("#FFFFFF");
    actsel_color.setAlphaF(0.5);
    m_active_selected_material->setDiffuse(actsel_color);
    m_active_selected_material->setAmbient(actsel_color);

    m_active_selected_object = {nullptr,nullptr};

    // Collapsed geometry renderer
    m_collapsed_geometry_renderer = new Qt3DExtras::QCuboidMesh(this);
    m_collapsed_geometry_renderer->setXExtent(0.1f);
    m_collapsed_geometry_renderer->setYExtent(0.1f);
    m_collapsed_geometry_renderer->setZExtent(0.1f);
  }

  Scenegraph::~Scenegraph()
  {
    if (m_transparency_layer) delete m_transparency_layer;
    if (m_select_layer) delete m_select_layer;
    if (m_active_layer) delete m_active_layer;
  }

  ScenegraphModel* Scenegraph::scenegraphModel() { return &m_scenegraph_model; }

  void Scenegraph::lazyPrepare()
  {
    for (const auto& so_dirty_pair : m_dirty_sceneobjects_global_matrix) {

      auto* so = so_dirty_pair.second;

      if (not so->m_global_frames_dirty_flag) continue;

      if (auto* so_parent = qobject_cast<SceneObject*>(so->parent()); so_parent)
        so->updateGlobalFrames(so_parent);
      else
        so->updateGlobalFrames(this);
    }
    m_dirty_sceneobjects_global_matrix.clear();
  }

  const SceneObject::SceneObjectVector& Scenegraph::sceneObjectChildren() const
  {
    return m_sceneobject_children;
  }

  const SceneObject* Scenegraph::activeSelected() const
  {
    return m_active_selected_object.first;
  }

  bool
  Scenegraph::registerScenario(const QString& category, const QString& name,
                               std::function<void(Scenario&)> initialize_fn,
                               std::function<void(Scenario&)> cleanup_fn)

  {
    const auto id = ScenarioId{category, name};
    if (m_scenarios.contains(id)) return false;

    auto* scenario
      = new Scenario(category, name, initialize_fn, cleanup_fn, this);

    m_scenarios[id] = {scenario, false};
    scenario->setParent(this);

    emit scenarioRegistered();

    return true;
  }

  bool
  Scenegraph::registerScenario(const QString& category, const QString& name,
                               std::function<void(Scenario&)> initialize_fn )

  {
    return registerScenario(category, name, initialize_fn,
                            Scenario::defaultScenarioCleanup);
  }

  void Scenegraph::loadScenario(const ScenarioId& id)
  {
    unloadCurrentScenario();

    auto scenario_it
      = std::find_if(m_scenarios.begin(), m_scenarios.end(),
                     [id](const ScenarioRecord& sr) {
                       if (not sr.m_scenario) return true;
                       if (sr.m_scenario->m_category == id.category()
                           and sr.m_scenario->m_name == id.name())
                         return true;
                       return false;
                     });

    if(scenario_it == m_scenarios.end()) return;

    auto scenario = scenario_it->m_scenario;
    connect(scenario, &Scenario::progressChanged, &m_scenegraph_model,
            &ScenegraphModel::reportScenarioLoadProgress);

    scenario->initialize();

    m_current_scenario_id = {scenario->m_category, scenario->m_name};
    m_scenegraph_model.update();
    emit scenarioLoaded(m_current_scenario_id.category(),
                        m_current_scenario_id.name());
  }

  void Scenegraph::unloadCurrentScenario()
  {
    if (not m_scenarios.contains(m_current_scenario_id)) return;
    auto scenario = m_scenarios.value(m_current_scenario_id).m_scenario;
    scenario->cleanup();

    const auto curr_scenario_id = m_current_scenario_id;
    m_current_scenario_id = {};
    m_scenegraph_model.update();
    emit scenarioUnloaded(curr_scenario_id.category(),
                        curr_scenario_id.name());

    disconnect(scenario, &Scenario::progressChanged, &m_scenegraph_model,
               &ScenegraphModel::reportScenarioLoadProgress);
  }

  bool Scenegraph::isScenarioLoaded(const Scenegraph::ScenarioId& id) const {
    return m_current_scenario_id == id;
  }

  void Scenegraph::requestCameraChange(QVector3D position, QVector3D viewCenter,
                                       QVector3D upVector)
  {
    emit cameraChangeRequested(position,viewCenter,upVector);
  }

  bool Scenegraph::visible(const SceneObject* sceneobject) const
  {
    return sceneobject->components().contains(m_collapsed_geometry_renderer)
           || sceneobject->components().contains(
             const_cast<Qt3DRender::QGeometryRenderer*>(
               sceneobject->geometryRenderer()));
  }

  void Scenegraph::markGlobalMatrixDirty(SceneObject* sceneobject)
  {
    m_dirty_sceneobjects_global_matrix.insert(
    {sceneobject->m_scene_depth_pos, sceneobject});
  }



  bool Scenegraph::collapsed(const SceneObject* sceneobject) const
  {
    return m_collapsed_objects.contains(sceneobject);
  }

  void Scenegraph::setCollapsed(SceneObject* sceneobject, bool state)
  {
    if (collapsed(sceneobject) == state) return;

    if (not visible(sceneobject) and not collapsed(sceneobject)) return;

    if (collapsed(sceneobject)) {
      m_collapsed_objects.removeOne(sceneobject);
      sceneobject->resetActiveGeometryRenderer();
    }
    else {
      m_collapsed_objects.append(sceneobject);
      sceneobject->resetActiveGeometryRenderer(m_collapsed_geometry_renderer);
      updateCollapsedSubTree(sceneobject,true);
    }

    m_scenegraph_model.update(
      sceneobject, {int(ScenegraphModel::ScenegraphModelRoles::Collapsed),
                    int(ScenegraphModel::ScenegraphModelRoles::Visible)});

    return;
  }

  void Scenegraph::updateCollapsedSubTree(SceneObject* sceneobject, bool state)
  {
    for (auto* node : sceneobject->childNodes()) {
      auto* so_child = qobject_cast<SceneObject*>(node);
      if (not so_child) continue;

      if (state) {
        if (selected(so_child)) setDeSelected(so_child, true);
        so_child->clearActiveGeometryRenderer();

        m_scenegraph_model.update(
          so_child, {int(ScenegraphModel::ScenegraphModelRoles::Collapsed),
                     int(ScenegraphModel::ScenegraphModelRoles::Visible)});
        updateCollapsedSubTree(so_child, state);
      }

    }
  }



  void Scenegraph::markSelected(SceneObject*                   sceneobject,
                                Qt3DRender::QGeometryRenderer* renderer)
  {
    assert(not m_selected_objects.contains(sceneobject));

    auto* sel_obj
      = createSelectionObject(sceneobject, renderer, m_select_material);

    m_selected_objects[sceneobject] = sel_obj;
  }
  void Scenegraph::markActiveSelected(SceneObject* sceneobject, Qt3DRender::QGeometryRenderer* renderer) {
    assert(m_active_selected_object.first not_eq sceneobject);

    auto* sel_obj = createSelectionObject(sceneobject, renderer,
                                          m_active_selected_material);

    m_active_selected_object = {sceneobject, sel_obj};
  }

  void Scenegraph::markActive(const SceneObject* sceneobject) {

    assert(m_selected_objects.contains(sceneobject));
    assert(m_active_selected_object.first not_eq sceneobject);

    auto* sel_obj = m_selected_objects.take(sceneobject);
    sel_obj->removeComponent(m_select_material);
    sel_obj->addComponent(m_active_selected_material);
    m_active_selected_object = {sceneobject,sel_obj};
  }

  void Scenegraph::unMarkSelected(const SceneObject* sceneobject) {

    assert(m_selected_objects.contains(sceneobject));

    auto* sel_obj = m_selected_objects.take(sceneobject);
    if (not sel_obj) return;

    sel_obj->setParent(reinterpret_cast<QNode*>(0x0));
    for (auto* component : sel_obj->components())
      sel_obj->removeComponent(component);
    sel_obj->deleteLater();
  }
  const SceneObject* Scenegraph::unMarkActiveSelected() {

    auto* actsel_so = m_active_selected_object.first;
    if (actsel_so == nullptr) return nullptr;

    auto* actsel_obj = m_active_selected_object.second;
    m_active_selected_object = {nullptr,nullptr};
    if (not actsel_obj) return actsel_so;

    actsel_obj->setParent(reinterpret_cast<QNode*>(0x0));
    for (auto* component : actsel_obj->components())
      actsel_obj->removeComponent(component);
    actsel_obj->deleteLater();

    return actsel_so;
  }

  const SceneObject* Scenegraph::unMarkActive()
  {
    const auto* actsel_so    = m_active_selected_object.first;
    if (actsel_so == nullptr) return nullptr;

    auto*       actsel_obj   = m_active_selected_object.second;
    m_active_selected_object = {nullptr, nullptr};
    if(not actsel_obj) return actsel_so;

    actsel_obj->removeComponent(m_active_selected_material);
    actsel_obj->addComponent(m_select_material);

    m_selected_objects[actsel_so] = actsel_obj;

    return actsel_so;
  }

  Qt3DCore::QEntity*
  Scenegraph::createSelectionObject(SceneObject*                   sceneobject,
                                    Qt3DRender::QGeometryRenderer* renderer,
                                    Qt3DRender::QMaterial* select_material)
  {
    if(not renderer) return nullptr;

    auto* sel_obj = new QEntity;
    sel_obj->addComponent(m_select_layer);
//    sel_obj->addComponent(select_material);
    sel_obj->addComponent(renderer);
    sel_obj->setParent(sceneobject);
    return sel_obj;
  }

  void Scenegraph::setSelected(SceneObject*                   sceneobject,
                               Qt3DRender::QGeometryRenderer* renderer,
                               bool                           multi)
  {
    if( sceneobject->components().contains(m_select_layer) )
      return;
    sceneobject->addComponent(m_select_layer);


    if( m_active_object and m_active_object->components().contains(m_active_layer))
      m_active_object->removeComponent(m_active_layer);

    m_active_object = sceneobject;
    m_active_object->addComponent(m_active_layer);


    m_scenegraph_model.update(
      sceneobject,
      {int(ScenegraphModel::ScenegraphModelRoles::Selected),
       int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});


//    if (activeSelected(sceneobject)) return;

//    if( selected(sceneobject) and not multi ) {
//      clearActiveSelected();
//      clearSelections();


//      markActiveSelected(sceneobject,renderer);
//      m_scenegraph_model.update(
//        sceneobject,
//        {int(ScenegraphModel::ScenegraphModelRoles::Selected),
//         int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});
//      return;
//    }
//    else if( selected(sceneobject) and multi) {

//      unMarkSelected(sceneobject);
//      const auto* actsel_so = unMarkActive();

//      markActiveSelected(sceneobject,renderer);

//      m_scenegraph_model.update(
//        actsel_so,
//        {int(ScenegraphModel::ScenegraphModelRoles::Selected),
//         int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});
//      m_scenegraph_model.update(
//        sceneobject,
//        {int(ScenegraphModel::ScenegraphModelRoles::Selected),
//         int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});
//      return;
//    }


//    if(not multi) {
//      clearActiveSelected();
//      clearSelections();
//    }


//    const auto* actsel_so = unMarkActive();
//    if (actsel_so not_eq nullptr)
//      m_scenegraph_model.update(
//        actsel_so,
//        {int(ScenegraphModel::ScenegraphModelRoles::Selected),
//         int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});

//    markActiveSelected(sceneobject,renderer);
//    m_scenegraph_model.update(
//      sceneobject,
//      {int(ScenegraphModel::ScenegraphModelRoles::Selected),
//       int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});

  }

  void Scenegraph::setDeSelected(SceneObject* sceneobject, bool multi)
  {
    if( m_active_object and m_active_object->components().contains(m_active_layer))
      m_active_object->removeComponent(m_active_layer);


    if( not sceneobject->components().contains(m_select_layer) )
      return;

    sceneobject->removeComponent(m_select_layer);


    m_scenegraph_model.update(
      sceneobject,
      {int(ScenegraphModel::ScenegraphModelRoles::Selected),
       int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});

    //    if (not multi){
    //      clearActiveSelected();

    //      if(selected(sceneobject)) {

    //        markActive(sceneobject);
    //        m_scenegraph_model.update(
    //          sceneobject,
    //          {int(ScenegraphModel::ScenegraphModelRoles::Selected),
    //           int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});
    //      }
    ////      else
    //      clearSelections();
    //      return;
    //    }


    //    if (activeSelected(sceneobject)){
    //      clearActiveSelected();
    //      return;
    //    }

    //    if(selected(sceneobject)) {

    //      const auto* actsel_so = multi ? unMarkActive() :
    //      unMarkActiveSelected(); if (actsel_so not_eq nullptr)
    //        m_scenegraph_model.update(
    //          actsel_so,
    //          {int(ScenegraphModel::ScenegraphModelRoles::Selected),
    //           int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});

    //      markActive(sceneobject);
    //      m_scenegraph_model.update(
    //        sceneobject,
    //        {int(ScenegraphModel::ScenegraphModelRoles::Selected),
    //         int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});

    //      return;
    //    }
  }

  bool Scenegraph::selected(const SceneObject* sceneobject) const
  {
    return sceneobject->components().contains(m_select_layer);
//    return m_active_selected_object.first == sceneobject
//           or m_selected_objects.contains(sceneobject);
  }

  bool Scenegraph::activeSelected(const SceneObject* sceneobject) const
  {
    return m_active_object == sceneobject;
//    return m_active_selected_object.first == sceneobject;
  }

  void Scenegraph::clearSelections()
  {
    // Keep sceneobject pointers
    auto sos = m_selected_objects.keys();

    // Remove selection objects and references there-of
    for (auto* sel_obj : m_selected_objects) {
      sel_obj->setParent(reinterpret_cast<QNode*>(0x0));
      for (auto* component : sel_obj->components())
        sel_obj->removeComponent(component);
    }
    qDeleteAll(m_selected_objects);
    m_selected_objects.clear();

    // Update scenegraph model
    for (const auto* so : sos)
      m_scenegraph_model.update(
        so, {int(ScenegraphModel::ScenegraphModelRoles::Selected)});
  }

  void Scenegraph::clearActiveSelected()
  {
    if (m_active_selected_object.first not_eq nullptr) {

      const auto* as_so = unMarkActiveSelected();
      if(as_so)
        m_scenegraph_model.update(
          as_so, {int(ScenegraphModel::ScenegraphModelRoles::Selected),
                  int(ScenegraphModel::ScenegraphModelRoles::ActiveSelected)});
    }
  }

  void Scenegraph::childEvent(QChildEvent* event)
  {
    const auto event_type        = event->type();
    auto*      child             = event->child();
    auto*      sceneobject_child = qobject_cast<SceneObject*>(child);

    if (sceneobject_child and event_type == QEvent::ChildAdded) {
      m_sceneobject_children.push_back(sceneobject_child);
      sceneobject_child->updateSceneDepthInfo(0);
    }
    else if (sceneobject_child and event_type == QEvent::ChildRemoved) {
      m_sceneobject_children.removeOne(sceneobject_child);
      sceneobject_child->updateSceneDepthInfo(-1);
    }

    m_scenegraph_model.update();
  }


}   // namespace integration
