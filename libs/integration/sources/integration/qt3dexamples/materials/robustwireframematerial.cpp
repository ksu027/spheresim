#include "robustwireframematerial.h"


namespace qt3dexamples
{

  RobustWireframeMaterial::RobustWireframeMaterial(Qt3DCore::QNode* parent)
    : QMaterial(parent)
  {
    m_effect = new Qt3DRender::QEffect();
    {
      m_ambient_color
        = new Qt3DRender::QParameter("ka", QVector3D(0.5f, 0.1f, 0.1f));
      m_diffuse_color
        = new Qt3DRender::QParameter("kd", QVector3D(0.7f, 0.7f, 0.7f));
      m_specular_color
        = new Qt3DRender::QParameter("ks", QVector3D(0.95f, 0.95f, 0.95f));
      m_alpha = new Qt3DRender::QParameter("alpha", 1.0);
      m_effect->addParameter(m_ambient_color);
      m_effect->addParameter(m_diffuse_color);
      m_effect->addParameter(m_specular_color);
      m_effect->addParameter(m_alpha);
      m_effect->addParameter(new Qt3DRender::QParameter("shininess", 150));

      auto* technique = new Qt3DRender::QTechnique();
      {
        technique->graphicsApiFilter()->setApi(
          Qt3DRender::QGraphicsApiFilter::OpenGL);
        technique->graphicsApiFilter()->setMajorVersion(3);
        technique->graphicsApiFilter()->setMinorVersion(1);
        technique->graphicsApiFilter()->setProfile(
          Qt3DRender::QGraphicsApiFilter::CoreProfile);

        auto* filter = new Qt3DRender::QFilterKey();
        {
          filter->setName("renderingStyle");
          filter->setValue("forward");
        }
        technique->addFilterKey(filter);

        m_light_position = new Qt3DRender::QParameter(
          "light.position", QVector4D(10.0, 10.0, 0.0, 1.0));
        technique->addParameter(m_light_position);
        technique->addParameter(new Qt3DRender::QParameter(
          "light.intensity", QVector3D(1.0, 1.0, 1.0)));
        technique->addParameter(new Qt3DRender::QParameter("line.width", 1.0));
        technique->addParameter(new Qt3DRender::QParameter(
          "line.color", QVector4D(1.0, 1.0, 1.0, 1.0)));

        auto* renderpass = new Qt3DRender::QRenderPass();
        {
          auto* shaderprogram = new Qt3DRender::QShaderProgram();
          {
            shaderprogram->setVertexShaderCode(
              Qt3DRender::QShaderProgram::loadSource(
                QUrl("qrc:/lib_qt3dexamples_shaders/qt3dexamples/materials/"
                     "robustwireframe/robustwireframe.vert")));
            shaderprogram->setGeometryShaderCode(
              Qt3DRender::QShaderProgram::loadSource(
                QUrl("qrc:/lib_qt3dexamples_shaders/qt3dexamples/materials/"
                     "robustwireframe/robustwireframe.geom")));
            shaderprogram->setFragmentShaderCode(
              Qt3DRender::QShaderProgram::loadSource(
                QUrl("qrc:/lib_qt3dexamples_shaders/qt3dexamples/materials/"
                     "robustwireframe/robustwireframe.frag")));
          }
          renderpass->setShaderProgram(shaderprogram);
        }
        technique->addRenderPass(renderpass);
      }
      m_effect->addTechnique(technique);
    }

    setEffect(m_effect);
  }

  QColor RobustWireframeMaterial::ambient() const
  {
    return m_ambient_color->value().value<QColor>();
  }

  void RobustWireframeMaterial::setAmbient(const QColor& color)
  {
    m_ambient_color->setValue(color);
    emit ambientChanged(color);
  }

  QColor RobustWireframeMaterial::diffuse() const
  {
    return m_diffuse_color->value().value<QColor>();
  }

  void RobustWireframeMaterial::setDiffuse(const QColor& color)
  {
    m_diffuse_color->setValue(color);
    emit diffuseChanged(color);
  }

  QColor RobustWireframeMaterial::specular() const
  {
    return m_specular_color->value().value<QColor>();
  }

  void RobustWireframeMaterial::setSpecular(const QColor& color)
  {
    m_specular_color->setValue(color);
    emit specularChanged(color);
  }


  QVector3D RobustWireframeMaterial::lightPosition() const
  {
    const auto lp4 = m_light_position->value().value<QVector4D>();
    return QVector3D(lp4.x(),lp4.y(),lp4.z());
  }

  void RobustWireframeMaterial::setLightPosition(const QVector3D& lp)
  {
    m_light_position->setValue(QVector4D(lp.x(), lp.y(), lp.z(), 1.0));
    emit lightPositionChanged(lp);
  }

  qreal RobustWireframeMaterial::alpha() const {

    return m_alpha->value().toReal();
  }

  void   RobustWireframeMaterial::setAlpha(const qreal& alpha) {

    m_alpha->setValue(alpha);
    emit alphaChanged(alpha);
  }

}   // namespace qt3dexamples
