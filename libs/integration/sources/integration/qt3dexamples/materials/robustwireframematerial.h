#ifndef QT3DEXAMPLES_MATERIALS_ROBUSTWIREFRAMEMATERIAL_H
#define QT3DEXAMPLES_MATERIALS_ROBUSTWIREFRAMEMATERIAL_H


// qt
#include <Qt3DRender>

namespace qt3dexamples {

  class RobustWireframeMaterial : public Qt3DRender::QMaterial {
    Q_OBJECT

    Q_PROPERTY(
      QColor ambient READ ambient WRITE setAmbient NOTIFY ambientChanged)
    Q_PROPERTY(
      QColor diffuse READ diffuse WRITE setDiffuse NOTIFY diffuseChanged)
    Q_PROPERTY(
      QColor specular READ specular WRITE setSpecular NOTIFY specularChanged)
    Q_PROPERTY(qreal alpha READ alpha WRITE setAlpha NOTIFY alphaChanged)

  public:
    RobustWireframeMaterial(Qt3DCore::QNode *parent = nullptr);
    ~RobustWireframeMaterial() override = default;


    QColor ambient() const;
    QColor diffuse() const;
    QColor specular() const;
    QVector3D lightPosition() const;
    qreal  alpha() const;

  public slots:
    void   setAmbient(const QColor& color);
    void   setDiffuse(const QColor& color);
    void   setSpecular(const QColor& color);
    void   setLightPosition(const QVector3D& light_position);
    void   setAlpha(const qreal& alpha);

  private:
    Qt3DRender::QEffect*    m_effect{nullptr};
    Qt3DRender::QParameter* m_ambient_color{nullptr};
    Qt3DRender::QParameter* m_diffuse_color{nullptr};
    Qt3DRender::QParameter* m_specular_color{nullptr};
    Qt3DRender::QParameter* m_light_position{nullptr};
    Qt3DRender::QParameter* m_alpha{nullptr};

  signals:
    void ambientChanged(QColor);
    void diffuseChanged(QColor);
    void specularChanged(QColor);
    void lightPositionChanged(QVector3D);
    void alphaChanged(qreal);
  };

}   // namespace qt3dexamples

#endif // QT3DEXAMPLES_MATERIALS_ROBUSTWIREFRAMEMATERIAL_H
