#ifndef INTEGRATION_SCENEGRAPH_H
#define INTEGRATION_SCENEGRAPH_H


#include "models/scenegraphmodel.h"
//#include "components/parametric/pcurvegeometry.h"
//#include "components/parametric/psurfacegeometry.h"

// qt
#include <QObject>
#include <QPointer>

// blaze
#include <gmlib2.h>

// qt
#include <Qt3DCore>
#include <Qt3DRender>
#include <Qt3DExtras>

// stl
#include <memory>
#include <vector>
#include <iostream>



#include "sceneobject.h"


namespace integration
{

  class Scenario;

  class Scenegraph : public Qt3DCore::QEntity {
    Q_OBJECT

    Q_PROPERTY(ScenegraphModel* scenegraphModel READ scenegraphModel CONSTANT)
    Q_PROPERTY(
      Qt3DRender::QLayer* selectLayer READ globalSelectLayerInstance CONSTANT)
    Q_PROPERTY(
      Qt3DRender::QLayer* activeLayer READ globalActiveLayerInstance CONSTANT)
    Q_PROPERTY(Qt3DRender::QLayer* transparencyLayer READ
                 globalTransparencyLayerInstance CONSTANT)

    // Friends
    friend class SceneObject;
    friend class ScenegraphModel;

  private:
    using ScenarioPtr = QPointer<Scenario>;
    struct ScenarioRecord {
      ScenarioPtr m_scenario;
      bool        m_loaded;
    };

  public:
    struct ScenarioId : QPair<QString, QString> {
      using Base = QPair<QString, QString>;
      using Base::Base;

      const QString& category() const { return first; }
      const QString& name() const { return second; }
    };



  public:
    explicit Scenegraph(QNode* parent = nullptr);
    ~Scenegraph() override;

    void simulate(std::chrono::nanoseconds dt);

    ScenegraphModel*    scenegraphModel();

    void                                  lazyPrepare();
    const SceneObject::SceneObjectVector& sceneObjectChildren() const;

    QSet<QPair<qint64, SceneObject*>> m_dirty_sceneobjects_global_matrix;

    const SceneObject* activeSelected() const;

//    void registerScenario(Scenario* scenario);
    [[nodiscard]] bool
    registerScenario(const QString& category, const QString& name,
                     std::function<void(Scenario&)> initialize_fn,
                     std::function<void(Scenario&)> cleanup_fn);

    [[nodiscard]] bool
    registerScenario(const QString& category, const QString& name,
                     std::function<void(Scenario&)> initialize_fn);

    void loadScenario(const ScenarioId& id);
    void unloadCurrentScenario();
    bool isScenarioLoaded(const ScenarioId& id) const;

    void requestCameraChange(QVector3D position, QVector3D viewCenter,
                             QVector3D upVector);

  private:
    ScenegraphModel                m_scenegraph_model;
    SceneObject::SceneObjectVector m_sceneobject_children;

    Qt3DExtras::QCuboidMesh*  m_collapsed_geometry_renderer;
    QList<const SceneObject*> m_collapsed_objects;

    SceneObject*        m_active_object {nullptr};

    QMap<const SceneObject*, QEntity*>    m_selected_objects;
    Qt3DExtras::QDiffuseSpecularMaterial* m_select_material;

    QPair<const SceneObject*, QEntity*>   m_active_selected_object;
    Qt3DExtras::QDiffuseSpecularMaterial* m_active_selected_material;


    bool visible(const SceneObject* sceneobject) const;
    bool collapsed(const SceneObject* sceneobject) const;
    void setCollapsed(SceneObject* sceneobject, bool state);
    void updateCollapsedSubTree(SceneObject* sceneobject, bool state);




    void markGlobalMatrixDirty(SceneObject* sceneobject);
    void setSelected(SceneObject*                   sceneobject,
                     Qt3DRender::QGeometryRenderer* renderer,
                     bool                           multi = false);
    void setDeSelected(SceneObject* sceneobject, bool multi = false);
    bool selected(const SceneObject* sceneobject) const;
    void clearSelections();
    void clearActiveSelected();
    bool activeSelected(const SceneObject* sceneobject) const;

    void markSelected(SceneObject*                   sceneobject,
                      Qt3DRender::QGeometryRenderer* renderer);
    void markActiveSelected(SceneObject*                   sceneobject,
                            Qt3DRender::QGeometryRenderer* renderer);
    void markActive(const SceneObject* sceneobject);

    void unMarkSelected(const SceneObject* sceneobject);
    const SceneObject* unMarkActiveSelected();
    const SceneObject* unMarkActive();

    QMap<ScenarioId, ScenarioRecord> m_scenarios;
    ScenarioId                       m_current_scenario_id{};


    Qt3DCore::QEntity*
    createSelectionObject(SceneObject*                   sceneobject,
                          Qt3DRender::QGeometryRenderer* renderer,
                          Qt3DRender::QMaterial*         select_material);

    // QObject interface
  protected:
    void childEvent(QChildEvent* event) override;


  signals:
    void scenarioRegistered();
    void scenarioLoaded(QString category, QString name);
    void scenarioUnloaded(QString category, QString name);
    void cameraChangeRequested(QVector3D position, QVector3D viewCenter,
                               QVector3D upVector);
    void auxilarySignal01();
    void auxilarySignal02();

    // Active select layers
  private:
    static QPointer<Qt3DRender::QLayer> m_select_layer;
    static QPointer<Qt3DRender::QLayer> m_active_layer;

  public:
    static QPointer<Qt3DRender::QLayer> globalSelectLayerInstance();
    static QPointer<Qt3DRender::QLayer> globalActiveLayerInstance();

    // Transparency layer
  private:
    static QPointer<Qt3DRender::QLayer> m_transparency_layer;
  public:
    static QPointer<Qt3DRender::QLayer> globalTransparencyLayerInstance();
  };


}   // namespace integration

#endif   // INTEGRATION_SCENEGRAPH_H
