#ifndef INTEGRATION_SCENARIO_H
#define INTEGRATION_SCENARIO_H

#define INTEGRATION_SCENARIO_THREAD_DEV 0

#include "scenegraph.h"

// qt
#include <QObject>

// stl
#include <algorithm>
#include <functional>

namespace integration
{

  class Scenario : public QObject {
    Q_OBJECT

    friend class Scenegraph;

  public:
    using InitFunc    = std::function<void(Scenario&)>;
    using CleanupFunc = std::function<void(Scenario&)>;

    explicit Scenario(const QString& category, const QString& name,
                      InitFunc initialize_fn, CleanupFunc cleanup_fn,
                      Scenegraph* scenegraph);
    virtual ~Scenario();

    // Delete copy
    Scenario(const Scenario& other) = delete;
    Scenario& operator=(const Scenario& other) = delete;

    // Delete move
    Scenario(Scenario&& other) = delete;
    Scenario& operator=(Scenario&& other) = delete;


    void setProgress(int progress);
    SceneObject* root();
    Scenegraph*  scenegraph();

    void deleteChildrenOfRootLater();

  protected:
    QPointer<Scenegraph> m_scenegraph;

  private:
    void initialize();
    void cleanup();

    QString               m_category;
    QString               m_name;
    int                   m_progress;
    InitFunc              m_initialize_fn;
    CleanupFunc           m_cleanup_fn;
    QPointer<SceneObject> m_scenario_root;
    std::mutex            m_mutex;

#if INTEGRATION_SCENARIO_THREAD_DEV
    using InitializeFutureWatcher = QFutureWatcher<SceneObject*>;
    InitializeFutureWatcher m_initialize_watcher;
    void handleInitializeDone();
#endif

  public:
    static void defaultScenarioCleanup(Scenario& scenario)
    {
      scenario.deleteChildrenOfRootLater();
    }

    static int minProgress() { return 0; }
    static int maxProgress() { return 1000; }

  signals:
    void progressChanged(int progress);
  };

}   // namespace integration

#endif   // INTEGRATION_SCENARIO_H
