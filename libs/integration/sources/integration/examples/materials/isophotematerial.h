#ifndef EXAMPLES_MATERIALS_ISOPHOTEMATERIAL_H
#define EXAMPLES_MATERIALS_ISOPHOTEMATERIAL_H


// qt
#include <Qt3DRender>



namespace examples
{

  class IsophoteMaterial : public Qt3DRender::QMaterial {
    Q_OBJECT

    Q_PROPERTY(QVector3D eyeVector READ eyeVector WRITE setEyeVector NOTIFY
                 eyeVectorChanged)
    Q_PROPERTY(QColor color1 READ color1 WRITE setColor1 NOTIFY color1Changed)
    Q_PROPERTY(QColor color2 READ color2 WRITE setColor2 NOTIFY color2Changed)
    Q_PROPERTY(int resolution READ resolution WRITE setResolution NOTIFY
                 resolutionChanged)
    Q_PROPERTY(bool local READ local WRITE setLocal NOTIFY localChanged)


  public:
    IsophoteMaterial(Qt3DCore::QNode* parent = nullptr);
    ~IsophoteMaterial() override = default;

    QVector3D eyeVector() const;
    QColor    color1() const;
    QColor    color2() const;
    int       resolution() const;
    bool      local() const;

  public slots:
    void setEyeVector(const QVector3D& eye_vector);
    void setColor1(const QColor& color);
    void setColor2(const QColor& color);
    void setResolution(int resolution);
    void setLocal(bool local);

  private:
    Qt3DRender::QEffect*        m_effect{nullptr};
    Qt3DRender::QParameter*     m_eye_vector{nullptr};
    Qt3DRender::QParameter*     m_color1{nullptr};
    Qt3DRender::QParameter*     m_color2{nullptr};
    Qt3DRender::QParameter*     m_resolution{nullptr};
    Qt3DRender::QParameter*     m_local{nullptr};
    Qt3DRender::QTechnique*     m_technique{nullptr};
    Qt3DRender::QFilterKey*     m_filter{nullptr};
    Qt3DRender::QRenderPass*    m_renderpass{nullptr};
    Qt3DRender::QShaderProgram* m_shaderprogram{nullptr};

  signals:
    void eyeVectorChanged(QVector3D);
    void color1Changed(QColor);
    void color2Changed(QColor);
    void resolutionChanged(int);
    void localChanged(bool);
  };

}   // namespace examples




#endif   // EXAMPLES_MATERIALS_ISOPHOTEMATERIAL_H
