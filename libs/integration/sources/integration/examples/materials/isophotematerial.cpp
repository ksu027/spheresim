#include "isophotematerial.h"

namespace examples
{


  IsophoteMaterial::IsophoteMaterial(Qt3DCore::QNode* parent)
    : Qt3DRender::QMaterial(parent)
  {
    m_effect = new Qt3DRender::QEffect();
    {

      m_technique = new Qt3DRender::QTechnique();
      {
        m_technique->graphicsApiFilter()->setApi(
          Qt3DRender::QGraphicsApiFilter::OpenGL);
        m_technique->graphicsApiFilter()->setMajorVersion(3);
        m_technique->graphicsApiFilter()->setMinorVersion(1);
        m_technique->graphicsApiFilter()->setProfile(
          Qt3DRender::QGraphicsApiFilter::CoreProfile);

        m_filter = new Qt3DRender::QFilterKey();
        {
          m_filter->setName("renderingStyle");
          m_filter->setValue("forward");
        }
        m_technique->addFilterKey(m_filter);


        m_technique->addParameter(new Qt3DRender::QParameter(
          "light.position", QVector4D(0.0, 0.0, 0.0, 1.0)));
        m_technique->addParameter(new Qt3DRender::QParameter(
          "light.intensity", QVector3D(1.0, 1.0, 1.0)));

        m_eye_vector = new Qt3DRender::QParameter("isophote.eye_vector",
                                                  QVector3D{1.0f, 1.0f, 1.0f});

        m_color1 = new Qt3DRender::QParameter("isophote.color1",
                                              QVector3D(1.0f, 0.0f, 0.0f));
        m_color2 = new Qt3DRender::QParameter("isophote.color2",
                                              QVector3D(1.0f, 1.0f, 1.0f));
        m_resolution = new Qt3DRender::QParameter("isophote.resolution", 20);
        m_local      = new Qt3DRender::QParameter("isophote.local", true);

        m_technique->addParameter(m_eye_vector);
        m_technique->addParameter(m_color1);
        m_technique->addParameter(m_color2);
        m_technique->addParameter(m_resolution);
        m_technique->addParameter(m_local);

        m_renderpass = new Qt3DRender::QRenderPass();
        {
          m_shaderprogram = new Qt3DRender::QShaderProgram();
          {
            m_shaderprogram->setVertexShaderCode(
              Qt3DRender::QShaderProgram::loadSource(QUrl(
                "qrc:/lib_examples_shaders/materials/isophote/isophote.vert")));
            m_shaderprogram->setFragmentShaderCode(
              Qt3DRender::QShaderProgram::loadSource(QUrl(
                "qrc:/lib_examples_shaders/materials/isophote/isophote.frag")));
          }
          m_renderpass->setShaderProgram(m_shaderprogram);
        }
        m_technique->addRenderPass(m_renderpass);
      }
      m_effect->addTechnique(m_technique);
    }

    setEffect(m_effect);
  }

  bool IsophoteMaterial::local() const { return m_local->value().toBool(); }

  void IsophoteMaterial::setLocal(bool local)
  {
    m_local->setValue(local);
    emit localChanged(local);
  }

  int IsophoteMaterial::resolution() const
  {
    return m_resolution->value().toInt();
  }

  void IsophoteMaterial::setResolution(int resolution)
  {
    m_resolution->setValue(resolution);
    emit resolutionChanged(resolution);
  }

  QColor IsophoteMaterial::color2() const
  {
    return m_color2->value().value<QColor>();
  }

  void IsophoteMaterial::setColor2(const QColor& color)
  {
    m_color2->setValue(color);
    emit color2Changed(color);
  }

  QColor IsophoteMaterial::color1() const
  {
    return m_color1->value().value<QColor>();
  }

  void IsophoteMaterial::setColor1(const QColor& color)
  {
    m_color1->setValue(color);
    emit color1Changed(color);
  }

  QVector3D IsophoteMaterial::eyeVector() const
  {
    return m_eye_vector->value().value<QVector3D>();
  }

  void IsophoteMaterial::setEyeVector(const QVector3D& eye_vector)
  {
    m_eye_vector->setValue(eye_vector);
    emit eyeVectorChanged(eye_vector);
  }

}   // namespace examples
