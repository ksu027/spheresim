#ifndef EXAMPLES_OBJECTS_PARAMETRIC_PARAMETRICOBJECTS_H
#define EXAMPLES_OBJECTS_PARAMETRIC_PARAMETRICOBJECTS_H

// Parametric point adapter
#include "points/appoint.h"

// Parametric cuve adapters
#include "curves/circle.h"
#include "curves/line.h"

// Parametric surface adapters
#include "surfaces/plane.h"
#include "surfaces/sphere.h"
#include "surfaces/torus.h"

// integration: Base Parameteric object types
#include "../../../parametric/objects/curve.h"
#include "../../../parametric/objects/surface.h"
#include "../../../parametric/objects/polygonsurface.h"

// integration
#include "../../../sceneobject.h"

// qt
#include <QObject>


namespace examples
{


  // Example curves
  using HermiteCurveP2V2 = integration::Curve<
    gmlib2::parametric::HermiteCurveP2V2<integration::SceneObject>>;
  using HermiteCurveP3V2 = integration::Curve<
    gmlib2::parametric::HermiteCurveP3V2<integration::SceneObject>>;
  using HermiteCurveP3V3 = integration::Curve<
    gmlib2::parametric::HermiteCurveP3V3<integration::SceneObject>>;

  // Example surfaces
  using BicubicCoonsPatch = integration::Surface<
    gmlib2::parametric::BicubicCoonsPatch<integration::SceneObject>>;
  using BilinearCoonsPatch = integration::Surface<
    gmlib2::parametric::BilinearCoonsPatch<integration::SceneObject>>;

  // Example polygon surface constructions
  using Polygon = integration::PolygonSurface<
    gmlib2::parametric::Polygon<integration::SceneObject>>;


}   // namespace examples


#endif   // EXAMPLES_OBJECTS_PARAMETRIC_PARAMETRICOBJECTS_H
