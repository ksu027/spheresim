#ifndef INTEGRATION_OBJECTS_PARAMETRIC_SURFACES_TORUS_H
#define INTEGRATION_OBJECTS_PARAMETRIC_SURFACES_TORUS_H

// integration
#include "../../../../parametric/objects/surface.h"
#include "../../../../sceneobject.h"

namespace examples
{

  class Torus : public integration::Surface<
                  gmlib2::parametric::Torus<integration::SceneObject>> {
    using Base = integration::Surface<
      gmlib2::parametric::Torus<integration::SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(integration::SurfaceMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY( double wheelRadius
                MEMBER m_wheelradius NOTIFY wheelRadiusChanged)
    Q_PROPERTY( double tubeRadiusOne
                MEMBER m_tuberadius1 NOTIFY tubeRadiusOneChanged)
    Q_PROPERTY( double tubeRadiusTwo
                MEMBER m_tuberadius2 NOTIFY tubeRadiusTwoChanged)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    Torus(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
    }

    // Signal(s)
  signals:
    void wheelRadiusChanged();
    void tubeRadiusOneChanged();
    void tubeRadiusTwoChanged();
  };


}   // namespace examples


#endif   // INTEGRATION_OBJECTS_PARAMETRIC_SURFACES_TORUS_H
