#ifndef EXAMPLES_OBJECTS_PARAMETRIC_SURFACES_SPHERE_H
#define EXAMPLES_OBJECTS_PARAMETRIC_SURFACES_SPHERE_H

// integration
#include "../../../../parametric/objects/surface.h"
#include "../../../../sceneobject.h"

namespace examples
{

  class Sphere : public integration::Surface<
                   gmlib2::parametric::Sphere<integration::SceneObject>> {
    using Base = integration::Surface<
      gmlib2::parametric::Sphere<integration::SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(integration::SurfaceMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    Sphere(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
    }

    // Signal(s)
  signals:
    void radiusChanged();
  };


}   // namespace examples


#endif   // EXAMPLES_OBJECTS_PARAMETRIC_SURFACES_SPHERE_H
