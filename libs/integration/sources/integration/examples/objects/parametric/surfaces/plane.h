#ifndef EXAMPLES_OBJECTS_PARAMETRIC_SURFACES_PLANE_H
#define EXAMPLES_OBJECTS_PARAMETRIC_SURFACES_PLANE_H

// integration
#include "../../../../parametric/objects/surface.h"
#include "../../../../sceneobject.h"

namespace examples
{

  class Plane : public integration::Surface<
                  gmlib2::parametric::Plane<integration::SceneObject>> {
    using Base = integration::Surface<
      gmlib2::parametric::Plane<integration::SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(integration::SurfaceMesh* defaultMesh READ defaultMesh)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    Plane(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
    }

    // Signal(s)
  signals:
  };

}   // namespace examples


#endif   // EXAMPLES_OBJECTS_PARAMETRIC_SURFACES_PLANE_H
