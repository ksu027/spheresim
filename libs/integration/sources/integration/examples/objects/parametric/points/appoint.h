#ifndef EXAMPLES_OBJECTS_PARAMETRIC_POINTS_APPOINT_H
#define EXAMPLES_OBJECTS_PARAMETRIC_POINTS_APPOINT_H

// integration
#include "../../../../parametric/objects/ppoint.h"
#include "../../../../sceneobject.h"

namespace examples
{

  class APPoint : public integration::PPoint<
                    gmlib2::parametric::PPoint<integration::SceneObject>> {
    using Base = integration::PPoint<
      gmlib2::parametric::PPoint<integration::SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(Qt3DExtras::QSphereMesh* defaultMesh READ defaultMesh)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    APPoint(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
    }

    // Signal(s)
  signals:
  };


}   // namespace examples

#endif   // EXAMPLES_OBJECTS_PARAMETRIC_POINTS_APPOINT_H
