#ifndef EXAMPLES_OBJECTS_PARAMETRIC_CURVES_CIRCLE_H
#define EXAMPLES_OBJECTS_PARAMETRIC_CURVES_CIRCLE_H

// integration
#include "../../../../sceneobject.h"
#include "../../../../parametric/objects/curve.h"

// qt
#include <Qt3DExtras>

namespace examples
{

  class Circle : public integration::Curve<
                   gmlib2::parametric::Circle<integration::SceneObject>> {
    using Base = integration::Curve<
      gmlib2::parametric::Circle<integration::SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(integration::CurveMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)
    // clang-format on


    // Constructor(s)
  public:
    template <typename... Ts>
    Circle(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
    }

    // Signal(s)
  signals:
    void radiusChanged();
  };


}   // namespace examples


#endif   // EXAMPLES_OBJECTS_PARAMETRIC_CURVES_CIRCLE_H
