#ifndef EXAMPLES_OBJECTS_PARAMETRIC_CURVES_LINE_H
#define EXAMPLES_OBJECTS_PARAMETRIC_CURVES_LINE_H

// integration
#include "../../../../parametric/objects/curve.h"
#include "../../../../sceneobject.h"

namespace examples
{

  class Line : public integration::Curve<
                 gmlib2::parametric::Line<integration::SceneObject>> {
    using Base
      = integration::Curve<gmlib2::parametric::Line<integration::SceneObject>>;
    Q_OBJECT

    // clang-format off
    Q_PROPERTY(integration::CurveMesh* defaultMesh READ defaultMesh)
    // clang-format on

    // Constructor(s)
  public:
    template <typename... Ts>
    Line(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
    }

    // Signal(s)
  signals:
  };


}   // namespace examples


#endif   // EXAMPLES_OBJECTS_PARAMETRIC_CURVES_LINE_H
