#include "traditionalgmlibcameracontroller.h"

#include "../objects/parametric/parametricobjects.h"

// integration
#include "../../scenegraph.h"

// qt
#include <QVector3D>


namespace examples
{

  TraditionalGMlibCameraController::TraditionalGMlibCameraController(
    Qt3DCore::QNode* parent)
    : QEntity(parent), m_left_mousebutton_action{new Qt3DInput::QAction()},
      m_right_mousebutton_action{new Qt3DInput::QAction()},
      m_alt_key_action{new Qt3DInput::QAction()},
      m_shift_key_action{new Qt3DInput::QAction()},
      m_ctrl_key_action{new Qt3DInput::QAction()},
      m_rx_axis_action{new Qt3DInput::QAxis()},
      m_ry_axis_action{new Qt3DInput::QAxis()},

      m_left_mousebutton_input{new Qt3DInput::QActionInput()},
      m_right_mousebutton_input{new Qt3DInput::QActionInput()},
      m_alt_key_input{new Qt3DInput::QActionInput()},
      m_shift_key_input{new Qt3DInput::QActionInput()},
      m_ctrl_key_input{new Qt3DInput::QActionInput()},

      m_q_key_action{new Qt3DInput::QAction()},
      m_q_key_input{new Qt3DInput::QActionInput()},

      m_mouse_rx_input{new Qt3DInput::QAnalogAxisInput()},
      m_mouse_ry_input{new Qt3DInput::QAnalogAxisInput()},

      m_keyboard_device{new Qt3DInput::QKeyboardDevice()},
      m_mouse_device{new Qt3DInput::QMouseDevice()},
      m_logical_device{new Qt3DInput::QLogicalDevice()},

      m_frame_action{new Qt3DLogic::QFrameAction()}
  {
    // Left mouse button input
    m_left_mousebutton_input->setButtons(QVector<int>() << Qt::LeftButton);
    m_left_mousebutton_input->setSourceDevice(m_mouse_device);
    m_left_mousebutton_action->addInput(m_left_mousebutton_input);

    // Right mouse button input
    m_right_mousebutton_input->setButtons(QVector<int>() << Qt::RightButton);
    m_right_mousebutton_input->setSourceDevice(m_mouse_device);
    m_right_mousebutton_action->addInput(m_right_mousebutton_input);

    // Mouse X input
    m_mouse_rx_input->setAxis(Qt3DInput::QMouseDevice::X);
    m_mouse_rx_input->setSourceDevice(m_mouse_device);
    m_rx_axis_action->addInput(m_mouse_rx_input);

    // Mouse Y input
    m_mouse_ry_input->setAxis(Qt3DInput::QMouseDevice::Y);
    m_mouse_ry_input->setSourceDevice(m_mouse_device);
    m_ry_axis_action->addInput(m_mouse_ry_input);

    // Alt key
    m_alt_key_input->setButtons(QVector<int>() << Qt::Key_Alt);
    m_alt_key_input->setSourceDevice(m_keyboard_device);
    m_alt_key_action->addInput(m_alt_key_input);

    // Shift key
    m_shift_key_input->setButtons(QVector<int>() << Qt::Key_Shift);
    m_shift_key_input->setSourceDevice(m_keyboard_device);
    m_shift_key_action->addInput(m_shift_key_input);

    // Control key
    m_ctrl_key_input->setButtons(QVector<int>() << Qt::Key_Control);
    m_ctrl_key_input->setSourceDevice(m_keyboard_device);
    m_ctrl_key_action->addInput(m_ctrl_key_input);

    // Q-key
    m_q_key_input->setButtons(QVector<int>() << Qt::Key_Q);
    m_q_key_input->setSourceDevice(m_keyboard_device);
    m_q_key_action->addInput(m_q_key_input);

    // Logic device
    m_logical_device->addAction(m_left_mousebutton_action);
    m_logical_device->addAction(m_right_mousebutton_action);
    m_logical_device->addAxis(m_rx_axis_action);
    m_logical_device->addAxis(m_ry_axis_action);
    m_logical_device->addAction(m_alt_key_action);
    m_logical_device->addAction(m_shift_key_action);
    m_logical_device->addAction(m_ctrl_key_action);
    m_logical_device->addAction(m_q_key_action);

    // Frame action
    QObject::connect(m_frame_action, &Qt3DLogic::QFrameAction::triggered, this,
                     &TraditionalGMlibCameraController::handleTriggered);

    // Disable logic when entity is disabled
    QObject::connect(this, &TraditionalGMlibCameraController::enabledChanged,
                     m_logical_device, &Qt3DInput::QLogicalDevice::setEnabled);

    addComponent(m_frame_action);
    addComponent(m_logical_device);
  }

  // TraditionalGMlibCameraController::~TraditionalGMlibCameraController()
  //{
  //  m_right_mousebutton_action->removeInput(m_right_mousebutton_input.get());
  //  m_left_mousebutton_action->removeInput(m_left_mousebutton_input.get());

  //  m_ry_axis_action->removeInput(m_mouse_ry_input.get());
  //  m_rx_axis_action->removeInput(m_mouse_rx_input.get());

  //  m_logical_device->removeAxis(m_ry_axis_action.get());
  //  m_logical_device->removeAxis(m_rx_axis_action.get());
  //  m_logical_device->removeAction(m_right_mousebutton_action.get());
  //  m_logical_device->removeAction(m_left_mousebutton_action.get());
  //}

  Qt3DRender::QCamera* TraditionalGMlibCameraController::camera() const
  {
    return m_camera;
  }

  integration::SceneModelOld* TraditionalGMlibCameraController::scenemodel() const
  {
    return m_scenemodel;
  }

  void TraditionalGMlibCameraController::setCamera(Qt3DRender::QCamera* camera)
  {
    if (m_camera != camera) {
      //    if (m_camera) unregisterDestructionHelper(m_camera);
      if (camera && !camera->parent()) camera->setParent(this);

      m_camera = camera;

      // Ensures proper bookkeeping
      //    if (m_camera)
      //      registerDestructionHelper(
      //        m_camera, &TraditionalGMlibCameraController::setCamera,
      //        m_camera);

      emit cameraChanged();
    }
  }

  void TraditionalGMlibCameraController::setSceneModel(
    integration::SceneModelOld* model)
  {
    if (m_scenemodel != model) {
      m_scenemodel = model;
      emit scenemodelChanged();
    }
  }

  TraditionalGMlibCameraController::SceneObjectVector
  TraditionalGMlibCameraController::selectedSceneObjects() const
  {
    SceneObjectVector ssos;
    if (m_scenemodel) {

      auto sel_indices
        = m_scenemodel->match(integration::SceneModelOld::SelectedRole, true);
      for (auto index : sel_indices)
        ssos.append(
          m_scenemodel->data(index, integration::SceneModelOld::ItemRole)
            .value<integration::SceneObject*>());
    }
    return ssos;
  }

  void TraditionalGMlibCameraController::handleTriggered(float dt)
  {
    if (not m_camera) return;

    bool hid_inputs_invoked = false;
    if (m_q_key_action->isActive() and m_ctrl_key_action->isActive()) {
      QGuiApplication::quit();
    }
    else if (m_left_mousebutton_action->isActive()
             and m_shift_key_action->isActive()) {
      htMoveSelectedObjects(dt);
      hid_inputs_invoked = true;
    }
    else if (m_left_mousebutton_action->isActive()
             and m_ctrl_key_action->isActive()) {
      htRotateSelectedObjects(dt);
      hid_inputs_invoked = true;
    }

    if (hid_inputs_invoked) {
      debug01();
    }
  }

  void TraditionalGMlibCameraController::htMoveSelectedObjects([
    [maybe_unused]] float dt) const
  {
    //    const auto& cam_dir  = m_camera->viewVector();
    //    const auto& cam_up   = m_camera->upVector();
    //    const auto& cam_side = QVector3D::crossProduct(cam_dir, cam_up);

    //    const auto& dv_x = m_rx_axis_action->value() * dt;
    //    const auto& dv_y = m_ry_axis_action->value() * dt;

    //    auto ssos = selectedSceneObjects();
    //    for (auto obj : ssos) {

    //      // clang-format off
    //// const auto obj_to_cam = obj->getPosition() - m_camera->getPosition();
    //// const auto near_plane = m_camera->nearPlane();
    //// const auto vp_h = 1.0/2.0;
    //// const auto focal_length = obj_to_cam.length() * vp_h /
    /// m_camera->fieldOfView(); / const auto dh = (26.0 * cam_dir * obj_to_cam
    /// * near_plane) / (focal_length * vp_h);
    //      // clang-format on

    //      auto dh = 1.0f;
    ////      auto v  = dv_x * dh * cam_side + dv_y * dh * cam_up;
    //      // obj->translate(obj::gmlib2::bases::AffineSpaceD3<>::Vector_Type{
    //      //        double(v.x()), double(v.y()), double(v.z())});
    //    }
  }

  void TraditionalGMlibCameraController::htRotateSelectedObjects([
    [maybe_unused]] float dt) const
  {
    //    const auto cam_dir  = m_camera->viewVector();
    //    const auto cam_up   = m_camera->upVector();
    //    const auto cam_side = QVector3D::crossProduct(cam_dir, cam_up);
    //    qDebug() << "rotate sel objs: ";
    //    qDebug() << "  cam dir:  " << cam_dir;
    //    qDebug() << "  cam up:   " << cam_up;
    //    qDebug() << "  cam side: " << cam_side;

    //    const auto dv_x = m_rx_axis_action->value();
    //    const auto dv_y = m_ry_axis_action->value();

    //    const auto rot_v   = dv_x * cam_up - dv_y * cam_side;
    //    const auto rot_ang = 2 * M_PI
    //                         * std::sqrt(std::pow(double(dv_x) / 800.0, 2)
    //                                     + std::pow(double(dv_y) / 600.0, 2))
    //                         * double(dt);
    //    //  const auto rot_ang = 30.0 * double(dt);

    //    auto ssos = selectedSceneObjects();
    //    for (auto obj : ssos) {
    //      //    obj->rotate(rot_ang,
    //      //    gmlib2::bases::AffineSpaceD3<>::EmbeddingVector_Type{
    //      //                           1.0, 0.0, 0.0, 1.0});
    //      //      obj->rotate(rot_ang,
    //      //                  gmlib2::bases::AffineSpaceD3<>::Vector_Type{
    //      //                    double(rot_v.x()), double(rot_v.y()),
    //      //                    double(rot_v.z())});
    //    }
  }

  void TraditionalGMlibCameraController::debug01() const
  {

    //    qDebug() << "---";
    //    qDebug() << "After hid inputs: ";
    //    qDebug() << " No. dirty sceneobjects (before global pos query): " <<
    //    m_scenemodel->m_scenegraph->m_dirty_sceneobjects_global_matrix.size();
    //    qDebug() << " Dirty sceneobject (global matrix):";
    //    for (const auto& so_dirty_pair :
    //    m_scenemodel->m_scenegraph->m_dirty_sceneobjects_global_matrix)
    //      qDebug() << "  - " << so_dirty_pair.second->objectName();

    //    auto* tp_surf = m_scenemodel->m_scenegraph->findChild<TPTorus*>("t0");
    //    auto* tp_surf =
    //    m_scenemodel->m_scenegraph->findChild<TPSphere*>("t1"); auto* tp_surf
    //    = m_scenemodel->m_scenegraph->findChild<TPPlane*>("t2");
    auto* tp_surf = m_scenemodel->m_scenegraph->findChild<Torus*>("t3");
    if (tp_surf) {
      std::cout << "----------------------------------" << std::endl;

      auto pos               = tp_surf->frameOriginParent();
      auto global_pos        = tp_surf->frameOriginGlobal();
      auto rframe            = tp_surf->vSpaceFrameParent();
      auto global_matrix     = tp_surf->vSpaceFrameGlobal();
      using PSurfPSpacePoint = Torus::PSpacePoint;
      auto local_eval
        = tp_surf->evaluateLocal(PSurfPSpacePoint{0.5, 0.5}, {{0, 0}});
      auto parent_eval
        = tp_surf->evaluateParent(PSurfPSpacePoint{0.5, 0.5}, {{0, 0}});
      auto global_eval
        = tp_surf->evaluateGlobal(PSurfPSpacePoint{0.5, 0.5}, {{0, 0}});
      std::cout << "  Pos:" << std::endl << pos << std::endl;
      std::cout << "  Pos (global):" << std::endl << global_pos << std::endl;
      std::cout << "  RFrame:" << std::endl << rframe << std::endl;
      std::cout << "  Matrix (global):" << std::endl
                << global_matrix << std::endl;
      std::cout << "  Local evaluation (at 0.5,0.5)" << std::endl
                << local_eval(0, 0) << std::endl;
      std::cout << "  Parent evaluation (at 0.5,0.5)" << std::endl
                << parent_eval(0, 0) << std::endl;
      std::cout << "  Global evaluation (at 0.5,0.5)" << std::endl
                << global_eval(0, 0) << std::endl;
      //      qDebug() << " No. dirty sceneobjects (after global pos query): "
      //      <<
      //      m_scenemodel->m_scenegraph->m_dirty_sceneobjects_global_matrix.size();
      std::cout << std::endl;
    }
  }

}   // namespace examples
