#include "groundgridoverlay.h"

// qt3d
#include <Qt3DExtras>

namespace examples
{

  namespace groundgridoverlay::detail
  {


    struct GGAxis : Qt3DCore::QEntity {
      GGAxis(QNode* parent, Qt3DExtras::QCylinderMesh* mesh,
             Qt3DExtras::QDiffuseSpecularMaterial* material,
             const QVector2D& origin, const QVector2D& direction,
             float scale = 1.0f)
        : Qt3DCore::QEntity(parent), m_mesh{mesh}, m_material{material},
          m_origin(origin), m_direction(direction), m_scale(scale)
      {
        m_transform = new Qt3DCore::QTransform(this);
        addComponent(m_transform);

        m_geom_h = new Qt3DCore::QEntity(this);
        m_geom_h->addComponent(m_mesh);
        m_geom_h->addComponent(m_material);
        m_geom_transform = new Qt3DCore::QTransform(m_geom_h);
        m_geom_h->addComponent(m_geom_transform);

        updateAxis();
      }

      Qt3DExtras::QCylinderMesh*            m_mesh;
      Qt3DExtras::QDiffuseSpecularMaterial* m_material;

      QVector2D m_origin;
      QVector2D m_direction;
      float     m_scale;

      Qt3DCore::QTransform* m_transform;
      Qt3DCore::QEntity*    m_geom_h;
      Qt3DCore::QTransform* m_geom_transform;

      QVector2D sDirection() const { return m_direction; }

      void updateAxis()
      {

        const auto scale = m_scale * 0.01f;
        m_geom_transform->setScale3D(
          QVector3D(scale, sDirection().length() * 2, scale));

        QVector3D dir, side, up, origin;

        {
          gmlib2::VectorT<float, 3> d{m_direction.x(), 0.0f, m_direction.y()};
          const auto d_liv = gmlib2::algorithms::linearIndependentVector(d);
          const auto s     = blaze::cross(d_liv, d);
          const auto u     = blaze::cross(d, s);

          dir  = QVector3D(d[0], d[1], d[2]).normalized();
          side = QVector3D(s[0], s[1], s[2]).normalized();
          up   = QVector3D(u[0], u[1], u[2]).normalized();
        }

        origin = QVector3D(m_origin[0], 0, m_origin[1]);

        QMatrix4x4 M;
        M(0, 0) = dir[0];
        M(1, 0) = dir[1];
        M(2, 0) = dir[2];
        M(3, 0) = 0.0f;

        M(0, 1) = side[0];
        M(1, 1) = side[1];
        M(2, 1) = side[2];
        M(3, 1) = 0.0f;

        M(0, 2) = up[0];
        M(1, 2) = up[1];
        M(2, 2) = up[2];
        M(3, 2) = 0.0f;

        M(0, 3) = origin[0];
        M(1, 3) = origin[1];
        M(2, 3) = origin[2];
        M(3, 3) = 1.0f;

        // Cylinder mesh offset
        M.rotate(90, 0, 0, -1);

        m_transform->setMatrix(M);
      }
    };



    struct GGTile : Qt3DCore::QEntity {
      GGTile(QNode* parent, Qt3DExtras::QPlaneMesh* mesh,
             Qt3DExtras::QDiffuseSpecularMaterial* material, QVector2D origin,
             QVector2D quadrant)
        : QEntity(parent), m_mesh(mesh), m_material(material), m_origin(origin),
          m_quadrant(quadrant)
      {
        addComponent(m_mesh);
        addComponent(m_material);

        m_transform = new Qt3DCore::QTransform(this);
        addComponent(m_transform);

        updateTransform();
      }

      Qt3DExtras::QPlaneMesh*               m_mesh;
      Qt3DExtras::QDiffuseSpecularMaterial* m_material;

      QVector2D m_origin;
      QVector2D m_quadrant;

      Qt3DCore::QTransform* m_transform;

      void updateTransform()
      {
        m_transform->setTranslation(
          QVector3D(m_origin.x() + m_quadrant.x() / 2.0f, 0.0f,
                    m_origin.y() + m_quadrant.y() / 2.0f));

        m_transform->setScale3D(
          QVector3D(m_quadrant.x(), 1.0f, m_quadrant.y()));
      }
    };

  }   // namespace GroundGridOverlay::detail



  GroundGridOverlay::GroundGridOverlay(
    Qt3DCore::QNode* parent, const QVector3D& origin, const QVector3D& xaxis,
    const QVector3D& yaxis, const QSize& size, const QColor xaxis_color,
    const QColor yaxis_color, const QColor zaxis_color)
    : QEntity(parent), m_origin(origin), m_xaxis(xaxis),
      m_yaxis(yaxis), m_size{size}, m_xaxis_color(xaxis_color),
      m_yaxis_color(yaxis_color), m_zaxis_color(zaxis_color)
  {

    m_axis_mesh = new Qt3DExtras::QCylinderMesh(this);
    m_axis_mesh->setLength(1.0f);
    m_axis_mesh->setRadius(1.0f);
    m_axis_mesh->setSlices(10);
    m_axis_mesh->setRings(2);

    m_xaxis_material = new Qt3DExtras::QDiffuseSpecularMaterial(this);
    m_xaxis_material->setDiffuse(m_xaxis_color);
    m_xaxis_material->setAmbient(m_xaxis_color);

    m_yaxis_material = new Qt3DExtras::QDiffuseSpecularMaterial(this);
    m_yaxis_material->setDiffuse(m_yaxis_color);
    m_yaxis_material->setAmbient(m_yaxis_color);

    m_sub_axes_color = QColor("gray");

    m_sub_axes_material = new Qt3DExtras::QDiffuseSpecularMaterial(this);
    m_sub_axes_material->setDiffuse(m_sub_axes_color);
    m_sub_axes_material->setAmbient(m_sub_axes_color);

    m_tile_color = QColor("darkgray");
    m_tile_color.setAlphaF(0.6);

    m_tile_mesh = new Qt3DExtras::QPlaneMesh(this);
    m_tile_mesh->setWidth(1.0f);
    m_tile_mesh->setHeight(1.0f);
    m_tile_mesh->setMeshResolution({2, 2});

    m_tile_material = new Qt3DExtras::QDiffuseSpecularMaterial(this);
    m_tile_material->setDiffuse(m_tile_color);
    m_tile_material->setAmbient(m_tile_color);

    m_transform = new Qt3DCore::QTransform(this);
    addComponent(m_transform);

    constructAxisAndTiles();
    updateTransform();
  }

  QVector3D GroundGridOverlay::origin() const { return m_origin; }

  QVector3D GroundGridOverlay::xAxis() const { return m_xaxis; }

  QVector3D GroundGridOverlay::yAxis() const { return m_yaxis; }

  float GroundGridOverlay::scale() const { return m_scale; }

  QSize GroundGridOverlay::size() const { return m_size; }

  void GroundGridOverlay::constructAxisAndTiles()
  {
    // scales
    const auto primary_axes_scale = 1.0f;
    const auto sub_axes_scale     = 0.5f;


    const auto paxis = QVector2D{1.0, 0.0};
    const auto saxis = QVector2D{0.0, 1.0};

    // Primary axis
    for (auto* axis : m_axes) axis->setParent(static_cast<QNode*>(nullptr));
    m_axes.clear();

    auto* xaxis = new groundgridoverlay::detail::GGAxis(
      this, m_axis_mesh, m_xaxis_material, QVector2D(0.0f, 0.0f),
      paxis * m_size.width(), primary_axes_scale);

    m_axes.push_back(xaxis);

    auto* yaxis = new groundgridoverlay::detail::GGAxis(
      this, m_axis_mesh, m_yaxis_material, QVector2D(0.0f, 0.0f),
      saxis * m_size.height(), primary_axes_scale);
    m_axes.push_back(yaxis);

    // Secondary axis

    // Construct size no. axis in forward and backwards xaxis direction
    for (auto i = 1; i <= m_size.width(); ++i) {

      auto* sub_xaxis_forward = new groundgridoverlay::detail::GGAxis(
        this, m_axis_mesh, m_sub_axes_material,
        QVector2D(0.0f, 0.0f) + saxis * i, paxis * m_size.width(),
        sub_axes_scale);

      auto* sub_xaxis_backward = new groundgridoverlay::detail::GGAxis(
        this, m_axis_mesh, m_sub_axes_material,
        QVector2D(0.0f, 0.0f) - saxis * i, paxis * m_size.width(),
        sub_axes_scale);

      m_axes.push_back(sub_xaxis_forward);
      m_axes.push_back(sub_xaxis_backward);
    }

    // Construct size no. axis in forward and backwards yaxis direction
    for (auto i = 1; i <= m_size.height(); ++i) {

      auto* sub_yaxis_forward = new groundgridoverlay::detail::GGAxis(
        this, m_axis_mesh, m_sub_axes_material,
        QVector2D(0.0f, 0.0f) + paxis * i, saxis * m_size.height(),
        sub_axes_scale);

      auto* sub_yaxis_backward = new groundgridoverlay::detail::GGAxis(
        this, m_axis_mesh, m_sub_axes_material,
        QVector2D(0.0f, 0.0f) - paxis * i, saxis * m_size.height(),
        sub_axes_scale);

      m_axes.push_back(sub_yaxis_forward);
      m_axes.push_back(sub_yaxis_backward);
    }


    // Tiles
#define TILE_VARIATION 0
#if TILE_VARIATION == 1
    for (auto i = 1; i <= m_size.width(); ++i) {
      for (auto j = 1; j <= m_size.height(); ++j) {

        auto* tile_q1 = new GroundGridOverlay::detail::GGTile(
          this, m_tile_mesh, m_tile_material,
          QVector2D(0.0f, 0.0f) + paxis * (i - 1) + saxis * (j - 1),
          QVector2D(m_scale, m_scale));

        auto* tile_q2 = new GroundGridOverlay::detail::GGTile(
          this, m_tile_mesh, m_tile_material,
          QVector2D(0.0f, 0.0f) - paxis * (i - 1) + saxis * (j - 1),
          QVector2D(-m_scale, m_scale));

        auto* tile_q3 = new GroundGridOverlay::detail::GGTile(
          this, m_tile_mesh, m_tile_material,
          QVector2D(0.0f, 0.0f) - paxis * (i - 1) - saxis * (j - 1),
          QVector2D(-m_scale, -m_scale));

        auto* tile_q4 = new GroundGridOverlay::detail::GGTile(
          this, m_tile_mesh, m_tile_material,
          QVector2D(0.0f, 0.0f) + paxis * (i - 1) - saxis * (j - 1),
          QVector2D(m_scale, -m_scale));

        m_tiles.push_back(tile_q1);
        m_tiles.push_back(tile_q2);
        m_tiles.push_back(tile_q3);
        m_tiles.push_back(tile_q4);
      }
    }
#elif TILE_VARIATION == 2
    auto* tile_q1 = new GroundGridOverlay::detail::GGTile(
      this, m_tile_mesh, m_tile_material, QVector2D(0.0f, 0.0f),
      QVector2D(m_scale * m_size.width(), m_scale * m_size.height()));

    auto* tile_q2 = new GroundGridOverlay::detail::GGTile(
      this, m_tile_mesh, m_tile_material, QVector2D(0.0f, 0.0f),
      QVector2D(-m_scale * m_size.width(), m_scale * m_size.height()));

    auto* tile_q3 = new GroundGridOverlay::detail::GGTile(
      this, m_tile_mesh, m_tile_material, QVector2D(0.0f, 0.0f),
      QVector2D(-m_scale * m_size.width(), -m_scale * m_size.height()));

    auto* tile_q4 = new GroundGridOverlay::detail::GGTile(
      this, m_tile_mesh, m_tile_material, QVector2D(0.0f, 0.0f),
      QVector2D(m_scale * m_size.width(), -m_scale * m_size.height()));

    m_tiles.push_back(tile_q1);
    m_tiles.push_back(tile_q2);
    m_tiles.push_back(tile_q3);
    m_tiles.push_back(tile_q4);
#endif
  }


  void GroundGridOverlay::updateTransform()
  {
    const auto dir  = m_xaxis.normalized();
    const auto side = m_yaxis.normalized();
    const auto up   = QVector3D::crossProduct(m_xaxis, m_yaxis).normalized();

    QMatrix4x4 M;
    M.setToIdentity();


    M(0, 0) = dir[0];
    M(1, 0) = dir[1];
    M(2, 0) = dir[2];
    M(3, 0) = 0.0f;

    M(0, 1) = side[0];
    M(1, 1) = side[1];
    M(2, 1) = side[2];
    M(3, 1) = 0.0f;

    M(0, 2) = up[0];
    M(1, 2) = up[1];
    M(2, 2) = up[2];
    M(3, 2) = 0.0f;

    M.rotate(-90, 1.0f, 0.0f, 0.0f);

    M(0, 3) = m_origin[0];
    M(1, 3) = m_origin[1];
    M(2, 3) = m_origin[2];

    m_transform->setMatrix(M);
  }

}   // namespace examples
