#ifndef EXAMPLES_UTILS_GROUNDGRIDOVERLAY_H
#define EXAMPLES_UTILS_GROUNDGRIDOVERLAY_H




// gmlib2
#include <gmlib2.h>

// qt
#include <Qt3DCore>
namespace Qt3DExtras
{
  class QPlaneMesh;
  class QCylinderMesh;
  class QDiffuseSpecularMaterial;
}   // namespace Qt3DExtras



namespace examples
{

  namespace groundgridoverlay::detail
  {
    struct GGAxis;
    struct GGTile;
  }   // namespace groundgridoverlay::detail


  class GroundGridOverlay : public Qt3DCore::QEntity {
    Q_OBJECT

    Q_PROPERTY(QVector3D origin READ origin)
    Q_PROPERTY(QVector3D xaxis READ xAxis)
    Q_PROPERTY(QVector3D yaxis READ yAxis)
    Q_PROPERTY(float scale READ scale NOTIFY scaleChanged)
    Q_PROPERTY(QSize size READ size NOTIFY sizeChanged)

  public:
    GroundGridOverlay(QNode* parent, const QVector3D& origin,
                      const QVector3D& xaxis, const QVector3D& yaxis,
                      const QSize& size,
                      const QColor xaxis_color = QColor("red"),
                      const QColor yaxis_color = QColor("green"),
                      const QColor zaxis_color = QColor("blue"));

    QVector3D origin() const;
    QVector3D xAxis() const;
    QVector3D yAxis() const;

    float scale() const;
    //  void  setScale(float scale);

    QSize size() const;
    //  void  setSize(const QSize& size);

  signals:
    void scaleChanged(float scale);
    void sizeChanged(QSize size);

  private:
    QVector3D m_origin;
    QVector3D m_xaxis;
    QVector3D m_yaxis;

    QSize m_size{10, 10};

    QColor m_xaxis_color;
    QColor m_yaxis_color;
    QColor m_zaxis_color;

    float m_scale{1.0f};


    QColor m_sub_axes_color;

    QColor m_tile_color;

    Qt3DExtras::QCylinderMesh*                  m_axis_mesh;
    Qt3DExtras::QDiffuseSpecularMaterial*       m_xaxis_material;
    Qt3DExtras::QDiffuseSpecularMaterial*       m_yaxis_material;
    Qt3DExtras::QDiffuseSpecularMaterial*       m_sub_axes_material;
    QVector<groundgridoverlay::detail::GGAxis*> m_axes;

    Qt3DExtras::QPlaneMesh*                     m_tile_mesh;
    Qt3DExtras::QDiffuseSpecularMaterial*       m_tile_material;
    QVector<groundgridoverlay::detail::GGTile*> m_tiles;

    Qt3DCore::QTransform* m_transform;

    void constructAxisAndTiles();
    void updateTransform();

    //  QVector3D sxaxis() const;
    //  QVector3D syaxis() const;
  };



}   // namespace examples

#endif   // EXAMPLES_UTILS_GROUNDGRIDOVERLAY_H
