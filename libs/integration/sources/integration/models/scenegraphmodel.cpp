#include "scenegraphmodel.h"

#include "../scenegraph.h"
#include "../sceneobject.h"

namespace integration
{

  namespace scenegraphmodel::detail
  {
    ModelFactoryBase::ModelFactoryBase() {}
    ModelFactoryBase::~ModelFactoryBase() {}
  }   // namespace scenegraphmodel::detail


  ScenegraphModel::ScenegraphModel(Scenegraph* scenegraph, QObject* parent)
    : QAbstractItemModel(parent), m_scenegraph{scenegraph}
  {

    registerSubtypeModel<const SceneObject*,SceneObjectModel>("SceneObjectModel");

    connect(scenegraph, &Scenegraph::scenarioRegistered, this,
            &ScenegraphModel::updateScenarios);
    connect(scenegraph, &Scenegraph::scenarioLoaded, this,
            &ScenegraphModel::updateScenarios);
    connect(scenegraph, &Scenegraph::scenarioUnloaded, this,
            &ScenegraphModel::updateScenarios);
  }

  QVariantList ScenegraphModel::scenarios() const
  {
    QVariantList s;
    for (const auto& scenario : m_scenegraph->m_scenarios.keys()) {

      const auto category  = scenario.first;
      const auto name      = scenario.second;
      const auto is_loaded = m_scenegraph->isScenarioLoaded({category, name});

      QJsonObject s_obj;
      s_obj["category"] = category;
      s_obj["name"]     = name;
      s_obj["isLoaded"] = is_loaded;
      s.append(s_obj);
    }

    return s;
  }

  void ScenegraphModel::loadScenario(const QVariant& category,
                                     const QVariant& name)
  {
    m_scenegraph->loadScenario({category.toString(), name.toString()});
  }

  void ScenegraphModel::unloadScenario()
  {
    m_scenegraph->unloadCurrentScenario();
  }

  void ScenegraphModel::reportScenarioLoadProgress(qreal current)
  {
    m_current_scenario_load_progress = current;
    emit currentScenarioLoadProgressChanged(current);
  }

  void ScenegraphModel::updateScenarios()
  {
    emit scenariosChanged(scenarios());
  }

  qreal ScenegraphModel::currentScenarioLoadProgress() const
  {
    return m_current_scenario_load_progress;
  }

  int ScenegraphModel::rowCount(const QModelIndex& index) const
  {
    if (not index.isValid()) return m_flat_scenegraph.size();
    return 0;
  }

  int ScenegraphModel::columnCount(const QModelIndex& index) const { return 1; }

  QModelIndex ScenegraphModel::index(const int row, const int column,
                                     const QModelIndex& parent) const
  {
    if (parent.isValid()) return QModelIndex();
    return createIndex(row, column, m_flat_scenegraph.value(row));
  }
  QModelIndex ScenegraphModel::parent(const QModelIndex& childIndex) const
  {
    return QModelIndex();
  }
  QVariant ScenegraphModel::data(const QModelIndex& index, const int role) const
  {
    if (not index.isValid()) return QVariant();

    const auto* item = m_flat_scenegraph.at(index.row());

    switch (ScenegraphModelRoles(role)) {
      case ScenegraphModelRoles::Display:
        if (item->m_sceneobject->objectName().length() > 0)
          return QVariant(item->m_sceneobject->objectName());
        return QVariant(subtypeIdentifier(index).name());
      case ScenegraphModelRoles::ModelItemIndentation:
        return QVariant(item->m_indent);
      case ScenegraphModelRoles::ModelItemExpanded:;
        return QVariant(modelItemExpanded(index));
      case ScenegraphModelRoles::ModelItemHidden:
        return QVariant(modelItemHidden(index));
      case ScenegraphModelRoles::HasChildren:
        return QVariant(sgHasChildren(index));
      case ScenegraphModelRoles::Collapsed:
        return QVariant(item->m_sceneobject->collapsed());
      case ScenegraphModelRoles::Visible:
        return QVariant(sgIsVisible(index));
      case ScenegraphModelRoles::Selected:
        return QVariant(item->m_sceneobject->selected());
      case ScenegraphModelRoles::SelectedMulti:
        return QVariant(item->m_sceneobject->selected());
      case ScenegraphModelRoles::ActiveSelected:
        return QVariant(item->m_sceneobject->activeSelected());
      case ScenegraphModelRoles::Subtype:
        return QVariant(subtypeIdentifier(index).archIdName());
      case ScenegraphModelRoles::SubtypeModel: {
        auto* subtype_model = subtypeModel(index);
        if (subtype_model) {

          return QVariant::fromValue(subtype_model);
        }
        else {

          return QVariant();
        }
      }
    }

    EnumSwitchEONVFReturnGuard;
  }



  void ScenegraphModel::addFlat(SceneObject* so, ScenegraphFlatItem* parent)
  {
    const auto indent = parent not_eq nullptr ? parent->m_indent : 0;


    // Update item record - BEGIN
    {
      // Record flat item
      m_flat_scenegraph.append(
        new ScenegraphFlatItem{parent, {}, so, indent + 1});

      // Set default values for expanded and hidden
      if(not m_expanded_map.contains(so)) m_expanded_map[so] = false;
      if (not m_hidden_map.contains(so))
        m_hidden_map[so] = parent not_eq nullptr ? true : false;

      // Record as child of parent -- SWAP WITH TRANSITIVE LOOKUP
      if (parent not_eq nullptr)
        parent->m_children.append(m_flat_scenegraph.back());
    }
    // Update item records - END

    // Handle/propagate through item's children
    auto* item = m_flat_scenegraph.back();
    for (auto* so_n : so->childNodes()) {
      auto* so_so = dynamic_cast<SceneObject*>(so_n);
      if (not so_so) continue;

      addFlat(so_so, item);
    }
  }

  bool ScenegraphModel::modelItemExpanded(const QModelIndex& index) const
  {

    if (not index.isValid()) return false;

    auto* so = sceneObjectFromIndex(index);
    return m_expanded_map.value(so, false);
  }

  bool ScenegraphModel::modelItemHidden(const QModelIndex& index) const
  {

    if (not index.isValid()) return false;

    auto* so = sceneObjectFromIndex(index);
    return m_hidden_map.value(so, false);
  }

  void ScenegraphModel::setModelItemExpanded(const QModelIndex& index,
                                             bool               status)
  {
    auto* item = m_flat_scenegraph[index.row()];
    auto* so   = item->m_sceneobject;

    m_expanded_map[so] = status;

    emit dataChanged(index, index,
                     {int(ScenegraphModelRoles::ModelItemExpanded)});

    for (auto* child : item->m_children) {
      auto child_index = indexFromItem(child);
      setModelItemHidden(child_index, not status);
    }
  }

  void ScenegraphModel::setModelItemHidden(const QModelIndex& index,
                                           bool               status)
  {
    auto* item = m_flat_scenegraph[index.row()];
    auto* so   = item->m_sceneobject;

    m_hidden_map[so] = status;

    emit dataChanged(index, index,
                     {int(ScenegraphModelRoles::ModelItemHidden)});

    for (auto* child : item->m_children) {
      auto child_index = indexFromItem(child);
      setModelItemHidden(
        child_index, status || not modelItemExpanded(index) );
    }
  }

  QModelIndex ScenegraphModel::indexFromItem(
    ScenegraphModel::ScenegraphFlatItem* item) const
  {
    auto item_it = std::find(std::begin(m_flat_scenegraph),
                             std::end(m_flat_scenegraph), item);
    if (item_it == std::end(m_flat_scenegraph)) return QModelIndex();
    return index(int(std::distance(std::begin(m_flat_scenegraph), item_it)), 0,
                 QModelIndex());
  }

  QModelIndex ScenegraphModel::indexFromSceneObject(const SceneObject* so) const
  {
    auto item_it = std::find_if(
      std::begin(m_flat_scenegraph), std::end(m_flat_scenegraph),
      [so](ScenegraphFlatItem* item) { return item->m_sceneobject == so; });
    if (item_it == std::end(m_flat_scenegraph)) return QModelIndex();
    return index(int(std::distance(std::begin(m_flat_scenegraph), item_it)), 0,
                 QModelIndex());
  }

  const SceneObject*
  ScenegraphModel::sceneObjectFromIndex(const QModelIndex& index) const
  {
    return m_flat_scenegraph.value(index.row())->m_sceneobject;
  }

  bool ScenegraphModel::sgHasChildren(const QModelIndex& parent) const
  {
    if (not parent.isValid()) return false;

    auto* item        = m_flat_scenegraph.at(parent.row());
    auto  child_nodes = item->m_sceneobject->childNodes();
    for (const auto* child_node : child_nodes)
      if (dynamic_cast<const SceneObject*>(child_node)) return true;

    return false;
  }

  bool ScenegraphModel::sgIsVisible(const QModelIndex& index) const
  {
    if (not index.isValid()) return false;
    auto* item = m_flat_scenegraph.at(index.row());
    return m_scenegraph->visible(item->m_sceneobject);
  }

  void ScenegraphModel::sgSetVisible(const QModelIndex& index, bool state)
  {
    if (not index.isValid()) return;
    auto* item = m_flat_scenegraph.at(index.row());
    auto* so = item->m_sceneobject;

    if (so->collapsed()) so->setCollapsed(false);

    if(state)
      so->resetActiveGeometryRenderer();
    else
      so->clearActiveGeometryRenderer();
    emit dataChanged(index, index, {int(ScenegraphModelRoles::Visible)});
  }

  void ScenegraphModel::sgSetCollapsed(const QModelIndex& index, bool state)
  {
    if (not index.isValid()) return;

    auto* item = m_flat_scenegraph.at(index.row());
    item->m_sceneobject->setCollapsed(state);
  }

  void ScenegraphModel::sgSetSelected(const QModelIndex& index, bool state,
                                      bool multi)
  {
    if (not index.isValid()) return;

    auto* item = m_flat_scenegraph.at(index.row());
    item->m_sceneobject->setSelected(state,multi);
  }


  bool ScenegraphModel::selected(const QModelIndex& index) const
  {
    if(not index.isValid()) return false;

    const auto* item = m_flat_scenegraph.at(index.row());
    return item->m_sceneobject->selected();
  }

  void ScenegraphModel::update()
  {
    beginResetModel();
    {
      qDeleteAll(m_flat_scenegraph);
      m_flat_scenegraph.clear();

      for (auto* so_n : m_scenegraph->childNodes()) {
        auto* so_so = dynamic_cast<SceneObject*>(so_n);
        if (not so_so) continue;
        addFlat(so_so, nullptr);
      }
    }
    endResetModel();
  }

  void ScenegraphModel::update(const SceneObject* so, const QVector<int>& roles)
  {
    const auto so_index = indexFromSceneObject(so);
    emit       dataChanged(so_index, so_index, roles);

    if(roles.contains(int(ScenegraphModelRoles::ActiveSelected))) {
      emit activeSelectedChanged(
        data(indexFromSceneObject(m_scenegraph->activeSelected()),
             int(ScenegraphModelRoles::SubtypeModel))
          .value<SceneObjectModel*>());
    }
  }


  QHash<int, QByteArray> ScenegraphModel::roleNames() const
  {
    QHash<int, QByteArray> role_names;
    // clang-format off
    role_names[int(ScenegraphModelRoles::Display)]              = "display";
    role_names[int(ScenegraphModelRoles::ModelItemIndentation)] = "modelItemIndentation";
    role_names[int(ScenegraphModelRoles::ModelItemExpanded)]    = "modelItemExpanded";
    role_names[int(ScenegraphModelRoles::ModelItemHidden)]      = "modelItemHidden";
    role_names[int(ScenegraphModelRoles::HasChildren)]          = "hasChildren";
    role_names[int(ScenegraphModelRoles::Visible)]              = "visible";
    role_names[int(ScenegraphModelRoles::Collapsed)]            = "collapsed";
    role_names[int(ScenegraphModelRoles::Selected)]             = "selected";
    role_names[int(ScenegraphModelRoles::SelectedMulti)]        = "selectedMulti";
    role_names[int(ScenegraphModelRoles::ActiveSelected)]       = "activeSelected";
    role_names[int(ScenegraphModelRoles::Subtype)]              = "subtype";
    role_names[int(ScenegraphModelRoles::SubtypeModel)]         = "subtypeModel";
    // clang-format on

    return role_names;
  }



  bool ScenegraphModel::setData(const QModelIndex& index, const QVariant& value,
                                int role)
  {
    if (not index.isValid()) return false;

    //    auto* item = m_flat_scenegraph[index.row()];

    switch (role) {
      case int(ScenegraphModelRoles::ModelItemExpanded):
        setModelItemExpanded(index, value.toBool());
        return true;
      case int(ScenegraphModelRoles::Visible):
        sgSetVisible(index, value.toBool());
        return true;
      case int(ScenegraphModelRoles::Collapsed):
        sgSetCollapsed(index, value.toBool());
        return true;
      case int(ScenegraphModelRoles::Selected):
        sgSetSelected(index, value.toBool(), false);
        return true;
      case int(ScenegraphModelRoles::SelectedMulti):
        sgSetSelected(index, value.toBool(), true);
        return true;
      default:
        return false;
    }
  }

}   // namespace integration
