#ifndef INTEGRATION_SCENEOBJECTMODEL_H
#define INTEGRATION_SCENEOBJECTMODEL_H

// Qt
#include <QAbstractItemModel>
#include <QDebug>
#include <Qt3DCore>
#include <Qt3DRender>


// stl
#include <functional>
#include <variant>


namespace integration {


  namespace sceneobjectmodel {
    constexpr auto displayName = "SceneObject";
  }

  class Scenegraph;
  class SceneObject;

  class SceneObjectModel : public QAbstractItemModel {
    Q_OBJECT

    Q_PROPERTY(QString activeCategory READ activeCategory WRITE
                 setActiveCategory NOTIFY activeCategoryChanged)

  public:
    enum class SceneObjectModelRoles : int {
      Display              = Qt::DisplayRole,
      ModelItemExpanded    = Qt::UserRole + 1,
      PropertyValue,
      PropertyType,
      PropertyName,
      PropertyDelegate,
    };

    explicit SceneObjectModel(const SceneObject* sceneobject,
                              QObject*           parent = nullptr);

  private:
    const SceneObject* m_sceneobject;
    const Scenegraph*  scenegraph() const;


    /////// CATEGORIES
  private:
    struct Category {
      QString m_name;
      QString m_shortname;
    };
    using CategoryList = QList<Category>;

    CategoryList m_categories;
    QString      m_active_category;
    void         addCategroy(const QString& name, const QString& short_name);



  public slots:
    QVariantList categories() const;
    void         setActiveCategory(const QString& name);
    QString      activeCategory() const;

  signals:
    void activeCategoryChanged(QVariant);

    /////// PROPERTIES
  private:
    struct Property {
      struct ModelItemStates{
        bool m_expanded;
      };
      QString                              m_type;
      QString                              m_name;
      ModelItemStates                      m_modelitem_states;
      std::function<QVariant()>            m_read_fn;
      std::function<bool(const QVariant&)> m_write_fn{
        [](const QVariant&) { return false; }};
    };
    using PropertyList        = QList<Property>;
    using CategoryPropertyMap = QMap<QString, PropertyList>;

    void registerProperty(
      const QString& category_name, const Property& property,
      const std::function<void()>& setup_fn = []() {});


    struct MeshComponent {
      QString m_delegate;
    };
    using MeshComponentMap = QMap<QString, MeshComponent>;
    void registerMeshComponent(const QString& name, const QString& delegate);


    struct MaterialComponent {
      QString                              m_delegate;
      std::function<Qt3DRender::QMaterial*()> m_new_fn
        = []() { return nullptr; };
    };
    using MaterialComponentMap = QMap<QString, MaterialComponent>;
    void
    registerMaterialComponent(const QString& name, const QString& delegate,
                              std::function<Qt3DRender::QMaterial*()> new_fn);


    MeshComponentMap     m_mesh_components;
    MaterialComponentMap m_material_components;
    CategoryPropertyMap  m_properties;



    QVariant propertyValue(const QModelIndex& index) const;
    QVariant propertyType(const QModelIndex& index) const;
    QVariant propertyName(const QModelIndex& index) const;
    QVariant propertyDelegate(const QModelIndex& index) const;
    bool     propertyItemExpanded(const QModelIndex& index) const;


    /////// PROPERTY DELEGATES
  private:
    bool registerPropertyDelegate(const QString& name, const QString& qml_url);
    QVariantMap m_property_delegates;


    // QAbstractItemModel interface
  public:
    QModelIndex index(int row, int column,
                      const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& child) const override;
    int         rowCount(const QModelIndex& parent) const override;
    int         columnCount(const QModelIndex& parent) const override;
    QVariant    data(const QModelIndex& index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    bool setData(const QModelIndex& index, const QVariant& value,
                 int role) override;
  };


}   // namespace integration

#endif // INTEGRATION_SCENEOBJECTMODEL_H
