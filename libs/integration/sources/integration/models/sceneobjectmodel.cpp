#include "sceneobjectmodel.h"


#include "../sceneobject.h"
#include "../scenegraph.h"

#include "../qt3dexamples/materials/robustwireframematerial.h"
#include "../examples/materials/isophotematerial.h"

#include "../parametric/components/curvegeometry.h"
#include "../parametric/components/surfacegeometry.h"
#include "../parametric/components/trianglesurfacegeometry.h"
#include "../parametric/components/polygonsurfacegeometry.h"

// qt
#include <Qt3DExtras>

namespace integration
{
  SceneObjectModel::SceneObjectModel(const SceneObject* sceneobject,
                                     QObject*           parent)
    : m_sceneobject{sceneobject}
  {
    // Register delegates
    const auto prop_id_label = QString("label");
    registerPropertyDelegate(
      prop_id_label, "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
                     "property_delegates/LabelProperty.qml");

    const auto prop_id_frame = QString("frame");
    registerPropertyDelegate(
      prop_id_frame, "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
                     "property_delegates/FrameProperty.qml");

    const auto prop_id_mesh = QString("mesh");
    registerPropertyDelegate(
      prop_id_mesh, "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
                    "property_delegates/MeshComponents.qml");

    const auto prop_id_materials = QString("materials");
    registerPropertyDelegate(
      prop_id_materials,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/MaterialComponents.qml");


    // Add categories
    const auto cat_id_so = QString("SceneObject");
    addCategroy(cat_id_so, "So");

    const auto cat_id_frame = QString("Frame");
    addCategroy(cat_id_frame, "Fr");

    const auto cat_id_components = QString("Components");
    addCategroy(cat_id_components, "QCs");

    // Add SceneObject Properties
    registerProperty(cat_id_so, {prop_id_label, "Object type", {true}, []() {
                                   return sceneobjectmodel::displayName;
                                 }});

    registerProperty(cat_id_so, {prop_id_label, "Selected", {true}, [this]() {
                                   return m_sceneobject->selected();
                                 }});


    // Add Frame Properties
    registerProperty(cat_id_frame,
                     {prop_id_frame, "Frame [wrt. parent]", {true}, [this]() {
                        const auto frame = m_sceneobject->pSpaceFrameParent();
                        QMatrix4x4 m;
                        for (auto i = 0UL; i < frame.rows(); ++i)
                          for (auto j = 0UL; j < frame.columns(); ++j)
                            m(int(i), int(j)) = float(frame(i, j));
                        return m;
                      }});
    registerProperty(cat_id_frame,
                     {prop_id_frame, "Frame [wrt. scene]", {true}, [this]() {
                        const auto frame = m_sceneobject->pSpaceFrameGlobal();
                        QMatrix4x4 m;
                        for (auto i = 0UL; i < frame.rows(); ++i)
                          for (auto j = 0UL; j < frame.columns(); ++j)
                            m(int(i), int(j)) = float(frame(i, j));
                        return m;
                      }});


    // Register Mesh components
    const auto mesh_name_pcurve = "CurveMesh";
    registerMeshComponent(
      mesh_name_pcurve,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/CurveMeshDelegate.qml");

    const auto mesh_name_psurface = "SurfaceMesh";
    registerMeshComponent(
      mesh_name_psurface,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/SurfaceMeshDelegate.qml");

    const auto mesh_name_ptrianglesurface = "TriangleSurfaceMesh";
    registerMeshComponent(
      mesh_name_ptrianglesurface,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/TriangleSurfaceMeshDelegate.qml");

    const auto mesh_name_ppolygonsurface = "PolygonSurfaceMesh";
    registerMeshComponent(
      mesh_name_ppolygonsurface,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/PolygonSurfaceMeshDelegate.qml");


    // Register mesh property
    registerProperty(
      cat_id_components,
      {prop_id_mesh,
       "Mesh",
       {true},
       [this, mesh_name_pcurve, mesh_name_psurface, mesh_name_ptrianglesurface,
        mesh_name_ppolygonsurface]() {
         const auto   components = m_sceneobject->components();
         QVariantList var_comps;
         for (auto* comp : components) {

           if (qobject_cast<CurveMesh*>(comp)
               and m_mesh_components.contains(mesh_name_pcurve)) {

             const auto mesh_info = m_mesh_components.value(mesh_name_pcurve);

             QVariantMap comp_obj;
             comp_obj["name"]      = QVariant(mesh_name_pcurve);
             comp_obj["component"] = QVariant::fromValue(comp);
             comp_obj["delegate"]  = QVariant(mesh_info.m_delegate);
             var_comps.append(comp_obj);
           }
           else if (qobject_cast<SurfaceMesh*>(comp)
                    and m_mesh_components.contains(mesh_name_psurface)) {

             const auto mesh_info = m_mesh_components.value(mesh_name_psurface);

             QVariantMap comp_obj;
             comp_obj["name"]      = QVariant(mesh_name_psurface);
             comp_obj["component"] = QVariant::fromValue(comp);
             comp_obj["delegate"]  = QVariant(mesh_info.m_delegate);
             var_comps.append(comp_obj);
           }
           else if (qobject_cast<TriangleSurfaceMesh*>(comp)
                    and m_mesh_components.contains(mesh_name_ptrianglesurface)) {

             const auto mesh_info
               = m_mesh_components.value(mesh_name_ptrianglesurface);

             QVariantMap comp_obj;
             comp_obj["name"]      = QVariant(mesh_name_ptrianglesurface);
             comp_obj["component"] = QVariant::fromValue(comp);
             comp_obj["delegate"]  = QVariant(mesh_info.m_delegate);
             var_comps.append(comp_obj);
           }
           else if (qobject_cast<PolygonSurfaceMesh*>(comp)
                    and m_mesh_components.contains(mesh_name_ppolygonsurface)) {

             const auto mesh_info
               = m_mesh_components.value(mesh_name_ppolygonsurface);

             QVariantMap comp_obj;
             comp_obj["name"]      = QVariant(mesh_name_ppolygonsurface);
             comp_obj["component"] = QVariant::fromValue(comp);
             comp_obj["delegate"]  = QVariant(mesh_info.m_delegate);
             var_comps.append(comp_obj);
           }
         }
         return var_comps;
       }});



    // Add Materials
    const auto mat_name_gooch = "Gooch";
    registerMaterialComponent(
      mat_name_gooch,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/QGoochMaterialDelegate.qml",
      []() {
        auto* gooch = new Qt3DExtras::QGoochMaterial;
        gooch->setAlpha(0.25f);
        gooch->setBeta(0.05f);
        gooch->setWarm(QColor("orange"));
        gooch->setCool(QColor("#530853"));
        gooch->setDiffuse(QColor("gray"));
        gooch->setSpecular(QColor("gray"));
        return gooch;
      });

    const auto mat_name_diff_spec = "Diffuse-Specular";
    registerMaterialComponent(
      mat_name_diff_spec,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/QDiffuseSpecularMaterialDelegate.qml",
      []() {
        auto* diff_spec = new Qt3DExtras::QDiffuseSpecularMaterial;
        return diff_spec;
      });

    const auto mat_name_robust_wireframe = "Robust Wireframe";
    registerMaterialComponent(
      mat_name_robust_wireframe,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/RobustWireframeMaterialDelegate.qml",
      []() {
        auto* robust_wireframe = new qt3dexamples::RobustWireframeMaterial;
        robust_wireframe->setDiffuse("orange");
        robust_wireframe->setAmbient("orange");
        robust_wireframe->setSpecular("white");
        robust_wireframe->setAlpha(1.0);
        return robust_wireframe;
      });


    const auto mat_name_isophote = "Isophote";
    registerMaterialComponent(
      mat_name_isophote,
      "qrc:/lib_examples_qml/demos/prototypingdemo/components/"
      "property_delegates/IsophoteMaterialDelegate.qml",
      []() {
        auto* isophote = new examples::IsophoteMaterial;
        isophote->setEyeVector({1., 1., 1.});
        isophote->setColor1("orange");
        isophote->setColor2("white");
        isophote->setResolution(20);
        isophote->setLocal(true);
        return isophote;
      });



    // Add Material Properties
    registerProperty(
      cat_id_components,
      {prop_id_materials,
       "Materials",
       {true},
       [this, mat_name_gooch, mat_name_diff_spec, mat_name_robust_wireframe,
        mat_name_isophote]() {
         const auto   components = m_sceneobject->components();
         QVariantList var_comps;
         for (auto* comp : components) {

           if (qobject_cast<Qt3DExtras::QGoochMaterial*>(comp)
               and m_material_components.contains(mat_name_gooch)) {

             const auto mat_info = m_material_components.value(mat_name_gooch);

             QVariantMap comp_obj;
             comp_obj["name"]      = QVariant(mat_name_gooch);
             comp_obj["component"] = QVariant::fromValue(comp);
             comp_obj["delegate"]  = QVariant(mat_info.m_delegate);
             var_comps.append(comp_obj);
           }
           else if (qobject_cast<Qt3DExtras::QDiffuseSpecularMaterial*>(comp)
                    and m_material_components.contains(mat_name_diff_spec)) {

             const auto mat_info
               = m_material_components.value(mat_name_diff_spec);

             QVariantMap comp_obj;
             comp_obj["name"]      = QVariant(mat_name_diff_spec);
             comp_obj["component"] = QVariant::fromValue(comp);
             comp_obj["delegate"]  = QVariant(mat_info.m_delegate);
             var_comps.append(comp_obj);
           }
           else if (qobject_cast<qt3dexamples::RobustWireframeMaterial*>(comp)
                    and m_material_components.contains(
                      mat_name_robust_wireframe)) {

             const auto mat_info
               = m_material_components.value(mat_name_robust_wireframe);

             QVariantMap comp_obj;
             comp_obj["name"]      = QVariant(mat_name_robust_wireframe);
             comp_obj["component"] = QVariant::fromValue(comp);
             comp_obj["delegate"]  = QVariant(mat_info.m_delegate);
             var_comps.append(comp_obj);
           }
           else if (qobject_cast<examples::IsophoteMaterial*>(comp)
                    and m_material_components.contains(mat_name_isophote)) {

             const auto mat_info
               = m_material_components.value(mat_name_isophote);

             QVariantMap comp_obj;
             comp_obj["name"]      = QVariant(mat_name_isophote);
             comp_obj["component"] = QVariant::fromValue(comp);
             comp_obj["delegate"]  = QVariant(mat_info.m_delegate);
             var_comps.append(comp_obj);
           }
         }
         return var_comps;
       },
       [this](const QVariant& info) {
         const auto map = info.toMap();
         if (not map.contains("action")) return false;

         const auto action = map.value("action").toString();
         if (action == "remove") {
           auto* component
             = map.value("component").value<Qt3DCore::QComponent*>();
           if (component) {
             SceneObject* so = const_cast<SceneObject*>(m_sceneobject);
             so->removeComponent(component);
             return true;
           }
         }
         else if (action == "apply") {

           const auto mat = map.value("material_name").toString();
           if (not m_material_components.contains(mat)) return false;

           auto*        new_mat = m_material_components.value(mat).m_new_fn();
           SceneObject* so      = const_cast<SceneObject*>(m_sceneobject);
           so->addComponent(new_mat);
           return true;
         }

         return false;
       }});





    // Set active category
    setActiveCategory(cat_id_so);
  }

  const Scenegraph* SceneObjectModel::scenegraph() const
  {
    return m_sceneobject->scenegraph();
  }

  QVariantList SceneObjectModel::categories() const
  {
    QVariantList categories;
    for (const auto& cat : m_categories) {
      QJsonObject cat_obj;
      cat_obj["name"]      = cat.m_name;
      cat_obj["shortName"] = cat.m_shortname;
      categories.append(cat_obj);
    }

    return categories;
  }

  void SceneObjectModel::setActiveCategory(const QString& name)
  {
    beginResetModel();
    m_active_category = name;
    endResetModel();

    emit activeCategoryChanged(name);
  }

  QString SceneObjectModel::activeCategory() const { return m_active_category; }

  void SceneObjectModel::registerProperty(const QString&  category_name,
                                          const Property& property,
                                          const std::function<void()>& setup_fn)
  {
    setup_fn();
    m_properties[category_name].append(property);
  }

  void SceneObjectModel::registerMeshComponent(const QString& name, const QString& delegate)
  {
    if (m_mesh_components.contains(name)) return;
    m_mesh_components[name] = {delegate};
  }

  void SceneObjectModel::registerMaterialComponent(
    const QString& name, const QString& delegate,
    std::function<Qt3DRender::QMaterial*()> new_fn)
  {
    if (m_material_components.contains(name)) return;
    m_material_components[name] = {delegate, new_fn};
  }

  void SceneObjectModel::addCategroy(const QString& name,
                                     const QString& short_name)
  {
    m_categories.append(Category{name, short_name});
  }

  QVariant SceneObjectModel::propertyValue(const QModelIndex& index) const
  {
    return m_properties.value(m_active_category).value(index.row()).m_read_fn();
  }

  QVariant SceneObjectModel::propertyType(const QModelIndex& index) const
  {
    return m_properties.value(m_active_category).value(index.row()).m_type;
  }

  QVariant SceneObjectModel::propertyName(const QModelIndex& index) const
  {
    return m_properties.value(m_active_category).value(index.row()).m_name;
  }

  QVariant SceneObjectModel::propertyDelegate(const QModelIndex& index) const
  {
    const auto type = propertyType(index);
    return m_property_delegates.value(type.toString());
  }

  bool SceneObjectModel::propertyItemExpanded(const QModelIndex& index) const
  {
    return m_properties.value(m_active_category)
      .value(index.row())
      .m_modelitem_states.m_expanded;
  }

  bool SceneObjectModel::registerPropertyDelegate(const QString& name,
                                                  const QString& qml_url)
  {
    // guard
    if (m_property_delegates.contains(name)) return false;

    m_property_delegates[name] = qml_url;
    return true;
  }

  QModelIndex SceneObjectModel::index(int row, int column,
                                      const QModelIndex& parent) const
  {
    if (parent.isValid()) return QModelIndex();
    return createIndex(row, column);
  }

  QModelIndex SceneObjectModel::parent(const QModelIndex& child) const
  {
    return QModelIndex();
  }

  int SceneObjectModel::rowCount(const QModelIndex& parent) const
  {
    if (not parent.isValid())
      return m_properties.value(m_active_category).size();
    return 0;
  }

  int SceneObjectModel::columnCount(const QModelIndex& parent) const
  {
    return 1;
  }

  QVariant SceneObjectModel::data(const QModelIndex& index, int role) const
  {
    if (not index.isValid()) return QVariant("invalid");

    switch (SceneObjectModelRoles(role)) {
      case SceneObjectModelRoles::Display:
        return sceneobjectmodel::displayName;
      case SceneObjectModelRoles::ModelItemExpanded:
        return QVariant(propertyItemExpanded(index));
      case SceneObjectModelRoles::PropertyValue:
        return propertyValue(index);
      case SceneObjectModelRoles::PropertyType:
        return propertyType(index);
      case SceneObjectModelRoles::PropertyName:
        return propertyName(index);
      case SceneObjectModelRoles::PropertyDelegate:
        return propertyDelegate(index);
    }
    EnumSwitchEONVFReturnGuard;
  }

  QHash<int, QByteArray> SceneObjectModel::roleNames() const
  {
    QHash<int, QByteArray> role_names;
    // clang-format off
    role_names[int(SceneObjectModelRoles::Display)]            = "display";
    role_names[int(SceneObjectModelRoles::ModelItemExpanded)]  = "modelItemExpanded";
    role_names[int(SceneObjectModelRoles::PropertyValue)]      = "propertyValue";
    role_names[int(SceneObjectModelRoles::PropertyType)]       = "propertyType";
    role_names[int(SceneObjectModelRoles::PropertyName)]       = "propertyName";
    role_names[int(SceneObjectModelRoles::PropertyDelegate)]   = "propertyDelegate";
    // clang-format on
    return role_names;
  }

  bool SceneObjectModel::setData(const QModelIndex& index,
                                 const QVariant& value, int role)
  {
    if (not index.isValid()) return false;

    switch (role) {
      case int(SceneObjectModelRoles::ModelItemExpanded):
        m_properties[m_active_category][index.row()]
          .m_modelitem_states.m_expanded
          = value.toBool();
        dataChanged(index, index,
                    {int(SceneObjectModelRoles::ModelItemExpanded)});
        return true;
      case int(SceneObjectModelRoles::PropertyValue): {
        const auto wrote
          = m_properties[m_active_category][index.row()].m_write_fn(value);
        if (wrote)
          dataChanged(index, index,
                      {int(SceneObjectModelRoles::PropertyValue)});
        return true;
      }
      default:
        return false;
    }
  }

}   // namespace integration
