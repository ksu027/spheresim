#include "scenemodel.h"

#include "sceneobjectcomponentmodel.h"
#include "../scenegraph.h"
#include "../parametric/components/surfacegeometry.h"

// qt
#include <Qt3DCore>
#include <Qt3DExtras>
#include <QVector3D>

// stl
#include <memory>

namespace integration
{

  SceneModelOld::SceneModelOld(Scenegraph* scenegraph) : m_scenegraph{scenegraph} {}

  int SceneModelOld::columnCount(const QModelIndex& /*parent*/) const { return 1; }

  int SceneModelOld::rowCount(const QModelIndex& parent) const
  {

    int  rows          = 0;
    auto parent_object = sceneObjectFromIndex(parent);
    if (parent_object)
      rows = sceneObjectChildren(parent_object).size();
    else
      rows = sceneObjectChildren(m_scenegraph).size();

    //  qDebug() << "rowCount for parent: " << parent;
    //  qDebug() << "  root rows: " << sceneObjectChildren(m_scenegraph).size();
    //  qDebug() << "  rows: " << rows;
    //  qDebug() << "\n";

    return rows;
  }

  QVariant SceneModelOld::headerData(int /*section*/,
                                  Qt::Orientation /*orientation*/,
                                  int /*role*/) const
  {
    return QVariant();
  }

  QVariant SceneModelOld::data(const QModelIndex& index, int role) const
  {
    //  qDebug() << "requesting data of index " << index << " for role "
    //           << roleNames().value(role);
    if (not index.isValid()) {
      //    qDebug() << "  index NOT valid \n";
      return QVariant();
    }
    //  qDebug() << "  index valid";

    auto* so = sceneObjectFromIndex(index);
    if (not so) {
      //    qDebug() << "  SceneObject NOT valid \n";
      return QVariant();
    }
    //  qDebug() << "  SceneObject valid";

    //  qDebug() << "\n";




    if (role == ItemRole) {
      return QVariant::fromValue(so);
    }
    else if (role == NameRole)
//      return QVariant(QVariant::fromValue(so).typeName());
      return QVariant("Type: N/A");
    else if (role == MeshRole) {
      for (auto* component : so->components()) {
        auto* mesh = qobject_cast<Qt3DRender::QGeometryRenderer*>(component);
        if (mesh) return QVariant::fromValue(mesh);
      }
    }
    else if (role == TransformRole) {
      for (auto* component : so->components()) {
        auto* transform = qobject_cast<Qt3DCore::QTransform*>(component);
        if (transform) return QVariant::fromValue(transform);
      }
    }
    else if (role == MaterialRole) {
      for (auto* component : so->components()) {
        auto* material = qobject_cast<Qt3DRender::QMaterial*>(component);
        if (material) return QVariant::fromValue(material);
      }
    }
    else if (role == ComponentModelRole) {
      return QVariant::fromValue(new SceneObjectComponentModel(so));
    }
    else if (role == SelectedRole) {
      return QVariant(so->selected());
    }

    return QVariant();
  }

  // bool SceneModelOld::setData(const QModelIndex& /*index*/,
  //                         const QVariant& /*value*/, int /*role*/)
  //{
  //  return true;
  //}

  QModelIndex SceneModelOld::index(int row, int column,
                                const QModelIndex& parent) const
  {

    //    qDebug() << "Index info of (row,col)[" << row << "," << column
    //             << "] with parent " << parent;

    if (parent.isValid() and parent.column() != 0) return QModelIndex();

    QModelIndex index           = QModelIndex();
    const auto  parent_children = [this](const auto& in_parent) {

      if (not in_parent.isValid() or in_parent.internalPointer() == m_scenegraph)
        return sceneObjectChildren(m_scenegraph);
      else
        return sceneObjectChildren(in_parent);

    }(parent);

    if (parent_children.size() > row)
      index = createIndex(row, column, parent_children.at(row));

    //    qDebug() << "  created index: " << index;
    //    qDebug() << "\n";

    return index;
  }

  QModelIndex SceneModelOld::parent(const QModelIndex& child) const
  {

    //  qDebug() << "Parent info of child " << child;

    QModelIndex index = QModelIndex();

    if (!child.isValid())
      index = QModelIndex();
    else {

      auto* child_item = sceneObjectFromIndex(child);

      auto* parent_item_scene = dynamic_cast<Scenegraph*>(child_item->parent());
      auto* parent_item = dynamic_cast<SceneObject*>(child_item->parent());

      if (not(parent_item or parent_item_scene))
        index = QModelIndex();
      else {

        const auto parent_children = parent_item
                                       ? sceneObjectChildren(parent_item)
                                       : sceneObjectChildren(m_scenegraph);

        index = createIndex(
          int(std::distance(parent_children.begin(),
                            std::find_if(parent_children.begin(),
                                         parent_children.end(),
                                         [child_item](const auto* ele) {
                                           return ele == child_item;
                                         }))),
          0, parent_item);
      }
    }

    //  qDebug() << "  created index: " << index;
    //  qDebug() << "\n";

    return index;
  }

  Qt::ItemFlags SceneModelOld::flags(const QModelIndex& /*index*/) const
  {
    return Qt::NoItemFlags;
  }

  QHash<int, QByteArray> SceneModelOld::roleNames() const
  {
    QHash<int, QByteArray> role_names;
    role_names[ItemRole]           = "item";
    role_names[NameRole]           = "name";
    role_names[MeshRole]           = "mesh";
    role_names[TransformRole]      = "transform";
    role_names[MaterialRole]       = "material";
    role_names[SelectedRole]       = "selected";
    role_names[ComponentModelRole] = "component_model";
    return role_names;
  }

  QModelIndexList SceneModelOld::match(int role, const QVariant& value) const
  {
    QModelIndexList matches;
    for (int i = 0; i < m_scenegraph->children().size(); ++i)
      matches.append(match(index(i, 0, QModelIndex()), role, value));
    return matches;
  }

  QModelIndexList SceneModelOld::match(const QModelIndex& start, int role,
                                    const QVariant& value, int /*hits*/,
                                    Qt::MatchFlags /*flags*/) const
  {
    QModelIndexList matches;
    QModelIndexList indices;
    indices.append(start);
    while (indices.size()) {
      auto index = indices.takeLast();
      if (data(index, role) == value) matches.append(index);
      indices.append(modelIndexChildren(index));
    }

    return matches;
  }

  SceneObject* SceneModelOld::sceneObjectFromIndex(const QModelIndex& index) const
  {
    if (index.isValid()) {
      auto* item = static_cast<SceneObject*>(index.internalPointer());
      if (item) return item;
    }

    return nullptr;
  }

  SceneModelOld::SceneObjectVector
  SceneModelOld::toSceneObjects(const Qt3DCore::QNodeVector& nodes) const
  {
    SceneObjectVector sovector;
    for (auto* node : nodes) {
      auto* sceneobject = dynamic_cast<SceneObject*>(node);
      if (sceneobject) sovector.push_back(sceneobject);
    }
    return sovector;
  }

  SceneModelOld::SceneObjectVector
  SceneModelOld::sceneObjectChildren(const QModelIndex& index) const
  {
    return sceneObjectChildren(sceneObjectFromIndex(index));
  }

  SceneModelOld::SceneObjectVector
  SceneModelOld::sceneObjectChildren(const SceneObject* sceneobject) const
  {

    if (sceneobject) return toSceneObjects(sceneobject->childNodes());

    return SceneObjectVector();
  }

  SceneModelOld::SceneObjectVector
  SceneModelOld::sceneObjectChildren(const Scenegraph* scenegraph) const
  {
    if (scenegraph) return toSceneObjects(scenegraph->childNodes());

    return SceneObjectVector();
  }

  QModelIndexList
  SceneModelOld::modelIndexChildren(const QModelIndex& parent) const
  {
    QModelIndexList child_indices;

    if (parent.isValid()) {

      auto no_children = rowCount(parent);
      for (decltype(no_children) i = 0; i < no_children; ++i) {
        auto child_idx = index(i, 0, parent);
        if (child_idx.isValid()) child_indices.append(child_idx);
      }
    }

    return child_indices;
  }

}   // namespace integration
