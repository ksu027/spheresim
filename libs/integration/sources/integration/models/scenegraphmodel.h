#ifndef INTEGRATION_SCENEGRAPHMODEL_H
#define INTEGRATION_SCENEGRAPHMODEL_H



// Early implementation adapted from
//   https://github.com/ColinDuquesnoy/QtQuickControls2.TreeView
// Author:  Colin Duquesnoy
// Date:    2019-06-25
// Licence: "super-open"


//#include "../initialization.h"
#include "sceneobjectmodel.h"

// Qt
#include <QAbstractItemModel>
#include <QDebug>
#include <QQmlEngine>
#include <QMetaType>

// stl
#include <typeinfo>
#include <memory>
#include <tuple>

namespace integration
{
  class Scenegraph;
  class SceneObject;


  namespace scenegraphmodel
  {

    namespace detail
    {

      struct ModelFactoryBase {
        ModelFactoryBase();
        virtual SceneObjectModel* create(const SceneObject*) const = 0;
        virtual ~ModelFactoryBase();
      };

      template <typename QAIM_T>
      struct ModelFactory : ModelFactoryBase {
        virtual SceneObjectModel* create(const SceneObject* so) const override
        {
          return new QAIM_T(so);
        }
      };

    }   // namespace detail


    template <typename T>
    const char* generateSubtypeArchIdName()
    {
      return typeid(std::remove_cv_t<std::remove_pointer_t<std::decay_t<T>>>)
        .name();
    }




  }   // namespace scenegraphmodel




  class ScenegraphModel : public QAbstractItemModel {
    Q_OBJECT

    Q_PROPERTY(QVariantList scenarios READ scenarios NOTIFY scenariosChanged)

    Q_PROPERTY(
      qreal currentScenarioLoadProgress READ currentScenarioLoadProgress NOTIFY
                                                                         currentScenarioLoadProgressChanged)

  public:
    enum class ScenegraphModelRoles : int {
      Display              = Qt::DisplayRole,
      ModelItemIndentation = Qt::UserRole + 1,
      ModelItemExpanded,
      ModelItemHidden,
      HasChildren,
      Visible,
      Collapsed,
      Selected,
      SelectedMulti,
      ActiveSelected,
      Subtype,
      SubtypeModel
    };

    struct SubtypeIdentifier : std::tuple<const char*, QString, const char*> {

      using Base = std::tuple<const char*, QString, const char*>;

      SubtypeIdentifier(const char*    arch_id_name               = "unknown",
                        const QString& name                       = "N/A",
                        const char* best_fit_subtype_arch_id_name = "unknown")
        : Base{arch_id_name, name, best_fit_subtype_arch_id_name}
      {
      }
      const char*    archIdName() const { return std::get<0>(*this); }
      const QString& name() const { return std::get<1>(*this); }
      const char*    bestFitSubtypeModuleArchIdName() const
      {
        return std::get<2>(*this);
      }

      void setArchIdName(const char* arch_id_name)
      {
        std::get<0>(*this) = arch_id_name;
      }
      void setName(const QString& name_in) { std::get<1>(*this) = name_in; }
      void setBestFitSubtypeModuleArchIdName(const char* arch_id_name)
      {
        std::get<2>(*this) = arch_id_name;
      }
    };

    ScenegraphModel(Scenegraph* scenegraph = nullptr,
                    QObject*    parent     = nullptr);

    QVariantList scenarios() const;


    Q_INVOKABLE void loadScenario(const QVariant& category,
                                  const QVariant& name);
    Q_INVOKABLE void unloadScenario();


    qreal m_current_scenario_load_progress{0.0};

  public slots:
    void updateScenarios();
    qreal currentScenarioLoadProgress() const;
    void reportScenarioLoadProgress(qreal current);


  signals:
    void scenariosChanged(QVariantList scenarios);

  private:
    Scenegraph* m_scenegraph;

    struct ScenegraphFlatItem;
    using ScenegraphFlatList = QList<ScenegraphFlatItem*>;

    struct ScenegraphFlatItem {
      ScenegraphFlatItem* m_parent;
      ScenegraphFlatList  m_children;
      SceneObject*        m_sceneobject;
      int                 m_indent;
    };

    ScenegraphFlatList             m_flat_scenegraph;
    QMap<const SceneObject*, bool> m_hidden_map;
    QMap<const SceneObject*, bool> m_expanded_map;

    void addFlat(SceneObject* so, ScenegraphFlatItem* parent);

    bool modelItemExpanded(const QModelIndex& index) const;
    bool modelItemHidden(const QModelIndex& index) const;

    void setModelItemExpanded(const QModelIndex& index, bool status);
    void setModelItemHidden(const QModelIndex& index, bool status);

    QModelIndex indexFromItem(ScenegraphFlatItem* item) const;
    QModelIndex indexFromSceneObject(const SceneObject* so) const;


    bool sgHasChildren(const QModelIndex& parent) const;
    bool sgIsVisible(const QModelIndex& parent) const;
    void sgSetVisible(const QModelIndex& parent, bool state);
    void sgSetCollapsed(const QModelIndex& index, bool state);
    void sgSetSelected(const QModelIndex& index, bool state, bool multi);

    bool selected( const QModelIndex& index ) const;



    // INTENTIONALLY UNSAFE
    const SceneObject* sceneObjectFromIndex(const QModelIndex& index) const;


    // Subtypes


    SubtypeIdentifier         subtypeIdentifier(const QModelIndex& index) const;
    virtual SubtypeIdentifier subtypeIdentifier(const SceneObject* so) const;




    QMap<QString, std::shared_ptr<scenegraphmodel::detail::ModelFactoryBase>>
      m_subtype_model_factories;

    mutable QMap<const SceneObject*, SceneObjectModel*> m_subtype_models;



    SceneObjectModel* subtypeModel(const QModelIndex& index) const;
    SceneObjectModel* subtypeModel(const SceneObject* so) const;

  public:
    template <typename Subtype_T, typename SceneObjectModel_T>
    void registerSubtypeModel(const char *type_name) {

      const auto subtype_archid
        = QString(scenegraphmodel::generateSubtypeArchIdName<Subtype_T>());

      if(m_subtype_model_factories.contains(subtype_archid)) return;

      qRegisterMetaType<SceneObjectModel_T*>(type_name);
      qmlRegisterUncreatableType<SceneObjectModel_T>(
        "com.uit.GMlib2QtIntegration", 1, 0, type_name,
        "Someone else owns this");

      m_subtype_model_factories[subtype_archid] = std::make_shared<
        scenegraphmodel::detail::ModelFactory<SceneObjectModel_T>>();
    }

  public slots:
    void update();
    void update(const SceneObject* so, const QVector<int>& roles = {});

  signals:
    void activeSelectedChanged(SceneObjectModel*) const;
    void currentScenarioLoadProgressChanged(qreal current);

    // QAbstractItemModel interface
  public:
    QHash<int, QByteArray> roleNames() const override;
    int                    rowCount(const QModelIndex& index) const override;
    int                    columnCount(const QModelIndex& index) const override;
    QModelIndex            index(const int row, const int column,
                                 const QModelIndex& parent) const override;
    QModelIndex            parent(const QModelIndex& childIndex) const override;
    QVariant data(const QModelIndex& index, const int role) const override;

    bool setData(const QModelIndex& index, const QVariant& value,
                 int role) override;
  };

}   // namespace integration

#endif   // INTEGRATION_SCENEGRAPHMODEL_H
