#ifndef INTEGRATION_MODELS_SCENEOBJECTCOMPONENTMODEL_H
#define INTEGRATION_MODELS_SCENEOBJECTCOMPONENTMODEL_H


// qt
//#include <QAbstractListModel>
#include <QAbstractItemModel>
#include <QEntity>

// stl
#include <memory>



namespace integration
{

  class SceneObject;

  class SceneObjectComponentModel : public QAbstractItemModel {
    Q_OBJECT
  public:
    enum { ComponentTypeRole = Qt::UserRole + 1, ComponentRole };
    enum class ComponentTypes {
      Material = 0,
      Mesh,
      ObjectPicker,
      Transform,
      Unknown
    };
    Q_ENUM(ComponentTypes)


    SceneObjectComponentModel(const SceneObject* const components);
    ~SceneObjectComponentModel() override;

  private:
    const SceneObject* const m_so;

    ComponentTypes typeOfComponent(const QObject* object) const;

    // Interface
  public:
    QHash<int, QByteArray> roleNames() const override;
    int                    rowCount(const QModelIndex& parent) const override;
    int         columnCount(const QModelIndex& parent) const override;
    QVariant    data(const QModelIndex& index, int role) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& child) const override;
  };

}   // namespace integration

#endif   // INTEGRATION_MODELS_SCENEOBJECTCOMPONENTMODEL_H
