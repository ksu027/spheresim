
#include "scenegraphmodel.h"


#include "../sceneobject.h"


// gmlib2
#include <gmlib2.h>


namespace integration
{


  namespace detail
  {


    void subtypeCurveIdentifier(
      ScenegraphModel::SubtypeIdentifier&           id,
      const gmlib2::parametric::Curve<SceneObject>* pcurve)
    {
      // Circle
      if (const auto* pcircle
          = dynamic_cast<const gmlib2::parametric::Circle<SceneObject>*>(
            pcurve);
          pcircle) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(pcircle)>());
        id.setName("Circle");
      }
      // Line
      else if (const auto* pline
               = dynamic_cast<const gmlib2::parametric::Line<SceneObject>*>(
                 pcurve);
               pline) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(pline)>());
        id.setName("Line");
      }
      // BezierCurve
      else if (const auto* pbeziercurve = dynamic_cast<
                 const gmlib2::parametric::BezierCurve<SceneObject>*>(pcurve);
               pbeziercurve) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(pbeziercurve)>());
        id.setName("BezierCurve");
      }
      // BSplineCurve
      else if (const auto* pbsplinecurve = dynamic_cast<
                 const gmlib2::parametric::BSplineCurve<SceneObject>*>(pcurve);
               pbsplinecurve) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pbsplinecurve)>());
        id.setName("BSplineCurve");
      }
      // NURBSCurve
      else if (const auto* pnurbscurve = dynamic_cast<
                 const gmlib2::parametric::NURBSCurve<SceneObject>*>(pcurve);
               pnurbscurve) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(pnurbscurve)>());
        id.setName("NURBSCurve");
      }
      // Blend-Spline Curve
      else if (const auto* pblendingspline = dynamic_cast<
                 const gmlib2::parametric::BlendingSplineCurve<SceneObject>*>(
                 pcurve);
               pblendingspline) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pblendingspline)>());
        id.setName("BlendingSplineCurve");
      }
    }


    void subtypeSurfaceIdentifier(
      ScenegraphModel::SubtypeIdentifier&             id,
      const gmlib2::parametric::Surface<SceneObject>* psurface)
    {
      // Plane
      if (const auto* pplane
          = dynamic_cast<const gmlib2::parametric::Plane<SceneObject>*>(
            psurface);
          pplane) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(pplane)>());
        id.setName("Plane");
      }
      // Sphere
      else if (const auto* psphere
               = dynamic_cast<const gmlib2::parametric::Sphere<SceneObject>*>(
                 psurface);
               psphere) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(psphere)>());
        id.setName("Sphere");
      }
      // Torus
      else if (const auto* ptorus
               = dynamic_cast<const gmlib2::parametric::Torus<SceneObject>*>(
                 psurface);
               ptorus) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(ptorus)>());
        id.setName("Torus");
      }
      // BezierSurface
      else if (const auto* pbeziersurface = dynamic_cast<
                 const gmlib2::parametric::BezierSurface<SceneObject>*>(
                 psurface);
               pbeziersurface) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pbeziersurface)>());
        id.setName("BezierSurface");
      }
      // PBiLinearCoons
      else if (const auto* pbilinearcoons = dynamic_cast<
                 const gmlib2::parametric::BilinearCoonsPatch<SceneObject>*>(
                 psurface);
               pbilinearcoons) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pbilinearcoons)>());
        id.setName("BilinearCoonsPatch");
      }
      // PBiCubicCoons
      else if (const auto* pbicubiccoons = dynamic_cast<
                 const gmlib2::parametric::BicubicCoonsPatch<SceneObject>*>(
                 psurface);
               pbicubiccoons) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pbicubiccoons)>());
        id.setName("BicubicCoonsPatch");
      }
      // BSplineSurface
      else if (const auto* pbsplinesurface = dynamic_cast<
                 const gmlib2::parametric::BSplineSurface<SceneObject>*>(
                 psurface);
               pbsplinesurface) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pbsplinesurface)>());
        id.setName("BSplineSurface");
      }
      // NURBSSurface
      else if (const auto* pnurbssurface = dynamic_cast<
                 const gmlib2::parametric::NURBSSurface<SceneObject>*>(
                 psurface);
               pnurbssurface) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pnurbssurface)>());
        id.setName("NURBSSurface");
      }
      // PBlendSplineSurface
      else if (const auto* pblendingsplinesurface
               = dynamic_cast<const gmlib2::parametric::BlendingSplineSurface<
                 SceneObject>*>(psurface);
               pblendingsplinesurface) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pblendingsplinesurface)>());
        id.setName("BlendingSplineSurface");
      }
    }

    void subtypeTriangleSurfaceIdentifier(
      ScenegraphModel::SubtypeIdentifier&                    id,
      const gmlib2::parametric::TriangleSurface<SceneObject>* ptrianglesurface)
    {
      // Polygon
      if (const auto* ptriangle
          = dynamic_cast<const gmlib2::parametric::Triangle<SceneObject>*>(
            ptrianglesurface);
          ptriangle) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(ptriangle)>());
        id.setName("Triangle");
      }
      // GeneralizedBezierPatch
      else if (const auto* beziertriangle
               = dynamic_cast<const gmlib2::parametric::BezierTriangle<
                 SceneObject>*>(ptrianglesurface);
               beziertriangle) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           beziertriangle)>());
        id.setName("BezierTriangle");
      }
      else if (const auto* blendingsplinetriangle
               = dynamic_cast<const gmlib2::parametric::BlendingSplineTriangle<
                 SceneObject>*>(ptrianglesurface);
               blendingsplinetriangle) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           blendingsplinetriangle)>());
        id.setName("BS Triangle");
      }
    }

    void subtypePolygonSurfaceIdentifier(
      ScenegraphModel::SubtypeIdentifier&                    id,
      const gmlib2::parametric::PolygonSurface<SceneObject>* ppolygonsurface)
    {
      // Polygon
      if (const auto* ppolygon
          = dynamic_cast<const gmlib2::parametric::Polygon<SceneObject>*>(
            ppolygonsurface);
          ppolygon) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(ppolygon)>());
        id.setName("Polygon");
      }
      // GeneralizedBezierPatch
      else if (const auto* generalizedbezierpatch
               = dynamic_cast<const gmlib2::parametric::GeneralizedBezierPatch<
                 SceneObject>*>(ppolygonsurface);
               generalizedbezierpatch) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           generalizedbezierpatch)>());
        id.setName("GeneralizedBezierPatch");
      }
    }

    void subtypePVolumeIdentifier(
      ScenegraphModel::SubtypeIdentifier&            id,
      const gmlib2::parametric::Volume<SceneObject>* pvolume)
    {
      // Cuboid
      if (const auto* pcuboid
          = dynamic_cast<const gmlib2::parametric::Cuboid<SceneObject>*>(
            pvolume);
          pcuboid) {
        id.setArchIdName(
          scenegraphmodel::generateSubtypeArchIdName<decltype(pcuboid)>());
        id.setName("Cuboid");
      }
      // BezierVolume
      else if (const auto* pbeziervolume = dynamic_cast<
                 const gmlib2::parametric::BezierVolume<SceneObject>*>(
                 pvolume);
               pbeziervolume) {
        id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                           pbeziervolume)>());
        id.setName("BezierVolume");
      }
    }

  }   // namespace detail



  ScenegraphModel::SubtypeIdentifier
  ScenegraphModel::subtypeIdentifier(const QModelIndex& index) const
  {
    // guard
    if (not index.isValid()) SubtypeIdentifier();

    const SceneObject* so = m_flat_scenegraph.at(index.row())->m_sceneobject;

    return subtypeIdentifier(so);
  }

  ScenegraphModel::SubtypeIdentifier
  ScenegraphModel::subtypeIdentifier(const SceneObject* so) const
  {

    // Unknown
    auto id = SubtypeIdentifier();

    // Guard
    if (not so) return id;

    // Fallback (sceneobject)
    const auto so_arch_id_name
      = scenegraphmodel::generateSubtypeArchIdName<decltype(so)>();
    id.setArchIdName(so_arch_id_name);
    id.setName("SceneObject");
    id.setBestFitSubtypeModuleArchIdName(so_arch_id_name);



    // Default known
    if (const auto* ppoint
        = dynamic_cast<const gmlib2::parametric::PPoint<SceneObject>*>(so);
        ppoint) {
      id.setArchIdName(
        scenegraphmodel::generateSubtypeArchIdName<decltype(ppoint)>());
      id.setName("PPoint");
    }
    else if (const auto* pcurve
             = dynamic_cast<const gmlib2::parametric::Curve<SceneObject>*>(so);
             pcurve) {
      id.setArchIdName(
        scenegraphmodel::generateSubtypeArchIdName<decltype(pcurve)>());
      id.setName("Curve");

      detail::subtypeCurveIdentifier(id, pcurve);
    }
    else if (const auto* psurface
             = dynamic_cast<const gmlib2::parametric::Surface<SceneObject>*>(
               so);
             psurface) {
      id.setArchIdName(
        scenegraphmodel::generateSubtypeArchIdName<decltype(psurface)>());
      id.setName("Surface");

      detail::subtypeSurfaceIdentifier(id, psurface);
    }
    else if (const auto* ptrianglesurface = dynamic_cast<
               const gmlib2::parametric::TriangleSurface<SceneObject>*>(so);
             ptrianglesurface) {
      id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                         ptrianglesurface)>());
      id.setName("TriangleSurface");

      detail::subtypeTriangleSurfaceIdentifier(id, ptrianglesurface);
    }
    else if (const auto* ppolygonsurface = dynamic_cast<
               const gmlib2::parametric::PolygonSurface<SceneObject>*>(so);
             ppolygonsurface) {
      id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                         ppolygonsurface)>());
      id.setName("PolygonSurface");

      detail::subtypePolygonSurfaceIdentifier(id, ppolygonsurface);
    }
    else if (const auto* pvolume
             = dynamic_cast<const gmlib2::parametric::Volume<SceneObject>*>(so);
             pvolume) {
      id.setArchIdName(scenegraphmodel::generateSubtypeArchIdName<decltype(
                         pvolume)>());
      id.setName("Volume");

      detail::subtypePVolumeIdentifier(id, pvolume);
    }

    return id;
  }

  SceneObjectModel*
  ScenegraphModel::subtypeModel(const QModelIndex& index) const
  {
    // guard
    if (not index.isValid()) return nullptr;

    const SceneObject* so = m_flat_scenegraph.at(index.row())->m_sceneobject;
    return subtypeModel(so);
  }
  SceneObjectModel* ScenegraphModel::subtypeModel(const SceneObject* so) const
  {
    // If model is create return model
    if (m_subtype_models.contains(so)) return m_subtype_models.value(so);

    // If model is supported construct model for so and return model
    const auto subtype_archid
      = QString(subtypeIdentifier(so).bestFitSubtypeModuleArchIdName());
    if (m_subtype_model_factories.contains(subtype_archid)) {
      const auto factory   = m_subtype_model_factories[subtype_archid];
      auto*      model     = factory->create(so);
      m_subtype_models[so] = model;
      m_subtype_models[so]->setParent(const_cast<ScenegraphModel*>(this));
      return m_subtype_models.value(so);
    }

    // Nop! doesn't exist returning nullptr
    return nullptr;
  }

}   // namespace integration
