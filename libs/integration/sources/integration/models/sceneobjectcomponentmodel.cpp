#include "sceneobjectcomponentmodel.h"

#include "../sceneobject.h"

// qt
#include <Qt3DRender>


namespace integration
{

  SceneObjectComponentModel::SceneObjectComponentModel(
    const SceneObject* const so)
    : m_so{so}
  {
//    qDebug() << "I'm starting my life: " << this;
  }

  SceneObjectComponentModel::~SceneObjectComponentModel()
  {
//    qDebug() << "I'm ending my life: " << this;
  }

  SceneObjectComponentModel::ComponentTypes
  SceneObjectComponentModel::typeOfComponent(const QObject* object) const
  {
    if (qobject_cast<const Qt3DRender::QGeometryRenderer*>(object))
      return ComponentTypes::Mesh;
    else if (qobject_cast<const Qt3DCore::QTransform*>(object))
      return ComponentTypes::Transform;
    else if (qobject_cast<const Qt3DRender::QMaterial*>(object))
      return ComponentTypes::Material;
    else if (qobject_cast<const Qt3DRender::QObjectPicker*>(object))
      return ComponentTypes::ObjectPicker;
    else
      return ComponentTypes::Unknown;
  }

  QVariant SceneObjectComponentModel::data(const QModelIndex& index,
                                           int                role) const
  {
    //  if (index.row() < 0 or index.row() >rowCount())
    if (not index.isValid()) return QVariant();

    auto* component      = m_so->components().at(index.row());
    auto  component_type = typeOfComponent(component);

    switch (role) {
      case ComponentRole:
        return QVariant::fromValue(component);
      case ComponentTypeRole:
        return QVariant::fromValue(component_type);
    }

    return QVariant();
  }

  QModelIndex SceneObjectComponentModel::index(int row, int column,
                                               const QModelIndex& /*parent*/) const
  {
    if (column != 0) return QModelIndex();
    if (row >= 0 or row < m_so->components().size())
      return createIndex(row, column, nullptr);
    return QModelIndex();
  }

  QModelIndex SceneObjectComponentModel::parent(const QModelIndex& /*child*/) const
  {
    return QModelIndex();
  }

  QHash<int, QByteArray> SceneObjectComponentModel::roleNames() const
  {
    QHash<int, QByteArray> role_names;
    role_names[ComponentTypeRole] = "component_type";
    role_names[ComponentRole]     = "component_object";
    return role_names;
  }

  int SceneObjectComponentModel::rowCount(const QModelIndex& /*parent*/) const
  {
    return m_so->components().size();
  }

  int SceneObjectComponentModel::columnCount(const QModelIndex& /*parent*/) const
  {
    return 1;
  }

}   // namespace integration
