#include "rendercaptureprovider.h"

namespace appcore
{

  RenderCaptureProvider::RenderCaptureProvider(QObject *parent)
    : QObject(parent), QQuickImageProvider(Image)
  {
    m_image = QImage(10, 10, QImage::Format_ARGB32);
    m_image.fill(QColor("#2C2A29").rgba());
  }

  void
  RenderCaptureProvider::updateImage(Qt3DRender::QRenderCaptureReply* reply)
  {
    m_image = reply->image();
  }

  QImage RenderCaptureProvider::requestImage(const QString& id, QSize* size,
                                             const QSize& requestedSize)
  {
    Q_UNUSED(id)
    Q_UNUSED(requestedSize)
    *size = m_image.size();
    return m_image;
  }

}   // namespace appcore
