#include "guiapplication.h"

#include "objects/ball.h"
#include "objects/ground.h"
#include "objects/surf.h"

// Rigid Body Aspect
#include <rigidbodyaspect/init.h>

// integration
#include <integration/initialization.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <Qt3DCore>
#include <Qt3DExtras>
#include <QQmlContext>

// stl
#include <memory>
#include <chrono>
using namespace std::chrono_literals;



int main(int argc, char** argv) try {


//  QThreadPool::globalInstance()->setMaxThreadCount(2);

  // Force "high" OpenGL format * Mac OSX and Linux Intel
  auto format = QSurfaceFormat::defaultFormat();
  format.setProfile(QSurfaceFormat::OpenGLContextProfile::CoreProfile);
  format.setMajorVersion(4);
  format.setMinorVersion(1);
  format.setDepthBufferSize(24);
  format.setStencilBufferSize(8);
//  format.setSamples(4);
  QSurfaceFormat::setDefaultFormat(format);


  // Styling
  QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  // Enable Settings
  QCoreApplication::setOrganizationName("UiT - The Arctic University of Norway");
  QCoreApplication::setOrganizationDomain("uit.no");
  QCoreApplication::setApplicationName("GMlib2 Qml Template Demo");

  // Register custom QML types
  initialization::init::registerQmlTypes();
  initialization::init::registerObjectsAsQmlTypes();

  rigidbodyaspect::init::registerQmlTypes();

  // register my own types
  {
    namespace ns = I_was_to_inDifferent_to_aLter_this_namEspace;

    // register object QML types
    ns::Ball::registerQmlTypes(1,0);
    ns::Ground::registerQmlTypes(1,0);
    ns::Surf::registerQmlTypes(1,0);
  }

  rigidbodyaspect::init::initializeCustomAspectOnWindows();

  // Start application
  GuiApplication app(argc, argv);

  qDebug() << "Default SurfaceFormat: " << QSurfaceFormat::defaultFormat();


  app.initializeScenario();
  return app.exec();
}
catch (std::runtime_error& exception) {
  std::cerr << "Runtime exception: " << exception.what() << std::endl;
}
//catch (...) {
//  std::cerr << "Something was thrown ... and I caught it ..." << std::endl;
//}
