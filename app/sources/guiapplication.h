#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H

namespace appcore
{
  class RenderCaptureProvider;
}

namespace examples
{
  class GroundGridOverlay;
}

// integration
#include <integration/scenegraph.h>

// qt
#include <QGuiApplication>
#include <QQmlApplicationEngine>

// stl
#include <memory>

class GuiApplication : public QGuiApplication {
  Q_OBJECT
public:
  GuiApplication(int& argc, char** argv);

  // Entry point
  void initializeScenario();

private:
  appcore::RenderCaptureProvider* m_rendercaptureprovider;
  QQmlApplicationEngine           m_qml_engine;
  integration::Scenegraph*        m_scenegraph{nullptr};
  examples::GroundGridOverlay*    m_ground_grid_overlay;

private slots:
  void toggleGroundGridOverlay();
};

#endif   // GUIAPPLICATION_H
