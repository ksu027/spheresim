#include "guiapplication.h"

#include "rendercaptureprovider.h"

// examples
#include <integration/examples/utils/groundgridoverlay.h>

// integration
#include <integration/scenegraph.h>


// qt
#include <QQmlContext>
#include <Qt3DExtras>

// stl
#include <exception>


// Details ^^,
namespace detail
{
  void drawObjectTree(const QObjectList& objects, const QString& indent)
  {
    for (auto* object : objects) {
      qDebug() << indent + "ObjectName: " + object->objectName();
      drawObjectTree(object->children(), indent + " ");
    }
  }
}   // namespace detail


GuiApplication::GuiApplication(int& argc, char** argv)
  : QGuiApplication(argc, argv)
{

  // Load external resources
  const auto resource_file_status
    = QResource::registerResource("app_example_binary_resources.rcc");
  qDebug() << "Resource file status: " << resource_file_status;


  m_rendercaptureprovider = new appcore::RenderCaptureProvider(this);

  qmlRegisterUncreatableType<appcore::RenderCaptureProvider>(
    "com.uit.GMlib2AppCore", 1, 0, "RenderCaptureProvider",
    "Tha' App Provides ^^,");
  m_qml_engine.rootContext()->setContextProperty(
    "appcore_rendercaptureprovider", m_rendercaptureprovider);
  m_qml_engine.addImageProvider("appcore_rendercapture",
                                m_rendercaptureprovider);

  // Load default QML
  m_qml_engine.load(QUrl("qrc:/lib_rb_qml/main.qml"));

  auto root_objects = m_qml_engine.rootObjects();

  // Runtime checks
  if (root_objects.isEmpty()) throw std::runtime_error("No QML root object");

  if (root_objects.size() not_eq 1)
    throw std::runtime_error("To many root object");

  // Fetch the root object
  auto* root_object = root_objects.at(0);

  auto  scene_root_entity_name = QString("scene_root_entity");
  auto* scene_root = root_object->findChild<integration::Scenegraph*>(
    scene_root_entity_name, Qt::FindChildrenRecursively);
  if (not scene_root)
    throw std::runtime_error("No <" + scene_root_entity_name.toStdString()
                             + "> root object defined!!");

  m_scenegraph = scene_root;

  m_ground_grid_overlay = new examples::GroundGridOverlay(
    m_scenegraph, QVector3D{0.0f, 0.0f, 0.0f}, QVector3D{0.0f, 0.0f, 1.0f},
    QVector3D{1.0f, 0.0f, 0.0f}, QSize{100, 100}, QColor("blue"), QColor("red"),
    QColor("green"));

  connect(root_object, SIGNAL(toggleGroundGridOverlay()), this,
          SLOT(toggleGroundGridOverlay()));
}

void GuiApplication::toggleGroundGridOverlay()
{
  m_ground_grid_overlay->setEnabled(!m_ground_grid_overlay->isEnabled());
}

void GuiApplication::initializeScenario()
{

  if (not m_scenegraph) return;

  // examples::initExampleScenario(m_scenegraph);

  ///////////////////////
  ///////////////////////
  ///                 ///
  ///                 ///
  ///  ADD STUFF ^^,  ///
  ///                 ///
  ///                 ///
  ///////////////////////
  ///////////////////////

  //  m_scenegraph->loadScenario( {"Examples", "Blending Spline Triangle"} );
}
