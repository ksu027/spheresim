#ifndef RENDERCAPTUREPROVIDER_H
#define RENDERCAPTUREPROVIDER_H


#include <QtQuick/QQuickImageProvider>
#include <Qt3DRender/QRenderCapture>

namespace appcore
{

  class RenderCaptureProvider : public QObject, public QQuickImageProvider {
    Q_OBJECT
  public:
    RenderCaptureProvider(QObject* parent);

    Q_INVOKABLE void updateImage(Qt3DRender::QRenderCaptureReply* reply);
    QImage           requestImage(const QString& id, QSize* size,
                                  const QSize& requestedSize) override;

  private:
    QImage m_image;
  };

}   // namespace appcore


#endif   // RENDERCAPTUREPROVIDER_H
