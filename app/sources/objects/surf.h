#ifndef SURF_H
#define SURF_H

#include <integration/parametric/objects/surface.h>

// qt
#include <QQmlEngine>


namespace I_was_to_inDifferent_to_aLter_this_namEspace
{
  class Surf : public integration::Surface<
                 gmlib2::parametric::BezierSurface<integration::SceneObject>> {
    using Base
      = Surface<gmlib2::parametric::BezierSurface<integration::SceneObject>>;
    Q_OBJECT

    Q_PROPERTY(integration::SurfaceMesh* defaultMesh READ defaultMesh)

    Q_PROPERTY(QVector2D scale READ scale WRITE setScale NOTIFY scaleChanged)

  public:
    // Constructor(s)

    template <typename... Ts>
    Surf(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    Q_INVOKABLE void setParametersQt(const QVector2D& s)
    {
      std::vector<Point> v_points;
      v_points.push_back(Point{0.0, 3.0, 0.0});
      v_points.push_back(Point{5.0, 3.0, 0.0});
      v_points.push_back(Point{0.0, 3.0, 5.0});
      v_points.push_back(Point{5.0, 3.0, 5.0});

      if (defaultMesh()) defaultMesh()->sample();
    }

    static void registerQmlTypes(int version_major, int version_minor)
    {
      if (qt_types_initialized) return;

      constexpr auto registertype_uri = "com.uit.STE6245";
      qmlRegisterType<Surf>(registertype_uri, version_major, version_minor,
                            "Surf");

      qt_types_initialized = true;
    }


    const QVector2D scale() const { return {1.0f, 1.0f}; }


    void setScale(const QVector2D& s) { emit(scaleChanged(s)); }



    // Signal(s)
  signals:
    void scaleChanged(const QVector2D) const;

  private:
    static bool qt_types_initialized;
  };

}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace


#endif   // SURF_H
