#ifndef BALL_H
#define BALL_H

#include <integration/parametric/objects/surface.h>

// qt
#include <QQmlEngine>

namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

  class Ball : public integration::Surface<
                 gmlib2::parametric::Sphere<integration::SceneObject>> {
    using Base = Surface<gmlib2::parametric::Sphere<integration::SceneObject>>;
    Q_OBJECT

    Q_PROPERTY(integration::SurfaceMesh* defaultMesh READ defaultMesh)

    Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)

    // Constructor(s)
  public:
    template <typename... Ts>
    Ball(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();

      connect(this, &Ball::radiusChanged, this->defaultMesh(),
              &integration::SurfaceMesh::sample);
    }

    static void registerQmlTypes(int version_major, int version_minor)
    {
      if (qt_types_initialized) return;

      constexpr auto registertype_uri = "com.uit.STE6245";
      qmlRegisterType<Ball>(registertype_uri, version_major, version_minor,
                            "Ball");

      qt_types_initialized = true;
    }

    // Signal(s)
  signals:
    void radiusChanged();

  private:
    static bool qt_types_initialized;
  };


}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace


#endif   // BALL_H
