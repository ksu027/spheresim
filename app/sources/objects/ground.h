#ifndef GROUND_H
#define GROUND_H

#include <integration/parametric/objects/surface.h>

// qt
#include <QQmlEngine>


namespace I_was_to_inDifferent_to_aLter_this_namEspace
{

  class Ground : public integration::Surface<
                   gmlib2::parametric::Plane<integration::SceneObject>> {
    using Base = Surface<gmlib2::parametric::Plane<integration::SceneObject>>;
    Q_OBJECT

    Q_PROPERTY(integration::SurfaceMesh* defaultMesh READ defaultMesh)

    Q_PROPERTY(
      QVector3D origin READ origin WRITE setOrigin NOTIFY originChanged)
    Q_PROPERTY(QVector3D uAxis READ uAxis WRITE setUAxis NOTIFY uAxisChanged)
    Q_PROPERTY(QVector3D vAxis READ vAxis WRITE setVAxis NOTIFY vAxisChanged)

  public:
    // Constructor(s)
    template <typename... Ts>
    Ground(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
    }

    Q_INVOKABLE void setParametersQt(const QVector3D& p, const QVector3D& u,
                                     const QVector3D& v)
    {
      m_pt = VectorH{double(p.x()), double(p.y()), double(p.z()), 1.0};
      m_u  = VectorH{double(u.x()), double(u.y()), double(u.z()), 0.0};
      m_v  = VectorH{double(v.x()), double(v.y()), double(v.z()), 0.0};
      if (defaultMesh()) defaultMesh()->sample();
    }

    static void registerQmlTypes(int version_major, int version_minor)
    {
      if (qt_types_initialized) return;

      constexpr auto registertype_uri = "com.uit.STE6245";
      qmlRegisterType<Ground>(registertype_uri, version_major, version_minor,
                              "Ground");

      qt_types_initialized = true;
    }

    const QVector3D origin() const
    {
      return {float(m_pt[0]), float(m_pt[1]), float(m_pt[2])};
    }

    const QVector3D uAxis() const
    {
      return {float(m_u[0]), float(m_u[1]), float(m_u[2])};
    }

    const QVector3D vAxis() const
    {
      return {float(m_v[0]), float(m_v[1]), float(m_v[2])};
    }

    void setOrigin(const QVector3D& p)
    {
      m_pt = VectorH{double(p.x()), double(p.y()), double(p.z()), 1.0};
      emit(originChanged(p));
    }

    void setUAxis(const QVector3D& u)
    {
      m_u = VectorH{double(u.x()), double(u.y()), double(u.z()), 0.0};
      emit(uAxisChanged(u));
    }

    void setVAxis(const QVector3D& v)
    {
      m_v = VectorH{double(v.x()), double(v.y()), double(v.z()), 0.0};
      emit(vAxisChanged(v));
    }

    // Signal(s)
  signals:
    void originChanged(const QVector3D) const;
    void uAxisChanged(const QVector3D) const;
    void vAxisChanged(const QVector3D) const;

  private:
    static bool qt_types_initialized;
  };

}   // namespace I_was_to_inDifferent_to_aLter_this_namEspace


#endif   // GROUND_H
