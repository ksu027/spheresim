#include <benchmark/benchmark.h>
#include <gmlib2.h>
#include <random>

static void BM_Integration_Exact(benchmark::State& state) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<double> uniF(-10.0,10.0);
    std::uniform_real_distribution<double> uniM(0.0,100.0);
    std::uniform_real_distribution<double> uniV(-100.0,100.0);
    std::uniform_real_distribution<double> uniD(0.0,1.0);

    const gmlib2::ProjectiveSpaceObject<>::Vector Fc({uniF(rng),uniF(rng),uniF(rng)});
    const gmlib2::ProjectiveSpaceObject<>::Vector v({uniV(rng),uniV(rng),uniV(rng)});
    const double m = uniM(rng);
    const double dt = 0.016;
    const auto k_drag = uniD(rng);
    for (auto _ : state)
  {
//      state.PauseTiming();

//      state.ResumeTiming();
//state.PauseTiming();
//      const gmlib2::ProjectiveSpaceObject<>::Vector Fc({0.0,-9.8,0.0});
//      const double m = 0.5;
//      const double dt = 0.16;
//      const gmlib2::ProjectiveSpaceObject<>::Vector v({0.0,1.0,2.0});
//state.ResumeTiming();
      benchmark::DoNotOptimize(k_drag);
      gmlib2::ProjectiveSpaceObject<>::Vector   a;
      benchmark::DoNotOptimize(a = Fc/m);
      gmlib2::ProjectiveSpaceObject<>::Vector  dv;
      gmlib2::ProjectiveSpaceObject<>::Vector  ds ;
      benchmark::DoNotOptimize(dv = a*dt);
      benchmark::DoNotOptimize(ds = (v+dv/2.0)*dt);
      benchmark::ClobberMemory();
  }
}
// Register the function as a benchmark



static void BM_Integration_Euler(benchmark::State& state) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<double> uniF(-10.0,10.0);
    std::uniform_real_distribution<double> uniM(0.0,100.0);
    std::uniform_real_distribution<double> uniV(-100.0,100.0);
    std::uniform_real_distribution<double> uniD(0.0,1.0);

    const gmlib2::ProjectiveSpaceObject<>::Vector Fc({uniF(rng),uniF(rng),uniF(rng)});
    const gmlib2::ProjectiveSpaceObject<>::Vector v({uniV(rng),uniV(rng),uniV(rng)});
    const double m = uniM(rng);
    const double dt = 0.016;
    const auto k_drag = uniD(rng);
    for (auto _ : state)
  {
//state.PauseTiming();

//state.ResumeTiming();
//state.PauseTiming();
//      const gmlib2::ProjectiveSpaceObject<>::Vector Fc({0.0,-9.8,0.0});
//      const double m = 0.5;
//      const double dt = 0.16;
//      const gmlib2::ProjectiveSpaceObject<>::Vector v({0.0,1.0,2.0});
//      const auto k_drag = 0.1;
//state.ResumeTiming();
//      benchmark::DoNotOptimize(k_drag);
      gmlib2::ProjectiveSpaceObject<>::Vector   F = Fc - k_drag*v;
      //benchmark::DoNotOptimize(F );
      gmlib2::ProjectiveSpaceObject<>::Vector k1 = dt*F/m;
      //benchmark::DoNotOptimize(k1 );
      gmlib2::ProjectiveSpaceObject<>::Vector  dv=k1;
      gmlib2::ProjectiveSpaceObject<>::Vector  ds=v*dt;
      benchmark::DoNotOptimize(dv=k1);
      benchmark::DoNotOptimize(ds=v*dt);
      benchmark::ClobberMemory();
  }
}
// Register the function as a benchmark



static void BM_Integration_Improved_Euler(benchmark::State& state) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<double> uniF(-10.0,10.0);
    std::uniform_real_distribution<double> uniM(0.0,100.0);
    std::uniform_real_distribution<double> uniV(-100.0,100.0);
    std::uniform_real_distribution<double> uniD(0.0,1.0);

    const gmlib2::ProjectiveSpaceObject<>::Vector Fc({uniF(rng),uniF(rng),uniF(rng)});
    const gmlib2::ProjectiveSpaceObject<>::Vector v({uniV(rng),uniV(rng),uniV(rng)});
    const double m = uniM(rng);
    const double dt = 0.016;
    const auto k_drag = uniD(rng);
    for (auto _ : state)
  {
//state.PauseTiming();
//state.ResumeTiming();
//state.PauseTiming();
//      const gmlib2::ProjectiveSpaceObject<>::Vector Fc({0.0,-9.8,0.0});
//      const double m = 0.5;
//      const double dt = 0.16;
//      const gmlib2::ProjectiveSpaceObject<>::Vector v({0.0,1.0,2.0});
//      const auto k_drag = 0.1;
//state.ResumeTiming();
      gmlib2::ProjectiveSpaceObject<>::Vector F = Fc - k_drag*v;
      //benchmark::DoNotOptimize();
      gmlib2::ProjectiveSpaceObject<>::Vector k1 = dt*F/m;
      //benchmark::DoNotOptimize();
      gmlib2::ProjectiveSpaceObject<>::Vector l1 = dt*v;
      //benchmark::DoNotOptimize();
      benchmark::DoNotOptimize(F = Fc - k_drag*(v+k1));
      gmlib2::ProjectiveSpaceObject<>::Vector k2 = dt*F/m;
      //benchmark::DoNotOptimize(k2 = dt*F/m);
      gmlib2::ProjectiveSpaceObject<>::Vector l2 = dt*(v+k1);
      //benchmark::DoNotOptimize(l2 = dt*(v+k1));

      gmlib2::ProjectiveSpaceObject<>::Vector  dv;
      gmlib2::ProjectiveSpaceObject<>::Vector  ds;
      benchmark::DoNotOptimize(dv = (k1+k2)/2.0);
      benchmark::DoNotOptimize(ds = (l1+l2)/2.0);
      benchmark::ClobberMemory();
  }
}
// Register the function as a benchmark


static void BM_Integration_RungeKutta(benchmark::State& state) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<double> uniF(-10.0,10.0);
    std::uniform_real_distribution<double> uniM(0.0,100.0);
    std::uniform_real_distribution<double> uniV(-100.0,100.0);
    std::uniform_real_distribution<double> uniD(0.0,1.0);

    const gmlib2::ProjectiveSpaceObject<>::Vector Fc({uniF(rng),uniF(rng),uniF(rng)});
    const gmlib2::ProjectiveSpaceObject<>::Vector v({uniV(rng),uniV(rng),uniV(rng)});
    const double m = uniM(rng);
    const double dt = 0.016;
    const auto k_drag = uniD(rng);
    for (auto _ : state)
  {
//state.PauseTiming();

//state.ResumeTiming();
//state.PauseTiming();
//      const gmlib2::ProjectiveSpaceObject<>::Vector Fc({0.0,-9.8,0.0});
//      const double m = 0.5;
//      const double dt = 0.16;
//      const gmlib2::ProjectiveSpaceObject<>::Vector v({0.0,1.0,2.0});
//      const auto k_drag = 0.1;
//state.ResumeTiming();

      gmlib2::ProjectiveSpaceObject<>::Vector F = Fc - k_drag*v;
      gmlib2::ProjectiveSpaceObject<>::Vector k1 = dt*F/m;
      gmlib2::ProjectiveSpaceObject<>::Vector l1 = dt*v;
      benchmark::DoNotOptimize(F = Fc - k_drag*(v+k1/2.0));
      gmlib2::ProjectiveSpaceObject<>::Vector k2 = dt*F/m;
      gmlib2::ProjectiveSpaceObject<>::Vector l2 = dt*(v+k1/2.0);
      benchmark::DoNotOptimize(F = Fc - k_drag*(v+k2/2.0));
      gmlib2::ProjectiveSpaceObject<>::Vector k3 = dt*F/m;
      gmlib2::ProjectiveSpaceObject<>::Vector l3 = dt*(v+k2/2.0);
      benchmark::DoNotOptimize(F = Fc - k_drag*(v+k3));
      gmlib2::ProjectiveSpaceObject<>::Vector k4 = dt*F/m;
      gmlib2::ProjectiveSpaceObject<>::Vector l4 = dt*(v+k3);
      gmlib2::ProjectiveSpaceObject<>::Vector  dv;
      gmlib2::ProjectiveSpaceObject<>::Vector  ds;
      benchmark::DoNotOptimize(dv = (k1+2.0*k2+2.0*k3+k4)/6.0);
      benchmark::DoNotOptimize(ds = (l1+2.0*l2+2.0*l3+l4)/6.0);
      benchmark::ClobberMemory();
  }
}
// Register the function as a benchmark


//// Define another benchmark
//static void BM_StringCopy(benchmark::State& state) {
//  std::string x = "hello";
//  for (auto _ : state)
//    std::string copy(x);
//}
//BENCHMARK(BM_StringCopy);


