#include <benchmark/benchmark.h>

#include "integration.cpp"
#include "unique.cpp"
#include "collision.cpp"
#include <gmlib2.h>
#include <random>


// benchmark integration methods
//BENCHMARK(BM_Integration_Exact);
//BENCHMARK(BM_Integration_Euler);
//BENCHMARK(BM_Integration_Improved_Euler);
//BENCHMARK(BM_Integration_RungeKutta);


// benchmark collision objects
//BENCHMARK(BM_Unique_Simple_Object);
//BENCHMARK(BM_Unique_Advanced_Object);

// intersection test
//BENCHMARK(BM_Sphere_Sphere_Rough);
//BENCHMARK(BM_Sphere_Sphere_Exact);
//BENCHMARK(BM_Sphere_Plane_Exact);
//BENCHMARK(BM_Sphere_Surf_Exact);

// dop test
// get location
//BENCHMARK(BM_Get_Frame_Parent);
//BENCHMARK(BM_Get_Pos);
// compute Normal
//BENCHMARK(BM_Compute_N);
//BENCHMARK(BM_Get_N);
// QSet contains
BENCHMARK(BM_Set_Contains);
BENCHMARK(BM_Set_Intersect);
BENCHMARK(BM_Set_Unite);

BENCHMARK_MAIN();
