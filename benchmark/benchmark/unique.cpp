#include <benchmark/benchmark.h>
#include <gmlib2.h>
#include <random>
#include <QSet>
#include <Qt3DCore>
#include <chrono>


using UniqueContainer = QSet<quint64>;
using Sphere          = double;
using FixedPlane      = double;
using Surf            = double;
using SphereQHash     = QHash<quint64,Sphere>;
using PlanesQHash     = QHash<quint64,FixedPlane>;
using SurfsQHash      = QHash<quint64,Surf>;
using seconds_type    = std::chrono::duration<double>;
using collisionType   = std::vector<std::tuple<seconds_type,quint64,quint64>>;
using collisionTypeAdvanced = std::vector<std::pair<seconds_type,QSet<quint64>>>;

// create unique on each step
static void BM_Unique_Simple_Object(benchmark::State& state) {
  for (auto _ : state)
  {
      state.PauseTiming();
      std::random_device dev;
      std::mt19937 rng(dev());
      std::uniform_int_distribution<std::mt19937::result_type> fake_id(1,10000);
      std::uniform_int_distribution<std::mt19937::result_type> sp_index(0,999);
      std::uniform_int_distribution<std::mt19937::result_type> all_index(0,2999);
      //std::uniform_int_distribution<std::mt19937::result_type> fake_time(0,16);
      std::uniform_real_distribution<double> fake_sphere(-1000.0,1000.0);
      std::uniform_real_distribution<double> uniT(0.0,10.0);
      std::uniform_real_distribution<double> fake_time(0.0,0.016);
      std::uniform_int_distribution<std::mt19937::result_type> probability(1,10);


      UniqueContainer      unique_container;
      SphereQHash          spheres;
      PlanesQHash          planes;
      SurfsQHash           surfs;
      std::vector<quint64> spheres_vector;
      std::vector<quint64> all_bodies_vector;
      collisionType        collisions;

      // create fake spheres
      for (auto i = 0; i<1000; i++)
      {
          const auto id = fake_id(rng);
          spheres.insert(id,fake_sphere(rng));
          spheres_vector.push_back(id);
          all_bodies_vector.push_back(id);
      }
      // create fake planes
      for (auto i = 0; i<1000; i++)
      {
          quint64 id;
          do
          {
              id = fake_id(rng);
          } while (spheres.contains(id));
          planes.insert(id,fake_sphere(rng));
          all_bodies_vector.push_back(id);
      }
      // create fake surfs
      for (auto i = 0; i<1000; i++)
      {
          quint64 id;
          do
          {
              id = fake_id(rng);
          } while (spheres.contains(id) or planes.contains(id));
          surfs.insert(id,fake_sphere(rng));
          all_bodies_vector.push_back(id);
      }

      // create fake collisions
      for (auto i = 0; i<100000; i++)
      {
          if (probability(rng)<=0) //0 - all random ; 3 - 30 % same; 10 - 100% same time of collision
          {
              std::chrono::microseconds ms(4);
              collisions.push_back({ms,spheres_vector[sp_index(rng)],all_bodies_vector[all_index(rng)]});
          } else
          {
              seconds_type ms(fake_time(rng));
              collisions.push_back({ms,spheres_vector[sp_index(rng)],all_bodies_vector[all_index(rng)]});
          }
      }
          auto collision_sort_criteria = [](const auto& a, const auto& b) {
            return std::get<0>(a) > std::get<0>(b);
          };

          auto                 unique_rem_criteria
            = [&unique_container, &spheres](const auto& a) {
                //check that active sphere was not in collision before
                const auto a_sid = std::get<1>(a);
                if (unique_container.contains(a_sid)) return true;
                unique_container.insert(a_sid);
                //check that passive is sphere and it was not in collision before
                const auto a_sid_second = std::get<2>(a);
                if (spheres.contains(a_sid_second))
                {
                  if (unique_container.contains(a_sid_second)) return true;
                  unique_container.insert(a_sid_second);
                }
                return false;
              };

          auto sort_and_make_unique_driver
            = [&collisions, collision_sort_criteria, &unique_container,
               unique_rem_criteria]() {
                std::sort(std::begin(collisions), std::end(collisions),
                          collision_sort_criteria);
                unique_container.clear();
                collisions.erase(std::rend(collisions).base(),
                                 std::remove_if(std::rbegin(collisions),
                                                std::rend(collisions),
                                                unique_rem_criteria).base());
              };
      state.ResumeTiming();
      //benchmark::DoNotOptimize(sort_and_make_unique_driver);
      sort_and_make_unique_driver();
      benchmark::ClobberMemory();
  }
}

static void cleanUpCollisions(collisionTypeAdvanced& collision_sets,const SphereQHash&  all_spheres)
{
  auto sort_criteria = [](const auto& a, const auto& b) {
    return std::get<0>(a) > std::get<0>(b);
  };
  //first sort with respect to time
  std::sort(std::begin(collision_sets),
            std::end(collision_sets),
            sort_criteria);

  //temporary resulting vector
  collisionTypeAdvanced new_collision_sets;

  const auto & all_spheres_ids = all_spheres.keys().toSet();

  //iterate through the given vector of the collisions to group and make unique collision sets
  for (auto i=collision_sets.rbegin(); i!=collision_sets.rend(); ++i)
  {
    auto current_set = *i;
    QSet<size_t> groups_to_delete;
    bool add_new_set = true;
    for (size_t j=0; j<new_collision_sets.size(); j++)
    {
      auto group = new_collision_sets[j];
      const auto& intersection = group.second & current_set.second & all_spheres_ids;
      if (intersection.size()>0)
      {
        // if time is the same - make groups
        if (group.first==current_set.first)
        {
          //group.second.unite(collision_sets[i].second);
          current_set.second.unite(group.second);
          groups_to_delete.insert(j);
        }
        else
        {
          // there was an intersection in the time before so there is no need to add it
          add_new_set = false;
        }
      }

    }
    // delete those groups in the same time which had
    auto compensate = 0;
    for (auto& index_delete_group:groups_to_delete)
    {
      new_collision_sets.erase(new_collision_sets.begin()+int(index_delete_group)-compensate);
      compensate++;
    }
    if (add_new_set)
    {
      new_collision_sets.emplace_back(current_set);
    }
  }

  collision_sets.clear();
  collision_sets = new_collision_sets;

  // may be sort is extra, or may be not (if we say that collision sets is a new collision set in reverse)
  std::sort(std::begin(collision_sets),
            std::end(collision_sets),
            sort_criteria);
//  return collision_sets;
};

static void BM_Unique_Advanced_Object(benchmark::State& state) {

    //for (auto _ : state)
    while (state.KeepRunning())
    {
        state.PauseTiming();
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> fake_id(1,10000);
        std::uniform_int_distribution<std::mt19937::result_type> sp_index(0,999);
        std::uniform_int_distribution<std::mt19937::result_type> all_index(0,2999);
        //std::uniform_int_distribution<std::mt19937::result_type> fake_time(0,1000);
        std::uniform_int_distribution<std::mt19937::result_type> size_of_set(1,100);
        std::uniform_real_distribution<double> fake_sphere(-1000.0,1000.0);
        std::uniform_real_distribution<double> uniT(0.0,10.0);
        std::uniform_real_distribution<double> fake_time(0.0,16.0);
        std::uniform_int_distribution<std::mt19937::result_type> probability(1,10);


        UniqueContainer      unique_container;
        SphereQHash          spheres;
        PlanesQHash          planes;
        SurfsQHash           surfs;
        std::vector<quint64> spheres_vector;
        std::vector<quint64> all_bodies_vector;
        collisionTypeAdvanced collision_sets;

        // create fake spheres
        for (auto i = 0; i<1000; i++)
        {
            const auto id = fake_id(rng);
            spheres.insert(id,fake_sphere(rng));
            spheres_vector.push_back(id);
            all_bodies_vector.push_back(id);
        }
        // create fake planes
        for (auto i = 0; i<1000; i++)
        {
            quint64 id;
            do
            {
                id = fake_id(rng);
            } while (spheres.contains(id));
            planes.insert(id,fake_sphere(rng));
            all_bodies_vector.push_back(id);
        }
        // create fake surfs
        for (auto i = 0; i<1000; i++)
        {
            quint64 id;
            do
            {
                id = fake_id(rng);
            } while (spheres.contains(id) or planes.contains(id));
            surfs.insert(id,fake_sphere(rng));
            all_bodies_vector.push_back(id);
        }

        // create fake collisions
        for (auto i = 0; i<1000; i++)
        {
            auto set_size = size_of_set(rng);
            QSet<quint64> col_set;
            col_set.insert(spheres_vector[sp_index(rng)]);
//            for (quint64 si=0; si<set_size;si++)
//            {
                quint64 id;
                do
                {
                    id = all_bodies_vector[all_index(rng)];
                } while (col_set.contains(id));
                col_set.insert(id);
//            }
//            if (probability(rng)<=10) //0 - all random ; 3 - 30 % same; 10 - 100% same time of collision
//            {
//                std::chrono::microseconds ms(4);
//                collision_sets.push_back({ms,col_set});
//            } else
//            {
                seconds_type ms(fake_time(rng));
                collision_sets.push_back({ms,col_set});
//            }
        }
        state.ResumeTiming();
        //benchmark::DoNotOptimize();
        cleanUpCollisions(collision_sets,spheres);
        benchmark::ClobberMemory();
    }
}
