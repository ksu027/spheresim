#include <benchmark/benchmark.h>
#include <gmlib2.h>
#include <random>
#include <QSet>
#include <Qt3DCore>

using seconds_type = std::chrono::duration<double>;

class FixedBody : public gmlib2::ProjectiveSpaceObject<> {
public:
  // members
  Unit              m_friction;
  Qt3DCore::QNodeId m_env_id;
};

using FixedPlane_Col = gmlib2::parametric::Plane<FixedBody>;
using Surf_Col       = gmlib2::parametric::BezierSurface<FixedBody>;

class SurroundSphere {
public:
  SurroundSphere(gmlib2::ProjectiveSpaceObject<>::Vector& in_c, double in_r)
    : m_center(in_c), m_radius(in_r)
  {
  }
  gmlib2::ProjectiveSpaceObject<>::Vector getCenter() { return m_center; }
  double                                  getRadius() { return m_radius; }

private:
  gmlib2::ProjectiveSpaceObject<>::Vector m_center;
  double                                  m_radius;
};
class Sphere_Col {
public:
  Sphere_Col(SurroundSphere& in_ss) : m_ss(in_ss) {}
  SurroundSphere&                         getSurroundSphere() { return m_ss; }
  seconds_type                            m_dt_tp;
  double                                  m_radius;
  gmlib2::ProjectiveSpaceObject<>::Vector m_ds;
  gmlib2::ProjectiveSpaceObject<>::Vector m_pos;

private:
  SurroundSphere m_ss;
};
