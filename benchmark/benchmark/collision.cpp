#include <benchmark/benchmark.h>
#include <gmlib2.h>
#include <random>
#include <QSet>
#include <Qt3DCore>

using seconds_type    = std::chrono::duration<double>;

class FixedBody : public gmlib2::ProjectiveSpaceObject<>  {
public:
  // members
  Unit              m_friction;
  Qt3DCore::QNodeId m_env_id;
};

using FixedPlane_Col = gmlib2::parametric::Plane<FixedBody>;
using Surf_Col           = gmlib2::parametric::BezierSurface<FixedBody>;

class SurroundSphere {
public:
  SurroundSphere(gmlib2::ProjectiveSpaceObject<>::Vector& in_c,double in_r):m_center(in_c),m_radius(in_r){}
  gmlib2::ProjectiveSpaceObject<>::Vector getCenter(){return m_center;}
  double getRadius(){return m_radius;}
private:
  gmlib2::ProjectiveSpaceObject<>::Vector m_center;
  double                                  m_radius;
};
class Sphere_Col {
public:
    Sphere_Col(SurroundSphere& in_ss):m_ss(in_ss){}
    SurroundSphere& getSurroundSphere() {return m_ss;}
    seconds_type m_dt_tp;
    double m_radius;
    gmlib2::ProjectiveSpaceObject<>::Vector m_ds;
    gmlib2::ProjectiveSpaceObject<>::Vector m_pos;
private:
    SurroundSphere m_ss;
};

static seconds_type detectCollisionSp(
                         Sphere_Col&        sphere_active,
                         Sphere_Col&        sphere_passive,
                         seconds_type       dt,
                         seconds_type       dt_rem_min_spare,
                         double             root_spare
        )
{
    const auto dt_rem_act = dt-sphere_active.m_dt_tp;
    const auto dt_rem_pas = dt-sphere_passive.m_dt_tp;
    auto dt_rem_min = std::min(dt_rem_act,dt_rem_pas);

    const auto go_after_ratio_act   = dt_rem_min/dt_rem_act;
    const auto go_after_ratio_pas   = dt_rem_min/dt_rem_pas;

    const auto go_before_ratio_act   = (dt_rem_act-dt_rem_min)/dt_rem_act;
    const auto go_before_ratio_pas   = (dt_rem_pas-dt_rem_min)/dt_rem_pas;
    if (dt_rem_min.count() <= 0) dt_rem_min = dt_rem_min_spare;

    const gmlib2::ProjectiveSpaceObject<>::Vector p_act = sphere_active.m_pos+sphere_active.m_ds*go_before_ratio_act;
    const auto r_act = sphere_active.m_radius;
    const gmlib2::ProjectiveSpaceObject<>::Vector p_pas = sphere_passive.m_pos+sphere_passive.m_ds*go_before_ratio_pas;
    const auto r_pas = sphere_passive.m_radius;
    const auto r     = r_act+r_pas;

    const gmlib2::ProjectiveSpaceObject<>::Vector ds_act = sphere_active.m_ds*go_after_ratio_act;
    const gmlib2::ProjectiveSpaceObject<>::Vector ds_pas = sphere_passive.m_ds*go_after_ratio_pas;

    const auto Q = p_act-p_pas;
    const auto R = ds_act-ds_pas;

    const auto QR = blaze::inner(Q,R);
    const auto RR = blaze::inner(R,R);
    const auto QQ = blaze::inner(Q,Q);

    auto root = pow(QR,2)-RR*(QQ-pow(r,2));

    //if (root < 0.0 ) return seconds_type(0);
    if (root < 0.0 ) root = root_spare;

    const auto x = (-QR-sqrt(root))/RR;

    if (x>0.0 and x<=1.0 ) {return sphere_active.m_dt_tp + (x * dt_rem_act);}
    //return seconds_type(0);
    return sphere_active.m_dt_tp + (x * dt_rem_act);
}

//exact sphere sphere collision detection
static void BM_Sphere_Sphere_Exact(benchmark::State& state) {

    //state.PauseTiming();
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_real_distribution<double> coord_gen(-1000.0,1000.0);
        std::uniform_real_distribution<double> radius_gen(0.1,100.0);
        std::uniform_real_distribution<double> fake_time(0.0001,0.015);
        gmlib2::ProjectiveSpaceObject<>::Vector center_1({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        SurroundSphere ss_1(center_1,radius_gen(rng));
        Sphere_Col sphere_active(ss_1);
        sphere_active.m_radius = radius_gen(rng);
        sphere_active.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_active.m_ds     = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_active.m_dt_tp  = seconds_type(fake_time(rng));


        gmlib2::ProjectiveSpaceObject<>::Vector center_2({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        SurroundSphere ss_2(center_2,radius_gen(rng));
        Sphere_Col sphere_passive(ss_2);
        sphere_passive.m_radius = radius_gen(rng);
        sphere_passive.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_passive.m_ds     = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_passive.m_dt_tp  = seconds_type(fake_time(rng));
        seconds_type dt(0.016);
        seconds_type collision_t;
        seconds_type  dt_rem_spare(fake_time(rng));
        double root_spare = radius_gen(rng);

        double dsn_spare = fake_time(rng);
        bool intersect;

        FixedPlane_Col plane_1;
        Surf_Col surf_1;
    //state.ResumeTiming();
    for (auto _ : state)
  {
      benchmark::DoNotOptimize(collision_t = detectCollisionSp(sphere_active,sphere_passive,dt,dt_rem_spare,root_spare));
      benchmark::ClobberMemory();
  }
}

//rough sphere sphere collision
static void BM_Sphere_Sphere_Rough(benchmark::State& state) {
//    state.PauseTiming();
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_real_distribution<double> coord_gen(-1000.0,1000.0);
        std::uniform_real_distribution<double> radius_gen(0.1,100.0);
        std::uniform_real_distribution<double> fake_time(0.0001,0.015);
        gmlib2::ProjectiveSpaceObject<>::Vector center_1({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        SurroundSphere ss_1(center_1,radius_gen(rng));
        Sphere_Col sphere_1(ss_1);

        sphere_1.m_radius = radius_gen(rng);
        sphere_1.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_1.m_ds     = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_1.m_dt_tp  = seconds_type(fake_time(rng));

        gmlib2::ProjectiveSpaceObject<>::Vector center_2({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        SurroundSphere ss_2(center_2,radius_gen(rng));
        Sphere_Col sphere_2(ss_2);

        sphere_2.m_radius = radius_gen(rng);
        sphere_2.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_2.m_ds     = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_2.m_dt_tp  = seconds_type(fake_time(rng));
        seconds_type dt(0.016);
        seconds_type collision_t;
        seconds_type  dt_rem_spare(fake_time(rng));
        double root_spare = radius_gen(rng);

        double dsn_spare = fake_time(rng);
        bool intersect;

        FixedPlane_Col plane_1;
        FixedPlane_Col surf_1;

//    state.ResumeTiming();

    for (auto _ : state)
  {
        benchmark::DoNotOptimize(intersect = blaze::length(sphere_1.getSurroundSphere().getCenter()-sphere_2.getSurroundSphere().getCenter())-(sphere_1.getSurroundSphere().getRadius()+sphere_2.getSurroundSphere().getRadius())>1e-6);
        benchmark::ClobberMemory();
  }
}



static seconds_type detectCollisionSpPL(Sphere_Col&        sphere,
                                    FixedPlane_Col&    plane,
                                    seconds_type dt,
                                    double dsn_spare)
{
  const auto dt_rem = dt - sphere.m_dt_tp;
  //was singularity exception - changed to NoCollision
  if (dt_rem.count() <= 0) {};

  const auto pl_eval
    = plane.evaluateParent(FixedPlane_Col::PSpacePoint{0.0, 0.0},
                           FixedPlane_Col::PSpaceSizeArray{1UL, 1UL});

  const auto q = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));

  const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
  const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
  const auto n = blaze::normalize(blaze::cross(pl_u, pl_v));

//      const auto p = sphere.frameOriginParent();
  const auto p = sphere.m_pos;
  const auto r = sphere.m_radius;

  // \vec{d} = ( \vec{q} + r \vec{n} ) - \pnt{p}
  const auto d  = (q + r * n) - p;
  const auto ds = sphere.m_ds;

  const auto dn  = blaze::inner(d, n);
  auto dsn = blaze::inner(ds, n);

  // if ds is in the same direction as the normal of the plane
  // or is moving in parallel to the plane
  if (dsn>0.0 or (std::abs(dsn) < 1e-6))
      dsn = dsn_spare;

  //collision time
  const auto x   = dn / dsn;

  if (x <= 1.0){
    return seconds_type(sphere.m_dt_tp + (x * dt_rem));

  }
    return seconds_type(sphere.m_dt_tp + (x * dt_rem));
}

//exact sphere plane collision
static void BM_Sphere_Plane_Exact(benchmark::State& state) {
//    state.PauseTiming();
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_real_distribution<double> coord_gen(-1000.0,1000.0);
        std::uniform_real_distribution<double> radius_gen(0.1,100.0);
        std::uniform_real_distribution<double> fake_time(0.0001,0.015);
        gmlib2::ProjectiveSpaceObject<>::Vector center_1({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        SurroundSphere ss_1(center_1,radius_gen(rng));
        Sphere_Col sphere_1(ss_1);

        sphere_1.m_radius = radius_gen(rng);
        sphere_1.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_1.m_ds     = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_1.m_dt_tp  = seconds_type(fake_time(rng));

        gmlib2::ProjectiveSpaceObject<>::Vector center_2({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        SurroundSphere ss_2(center_2,radius_gen(rng));
        Sphere_Col sphere_2(ss_2);

        sphere_2.m_radius = radius_gen(rng);
        sphere_2.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_2.m_ds     = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_2.m_dt_tp  = seconds_type(fake_time(rng));
        seconds_type dt(0.016);
        seconds_type collision_t;
        seconds_type  dt_rem_spare(fake_time(rng));
        double root_spare = radius_gen(rng);

        double dsn_spare = fake_time(rng);
        bool intersect;

        FixedPlane_Col plane_1;
        Surf_Col surf_1;

//    state.ResumeTiming();

    for (auto _ : state)
  {
        benchmark::DoNotOptimize(detectCollisionSpPL(sphere_1,plane_1,dt,dsn_spare));
        benchmark::ClobberMemory();
  }
}


static seconds_type intersectDsSurf(const Sphere_Col &sphere, const gmlib2::ProjectiveSpaceObject<>::Vector &ds, const Surf_Col &surf)
  {
    //const GM2Vector p = sphere.frameOriginParent();
    const gmlib2::ProjectiveSpaceObject<>::Vector p = sphere.m_pos;
    auto            r = sphere.m_radius;
    auto u=0.0, v=0.0, t=0.0, du=1.0, dv=1.0, dt = 1.0;
    gmlib2::ProjectiveSpaceObject<>::Vector q_betta;
    gmlib2::ProjectiveSpaceObject<>::Vector Sn;

    auto iter =0;
    while (((std::abs(du)>1e-6) or
            (std::abs(dv)>1e-6) or
            (std::abs(dt)>1e-6)) and iter<100 /*and t<1.0+constants::Zero*/)
    {
      iter++;
      const auto surf_eval_d1 = surf.evaluateParent(FixedPlane_Col::PSpacePoint{u,v},
                                                    FixedPlane_Col::PSpaceSizeArray{1UL,1UL});

      const auto q         = blaze::subvector<0UL,3UL>(surf_eval_d1(0UL,0UL));
      const auto Su        = blaze::subvector<0UL,3UL>(surf_eval_d1(1UL,0UL));
      const auto Sv        = blaze::subvector<0UL,3UL>(surf_eval_d1(0UL,1UL));
      const auto SuCrossSv = blaze::cross(Su,Sv);
                 Sn        = blaze::normalize(SuCrossSv);

      auto p_alpha       = p + ds*t;
      auto b_vector      = p_alpha - q - Sn*r;

      blaze::DynamicMatrix<double> A (3UL,3UL);
      A = {{Su[0],Sv[0],-ds[0]},
           {Su[1],Sv[1],-ds[1]},
           {Su[2],Sv[2],-ds[2]}};
      //blaze::DynamicVector<GM2Vector,blaze::columnVector> b {p_alpha - q_alpha - Sn*r};
      blaze::DynamicVector<double,blaze::columnVector> b {b_vector};

      auto A_inv =blaze::inv(A);
      blaze::DynamicVector<double,blaze::columnVector> x =A_inv*b;

      du = x[0];
      dv = x[1];
      dt = x[2];

      u += du;
      v += dv;
      t += dt;

      q_betta=q;
    };

    if (iter==30) t = 30;
    return seconds_type(t);
  }


//exact sphere surf collision
static void BM_Sphere_Surf_Exact(benchmark::State& state) {
//    state.PauseTiming();
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_real_distribution<double> coord_gen(-1000.0,1000.0);
        std::uniform_real_distribution<double> radius_gen(0.1,100.0);
        std::uniform_real_distribution<double> fake_time(0.0001,0.015);
        gmlib2::ProjectiveSpaceObject<>::Vector center_1({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        SurroundSphere ss_1(center_1,radius_gen(rng));
        Sphere_Col sphere_1(ss_1);

        sphere_1.m_radius = radius_gen(rng);
        sphere_1.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_1.m_ds     = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_1.m_dt_tp  = seconds_type(fake_time(rng));

        gmlib2::ProjectiveSpaceObject<>::Vector center_2({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        SurroundSphere ss_2(center_2,radius_gen(rng));
        Sphere_Col sphere_2(ss_2);

        sphere_2.m_radius = radius_gen(rng);
        sphere_2.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_2.m_ds     = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
        sphere_2.m_dt_tp  = seconds_type(fake_time(rng));
        seconds_type dt(0.016);
        seconds_type collision_t;
        seconds_type  dt_rem_spare(fake_time(rng));
        double root_spare = radius_gen(rng);

        double dsn_spare = fake_time(rng);
        bool intersect;

        FixedPlane_Col plane_1;
        Surf_Col surf_1;

//    state.ResumeTiming();

    for (auto _ : state)
  {
        benchmark::DoNotOptimize(intersectDsSurf(sphere_1,sphere_1.m_ds,surf_1));
        benchmark::ClobberMemory();
  }
}


static void BM_Get_Frame_Parent(benchmark::State& state) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_real_distribution<double> coord_gen(-1000.0,1000.0);
    std::uniform_real_distribution<double> radius_gen(0.1,100.0);
    gmlib2::ProjectiveSpaceObject<>::Vector center_1({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
    SurroundSphere ss_1(center_1,radius_gen(rng));
    Sphere_Col sphere_1(ss_1);
    sphere_1.m_radius = radius_gen(rng);
    sphere_1.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
    FixedPlane_Col plane_1;
  for (auto _ : state)
  {
        benchmark::DoNotOptimize(plane_1.frameOriginParent());
        benchmark::ClobberMemory();
  }
}

static void BM_Get_Pos(benchmark::State& state) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_real_distribution<double> coord_gen(-1000.0,1000.0);
  std::uniform_real_distribution<double> radius_gen(0.1,100.0);
  gmlib2::ProjectiveSpaceObject<>::Vector center_1({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
  SurroundSphere ss_1(center_1,radius_gen(rng));
  Sphere_Col sphere_1(ss_1);
  sphere_1.m_radius = radius_gen(rng);
  sphere_1.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
  FixedPlane_Col plane_1;


  for (auto _ : state)
  {
        benchmark::DoNotOptimize(sphere_1.m_pos);
        benchmark::ClobberMemory();
  }
}

static gmlib2::ProjectiveSpaceObject<>::Vector get_N(FixedPlane_Col& plane)
{
    const auto pl_eval
      = plane.evaluateParent(FixedPlane_Col::PSpacePoint{0.0, 0.0},
                             FixedPlane_Col::PSpaceSizeArray{1UL, 1UL});
    const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));
    const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
    const auto n = blaze::normalize(blaze::cross(pl_u, pl_v));
    return n;
}

using ContactNTtype = QHash<std::pair<quint64,quint64>,gmlib2::ProjectiveSpaceObject<>::Vector>;

static void BM_Compute_N(benchmark::State& state) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_real_distribution<double> coord_gen(-1000.0,1000.0);
  std::uniform_real_distribution<double> radius_gen(0.1,100.0);
  gmlib2::ProjectiveSpaceObject<>::Vector center_1({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
  SurroundSphere ss_1(center_1,radius_gen(rng));
  Sphere_Col sphere_1(ss_1);
  sphere_1.m_radius = radius_gen(rng);
  sphere_1.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
  FixedPlane_Col plane_1;

  std::uniform_int_distribution<std::mt19937::result_type> fake_id(1,10000);
  std::uniform_int_distribution<std::mt19937::result_type> remember_id(1,10);
  ContactNTtype set_of_ns;
  quint64 id_1;
  quint64 id_2;
  quint64 id__1;
  quint64 id__2;
  for (auto i = 0; i<1000000;i++)
  {
      id_1 = fake_id(rng);
      id_2 = fake_id(rng);
      set_of_ns.insert({id_1,id_2},gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)}));
      if (remember_id(rng)<=2)
      {
          id__1 = id_1;
          id__2 = id_2;
      }
  }

  for (auto _ : state)
  {
        benchmark::DoNotOptimize(set_of_ns);
        benchmark::DoNotOptimize(get_N(plane_1));
        benchmark::ClobberMemory();
  }
}

static void BM_Get_N(benchmark::State& state) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_real_distribution<double> coord_gen(-1000.0,1000.0);
  std::uniform_real_distribution<double> radius_gen(0.1,100.0);
  gmlib2::ProjectiveSpaceObject<>::Vector center_1({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
  SurroundSphere ss_1(center_1,radius_gen(rng));
  Sphere_Col sphere_1(ss_1);
  sphere_1.m_radius = radius_gen(rng);
  sphere_1.m_pos    = gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)});
  FixedPlane_Col plane_1;

  std::uniform_int_distribution<std::mt19937::result_type> fake_id(1,10000);
  std::uniform_int_distribution<std::mt19937::result_type> remember_id(1,10);
  ContactNTtype set_of_ns;
  quint64 id_1;
  quint64 id_2;
  quint64 id__1;
  quint64 id__2;
  for (auto i = 0; i<10000;i++)
  {
      id_1 = fake_id(rng);
      id_2 = fake_id(rng);
      set_of_ns.insert({id_1,id_2},gmlib2::ProjectiveSpaceObject<>::Vector({coord_gen(rng),coord_gen(rng),coord_gen(rng)}));
      if (remember_id(rng)<=2)
      {
          id__1 = id_1;
          id__2 = id_2;
      }
  }
  gmlib2::ProjectiveSpaceObject<>::Vector n;

  for (auto _ : state)
  {
      benchmark::DoNotOptimize(set_of_ns);
      //benchmark::DoNotOptimize(n = set_of_ns[{id__1,id__2}]);
      benchmark::DoNotOptimize(set_of_ns.contains({id__1,id__2}));
      benchmark::ClobberMemory();
  }
}

static void BM_Set_Contains(benchmark::State& state) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> fake_id(1,15000);
  QSet<quint64> set_of_ids;
  for (auto i = 0; i<10000;i++)
  {
      set_of_ids.insert(fake_id(rng));
  }

  QSet<quint64> set_of_ids_2;
  for (auto i = 0; i<10000;i++)
  {
      set_of_ids_2.insert(fake_id(rng));
  }

  for (auto _ : state)
  {
      benchmark::DoNotOptimize(set_of_ids.begin());
      benchmark::DoNotOptimize(set_of_ids_2.begin());
      //benchmark::DoNotOptimize(set_of_ids.contains(fake_id(rng)));
      benchmark::ClobberMemory();
  }
}

static void BM_Set_Intersect(benchmark::State& state) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> fake_id(1,15000);
  QSet<quint64> set_of_ids;
  for (auto i = 0; i<10000;i++)
  {
      set_of_ids.insert(fake_id(rng));
  }
  QSet<quint64> set_of_ids_2;
  for (auto i = 0; i<10000;i++)
  {
      set_of_ids_2.insert(fake_id(rng));
  }

  for (auto _ : state)
  {
      benchmark::DoNotOptimize(set_of_ids.begin());
      benchmark::DoNotOptimize(set_of_ids_2.begin());
      //benchmark::DoNotOptimize(set_of_ids.intersect(set_of_ids_2));
      benchmark::ClobberMemory();
  }
}

static void BM_Set_Unite(benchmark::State& state) {
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> fake_id(1,15000);
    QSet<quint64> set_of_ids;
    for (auto i = 0; i<10000;i++)
    {
        set_of_ids.insert(fake_id(rng));
    }
    QSet<quint64> set_of_ids_2;
    for (auto i = 0; i<10000;i++)
    {
        set_of_ids_2.insert(fake_id(rng));
    }

  for (auto _ : state)
  {
      benchmark::DoNotOptimize(set_of_ids.begin());
      benchmark::DoNotOptimize(set_of_ids_2.begin());
      //benchmark::DoNotOptimize(set_of_ids.unite(set_of_ids_2));
      benchmark::ClobberMemory();
  }
}
